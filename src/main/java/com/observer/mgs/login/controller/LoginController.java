
package com.observer.mgs.login.controller;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.sql.Date;

import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.observer.mgs.common.exception.document.AuthRegistException;
import com.observer.mgs.common.exception.member.LoginFailedException;
import com.observer.mgs.common.exception.member.MemberModifyException;
import com.observer.mgs.common.exception.member.MemberRegistException;
import com.observer.mgs.common.exception.member.MemberRemoveException;
import com.observer.mgs.document.model.dto.DocAuthorityDTO;
import com.observer.mgs.document.model.service.DocumentService;
import com.observer.mgs.login.model.dto.LoginDTO;
import com.observer.mgs.login.model.service.LoginService;

@Controller
@RequestMapping("/member")
@SessionAttributes("loginMember") 
public class LoginController {
	
	private final LoginService loginService;
	private final BCryptPasswordEncoder passwordEncoder;
	private final DocumentService docService;
	private final JavaMailSender mailSender;
	
	@Autowired
	public LoginController(LoginService loginService, BCryptPasswordEncoder passwordEncoder, DocumentService docService, JavaMailSender mailSender) {
		
		this.loginService = loginService;
		this.passwordEncoder = passwordEncoder;
		this.docService = docService;
		this.mailSender = mailSender;
	}

	@GetMapping("/log-in-join")
	public void registMember() {}
	
	@PostMapping(value="/idDupCheck", produces="text/plain; charset=utf-8")
	@ResponseBody
	public String checkDuplication(@RequestParam(required = false) String userId) {
		
		System.out.println(" 넘어온 값 : " + userId);
		
		String result = "사용 가능한 아이디 입니다.";
		
		if("".equals(userId)) {
			result = "아이디를 입력해 주세요";
			//userId 를 "" 과 비교해서 맞으면 "아이디를 입력해주세요" 아니면 다음 구문을 실행. 
		} else if(loginService.selectMemberById(userId)) {  // 기존에 있던 아이디와 입력한 거 비교해서 있으면 "아이디 이미 존재"
			result = "아이디가 이미 존재합니다.";
		}
		
		return result;
	}

	@PostMapping("/log-in")
	public String login(@ModelAttribute LoginDTO login,Model model, RedirectAttributes rttr) throws LoginFailedException {
		// 뷰에서 입력한 아이디랑 패스워드를 갖고와 값 확인
		System.out.println(" 아이디: " + login.getId());
		System.out.println(" 비밀번호: " + login.getPwd());	
		System.out.println("result : " + login);
		
	    // 전달받은 매개변수의 값을 이용해서 서비스의 findMember메소드를 이용하여 사용자 정보를 LoginDTO로 전달 받아 result값을 변수에 대입한다.
		LoginDTO result = loginService.findMember(login);
		
		String page = "";
		
		// 입력한 값이 빈 값이 아니면 /main/main에 있는 값을 loginMember에 담는다.
		if((result != null)) {
			System.out.println("로그인 값 확인 : " + result);
			model.addAttribute("loginMember",result);
			
			page = "redirect:/main/main";
		} else {
			// 만약 result 값이 있으면 세션에 추가하고 아니면 실패라는 메세지를 띄운다.
			
			System.out.println("로그인 실패 값 : " + result);
			rttr.addFlashAttribute("message", "아이디와 비밀번호를 확인해 주세요");
			page = "redirect:/";
			
		}
		
		
		return page;
	}
	
	
	@PostMapping("/log-in-join")
	public String registMember(@ModelAttribute LoginDTO login,@RequestParam("year") int year,@RequestParam("month") int month ,@RequestParam("day") int day 
			, HttpServletRequest request, RedirectAttributes rttr) throws MemberRegistException, LoginFailedException, AuthRegistException{
		
		System.out.println("birthday : " + year + month + day );
		
		Date birthday =  new Date(year,month,day);
		
		System.out.println(birthday);
		
		String phone = request.getParameter("phone").replace("-", "");
		String cphone = request.getParameter("cphone").replace("-", "");
		
		String address = request.getParameter("zipCode")+ "$" + request.getParameter("address2")+ "$" + request.getParameter("address3");
		DocAuthorityDTO auth = new DocAuthorityDTO();
		
		
		login.setBirthday(birthday);
		login.setAddress(address);
		login.setPwd(passwordEncoder.encode(login.getPwd()));
		System.out.println("비밀번호 확인 : " + passwordEncoder.encode(login.getPwd()));
		System.out.println(" 값 : " + login);
		
		auth.setDeptCode(login.getDepname());
		auth.setTeamCode(login.getTeamname());
		auth.setLevCode(107);
		auth.setAuthorityYN("N");
		auth.setAuthGrade("N");
		
		loginService.registMember(login);
		docService.insertAuth(auth);
		
		rttr.addFlashAttribute("message", "회원가입에 성공하셨습니다.");
		
		return "redirect:/";
	}
	
	
	
	@GetMapping("/logout")
	public String logout(SessionStatus status) {
		
		status.setComplete();
		
		return "redirect:/";
	}

	
	@GetMapping("/my-page")
	public void modifyMember() {}
	
	@PostMapping("/my-page")
	public String modifyMember(@ModelAttribute LoginDTO login, HttpServletRequest request, RedirectAttributes rttr) throws MemberModifyException {
		
		String phone = request.getParameter("phone").replace("-", "");
		
		String address = request.getParameter("zipCode") + "$" + request.getParameter("address1") + "$" + request.getParameter("address2");
		
		login.setId(((LoginDTO) request.getSession().getAttribute("loginMember")).getId());
		login.setPhone(phone);
		login.setAddress(address);
		login.setPwd(passwordEncoder.encode(login.getPwd()));
		
		System.out.println("member : " + login);
		
		loginService.modifyMember(login);
		
		request.getSession().setAttribute("loginMember", login);
		
		rttr.addFlashAttribute("message", "수정 성공");
		
		return "redirect:/";
	}
	
	
	@GetMapping("/delete")
	public String deleteMember(@ModelAttribute LoginDTO login, SessionStatus status, RedirectAttributes rttr) throws MemberRemoveException {
		
		loginService.removeMember(login);
		
		rttr.addFlashAttribute("message", "회원 탈퇴에 성공했습니다.");
		status.setComplete();
		
		
		return "redirect:/";
	}
	@GetMapping("/find-id")
	public void findId() {}
	
	@PostMapping("/find-id")
	public String findId(@ModelAttribute LoginDTO login, Model model) {
		
		System.out.println(login);
		
		LoginDTO member = loginService.selectFindId(login);
		
		System.out.println(member);
		
		model.addAttribute("member", member);
		
		return "member/find-id";
	}
	
	@GetMapping("/find-pwd")
	public void findPwd() {}
	
	@PostMapping("/find-pwd")
	public String findPwd(@ModelAttribute LoginDTO login, RedirectAttributes rttr) throws Exception{
		
		String tmpPwd = UUID.randomUUID().toString().replaceAll("-", "").substring(0, 6);
		
		System.out.println(tmpPwd);
		
		String subject = login.getId() + "님의 임시 비밀번호가 발급되었습니다.";
        String content = "임시 비밀번호는 " + tmpPwd + "입니다.";
        String from = "paeroo3400@gmail.com";
        String to = login.getEmail();
		
        login.setPwd(passwordEncoder.encode(tmpPwd));
        
        loginService.updatePwd(login);
        
        try {
            MimeMessage mail = mailSender.createMimeMessage();
            MimeMessageHelper mailHelper = new MimeMessageHelper(mail,true,"UTF-8");
          
            
            mailHelper.setFrom(from);
            mailHelper.setTo(to);
            mailHelper.setSubject(subject);
            mailHelper.setText(content, true);
            
            mailSender.send(mail);
            
        } catch(Exception e) {
            e.printStackTrace();
        }
        
        rttr.addFlashAttribute("message", "임시비밀번호를 전송하였습니다.");
        
		return "redirect:/";
	}
	
	@PostMapping("/sendCode")
	@ResponseBody
    public String sendMailTest(@ModelAttribute LoginDTO login) throws Exception{
		
		int scode = (int)((Math.random() * (999999 - 100000 + 1)) + 100000);
        
        String subject = login.getName() + "님의 아이디를 찾기 위한 인증번호 이메일 입니다.";
        String content = "인증번호는 " + scode + "입니다.";
        String from = "paeroo3400@gmail.com";
        String to = login.getEmail();
        
        System.out.println(scode);
        
        login.setScode(scode);
        
        loginService.updateScode(login);
        
        try {
            MimeMessage mail = mailSender.createMimeMessage();
            MimeMessageHelper mailHelper = new MimeMessageHelper(mail,true,"UTF-8");
            
            mailHelper.setFrom(from);
            mailHelper.setTo(to);
            mailHelper.setSubject(subject);
            mailHelper.setText(content, true);
            
            mailSender.send(mail);
            
        } catch(Exception e) {
            e.printStackTrace();
        }
        return "/member/find-id";
    }
	

	@PostMapping("/sendCodePwd")
	@ResponseBody
	public String sendMailPwd(@ModelAttribute LoginDTO login) throws Exception{
		
		int scode = (int)((Math.random() * (999999 - 100000 + 1)) + 100000);
		
		String subject = login.getId() + "님의 비밀번호를 찾기 위한 인증번호 이메일 입니다.";
		String content = "인증번호는 " + scode + "입니다.";
		String from = "paeroo3400@gmail.com";
		String to = login.getEmail();
		
		System.out.println(scode);
		
		login.setScode(scode);
		
		loginService.updateScodePwd(login);
		
		try {
			MimeMessage mail = mailSender.createMimeMessage();
			MimeMessageHelper mailHelper = new MimeMessageHelper(mail,true,"UTF-8");
			// true는 멀티파트 메세지를 사용하겠다는 의미
			
			/*
			 * 단순한 텍스트 메세지만 사용시엔 아래의 코드도 사용 가능 
			 * MimeMessageHelper mailHelper = new MimeMessageHelper(mail,"UTF-8");
			 */
			
			mailHelper.setFrom(from);
			// 빈에 아이디 설정한 것은 단순히 smtp 인증을 받기 위해 사용 따라서 보내는이(setFrom())반드시 필요
			// 보내는이와 메일주소를 수신하는이가 볼때 모두 표기 되게 원하신다면 아래의 코드를 사용하시면 됩니다.
			//mailHelper.setFrom("보내는이 이름 <보내는이 아이디@도메인주소>");
			mailHelper.setTo(to);
			mailHelper.setSubject(subject);
			mailHelper.setText(content, true);
			// true는 html을 사용하겠다는 의미입니다.
			
			/*
			 * 단순한 텍스트만 사용하신다면 다음의 코드를 사용하셔도 됩니다. mailHelper.setText(content);
			 */
			
			mailSender.send(mail);
			
		} catch(Exception e) {
			e.printStackTrace();
		}
		return "/member/find-id";
	}
	
	

	/* 페이지 이동을 위한 매핑*/
	@GetMapping("/log-in-check")
	public String test4() {
		return "member/log-in-check";
	}
	
	@GetMapping("/log-in-main")
	public String test5() {
		return "member/log-in-main";
	}


}
