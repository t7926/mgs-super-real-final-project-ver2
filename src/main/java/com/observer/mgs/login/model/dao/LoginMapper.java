package com.observer.mgs.login.model.dao;

import com.observer.mgs.login.model.dto.LoginDTO;

public interface LoginMapper {

	String selectMemberById(String userId);

	LoginDTO selectMember(LoginDTO login);

	int insertMember(LoginDTO login);

	String selectEncryptedPwd(LoginDTO login);

	int updateMember(LoginDTO login);
	
	int deleteMember(LoginDTO login);

	int updateScode(LoginDTO login);

	LoginDTO selectFindId(LoginDTO loginDTO);

	int updatePwd(LoginDTO login);

	int updateScodePwd(LoginDTO login);

}
 