package com.observer.mgs.login.model.service;


import com.observer.mgs.common.exception.member.LoginFailedException;
import com.observer.mgs.common.exception.member.MemberModifyException;
import com.observer.mgs.common.exception.member.MemberRegistException;
import com.observer.mgs.common.exception.member.MemberRemoveException;
import com.observer.mgs.common.exception.member.UpdateCodeException;
import com.observer.mgs.common.exception.member.UpdatePwdException;
import com.observer.mgs.login.model.dto.LoginDTO;

public interface LoginService {

	boolean selectMemberById(String userId);

	LoginDTO findMember(LoginDTO login) throws LoginFailedException;


	void registMember(LoginDTO login) throws MemberRegistException;

	void modifyMember(LoginDTO login) throws MemberModifyException;

	void removeMember(LoginDTO login) throws MemberRemoveException;

	void updateScode(LoginDTO login) throws UpdateCodeException;

	LoginDTO selectFindId(LoginDTO loginDTO);

	void updatePwd(LoginDTO login) throws UpdatePwdException;

	void updateScodePwd(LoginDTO login);

		
	
	


	

	

	
}
