package com.observer.mgs.login.model.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.observer.mgs.common.exception.member.LoginFailedException;
import com.observer.mgs.common.exception.member.MemberModifyException;
import com.observer.mgs.common.exception.member.MemberRegistException;
import com.observer.mgs.common.exception.member.MemberRemoveException;
import com.observer.mgs.common.exception.member.UpdateCodeException;
import com.observer.mgs.common.exception.member.UpdatePwdException;
import com.observer.mgs.login.model.dao.LoginMapper;
import com.observer.mgs.login.model.dto.LoginDTO;



@Service
public class LoginServiceImpl implements LoginService {

   private final LoginMapper mapper;
   private final BCryptPasswordEncoder passwordEncoder;

   
   @Autowired
   public LoginServiceImpl(LoginMapper mapper, BCryptPasswordEncoder passwordEncoder) {   
      this.mapper = mapper;
      this.passwordEncoder = passwordEncoder;
   }
    
	/**
	 *<pre>
	 * 아이디로 회원 조회용 메소드
	 *</pre>
	 */
	@Override
	public boolean selectMemberById(String userId) {
		
		String result = mapper.selectMemberById(userId);
	
		return result != null? true : false;
	}
   
   
	/**
	 * <pre>
	 * 		회원 가입용 메소드
	 * </pre>
	 * @param member 회원 가입 정보
	 * @throws MemberRegistException 
	 */
	@Override
	public void registMember(LoginDTO login) throws MemberRegistException {
		
		int result = mapper.insertMember(login);
		// 컨트롤러에서 담아온 login 정보를 result에 담아 인설트
		if(result < 0) {
			throw new MemberRegistException("회원 가입에 실패하였습니다.");
		}
	}

   
   
   
   
   /**
    * <pre>
    *    회원 로그인용 메소드
    * </pre>
    * @param member : 로그인 정보(아이디, 비밀번호)
    * @throws com.observer.mgs.common.exception.member.LoginFailedException 
    */
   @Override
   public LoginDTO findMember(LoginDTO login) throws LoginFailedException {

	   System.out.println("값 확인!! :" + passwordEncoder.matches(login.getPwd(), mapper.selectEncryptedPwd(login)));
	   // 입력받은 비밀번호와 패스워드인코더가 비교하여 값이 맞는지 확인, 들고온 암호화된 값
	   if(!passwordEncoder.matches(login.getPwd(), mapper.selectEncryptedPwd(login))) {
		   // 비교한 두 갑사이 일치하지 않은 경우 로그인 실패
		   throw new LoginFailedException("로그인에 실패하였습니다. 아이디와 비밀번호를 확인해 주세요");
	   }
	   //LoginDTO result = mapper.selectMember(login);
	 
      return mapper.selectMember(login);
   }


   /**
	 *<pre>
	 * 회원정보 수정용 메소드
	 * </pre>
	 * @param 회원정보 수정용 데이터
	 * @throws MemberModifyException 
	 */
	@Override
	public void modifyMember(LoginDTO login) throws MemberModifyException {
		
		int result = mapper.updateMember(login);
		
		if(!(result > 0)) {
			throw new MemberModifyException("회원 정보 수정에 실패하셨습니다");
		}
	}



	/**
	 *<pre>
	 *   회원 탈퇴용 메소드
	 *</pre>
	 *@param member 아이디
	 * @throws MemberRemoveException 
	 * @throws MemberRegistException 
	 */
	@Override
	public void removeMember(LoginDTO login) throws MemberRemoveException  {
		
		int result = mapper.deleteMember(login);
		
		if((result<0)) {
			throw new MemberRemoveException("회원 탈퇴에 실패하였습니다.");
		}
		
	}

	@Override
	public void updateScode(LoginDTO login) throws UpdateCodeException {
		
		int result = mapper.updateScode(login);
		
		if(!(result > 0)) {
			throw new UpdateCodeException("인증번호 수정에 실패하였습니다.");
		}
	}

	@Override
	public LoginDTO selectFindId(LoginDTO loginDTO) {
		
		return mapper.selectFindId(loginDTO);
	}

	@Override
	public void updatePwd(LoginDTO login) throws UpdatePwdException {
		
		int result = mapper.updatePwd(login);
		
		if(!(result > 0)) {
			throw new UpdatePwdException("인증번호 수정에 실패하였습니다.");
		}
	}

	@Override
	public void updateScodePwd(LoginDTO login) {
		int result = mapper.updateScodePwd(login);
	}








}