package com.observer.mgs.login.model.dto;

import java.sql.Date;

public class LoginDTO {

	
	private String id;
	private String pwd;
	private String name;
	private Date birthday;
	private String gender;
	private String email;
    private String phone;
    private String cphone;
    private String address;
    private String status;
    private int depname;
    private int deplevel;
    private int empnum;   
    private int teamname;
    private int scode;
    private String authority;
    
    
	public LoginDTO() {}


	public LoginDTO(String id, String pwd, String name, Date birthday, String gender, String email, String phone,
			String cphone, String address, String status, int depname, int deplevel, int empnum, int teamname,
			int scode, String authority) {
		super();
		this.id = id;
		this.pwd = pwd;
		this.name = name;
		this.birthday = birthday;
		this.gender = gender;
		this.email = email;
		this.phone = phone;
		this.cphone = cphone;
		this.address = address;
		this.status = status;
		this.depname = depname;
		this.deplevel = deplevel;
		this.empnum = empnum;
		this.teamname = teamname;
		this.scode = scode;
		this.authority = authority;
	}


	public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getPwd() {
		return pwd;
	}


	public void setPwd(String pwd) {
		this.pwd = pwd;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Date getBirthday() {
		return birthday;
	}


	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}


	public String getGender() {
		return gender;
	}


	public void setGender(String gender) {
		this.gender = gender;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getPhone() {
		return phone;
	}


	public void setPhone(String phone) {
		this.phone = phone;
	}


	public String getCphone() {
		return cphone;
	}


	public void setCphone(String cphone) {
		this.cphone = cphone;
	}


	public String getAddress() {
		return address;
	}


	public void setAddress(String address) {
		this.address = address;
	}


	public String getStatus() {
		return status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public int getDepname() {
		return depname;
	}


	public void setDepname(int depname) {
		this.depname = depname;
	}


	public int getDeplevel() {
		return deplevel;
	}


	public void setDeplevel(int deplevel) {
		this.deplevel = deplevel;
	}


	public int getEmpnum() {
		return empnum;
	}


	public void setEmpnum(int empnum) {
		this.empnum = empnum;
	}


	public int getTeamname() {
		return teamname;
	}


	public void setTeamname(int teamname) {
		this.teamname = teamname;
	}


	public int getScode() {
		return scode;
	}


	public void setScode(int scode) {
		this.scode = scode;
	}


	public String getAuthority() {
		return authority;
	}


	public void setAuthority(String authority) {
		this.authority = authority;
	}


	@Override
	public String toString() {
		return "LoginDTO [id=" + id + ", pwd=" + pwd + ", name=" + name + ", birthday=" + birthday + ", gender="
				+ gender + ", email=" + email + ", phone=" + phone + ", cphone=" + cphone + ", address=" + address
				+ ", status=" + status + ", depname=" + depname + ", deplevel=" + deplevel + ", empnum=" + empnum
				+ ", teamname=" + teamname + ", scode=" + scode + ", authority=" + authority + "]";
	}

}
