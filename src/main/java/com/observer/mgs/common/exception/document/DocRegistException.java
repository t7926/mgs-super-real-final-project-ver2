package com.observer.mgs.common.exception.document;

public class DocRegistException extends Exception{

	public DocRegistException(String msg) {
		super(msg);
	}
}
