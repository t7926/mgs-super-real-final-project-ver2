package com.observer.mgs.common.exception.member;

public class MemberRemoveException extends Exception {

	public MemberRemoveException(String msg) {
		super(msg);
	}

}
