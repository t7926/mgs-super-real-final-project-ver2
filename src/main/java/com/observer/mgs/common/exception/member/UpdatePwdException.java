package com.observer.mgs.common.exception.member;

public class UpdatePwdException extends Exception {

	public UpdatePwdException(String msg) {
		super(msg);
	}
}
