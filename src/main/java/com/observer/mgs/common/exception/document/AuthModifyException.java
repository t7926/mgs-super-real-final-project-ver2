package com.observer.mgs.common.exception.document;

public class AuthModifyException extends Exception{
	
	public AuthModifyException(String msg) {
		super(msg);
	}
}
