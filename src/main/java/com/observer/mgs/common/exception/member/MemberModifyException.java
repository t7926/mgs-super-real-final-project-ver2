package com.observer.mgs.common.exception.member;

public class MemberModifyException extends Exception {

	public MemberModifyException(String msg) {
		super(msg);
	}

}
