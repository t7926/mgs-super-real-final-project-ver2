package com.observer.mgs.common.exception.member;

public class UpdateCodeException extends Exception{

	public UpdateCodeException(String msg) {
		super(msg);
	}
}
