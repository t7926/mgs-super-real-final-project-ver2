package com.observer.mgs.common.exception.member;

public class MemberRegistException extends Exception {

	public MemberRegistException(String msg) {
		super(msg);
	}
}
