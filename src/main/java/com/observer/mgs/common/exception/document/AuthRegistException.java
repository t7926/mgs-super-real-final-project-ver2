package com.observer.mgs.common.exception.document;

public class AuthRegistException extends Exception{

	public AuthRegistException(String msg) {
		super(msg);
	}
}
