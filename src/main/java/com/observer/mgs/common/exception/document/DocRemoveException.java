package com.observer.mgs.common.exception.document;

public class DocRemoveException extends Exception{
	
	public DocRemoveException(String msg) {
		super(msg);
	}
}
