package com.observer.mgs.schedule.model.dao;

import java.util.List;
import java.util.Map;

import com.observer.mgs.schedule.model.dto.ScheduleDTO;

public interface ScheduleMapper {

	int insertSchedule(Map<String, Object> mapInfo);

	List<ScheduleDTO> selectSchedule(Map<String, Object> mapInfo);

	int deleteSchedule(Map<String, Object> mapInfo);

}
