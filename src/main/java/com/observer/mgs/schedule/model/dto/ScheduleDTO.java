package com.observer.mgs.schedule.model.dto;

public class ScheduleDTO {
	
	private String title;
	private String start;
	private String end;
	private int emp_num;
	
	
	public ScheduleDTO() {
		// TODO Auto-generated constructor stub
	}


	public ScheduleDTO(String title, String start, String end, int emp_num) {
		super();
		this.title = title;
		this.start = start;
		this.end = end;
		this.emp_num = emp_num;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public String getStart() {
		return start;
	}


	public void setStart(String start) {
		this.start = start;
	}


	public String getEnd() {
		return end;
	}


	public void setEnd(String end) {
		this.end = end;
	}


	public int getEmp_num() {
		return emp_num;
	}


	public void setEmp_num(int emp_num) {
		this.emp_num = emp_num;
	}


	@Override
	public String toString() {
		return "ScheduleDTO [title=" + title + ", start=" + start + ", end=" + end + ", emp_num=" + emp_num + "]";
	}


	

}
