package com.observer.mgs.schedule.model.service;

import java.util.List;
import java.util.Map;

import com.observer.mgs.schedule.model.dto.ScheduleDTO;

public interface ScheduleService {

	int insertSchedule(Map<String, Object> mapInfo);

	List<ScheduleDTO> selectSchedule(Map<String, Object> mapInfo);

	int deleteSchedule(Map<String, Object> mapInfo);

}
