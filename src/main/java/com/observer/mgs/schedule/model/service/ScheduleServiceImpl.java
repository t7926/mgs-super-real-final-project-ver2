package com.observer.mgs.schedule.model.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.observer.mgs.schedule.model.dao.ScheduleMapper;
import com.observer.mgs.schedule.model.dto.ScheduleDTO;

@Service
public class ScheduleServiceImpl implements ScheduleService{
	
	private final ScheduleMapper mapper;
	
	@Autowired
	public ScheduleServiceImpl(ScheduleMapper mapper) {
		this.mapper = mapper;
	}

	/**
	 * <pre>
	 * 	일정 추가하는 메소드
	 * </pre>
	 */
	@Override
	public int insertSchedule(Map<String, Object> mapInfo) {

		return mapper.insertSchedule(mapInfo);
		
		
	}

	/**
	 * <pre>
	 * 	일정 조회하는 메소드
	 * </pre>
	 */
	@Override
	public List<ScheduleDTO> selectSchedule(Map<String, Object> mapInfo) {
		
		return mapper.selectSchedule(mapInfo);
	}

	/**
	 * <pre>
	 * 	일정을 삭제하는 메소드
	 * </pre>
	 */
	@Override
	public int deleteSchedule(Map<String, Object> mapInfo) {

		return mapper.deleteSchedule(mapInfo);
	
	}

}
