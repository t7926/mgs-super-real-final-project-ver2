package com.observer.mgs.schedule.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.observer.mgs.login.model.dto.LoginDTO;
import com.observer.mgs.schedule.model.dto.ScheduleDTO;
import com.observer.mgs.schedule.model.service.ScheduleService;

@Controller
@RequestMapping("/schedule")
public class ScheduleController {

	private final ScheduleService scheduleService;

	@Autowired
	public ScheduleController(ScheduleService scheduleService) {
		this.scheduleService = scheduleService;
	}

	@PostMapping(value = "/insert", produces = "text/plain; charset=utf-8")
	@ResponseBody
	public String Scheduleinsert(@RequestParam("title") String title, @RequestParam("startStr") String startStr,
			@RequestParam("endStr") String endStr, @RequestParam("openScope") String openScope,
			HttpServletRequest request) {

		System.out.println("title : " + title);
		System.out.println("startStr : " + startStr);
		System.out.println("endStr : " + endStr);
		System.out.println("openScope : " + openScope);

		Map<String, Object> mapInfo = new HashMap<String, Object>();
		if (openScope.equals("나만보기")) {
			mapInfo.put("openScope", 'Y');
		} else {
			mapInfo.put("openScope", 'N');
		}

		HttpSession session = request.getSession();

		int empnum = ((LoginDTO) session.getAttribute("loginMember")).getEmpnum();

		mapInfo.put("title", title);
		mapInfo.put("startStr", startStr);
		mapInfo.put("endStr", endStr);
		mapInfo.put("empnum", empnum);

		int result = scheduleService.insertSchedule(mapInfo);

		if(result >= 1) {
			return "success";
		} else {
			return "error";
		}
	}

	@GetMapping(value = "/select", produces = "application/json; charset=utf-8")
	@ResponseBody
	public String Scheduleselect(HttpServletRequest request, @RequestParam("value") String value) {

		HttpSession session = request.getSession();
		int empnum = ((LoginDTO) session.getAttribute("loginMember")).getEmpnum();

		
		
		System.out.println("empnum : " + empnum);
		System.out.println("value : " + value);


		Map<String, Object> mapInfo = new HashMap<String, Object>();
		mapInfo.put("empnum", empnum);
		mapInfo.put("value", value);

		List<ScheduleDTO> ScheduleList = scheduleService.selectSchedule(mapInfo);
		System.out.println(ScheduleList);
		
		  // GSON객체 생성 
		Gson gson = new Gson();
		  
		  // DTO객체 -> JSON객체 
		 
		String scheduleJson = gson.toJson(ScheduleList);
		  
		System.out.println(scheduleJson);
	

		return scheduleJson;
	}
	
	@PostMapping(value = "/delete", produces = "text/plain; charset=utf-8")
	@ResponseBody
	public String Scheduledelete(
			@RequestParam("title") String title, 
			@RequestParam("startStr") String startStr,
			@RequestParam("endStr") String endStr, 
			HttpServletRequest request) {

		HttpSession session = request.getSession();
		int empnum = ((LoginDTO) session.getAttribute("loginMember")).getEmpnum();
		
		Map<String, Object> mapInfo = new HashMap<String, Object>();
		mapInfo.put("title", title);
		mapInfo.put("startStr", startStr);
		mapInfo.put("endStr", endStr);
		mapInfo.put("empnum", empnum);
		
	    int result = scheduleService.deleteSchedule(mapInfo);
	    
	    
	    if(result >= 1) {
	    	return "success";
	    } else {
	    	return "fail";
	    }
			

	}
}
