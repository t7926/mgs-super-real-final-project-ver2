package com.observer.mgs.board.club.controller;


import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.observer.mgs.board.club.model.dto.ClubDTO;
import com.observer.mgs.board.club.model.service.ClubService;
import com.observer.mgs.login.model.dto.LoginDTO;

@Controller
@RequestMapping("/board")
public class ClubController {
	
	private static final String ClubDTO = null;
	private final ClubService clubService;
	
	@Autowired
	public ClubController(ClubService clubService) {
		this.clubService = clubService;
	}
	
	/* 요청입력 */
	@GetMapping("/club-request")
	public void registRequest() {}
	
	@PostMapping("/club-request")
	public String registRequest(@ModelAttribute ClubDTO club, HttpServletRequest request, RedirectAttributes rttr ) {
		System.out.println("request" + club);
		java.sql.Date date = new java.sql.Date(System.currentTimeMillis());
		club.setClubReqDate(date);
		
		HttpSession session = request.getSession();
		
		LoginDTO loginDTO = new LoginDTO();
		System.out.println("login:" + loginDTO);
		int empnum = ((LoginDTO) session.getAttribute("loginMember")).getEmpnum();
		String name = ((LoginDTO) session.getAttribute("loginMember")).getName();
		
		club.setEmpNum(empnum);
		club.setWriter(name);
		
		
		
		int result = 0;
		try {
			result = clubService.registRequest(club);
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		String page = "";
		if(result > 0) {
			
			page = "redirect:/board/club-request-list";
			rttr.addFlashAttribute("message", "요청에 성공하셨습니다.");
		} else {
			
			page = "redirect:/board/club-request";
			rttr.addFlashAttribute("message", "오청에 실패하셨습니다.");
		}
		
		return page;
	}
	
	/* 신청 조회하기 */
	@GetMapping("/club-request-list")
	public String selectRequest(Model model) {
		
		List<ClubDTO> requestList = clubService.selectRequest();
		System.out.println("요청리스트 : " + requestList);

		model.addAttribute("requestList", requestList);
		
		return "/board/club-request-list";
		
	}
	
	/* 신청리스트 상세보기 */
	@GetMapping("/club-request-detail")
	public String selectRequestDetail(@RequestParam int clubReqNo, Model model) {
		
		System.out.println("요청번호 : " + clubReqNo);
		ClubDTO requestDetail = clubService.selectRequestDetail(clubReqNo);
		model.addAttribute("requestDetail", requestDetail);
		model.addAttribute("clubReqNo", clubReqNo);
//		model.addAttribute("statusNo", statusNo);
		
		return "/board/club-request-detail";
		
	}
	
	@GetMapping("/club-request-modify")
	public String modifyRequest(@RequestParam int clubReqNo, Model model) {
		
		ClubDTO club = clubService.selectRequestDetail(clubReqNo);
		model.addAttribute("club", club);
		return "/board/club-request-modify";
	}
	
	@PostMapping("/club-request-modify")
	public String modifyRequest(@ModelAttribute ClubDTO club , HttpServletRequest request ,RedirectAttributes rttr) {
		
//		String status = request.getParameter("status");
//		System.out.println("상태값 : " + status);
		System.out.println("클럽: " + club);
		
		String status= club.getStatusName();
		int statusNo = club.getStatusNo();
		
		System.out.println("상태 : " + status);
		System.out.println("상태번호 : " + statusNo);
		
		
		
		int result = 0;
		try {
			result = clubService.modifyRequest(club);
		} catch (Exception e) {
			
			e.printStackTrace();
		}









		
		String page = "";
		if(result > 0) {
			
			page = "redirect:/board/club-request-list";
			rttr.addFlashAttribute("message", "완료하셨습니다.");
			
		} else {
			page = "redirect:/board/club-request-list";
			rttr.addFlashAttribute("message", "실패하셨습니다.");
			
		}
		
		return page;
	}

	
}
