package com.observer.mgs.board.club.model.dto;

public class StatusDTO {

	private int statusNo;
	private String statusName;
	
	public StatusDTO() {
		
	}

	public StatusDTO(int statusNo, String statusName) {
		super();
		this.statusNo = statusNo;
		this.statusName = statusName;
	}

	public int getStatusNo() {
		return statusNo;
	}

	public void setStatusNo(int statusNo) {
		this.statusNo = statusNo;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	@Override
	public String toString() {
		return "StatusDTO [statusNo=" + statusNo + ", statusName=" + statusName + "]";
	}
	
}
