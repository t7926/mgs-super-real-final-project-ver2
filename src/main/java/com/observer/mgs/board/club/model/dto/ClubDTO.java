package com.observer.mgs.board.club.model.dto;

import java.sql.Date;

public class ClubDTO {
	
	private int clubReqNo;
	private String clubReqName;
	private String clubReqReason;
	private Date clubReqDate;
	private String writer;
	private int empNum;


	private String clubReqStatus;

	private int statusNo;

	private String clubCancleReason;
	private String statusName;
	
	public ClubDTO(){}

	public ClubDTO(int clubReqNo, String clubReqName, String clubReqReason, Date clubReqDate, String writer, int empNum,
			int statusNo, String clubCancleReason, String statusName) {
		super();
		this.clubReqNo = clubReqNo;
		this.clubReqName = clubReqName;
		this.clubReqReason = clubReqReason;
		this.clubReqDate = clubReqDate;
		this.writer = writer;
		this.empNum = empNum;
		this.statusNo = statusNo;
		this.clubCancleReason = clubCancleReason;
		this.statusName = statusName;
	}

	public int getClubReqNo() {
		return clubReqNo;
	}

	public void setClubReqNo(int clubReqNo) {
		this.clubReqNo = clubReqNo;
	}

	public String getClubReqName() {
		return clubReqName;
	}

	public void setClubReqName(String clubReqName) {
		this.clubReqName = clubReqName;
	}

	public String getClubReqReason() {
		return clubReqReason;
	}

	public void setClubReqReason(String clubReqReason) {
		this.clubReqReason = clubReqReason;
	}

	public Date getClubReqDate() {
		return clubReqDate;
	}

	public void setClubReqDate(Date clubReqDate) {
		this.clubReqDate = clubReqDate;
	}

	public String getWriter() {
		return writer;
	}

	public void setWriter(String writer) {
		this.writer = writer;
	}

	public int getEmpNum() {
		return empNum;
	}

	public void setEmpNum(int empNum) {
		this.empNum = empNum;
	}

	public int getStatusNo() {
		return statusNo;
	}

	public void setStatusNo(int statusNo) {
		this.statusNo = statusNo;
	}

	public String getClubCancleReason() {
		return clubCancleReason;
	}

	public void setClubCancleReason(String clubCancleReason) {
		this.clubCancleReason = clubCancleReason;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	@Override
	public String toString() {
		return "ClubDTO [clubReqNo=" + clubReqNo + ", clubReqName=" + clubReqName + ", clubReqReason=" + clubReqReason
				+ ", clubReqDate=" + clubReqDate + ", writer=" + writer + ", empNum=" + empNum + ", statusNo="
				+ statusNo + ", clubCancleReason=" + clubCancleReason + ", statusName=" + statusName + "]";
	}

	

	

	
	
	
	
	
}
