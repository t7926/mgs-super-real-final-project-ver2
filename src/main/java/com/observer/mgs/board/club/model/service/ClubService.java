package com.observer.mgs.board.club.model.service;


import java.util.List;

import com.observer.mgs.board.club.model.dto.ClubDTO;

public interface ClubService {

	int registRequest(ClubDTO club);

	List<ClubDTO> selectRequest();

	ClubDTO selectRequestDetail(int clubReqNo);

	int modifyRequest(ClubDTO club);



	

}
