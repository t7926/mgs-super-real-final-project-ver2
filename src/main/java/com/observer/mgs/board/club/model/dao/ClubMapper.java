package com.observer.mgs.board.club.model.dao;

import java.util.List;

import com.observer.mgs.board.club.model.dto.ClubDTO;

public interface ClubMapper {

	int registRequest(ClubDTO club);

	ClubDTO selectReuqest(int clubReqNo);

	List<ClubDTO> selectRequest();

	ClubDTO selectRequestDetail(int clubReqNo);

	int modifyRequest(ClubDTO club);

}
