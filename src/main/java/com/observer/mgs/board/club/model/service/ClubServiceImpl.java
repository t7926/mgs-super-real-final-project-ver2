package com.observer.mgs.board.club.model.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.observer.mgs.board.club.model.dao.ClubMapper;
import com.observer.mgs.board.club.model.dto.ClubDTO;

@Service
public class ClubServiceImpl implements ClubService{
	
	private final ClubMapper mapper;
	
	@Autowired
	public ClubServiceImpl(ClubMapper mapper) {
		this.mapper = mapper;
	}
		
	/**
	 * <pre>
	 *  동호회 요청 등록
	 * </pre>
	 */
	@Override
	public int registRequest(ClubDTO club) {
		
		int result = 0;
		try {
			result = mapper.registRequest(club);
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}


	/**
	 * <pre>
	 *  동호회 요청 리스트
	 * </pre>
	 */
	@Override
	public List<ClubDTO> selectRequest() {
		
		
		List<ClubDTO> requestList = mapper.selectRequest();
		
		return requestList;
	}

	
	@Override
	public ClubDTO selectRequestDetail(int clubReqNo) {
		
		return mapper.selectRequestDetail(clubReqNo);

	}

	

	@Override
	public int modifyRequest(ClubDTO club) {
		
		int result = 0;
		try {
			result = mapper.modifyRequest(club);
		} catch (Exception e) {
	
			e.printStackTrace();
		}
		return result;
		
	}

	

}
