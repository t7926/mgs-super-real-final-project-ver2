package com.observer.mgs.board.model.service;

import java.util.List;
import java.util.Map;

import com.observer.mgs.board.common.paging.SelectCriteria;
import com.observer.mgs.board.model.dto.NoticeBoardDTO;

public interface BoardService {

	int selectTotalCount(Map<String, String> searchMap);

	int registNotice(NoticeBoardDTO notice) throws Exception;

	NoticeBoardDTO selectNoticeDetail(int noticeNo);

	int modifyNotice(NoticeBoardDTO notice) throws Exception;

	void deleteNotice(int noticeNo);
	
	List<NoticeBoardDTO> selectBoardList(SelectCriteria selectCriteria);



}
