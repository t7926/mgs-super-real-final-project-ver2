package com.observer.mgs.board.model.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.observer.mgs.board.common.paging.SelectCriteria;
import com.observer.mgs.board.exception.board.NoticeRegistException;
import com.observer.mgs.board.model.dao.BoardMapper;
import com.observer.mgs.board.model.dto.NoticeBoardDTO;

@Service
public class BoardServiceImpl implements BoardService{

	private final BoardMapper mapper;
	
	@Autowired
	public BoardServiceImpl(BoardMapper mapper) {
		this.mapper = mapper;
	}
	
	/**
	 * <pre>
	 *  해당 게시글 전체 갯수 조회용 메소드 
	 * </pre>
	 */
	@Override
	public int selectTotalCount(Map<String, String> searchMap) {
		
		return mapper.selectTotalCount(searchMap);
	}

	/**
	 * <pre>
	 *  공지사항 등록 메소드
	 * </pre>
	 * @throws NoticeRegistException 
	 */
	@Override
	public int registNotice(NoticeBoardDTO notice) {
		
		int result = 0;
		try {
			result = mapper.insertNotice(notice);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}

	/**
	 * <pre>
	 *  공지사항 상세보기
	 * </pre>
	 *  @param borCode 공지사항 번호
	 */
	@Override
	public NoticeBoardDTO selectNoticeDetail(int noticeNo) {
		
		NoticeBoardDTO noticeDetail = null;
		/* 조회수 증가 */
		int result = mapper.incrementNoticeCount(noticeNo);
		
		/* 조회수 증가 성공 후 상세보기 */
		if(result > 0) {
			noticeDetail = mapper.selectNoticeDetail(noticeNo);
		}
		
		return noticeDetail;
	}

	/**
	 * <pre>
	 *  공지사항 수정 메소드
	 * </pre>
	 * @return 
	 */
	@Override
	public int modifyNotice(NoticeBoardDTO notice) {
		
		int result = 0;
		try {
			result = mapper.modifyNotice(notice);
		} catch (Exception e) {
	
			e.printStackTrace();
		}
		return result;
		
		
	}

	/**
	 * <pre>
	 * 공지사항 삭제 메소드
	 * </pre>
	 */
	@Override
	public void deleteNotice(int noticeNo) {
		
		int result = mapper.deleteNotice(noticeNo);
		
	}


	/**
	 * <pre>
	 *  게시글 전체 조회용 메소드
	 * </pre>
	 */
	@Override
	public List<NoticeBoardDTO> selectBoardList(SelectCriteria selectCriteria) {
		return mapper.selectBoardList(selectCriteria);
	}

	

}
