package com.observer.mgs.board.model.dto;

import java.sql.Date;

public class NoticeBoardDTO {
	private int noticeNo;
	private String noticeTitle;
	private Date noticeDate;
	private int noticeViews;
	private String noticeName; //작성자 회원정보에서 가져오기
	private String noticeContents;
	private int empNum;
	
	public NoticeBoardDTO() {	
	}

	public NoticeBoardDTO(int noticeNo, String noticeTitle, Date noticeDate, int noticeViews, String noticeName,
			String noticeContents, int empNum) {
		super();
		this.noticeNo = noticeNo;
		this.noticeTitle = noticeTitle;
		this.noticeDate = noticeDate;
		this.noticeViews = noticeViews;
		this.noticeName = noticeName;
		this.noticeContents = noticeContents;
		this.empNum = empNum;
	}

	public int getnoticeNo() {
		return noticeNo;
	}

	public void setnoticeNo(int noticeNo) {
		this.noticeNo = noticeNo;
	}

	public String getNoticeTitle() {
		return noticeTitle;
	}

	public void setNoticeTitle(String noticeTitle) {
		this.noticeTitle = noticeTitle;
	}

	public Date getNoticeDate() {
		return noticeDate;
	}

	public void setNoticeDate(Date noticeDate) {
		this.noticeDate = noticeDate;
	}

	public int getNoticeViews() {
		return noticeViews;
	}

	public void setNoticeViews(int noticeViews) {
		this.noticeViews = noticeViews;
	}

	public String getNoticeName() {
		return noticeName;
	}

	public void setNoticeName(String noticeName) {
		this.noticeName = noticeName;
	}

	public String getNoticeContents() {
		return noticeContents;
	}

	public void setNoticeContents(String noticeContents) {
		this.noticeContents = noticeContents;
	}

	public int getEmpNum() {
		return empNum;
	}

	public void setEmpNum(int empNum) {
		this.empNum = empNum;
	}

	@Override
	public String toString() {
		return "NoticeBoardDTO [noticeNo=" + noticeNo + ", noticeTitle=" + noticeTitle + ", noticeDate="
				+ noticeDate + ", noticeViews=" + noticeViews + ", noticeName=" + noticeName + ", noticeContents="
				+ noticeContents + ", empNum=" + empNum + "]";
	}

	
	
	
}
