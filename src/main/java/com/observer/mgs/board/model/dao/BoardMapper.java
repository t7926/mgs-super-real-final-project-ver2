package com.observer.mgs.board.model.dao;

import java.util.List;
import java.util.Map;

import com.observer.mgs.board.common.paging.SelectCriteria;
import com.observer.mgs.board.model.dto.NoticeBoardDTO;

public interface BoardMapper {

	int insertNotice(NoticeBoardDTO notice) throws Exception;

	int incrementNoticeCount(int noticeNo);

	NoticeBoardDTO selectNoticeDetail(int noticeNo);

	int modifyNotice(NoticeBoardDTO notice) throws Exception;

	int deleteNotice(int noticeNo);

	int selectTotalCount(Map<String, String> searchMap);

	List<NoticeBoardDTO> selectBoardList(SelectCriteria selectCriteria);

	

}
