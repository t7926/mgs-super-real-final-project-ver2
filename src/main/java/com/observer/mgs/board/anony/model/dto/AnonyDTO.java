package com.observer.mgs.board.anony.model.dto;

import java.sql.Date;

public class AnonyDTO {

	private int anoNo;
	private String anoName;
	private String anoTitle;
	private String anoContents;
	private String anoPassword;
	private Date anoDate;
	private int anoViews;
	
	public AnonyDTO() {}

	public AnonyDTO(int anoNo, String anoName, String anoTitle, String anoContents, String anoPassword, Date anoDate,
			int anoViews) {
		super();
		this.anoNo = anoNo;
		this.anoName = anoName;
		this.anoTitle = anoTitle;
		this.anoContents = anoContents;
		this.anoPassword = anoPassword;
		this.anoDate = anoDate;
		this.anoViews = anoViews;
	}

	public int getAnoNo() {
		return anoNo;
	}

	public void setAnoNo(int anoNo) {
		this.anoNo = anoNo;
	}

	public String getAnoName() {
		return anoName;
	}

	public void setAnoName(String anoName) {
		this.anoName = anoName;
	}

	public String getAnoTitle() {
		return anoTitle;
	}

	public void setAnoTitle(String anoTitle) {
		this.anoTitle = anoTitle;
	}

	public String getAnoContents() {
		return anoContents;
	}

	public void setAnoContents(String anoContents) {
		this.anoContents = anoContents;
	}

	public String getAnoPassword() {
		return anoPassword;
	}

	public void setAnoPassword(String anoPassword) {
		this.anoPassword = anoPassword;
	}

	public Date getAnoDate() {
		return anoDate;
	}

	public void setAnoDate(Date anoDate) {
		this.anoDate = anoDate;
	}

	public int getAnoViews() {
		return anoViews;
	}

	public void setAnoViews(int anoViews) {
		this.anoViews = anoViews;
	}

	@Override
	public String toString() {
		return "AnonyDTO [anoNo=" + anoNo + ", anoName=" + anoName + ", anoTitle=" + anoTitle + ", anoContents="
				+ anoContents + ", anoPassword=" + anoPassword + ", anoDate=" + anoDate + ", anoViews=" + anoViews
				+ "]";
	}
	
	
}
