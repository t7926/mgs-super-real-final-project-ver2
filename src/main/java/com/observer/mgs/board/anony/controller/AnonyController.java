package com.observer.mgs.board.anony.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.observer.mgs.board.anony.model.dto.AnonyDTO;
import com.observer.mgs.board.anony.model.service.AnonyService;
import com.observer.mgs.board.common.paging.Pagenation;
import com.observer.mgs.board.common.paging.SelectCriteria;

@Controller
@RequestMapping("/board")
public class AnonyController {

	private final AnonyService anonyService;
	
	@Autowired
	public AnonyController(AnonyService anonyService) {
		this.anonyService = anonyService;
	}
	
	

	
	/* 익명 조회 페이징 검색 */

	@GetMapping("/anonymous-list")
	public ModelAndView AnonySelectList(@RequestParam(defaultValue = "1") int currentPage, @ModelAttribute SelectCriteria searchCriteria,
	         ModelAndView mv, HttpServletRequest request) {
	
		String searchCondition = searchCriteria.getSearchCondition();
		String searchValue = searchCriteria.getSearchValue();
		
		
//		HttpSession session = request.getSession();
//		
//		LoginDTO loginDTO = new LoginDTO();
//		System.out.println("login:" + loginDTO);
//		int empnum = ((LoginDTO) session.getAttribute("loginMember")).getEmpnum();
//		String name = ((LoginDTO) session.getAttribute("loginMember")).getName();
		
		Map<String, String> searchMap = new HashMap<>();
		searchMap.put("searchCondition", searchCondition);
		searchMap.put("searchValue", searchValue);
		
		System.out.println("컨트롤러에서 검색 조건 확인하기 : " + searchMap);
		
		/* 전체 게시물 수가 필요하다. 
		 * 1. 데이터베이스에서 먼저 전체 게시물 수를 조회
		 * 2. 검색조건이 있는 경우 검색 조건에 맞는 전체 게시물 수를 조회
		 * */
		int totalCount = anonyService.selectTotalCount(searchMap);
		
		System.out.println("totalAnonyCount : " + totalCount);
		
		/* 한 페이지에 보여줄 게시물 수 */
		int limit = 10;
		
		/* 한 번에 보여질 페이징 버튼의 갯수*/
		int buttonAmount = 5; 
		
		/* 페이징 처리를 위한 로직 호출 후 페이징 처리에 관한 정보를 담고 있는 인스턴스를 반환*/
		SelectCriteria selectCriteria = null;
		
		if(searchCondition != null && !"".equals(searchCondition)) {
		
			selectCriteria = Pagenation.getSelectCriteria(currentPage, totalCount, limit, buttonAmount, searchCondition, searchValue);
		} else {
			selectCriteria = Pagenation.getSelectCriteria(currentPage, totalCount, limit, buttonAmount);
		}
		
		System.out.println("selectCriteria : " + selectCriteria);
		
		List<AnonyDTO> anonyList= anonyService.selectAnonyList(selectCriteria);
		
		System.out.println("anonyList : " + anonyList);
		
		mv.addObject("anonyList", anonyList);
		mv.addObject("selectCriteria", selectCriteria);
		mv.addObject("type","anony");
		mv.setViewName("/board/anonymous-list");
		return mv;
	}
	
	/* 익명게시판 글쓰기 */
	@GetMapping("/anonymous-write")
	public void registAnony() {}
	
	@PostMapping("/anonymous-write")
	public String registAnony(@ModelAttribute AnonyDTO anony, HttpServletRequest request, RedirectAttributes rttr) {
		java.sql.Date date = new java.sql.Date(System.currentTimeMillis());
		anony.setAnoDate(date);
		
			
		int result = 0;
		try {
			result = anonyService.registAnony(anony);
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		
		
		String page = "";
		if(result > 0) {
			
			page = "redirect:/board/anonymous-list";
			rttr.addFlashAttribute("message", "익명게시글 등록에 성공하셨습니다.");
			
		} else {
			
			page = "redirect:/board/anonymous-list";
			rttr.addFlashAttribute("message", "익명게시글 등록에 실패하셨습니다.");
		}
		
		return page;
	}
	
	/* 상세보기 */
	@GetMapping("/anonymous-detail")
	public String selectAnonyDetail(@RequestParam int anoNo, Model model, HttpServletRequest request) {

		System.out.println("anoNo" + anoNo);
		AnonyDTO anonyDetail = anonyService.selectAnonyDetail(anoNo);
		model.addAttribute("anony", anonyDetail);
		model.addAttribute("anoNo",anoNo);
		
		return "/board/anonymous-detail";
	}
	
	
	/* 수정하기 */
	@GetMapping("/anonymous-modify")
	public String anonyNotice(@RequestParam int anoNo, Model model) {
		
		AnonyDTO anony = anonyService.selectAnonyDetail(anoNo);
		model.addAttribute("anony", anony);
		return "/board/anonymous-modify";
	}
	
	@PostMapping("/anonymous-modify")
	public String modifyAnony(@ModelAttribute AnonyDTO anony, HttpServletRequest request, RedirectAttributes rttr) {
		
		
		System.out.println("수정쓰 : " + anony);
		int result = 0;
		try {
			anonyService.modifyAnony(anony);
		} catch (Exception e) {
			
			e.printStackTrace();
		}

		
		String page = "";
		if(result > 0) {
			
			page = "redirect:/board/anonymous-list";
			rttr.addFlashAttribute("message", "익명게시글 수정에 실패하셨습니다.");
			
		} else {
			page = "redirect:/board/anonymous-list";
			rttr.addFlashAttribute("message", "익명게시글 수정에 성공하셨습니다.");
			
		}
		
		return page;
	}
	
	/* 삭제하기 */
	@GetMapping("/anonymous-delete")
	public String deleteAnony(@RequestParam int anoNo, RedirectAttributes rttr) {
		
		anonyService.deleteAnony(anoNo);
		
		rttr.addFlashAttribute("message", "익명게시글 삭제에 성공하였습니다");
		
		return "redirect:/board/anonymous-list";
	}
	
	/* 삭제비번 페이지이동  */
	@GetMapping("/deletePassword")
	private String deletePassword(@RequestParam int anoNo, Model model){	
		
		System.out.println("anoNo 들어옴: " + anoNo);
//		System.out.println("anoPassword다 : " + anoPassword);
		AnonyDTO anonyDelete = anonyService.deletePassword(anoNo);
		
		model.addAttribute("anony", anonyDelete);
		model.addAttribute("anoNo", anoNo);
		
		return "/board/anonymous-delete-pw";	
	}
	
	
	/* 삭제비번 확인*/
	@PostMapping(value="/checkPassword1", produces="text/plain; charset=utf-8")
	@ResponseBody
	public String checkPassword1(@RequestParam("anoPassword") String anoPassword, @RequestParam("anoNo") int anoNo) throws Exception{
		System.out.println("패스워드" + anoPassword);
		
		AnonyDTO check = anonyService.deletePassword(anoNo);
		
		return "";
		
	}
	
	/* 수정비번 페이지이동  */
	@GetMapping("/modifyPassword")
	private String modifyPassword(@RequestParam int anoNo, Model model){	
		
		System.out.println("anoNo 들어옴: " + anoNo);
//		System.out.println("anoPassword다 : " + anoPassword);
		AnonyDTO anonyModify = anonyService.modifyPassword(anoNo);
		
		model.addAttribute("anony", anonyModify);
		model.addAttribute("anoNo", anoNo);
		
		return "/board/anonymous-modify-pw";	
	}
	

	
	/* 수정비번 확인*/
	@PostMapping(value="/checkPassword2", produces="text/plain; charset=utf-8")	
	@ResponseBody
	public String checkPassword2(@RequestParam("anoPassword") String anoPassword, @RequestParam("anoNo") int anoNo) throws Exception{
		System.out.println("패스워드" + anoPassword);
		
		AnonyDTO check	 = anonyService.modifyPassword(anoNo);
		
		return "";
		
	}
	
	
}

