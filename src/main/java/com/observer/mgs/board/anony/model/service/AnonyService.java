package com.observer.mgs.board.anony.model.service;

import java.util.List;
import java.util.Map;

import com.observer.mgs.board.anony.model.dto.AnonyDTO;
import com.observer.mgs.board.common.paging.SelectCriteria;

public interface AnonyService {

	int selectTotalCount(Map<String, String> searchMap);

	List<AnonyDTO> selectAnonyList(SelectCriteria selectCriteria);

	int registAnony(AnonyDTO anony) throws Exception;

	AnonyDTO selectAnonyDetail(int anoNo);

	int modifyAnony(AnonyDTO anony) throws Exception;

	void deleteAnony(int anoNo);
	
	AnonyDTO deletePassword(int anoNo);

	AnonyDTO modifyPassword(int anoNo);

	

	

	





}
