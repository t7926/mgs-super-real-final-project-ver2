package com.observer.mgs.board.anony.model.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.observer.mgs.board.anony.model.dao.AnonyMapper;
import com.observer.mgs.board.anony.model.dto.AnonyDTO;
import com.observer.mgs.board.common.paging.SelectCriteria;

@Service
public class AnonyServiceImpl implements AnonyService{

	private final AnonyMapper mapper;
	
	@Autowired
	public AnonyServiceImpl(AnonyMapper mapper) {
		this.mapper = mapper;
	}
	
	/**
	 *<pre>
	 * 익명 게시글 전체 갯수 조회용 메소드
	 *</pre>
	 */
	@Override
	public int selectTotalCount(Map<String, String> searchMap) {
		
		return mapper.selectTotalCount(searchMap);
	}

	/**
	 *<pre>
	 * 익명게시글 전체 조회용 메소드
	 *</pre>
	 */
	@Override
	public List<AnonyDTO> selectAnonyList(SelectCriteria selectCriteria) {
		
		return mapper.selectAnonyList(selectCriteria);
	}

	/**
	 * <pre>
	 *  익명게시글 등록
	 * </pre>
	 */
	@Override
	public int registAnony(AnonyDTO anony) {
		
		int result = 0;
		try {
			result = mapper.insertAnony(anony);
		}catch(Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}

	/**
	 * <pre>
	 *  익명게시글 상세보기
	 * </pre>
	 */
	@Override
	public AnonyDTO selectAnonyDetail(int anoNo) {
		
		AnonyDTO anonyDetail = null;
		
		int result = mapper.incrementAnonyCount(anoNo);
		
		if(result > 0) {
			anonyDetail = mapper.selectAnonyDetail(anoNo);
		}
		
		return anonyDetail;
	}

	/**
	 * <pre>
	 *  익명게시글 수정 메소드
	 * </pre>
	 */
	@Override
	public int modifyAnony(AnonyDTO anony) {
		
		int result = 0;
		try {
			result = mapper.modifyAnony(anony);
		} catch (Exception e) {
	
			e.printStackTrace();
		}
		return result;
	}

	/**
	 * <pre>
	 * 	익명게시글 삭제 메소드
	 * </pre>
	 */
	@Override
	public void deleteAnony(int anoNo) {
		
		int result = mapper.deleteAnony(anoNo);
	}

	

	/**
	 * <pre>
	 *  삭제 비번 이동
	 * </pre>
	 */
	@Override
	public AnonyDTO deletePassword(int anoNo) {
		
		return mapper.deletePassword(anoNo);
	}

	/**
	 * <pre>
	 *  수정 비번 이동
	 * </pre>
	 */
	@Override
	public AnonyDTO modifyPassword(int anoNo) {
		
		return mapper.modifyPassword(anoNo);
	}

	
	

}
