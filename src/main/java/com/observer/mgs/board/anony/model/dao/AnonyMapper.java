package com.observer.mgs.board.anony.model.dao;

import java.util.List;
import java.util.Map;

import com.observer.mgs.board.anony.model.dto.AnonyDTO;
import com.observer.mgs.board.common.paging.SelectCriteria;

public interface AnonyMapper {

	int selectTotalCount(Map<String, String> searchMap);

	List<AnonyDTO> selectAnonyList(SelectCriteria selectCriteria);

	int insertAnony(AnonyDTO anony) throws Exception;

	int incrementAnonyCount(int anoNo);

	AnonyDTO selectAnonyDetail(int anoNo);

	int modifyAnony(AnonyDTO anony) throws Exception;

	int deleteAnony(int anoNo);


	AnonyDTO deletePassword(int anoNo);

	AnonyDTO modifyPassword(int anoNo);





}
