package com.observer.mgs.board.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.observer.mgs.board.common.paging.Pagenation;
import com.observer.mgs.board.common.paging.SelectCriteria;
import com.observer.mgs.board.model.dto.NoticeBoardDTO;
import com.observer.mgs.board.model.service.BoardService;
import com.observer.mgs.login.model.dto.LoginDTO;

@Controller
@RequestMapping("/board")
public class BoardController {
	
	private final BoardService boardService;
	
	@Autowired
	public BoardController(BoardService boardService) {
		this.boardService = boardService;
	}
	
	/* 조회 페이징 검색*/
	@GetMapping("/list")
	public ModelAndView BoardSelectList(@RequestParam(defaultValue = "1") int currentPage, @ModelAttribute SelectCriteria searchCriteria,
	         ModelAndView mv, HttpServletRequest request) {
	
		String searchCondition = searchCriteria.getSearchCondition();
		String searchValue = searchCriteria.getSearchValue();
		
		
		HttpSession session = request.getSession();
		
		LoginDTO loginDTO = new LoginDTO();
		System.out.println("login:" + loginDTO);
		int empnum = ((LoginDTO) session.getAttribute("loginMember")).getEmpnum();
		String name = ((LoginDTO) session.getAttribute("loginMember")).getName();
		
		Map<String, String> searchMap = new HashMap<>();
		searchMap.put("searchCondition", searchCondition);
		searchMap.put("searchValue", searchValue);
		
		System.out.println("컨트롤러에서 검색 조건 확인하기 : " + searchMap);
		
		/* 전체 게시물 수가 필요하다. 
		 * 1. 데이터베이스에서 먼저 전체 게시물 수를 조회
		 * 2. 검색조건이 있는 경우 검색 조건에 맞는 전체 게시물 수를 조회
		 * */
		int totalCount = boardService.selectTotalCount(searchMap);
		
		System.out.println("totalBoardCount : " + totalCount);
		
		/* 한 페이지에 보여줄 게시물 수 */
		int limit = 10;
		
		/* 한 번에 보여질 페이징 버튼의 갯수*/
		int buttonAmount = 5; 
		
		/* 페이징 처리를 위한 로직 호출 후 페이징 처리에 관한 정보를 담고 있는 인스턴스를 반환*/
		SelectCriteria selectCriteria = null;
		
		if(searchCondition != null && !"".equals(searchCondition)) {
		
			selectCriteria = Pagenation.getSelectCriteria(currentPage, totalCount, limit, buttonAmount, searchCondition, searchValue);
		} else {
			selectCriteria = Pagenation.getSelectCriteria(currentPage, totalCount, limit, buttonAmount);
		}
		
		System.out.println("selectCriteria : " + selectCriteria);
		
		List<NoticeBoardDTO> noticeList = boardService.selectBoardList(selectCriteria);
		
		System.out.println("noticeList : " + noticeList);
		
		mv.addObject("noticeList", noticeList);
		mv.addObject("selectCriteria", selectCriteria);
		mv.addObject("type","board");
		mv.setViewName("/board/notice-list");
		return mv;
	}
	
	
	/* 글쓰기 */
	@GetMapping("/notice-write")
	public void registNotice() {}

	@PostMapping("/notice-write")
	public String registNotice(@ModelAttribute NoticeBoardDTO notice, HttpServletRequest request, RedirectAttributes rttr) {
		java.sql.Date date = new java.sql.Date(System.currentTimeMillis());
		notice.setNoticeDate(date);
		
		HttpSession session = request.getSession();
		
		LoginDTO loginDTO = new LoginDTO();
		System.out.println("login:" + loginDTO);
		int empnum = ((LoginDTO) session.getAttribute("loginMember")).getEmpnum();
		String name = ((LoginDTO) session.getAttribute("loginMember")).getName();
		
		notice.setEmpNum(empnum);
		notice.setNoticeName(name);
		
		int result = 0;
		try {
			result = boardService.registNotice(notice);
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		
		
		String page = "";
		if(result > 0) {
			
			page = "redirect:/board/list";
			rttr.addFlashAttribute("message", "공지사항 등록에 성공하셨습니다.");
			
		} else {
			
			page = "redirect:/board/list";
			rttr.addFlashAttribute("message", "공지사항 등록에 실패하셨습니다.");
		}
		
		return page;
	}
	
	/* 상세보기 */
	@GetMapping("/notice-detail")
	public String selectNoticeDetail(@RequestParam int noticeNo, Model model, HttpServletRequest request) {
			
		System.out.println("noticeNo" + noticeNo);
		NoticeBoardDTO noticeDetail = boardService.selectNoticeDetail(noticeNo);
		model.addAttribute("notice", noticeDetail);
		model.addAttribute("noticeNo",noticeNo);
		
		return "/board/notice-detail";
	}
	
	
	/* 수정하기 */
	@GetMapping("/notice-modify")
	public String modifyNotice(@RequestParam int noticeNo, Model model) {
		
		NoticeBoardDTO notice = boardService.selectNoticeDetail(noticeNo);
		model.addAttribute("notice", notice);
		return "/board/notice-modify";
	}	
	
	@PostMapping("/notice-modify")
	public String modifyNotice(@ModelAttribute NoticeBoardDTO notice, HttpServletRequest request, RedirectAttributes rttr) {
		
		HttpSession session = request.getSession();
		
		LoginDTO loginDTO = new LoginDTO();
		System.out.println("login:" + loginDTO);
		int empnum = ((LoginDTO) session.getAttribute("loginMember")).getEmpnum();
		String name = ((LoginDTO) session.getAttribute("loginMember")).getName();
		
		notice.setEmpNum(empnum);
		notice.setNoticeName(name);
		
		System.out.println("notice : " + notice);
		int result = 0;
		try {
			boardService.modifyNotice(notice);
		} catch (Exception e) {
			
			e.printStackTrace();
		}

		
		String page = "";
		if(result > 0) {
			page = "redirect:/board/list";
			rttr.addFlashAttribute("message", "공지사항 수정에 실패하셨습니다.");
			
		} else {
			page = "redirect:/board/list";
			rttr.addFlashAttribute("message", "공지사항 수정에 성공하셨습니다.");	
		}
		return page;
	}
	
	
	/* 삭제하기 */
	@GetMapping("/delete")
	public String deleteNotice(@RequestParam int noticeNo, RedirectAttributes rttr) {
		
		boardService.deleteNotice(noticeNo);
		
		rttr.addFlashAttribute("message", "공지사항 삭제에 성공하였습니다");
		
		return "redirect:/board/list";
	}
	
	
}
