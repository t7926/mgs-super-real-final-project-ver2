package com.observer.mgs.board.exception.board;

public class AnonyRegistException extends Exception{

	public AnonyRegistException(String msg) {
		super(msg);
	}
}
