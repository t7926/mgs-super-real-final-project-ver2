package com.observer.mgs.board.exception.board;

public class NoticeRegistException extends Exception{
	
	public NoticeRegistException(String msg) {
		super(msg);
	}
}
