package com.observer.mgs.board.exception.board;

public class NoticeModifyException extends Exception{

	public NoticeModifyException(String msg) {
		super(msg);
	}
}
