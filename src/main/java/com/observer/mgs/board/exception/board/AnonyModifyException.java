package com.observer.mgs.board.exception.board;

public class AnonyModifyException extends Exception{

	public AnonyModifyException(String msg) {
		super(msg);
	}
}
