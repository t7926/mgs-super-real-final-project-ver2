
  package com.observer.mgs.superauth.model.service;
  
  import java.util.List;
  
  import com.observer.mgs.superauth.model.dto.SuperauthDTO;
  
  public interface SuperauthService {
  
  List<SuperauthDTO> selectAllMemberList();

  SuperauthDTO findSuperauthDetail(int no);

  int modifyAuth(SuperauthDTO superauthDTO);
  
  }
 