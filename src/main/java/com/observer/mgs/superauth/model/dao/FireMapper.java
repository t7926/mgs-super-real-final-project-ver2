package com.observer.mgs.superauth.model.dao;

import java.util.List;

import com.observer.mgs.superauth.model.dto.FireDTO;


public interface FireMapper {

	List<FireDTO> selectMemberList();

	FireDTO findMemberDetail(int no);

	int deleteMember(FireDTO fire);

}
