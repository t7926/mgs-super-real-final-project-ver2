
  package com.observer.mgs.superauth.model.dto;
 
  public class SuperauthDTO {
  
  private int empnum; 
  private String empname; 
  private String deptname; 
  private String levname; 
  private String authright_yn;
  private String boardright_yn;
  
  public SuperauthDTO() {}

  public SuperauthDTO(int empnum, String empname, String deptname, String levname, String authright_yn,
		String boardright_yn) {
	super();
	this.empnum = empnum;
	this.empname = empname;
	this.deptname = deptname;
	this.levname = levname;
	this.authright_yn = authright_yn;
	this.boardright_yn = boardright_yn;
  }

  public int getEmpnum() {
	 return empnum;
  }

  public void setEmpnum(int empnum) {
	this.empnum = empnum;
  }

  public String getEmpname() {
	return empname;
  }

  public void setEmpname(String empname) {
	this.empname = empname;
  }

  public String getDeptname() {
	return deptname;
  }

  public void setDeptname(String deptname) {
	this.deptname = deptname;
  }

  public String getLevname() {
	return levname;
  }

  public void setLevname(String levname) {
	this.levname = levname;
  }

  public String getAuthright_yn() {
	return authright_yn;
  }

  public void setAuthright_yn(String authright_yn) {
	this.authright_yn = authright_yn;
  }

  public String getBoardright_yn() {
	return boardright_yn;
  }

  public void setBoardright_yn(String boardright_yn) {
	this.boardright_yn = boardright_yn;
  }

  @Override
  public String toString() {
	return "SuperauthDTO [empnum=" + empnum + ", empname=" + empname + ", deptname=" + deptname + ", levname=" + levname
			+ ", authright_yn=" + authright_yn + ", boardright_yn=" + boardright_yn + "]";
  }


  
  
  
  }
 

