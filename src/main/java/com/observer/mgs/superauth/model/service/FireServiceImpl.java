package com.observer.mgs.superauth.model.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.observer.mgs.common.exception.member.MemberRemoveException;
import com.observer.mgs.superauth.model.dao.FireMapper;

import com.observer.mgs.superauth.model.dto.FireDTO;


@Service
public class FireServiceImpl implements FireService{

	private final FireMapper mapper;
	
	  @Autowired 
	  public FireServiceImpl(FireMapper mapper) {
	     this.mapper = mapper; 
	  }

   /* 멤버 상세 조회*/	
	  
	@Override
	public List<FireDTO> selectAllMemberList() {
		
	List<FireDTO> allmemberList = mapper.selectMemberList();
	
		return allmemberList;
	}

	@Override
	public FireDTO findMemberDetail(int no) {
		
		return mapper.findMemberDetail(no);
	}

	
	/* 강제 탈퇴*/
	
	@Override
	public void removeMember(FireDTO fire) throws MemberRemoveException{
		
		int result = mapper.deleteMember(fire);
		
		if((result<0)) {
			throw new MemberRemoveException("실패 하였습니다.");
		}
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
