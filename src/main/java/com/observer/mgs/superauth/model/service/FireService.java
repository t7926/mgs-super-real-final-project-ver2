package com.observer.mgs.superauth.model.service;

import java.util.List;

import com.observer.mgs.common.exception.member.MemberRemoveException;
import com.observer.mgs.superauth.model.dto.FireDTO;

public interface FireService {

	List<FireDTO> selectAllMemberList();

	FireDTO findMemberDetail(int no);

	void removeMember(FireDTO fire) throws MemberRemoveException;

}
