
  package com.observer.mgs.superauth.model.dto;

import java.sql.Date;

public class FireDTO {
  
  private int empNum; 
  private String empname; 
  private String deptname; 
  private String levname; 
  private Date birthday;
  private String cphone;
  private String email;
  private String status;
  
  public FireDTO() {}

public FireDTO(int empNum, String empname, String deptname, String levname, Date birthday, String cphone, String email,
		String status) {
	super();
	this.empNum = empNum;
	this.empname = empname;
	this.deptname = deptname;
	this.levname = levname;
	this.birthday = birthday;
	this.cphone = cphone;
	this.email = email;
	this.status = status;
}

public int getEmpNum() {
	return empNum;
}

public void setEmpNum(int empNum) {
	this.empNum = empNum;
}

public String getEmpname() {
	return empname;
}

public void setEmpname(String empname) {
	this.empname = empname;
}

public String getDeptname() {
	return deptname;
}

public void setDeptname(String deptname) {
	this.deptname = deptname;
}

public String getLevname() {
	return levname;
}

public void setLevname(String levname) {
	this.levname = levname;
}

public Date getBirthday() {
	return birthday;
}

public void setBirthday(Date birthday) {
	this.birthday = birthday;
}

public String getCphone() {
	return cphone;
}

public void setCphone(String cphone) {
	this.cphone = cphone;
}

public String getEmail() {
	return email;
}

public void setEmail(String email) {
	this.email = email;
}

public String getStatus() {
	return status;
}

public void setStatus(String status) {
	this.status = status;
}

@Override
public String toString() {
	return "FireDTO [empNum=" + empNum + ", empname=" + empname + ", deptname=" + deptname + ", levname=" + levname
			+ ", birthday=" + birthday + ", cphone=" + cphone + ", email=" + email + ", status=" + status + "]";
}

  
  
  
  
  }
 

