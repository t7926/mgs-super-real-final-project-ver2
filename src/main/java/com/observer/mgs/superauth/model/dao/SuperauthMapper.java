package com.observer.mgs.superauth.model.dao;
  
import java.util.List;
  
import com.observer.mgs.superauth.model.dto.SuperauthDTO;
  
public interface SuperauthMapper {
  
    List<SuperauthDTO> selectAuthList();

	SuperauthDTO findSuperauthDetail(int no);

	int modifyAuth(SuperauthDTO superauthDTO);

	SuperauthDTO selectAuthData(SuperauthDTO superauthDTO);

	int insertAuth(SuperauthDTO superauth);
  
}
 