package com.observer.mgs.superauth.model.service;
  
import java.util.List;  
import org.springframework.beans.factory.annotation.Autowired; 
import org.springframework.stereotype.Service;
import com.observer.mgs.superauth.model.dao.SuperauthMapper; 
import com.observer.mgs.superauth.model.dto.SuperauthDTO;
  
@Service 
public class SuperauthServiceImpl implements SuperauthService{

  private final SuperauthMapper mapper;
  
  
  @Autowired 
  public SuperauthServiceImpl(SuperauthMapper mapper) {
     this.mapper = mapper; 
  }
  
  /* 상세 조회*/
  @Override 
  public List<SuperauthDTO> selectAllMemberList() {
  
       List<SuperauthDTO> superauthList = mapper.selectAuthList();
  
       return superauthList; 
       
  }


  /* 권한 수정 */

   @Override
   public int modifyAuth(SuperauthDTO superauthDTO) {
	
	 int result = 0; // 수정 업데이트에서는 결과 값이 0 혹은 1 인 성공한 행의 갯수가 들어오므로 int를 사용한다.
	 /* empnum을 이용해서 권한 테이블을 조회한다.*/
	 SuperauthDTO superauth = mapper.findSuperauthDetail(superauthDTO.getEmpnum()); // 넘겨온 dto 값을 selectAuthData에 담아 superauth에 넣어준다.
	 
	 //  조회된 결과에서  null일 경우 이면 insert를 진행한다.
	 // Authright_yn  , Boardright_yn  -> 인설트
     if (superauth.getAuthright_yn() != null || superauth.getBoardright_yn() != null) {
	 
    	result = mapper.modifyAuth(superauthDTO); // 수정이 성공하였을 때 값을 당기 위해 result를 쓴다.
    	 
     } else {
    	result = mapper.insertAuth(superauthDTO); // 인설트가 성공하였을 때 값을 당기 위해 result를 쓴다.
    	
     } 
	 // 조회된 결과가 null이 아닌 경우 이면 
	 // Authright_yn 이 Y / N 일 경우 -> 업데이트
	 // Boardright_yn 이 Y /N 일 경우 -> 업데이트
	
	 return result;
	 
  }

  @Override
   public SuperauthDTO findSuperauthDetail(int no) {
	
	 return mapper.findSuperauthDetail(no);
  }
  
  
  
 
  



  
  
}
 
