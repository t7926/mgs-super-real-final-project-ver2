package com.observer.mgs.superauth.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.observer.mgs.superauth.model.dto.SuperauthDTO;
import com.observer.mgs.superauth.model.service.SuperauthService;

@Controller
@RequestMapping("/superauth")
public class SuperauthController {

    private final SuperauthService superauthService;
	
	@Autowired
	public SuperauthController(SuperauthService superauthService) {
		this.superauthService = superauthService;
	}
	
	/*전체 회원 띄우기*/
		
	@GetMapping("/superauth")
	public String findSuperauth(Model model) {
		
		List<SuperauthDTO> superauthList = superauthService.selectAllMemberList();
		
		model.addAttribute("superauthList", superauthList);
		
		return "/superauth/superauth";
	}
	
	
	/* 상세 보기 */

	@GetMapping("/detail")
	public String findSuperauthDetail(@RequestParam int empNum, Model model) {
		
		System.out.println("값 확인 : " + empNum);
		
		SuperauthDTO authDetail = superauthService.findSuperauthDetail(empNum);
		
		model.addAttribute("superauthDetail", authDetail);
		
		return "/superauth/superauth-detail";
	}
	
	@PostMapping("/update")
	public String modifyAuth(@ModelAttribute SuperauthDTO superauthDTO, RedirectAttributes rttr) {
//		
//		System.out.println(result);
		
		/* 데이터를 갖고 온다*/

		System.out.println(" 확인 1: " + superauthDTO.getAuthright_yn());
		System.out.println(" 확인 2: " + superauthDTO.getBoardright_yn());
		System.out.println(" 확인 3: " + superauthDTO.getEmpnum());
		int result = superauthService.modifyAuth(superauthDTO);
		System.out.println("result : " + result);
		//SuperauthDTO result = superauthService.findSuperauthDetail(superauthDTO);
		if(result > 0) {
			
			rttr.addFlashAttribute("message", "수정 완료");
		} else {
			
			rttr.addFlashAttribute("message", "수정 실패");
		}
		
//		rttr.addFlashAttribute("message", "수정 완료");
		
		return "redirect:/superauth/superauth";
	}
	
	
	
	
	
	
	
	
	
	
}







