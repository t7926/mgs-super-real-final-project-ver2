package com.observer.mgs.superauth.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.observer.mgs.common.exception.member.MemberRemoveException;
import com.observer.mgs.superauth.model.dto.FireDTO;
import com.observer.mgs.superauth.model.service.FireService;




@Controller
@RequestMapping("/superauth")
public class FireController {

    private final FireService fireService;
	
	@Autowired
	public FireController(FireService fireService) {
		this.fireService = fireService;
	}
	
	
	/*전체 회원 조회*/
	@GetMapping("/superauth-fire")
    public String findAllMember(Model model) {
		
    List<FireDTO> allmemberList = fireService.selectAllMemberList();
		
    model.addAttribute("allmemberList", allmemberList);
		
		
		
		return "/superauth/superauth-fire";
	}
	
	
	/* 상세 보기*/
	
	
	@GetMapping("/fire-detail")
	public String findMemberDetail(@RequestParam int empNum, Model model) {
		
		System.out.println("값 확인용 : " + empNum);
		
		FireDTO memberDetail = fireService.findMemberDetail(empNum);
		
		model.addAttribute("memberDetail", memberDetail);
		
		return "/superauth/superauth-fire-detail";
	}
	
	
	@PostMapping("/delete")
	public String deleteMember(@ModelAttribute FireDTO fire, SessionStatus status, RedirectAttributes rttr) throws MemberRemoveException {
		
		//System.out.println(" 파이어 값 확인 : " + fire ) ;
		
		fireService.removeMember(fire);
		
		
		rttr.addFlashAttribute("message", "회원 탈퇴에 성공했습니다.");
		status.setComplete();
		
		
		return "redirect:/main/main";
	}
	
	
	
	
	
	
	
	
	
	
	
}
