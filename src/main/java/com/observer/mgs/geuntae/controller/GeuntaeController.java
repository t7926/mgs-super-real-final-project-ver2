package com.observer.mgs.geuntae.controller;

import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.observer.mgs.geuntae.exception.worktime.SearchPaymentUserException;
import com.observer.mgs.geuntae.model.dto.AnnualVacationDTO;
import com.observer.mgs.geuntae.model.dto.ProfileDTO;
import com.observer.mgs.geuntae.model.dto.VacationDTO;
import com.observer.mgs.geuntae.model.dto.VacationDeleteDTO;
import com.observer.mgs.geuntae.model.dto.WorktimeDTO;
import com.observer.mgs.geuntae.model.dto.WorktimeModDTO;
import com.observer.mgs.geuntae.model.dto.WorktimebyManagerDTO;
import com.observer.mgs.geuntae.model.dto.paymentUserDTO;
import com.observer.mgs.geuntae.model.service.GeuntaeService;
import com.observer.mgs.login.model.dto.LoginDTO;

@Controller
@RequestMapping("/geuntae")
public class GeuntaeController {

	private final GeuntaeService geuntaeService;

	@Autowired
	public GeuntaeController(GeuntaeService geuntaeService) {

		this.geuntaeService = geuntaeService;
	}

//	근무시간 조회
	@GetMapping("/calcul-time")
	public void worktimeTable(HttpServletRequest request, @RequestParam(value = "year", required = false) String year,
			@RequestParam(value = "month", required = false) String month, Model model) {

		if (year == null) {
			year = "2022";

		}

		if (month == null) {
			month = "07";
		}

		Map<String, Object> mapInfo = new HashMap<String, Object>();
		HttpSession session = request.getSession();

		int empnum = ((LoginDTO) session.getAttribute("loginMember")).getEmpnum();
		String duration = year + "/" + month;

		mapInfo.put("empnum", empnum);
		mapInfo.put("duration", duration);

		List<WorktimeDTO> worktimeList = geuntaeService.selectWorktimeList(mapInfo);

		System.out.println("worktimeList" + worktimeList);

		model.addAttribute("worktimeList", worktimeList);
		model.addAttribute("year", year);
		model.addAttribute("month", month);

		request.getSession();
	}

//	근무시간 수정요청
	@PostMapping("/calcul-time")
	public String worktimeModifyRequest(@ModelAttribute WorktimeModDTO worktimeModDTO, Model model)
			throws SearchPaymentUserException {

		worktimeModDTO.setRequest_date(new java.sql.Date(System.currentTimeMillis()));
		geuntaeService.insertWorkTimeModRequest(worktimeModDTO);

		return "redirect:calcul-time";

	}

//	근무시간 수정요청 모달창 내 결재자 검색기능
	@GetMapping(value = "/search", produces = "application/json; charset=utf-8")
	@ResponseBody
	public String search(HttpServletResponse response, @RequestParam("search_contents") String search_contents,
			@RequestParam("search_method") String search_method) {
		System.out.println("검색서비스 시작!!");

//		1) 검색조건 가져오기
		Map<String, Object> search_map = new HashMap<String, Object>();
		search_map.put("search_method", search_method);
		search_map.put("search_contents", search_contents);

//		2) 검색결과
		List<paymentUserDTO> userList = geuntaeService.findPno(search_map);
		System.out.println(userList);

//		3) 검색결과 ajax로 전달
		response.setContentType("application/json; charset=utf-8");
		Gson gson = new GsonBuilder().setPrettyPrinting().setFieldNamingPolicy(FieldNamingPolicy.IDENTITY)
				.serializeNulls().disableHtmlEscaping().create();

		return gson.toJson(userList);

	}
	
//	근무시간 수정요청 모달창 내 결재자 검색기능
	@GetMapping(value = "/search_emp", produces = "application/json; charset=utf-8")
	@ResponseBody
	public String searchemp(HttpServletResponse response, @RequestParam("search_contents") String search_contents,
			@RequestParam("search_method") String search_method) {
		

//		1) 검색조건 가져오기
		Map<String, Object> search_map = new HashMap<String, Object>();
		search_map.put("search_method", search_method);
		search_map.put("search_contents", search_contents);

//		2) 검색결과
		List<paymentUserDTO> userList = geuntaeService.findPno(search_map);
		System.out.println(userList);

//		3) 검색결과 ajax로 전달
		response.setContentType("application/json; charset=utf-8");
		Gson gson = new GsonBuilder().setPrettyPrinting().setFieldNamingPolicy(FieldNamingPolicy.IDENTITY)
				.serializeNulls().disableHtmlEscaping().create();

		return gson.toJson(userList);

	}
	

	
	

	@GetMapping("/wait-sign")
	public void manager(Model model, HttpServletRequest request) {
		Map<String, Object> mapInfo = new HashMap<String, Object>();
		HttpSession session = request.getSession();
		int empnum = ((LoginDTO) session.getAttribute("loginMember")).getEmpnum();
		mapInfo.put("empnum", empnum);

		List<WorktimeDTO> requestedModWorkTimeList = geuntaeService.requestedModWorkTimeList(mapInfo);
		List<VacationDTO> requestedVacationList = geuntaeService.requestedVacationList(mapInfo);
		List<VacationDeleteDTO> requestedDeleteVacationList = geuntaeService.requestedDeleteVacationList(mapInfo);
		

		model.addAttribute("requestedModWorkTimeList", requestedModWorkTimeList);
		model.addAttribute("requestedVacationList", requestedVacationList);
		model.addAttribute("requestedDeleteVacationList", requestedDeleteVacationList);
	}

//	근무시간 수정 요청에 대하여 관리자가 승인 또는 반려시 실행
	@PostMapping(value = "/wait-sign/worktime", produces = "text/plain; charset=utf-8")
	@ResponseBody
	public String manager(HttpServletResponse response, @RequestParam("workNum") int workNum,
			@RequestParam("paymentResult") String paymentResult, @RequestParam("start") String start,
			@RequestParam("end") String end) {

		Map<String, Object> payment_work_map = new HashMap<String, Object>();
		// 로그인 구현되면 세션에서 이름 가져오기!!
		payment_work_map.put("manager_real", "1");

		payment_work_map.put("start", start);
		payment_work_map.put("end", end);
		payment_work_map.put("workNum", workNum);
		payment_work_map.put("paymentResult", paymentResult);

		int result = geuntaeService.updatePaymentWork(payment_work_map);

		if (result >= 1) {
			return "성공";
		} else {
			return "실패";
		}

	}
	
//	관리자가 휴가이력 삭제요청에 대하여 승인/반려 시 실행되는 ajax
	@PostMapping(value="/wait-sign/vacationDelete", produces = "text/plain; charset=utf-8")
	@ResponseBody
	public String DeleteVacation(HttpServletResponse response, @RequestParam("vacationNumber") int vacationNumber,
			@RequestParam("paymentResult") String paymentResult,
			@RequestParam("vacationCategory") String vacationCategory,
			HttpServletRequest request) {
		
		Map<String, Object> mapInfo = new HashMap<String, Object>();
		HttpSession session = request.getSession();
		int empnum = ((LoginDTO) session.getAttribute("loginMember")).getEmpnum();
		
		mapInfo.put("empnum", empnum);
		mapInfo.put("paymentResult", paymentResult);
		mapInfo.put("vacationNumber", vacationNumber);
		mapInfo.put("vacationCategory", vacationCategory);
		
		int result = geuntaeService.updatePaymentVacation(mapInfo);
		
		if(result >= 1) {
			return "성공";
		} else {
			return "실패";
		}
		
	}

	@GetMapping("/vacation-table")
	public void vacationTable(HttpServletRequest request, @RequestParam(value = "year", required = false) String year,
			Model model) {
		if (year == null) {
			year = "2022";
		}
		Map<String, Object> mapInfo = new HashMap<String, Object>();
		HttpSession session = request.getSession();
		int empnum = ((LoginDTO) session.getAttribute("loginMember")).getEmpnum();
		mapInfo.put("empnum", empnum);
		mapInfo.put("year", year);

		List<VacationDTO> vacationList = geuntaeService.selectVacationList(mapInfo);

		System.out.println("vacationList" + vacationList);
		model.addAttribute("year", year);
		model.addAttribute("vacationList", vacationList);
	}

	@PostMapping(value = "/vacation-table/deleteRequest", produces = "text/plain; charset=utf-8")
	@ResponseBody
	public String manager(HttpServletResponse response, @RequestParam("search_result_no") int search_result_no,
			@RequestParam("vacNum") int vacNum, @RequestParam("reason") String reason) {
		System.out.println("deleteRequest 테스트");

		
		  Map<String,Object>map_info= new HashMap<String,Object>(); 
		  
		  map_info.put("payment_user_no", search_result_no);
		  map_info.put("vacNum", vacNum);
		  map_info.put("reason", reason);
		  
		int result = geuntaeService.insertRequestDeleteVacation(map_info);

		if (result >= 1) {
			return "성공";
		} else {
			return "실패";
		}

	}

//	휴가에 대하여 관리자가 승인 또는 반려시 실행
	@PostMapping(value = "/wait-sign/vacation", produces = "text/plain; charset=utf-8")
	@ResponseBody
	public String manager(HttpServletResponse response, HttpServletRequest request,
			@RequestParam("vacationNum") int vacationNum, @RequestParam("paymentResult") String paymentResult,
			@RequestParam("vacation_category") String vacation_category, @RequestParam("margin") int margin,
			@RequestParam("request_Empnum") int request_Empnum) {

		Map<String, Object> map_info = new HashMap<String, Object>();
		HttpSession session = request.getSession();
		int paymentNum = ((LoginDTO) session.getAttribute("loginMember")).getEmpnum();

		map_info.put("vacationNum", vacationNum);
		map_info.put("paymentResult", paymentResult);
		map_info.put("vacation_category", vacation_category);
		map_info.put("margin", margin);
		map_info.put("paymentNum", paymentNum);
		map_info.put("request_Empnum", request_Empnum);

		boolean result = geuntaeService.paymentVacation(map_info);
		if (result) {
			return "성공";
		} else {
			return "실패";
		}
	}

	@GetMapping("/vacation-register")
	public void vacationRegister(HttpServletRequest request, Model model) {
		HttpSession session = request.getSession();
		int empnum = ((LoginDTO) session.getAttribute("loginMember")).getEmpnum();

		AnnualVacationDTO annualVacationdto = geuntaeService.selectAnualVacation(empnum);

		model.addAttribute("annualVacationdto", annualVacationdto);
	}

	@PostMapping("/vacation-register")
	public String VacationRegister(HttpServletRequest request, @ModelAttribute VacationDTO vacationDTO) {

		System.out.println("메인 : " + vacationDTO.getManager_main());

		if (!vacationDTO.getManager_main().equals("") || vacationDTO.getManager_main() != null) {
			System.out.println("정상작동");
			HttpSession session = request.getSession();
			int empnum = ((LoginDTO) session.getAttribute("loginMember")).getEmpnum();
			vacationDTO.setEmp_num(empnum);
			vacationDTO.setRequest_date(new java.sql.Date(System.currentTimeMillis()));
			System.out.println("vacationDTO: " + vacationDTO);
			geuntaeService.insertVacation(vacationDTO);

		}

		return "redirect:vacation-register";

	}

	@GetMapping("/vacation-cancel")
	public String cancelVcation(@RequestParam("vac_num") int vac_num) {

		System.out.println("휴가 신청 취소 컨트롤러 작동" + vac_num);
		int result = geuntaeService.cancelVacation(vac_num);
		if (result >= 1) {
			System.out.println("성공!");
		}

		return "redirect:vacation-table";
	}

	@GetMapping("/human-resource")
	public void humanResource() {

	}
	
	@GetMapping(value = "/search_profile", produces = "application/json; charset=utf-8")
	@ResponseBody
	public String manager(@RequestParam("emp_no") int emp_no) {
		System.out.println("테스트");
		
		Map<String,Object>map_info= new HashMap<String,Object>();
	
		map_info.put("emp_no", emp_no);
		ProfileDTO result = geuntaeService.selectProfile(map_info);
		
		System.out.println(result);
		//GSON 객체 생성
		Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();

		//ProfileDTO 객체 ==> JSON문자열
		String profileJson = gson.toJson(result);
		
		System.out.println(profileJson);
		
		return profileJson;
	}
	
	@PostMapping(value = "/annualValuePlus", produces = "text/plain; charset=utf-8")
	@ResponseBody
	public String plusAnnual(@RequestParam("emp_no") int emp_no, @RequestParam("annualValue") int annualValue) {
		
		Map<String,Object>map_info= new HashMap<String,Object>();

		map_info.put("emp_no", emp_no);
		map_info.put("annualValue", annualValue);
		
		int result = geuntaeService.updateAnnual(map_info);
		
		if(result>= 1) {
			return "success";
		} else {
			return"fail";
		}
		
	}
	
	
	@GetMapping(value = "/WorkbyManager", produces = "application/json; charset=utf-8")
	@ResponseBody
	public String WorkbyManager(@RequestParam("emp_no") int emp_no,@RequestParam("Workdate") String Workdate) {
		
		Map<String,Object>map_info= new HashMap<String,Object>();
				
		map_info.put("emp_no", emp_no);
		map_info.put("Workdate", Workdate);
		
		System.out.println("emp_no : " + emp_no);
		System.out.println("Workdate : " + Workdate);
		
		WorktimebyManagerDTO result = geuntaeService.selectWorktimeByManager(map_info);
		System.out.println(result);
		
		//GSON 객체 생성
		Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();

		//DTO 객체 ==> JSON문자열
		String WorkTimeJson = gson.toJson(result);
				
		System.out.println(WorkTimeJson);	
		
		return WorkTimeJson;

	}
	
	
	@PostMapping(value = "/WorkModbyManager", produces = "text/plain; charset=utf-8")
	@ResponseBody
	public String WorkModbyManager(
			@RequestParam("workStart") String workStart,
			@RequestParam("workEnd") String workEnd,
			@RequestParam("emp_no") int emp_no,
			@RequestParam("Workdate") String workdate) {
		
		Map<String,Object>map_info= new HashMap<String,Object>();
		
		map_info.put("workStart", workStart);
		map_info.put("workEnd", workEnd);
		map_info.put("emp_no", emp_no);
		map_info.put("workdate", workdate);

		int result = geuntaeService.updateWorktimeByManager(map_info);

		if(result>=1) {
			return "success";
		} else {
			return "fail";
		}
				
	}
	
	@GetMapping(value = "/selectVacation", produces = "application/json; charset=utf-8")
	@ResponseBody
	public String selectVacation(@RequestParam("emp_no") int emp_no,@RequestParam("date") String date) {
		
		Map<String,Object>map_info= new HashMap<String,Object>();
		
		map_info.put("emp_no", emp_no);
		map_info.put("date", date);
		
		VacationDTO vacation = geuntaeService.selectVacation(map_info);
		
		System.out.println(vacation);
		//GSON 객체 생성
		Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
		
		//DTO 객체 ==> JSON문자열
		String vacationJson = gson.toJson(vacation);
		
		System.out.println(vacationJson);
		return vacationJson;
	}
	
	
	@PostMapping(value = "/deleteVacationManager", produces = "text/plain; charset=utf-8")
	@ResponseBody
	public String deleteVacationManager(@RequestParam("emp_no") int emp_no
			, @RequestParam("vacationDate") Date vacationDate
			, @RequestParam("vac_no") int vac_no
			, @RequestParam("vac_category") String vac_category
			
			) {
		
		System.out.println("emp_no : " + emp_no);
		System.out.println("vacationDate : " + vacationDate);
		System.out.println("vac_no : " + vac_no);
		System.out.println("vac_category : " + vac_category);
		
		Map<String,Object>map_info= new HashMap<String,Object>();
		
		map_info.put("emp_no", emp_no);
		map_info.put("vacationDate", vacationDate);
		map_info.put("vacationNumber", vac_no);
		map_info.put("vac_category", vac_category);
		
		int result = geuntaeService.deleteVacation(map_info);
		
		return "";
	}
	
	
	@GetMapping(value = "/main_worktime", produces = "application/json; charset=utf-8")
	@ResponseBody
	public String mainWorktime(HttpServletRequest request) {
		HttpSession session = request.getSession();
		int empnum = ((LoginDTO) session.getAttribute("loginMember")).getEmpnum();
		
		WorktimeDTO worktime = geuntaeService.selectMainWorktime(empnum);
		
		//GSON 객체 생성
		Gson gson = new Gson();

		//DTO 객체 ==> JSON문자열
		String worktimejson = gson.toJson(worktime);
		
		
		System.out.println(worktime);
		System.out.println(worktimejson);
		
		return worktimejson;
	}
	
	
	@PostMapping(value = "/main_worktimeRegist", produces = "text/plain; charset=utf-8")
	@ResponseBody
	public String mainWorktimeRegist(HttpServletRequest request, @RequestParam("category") String category) {
		HttpSession session = request.getSession();
		int empnum = ((LoginDTO) session.getAttribute("loginMember")).getEmpnum();
		
		System.out.println("category : " + category);
		Map<String, Object> mapInfo = new HashMap<String, Object>();
		mapInfo.put("empnum", empnum);
		mapInfo.put("category", category);
		
		
		int result = geuntaeService.registMainWorktime(mapInfo);
		
		if(result == 1) {
			return "success";
		} else {
			return "fail";
		}
		
	}
	
}
