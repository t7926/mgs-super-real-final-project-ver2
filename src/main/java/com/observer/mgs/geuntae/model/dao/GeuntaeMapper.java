package com.observer.mgs.geuntae.model.dao;

import java.util.List;
import java.util.Map;

import com.observer.mgs.geuntae.model.dto.AnnualVacationDTO;
import com.observer.mgs.geuntae.model.dto.ProfileDTO;
import com.observer.mgs.geuntae.model.dto.VacationDTO;
import com.observer.mgs.geuntae.model.dto.VacationDeleteDTO;
import com.observer.mgs.geuntae.model.dto.WorktimeDTO;
import com.observer.mgs.geuntae.model.dto.WorktimeModDTO;
import com.observer.mgs.geuntae.model.dto.WorktimebyManagerDTO;
import com.observer.mgs.geuntae.model.dto.paymentUserDTO;

public interface GeuntaeMapper {

	List<WorktimeDTO> selectWorktimeList(Map<String, Object> duration);

	List<paymentUserDTO> findPaymentUser(Map<String, Object> search_map);

	int insertWorkTimeModRequest(WorktimeModDTO worktimeModDTO);

	int worktime_ModTF_result(int work_no);

	List<WorktimeDTO> selectModifyRequestList(Map mapInfo);

	int updateWorktime(Map<String, Object> payment_work_map);

	int updateModWorktime(Map<String, Object> payment_work_map);

	void insertVacation(VacationDTO vacationDTO);

	List<VacationDTO> selectVacation(Map<String, Object> mapInfo);

	List<VacationDTO> selectRequestedVacation(Map<String, Object> mapInfo);

	int updateVacationTable(Map<String, Object> map_info);

	int updateAnnualvacation(Map<String, Object> map_info);

	AnnualVacationDTO selectAnualVacation(int empnum);

	int insertAnnualHistory(Map<String, Object> map_info);

	int updateVacationTableforCancel(int vac_num);

	int insertDeleteVacationRequested(Map<String, Object> map_info);

	int updateVacationTableforDelete(Map<String, Object> map_info);

	List<VacationDeleteDTO> selectDeleteVacation(Map<String, Object> mapInfo);

	int updatePaymentVacation(Map<String, Object> mapInfo);


	int updateDeleteVacation(Map<String, Object> mapInfo);

	int updateAnnualvacationTable(Map<String, Object> mapInfo);

	int insertAnnualVacationHistoryTable(Map<String, Object> mapInfo);

	ProfileDTO selectProfile(Map<String, Object> map_info);

	int updateAnnualBymanager(Map<String, Object> map_info);

	int insertAnnualHistoryBymanager(Map<String, Object> map_info);

	WorktimebyManagerDTO selectWorktimeByManager(Map<String, Object> map_info);

	int updateWorktimeByManager(Map<String, Object> map_info);

	VacationDTO selectVacationByManager(Map<String, Object> map_info);

	int updateVacationManager(Map<String, Object> map_info);

	WorktimeDTO selectMainWorktime(int empnum);

	int insertMainWorktime(Map<String, Object> mapInfo);

	int updateMainWorktime(Map<String, Object> mapInfo);











}


