package com.observer.mgs.geuntae.model.dto;

import java.sql.Date;

public class WorktimeDTO {
	
	private Date work_date;
	private String work_start_time;
	private String work_end_time;
	private int work_no;
	private int emp_num;
	private float work_time;
	private String modTF;
	
	private WorktimeModDTO worktimemodify;
	private UserDTO user;
	
	public WorktimeDTO() {
		// TODO Auto-generated constructor stub
	}

	public WorktimeDTO(Date work_date, String work_start_time, String work_end_time, int work_no, int emp_num,
			float work_time, String modTF, WorktimeModDTO worktimemodify, UserDTO user) {
		super();
		this.work_date = work_date;
		this.work_start_time = work_start_time;
		this.work_end_time = work_end_time;
		this.work_no = work_no;
		this.emp_num = emp_num;
		this.work_time = work_time;
		this.modTF = modTF;
		this.worktimemodify = worktimemodify;
		this.user = user;
	}

	public Date getWork_date() {
		return work_date;
	}

	public void setWork_date(Date work_date) {
		this.work_date = work_date;
	}

	public String getWork_start_time() {
		return work_start_time;
	}

	public void setWork_start_time(String work_start_time) {
		this.work_start_time = work_start_time;
	}

	public String getWork_end_time() {
		return work_end_time;
	}

	public void setWork_end_time(String work_end_time) {
		this.work_end_time = work_end_time;
	}

	public int getWork_no() {
		return work_no;
	}

	public void setWork_no(int work_no) {
		this.work_no = work_no;
	}

	public int getEmp_num() {
		return emp_num;
	}

	public void setEmp_num(int emp_num) {
		this.emp_num = emp_num;
	}

	public float getWork_time() {
		return work_time;
	}

	public void setWork_time(float work_time) {
		this.work_time = work_time;
	}

	public String getModTF() {
		return modTF;
	}

	public void setModTF(String modTF) {
		this.modTF = modTF;
	}

	public WorktimeModDTO getWorktimemodify() {
		return worktimemodify;
	}

	public void setWorktimemodify(WorktimeModDTO worktimemodify) {
		this.worktimemodify = worktimemodify;
	}

	public UserDTO getUser() {
		return user;
	}

	public void setUser(UserDTO user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "WorktimeDTO [work_date=" + work_date + ", work_start_time=" + work_start_time + ", work_end_time="
				+ work_end_time + ", work_no=" + work_no + ", emp_num=" + emp_num + ", work_time=" + work_time
				+ ", modTF=" + modTF + ", worktimemodify=" + worktimemodify + ", user=" + user + "]";
	}
	
	
	
	
	
	
	

	
	
	
}
