package com.observer.mgs.geuntae.model.dto;

import java.sql.Date;

public class WorktimebyManagerDTO {
	
	private Date date;
	private int emp_num;
	private String start;
	private String end;
	
	public WorktimebyManagerDTO() {
		// TODO Auto-generated constructor stub
	}

	public WorktimebyManagerDTO(Date date, int emp_num, String start, String end) {
		super();
		this.date = date;
		this.emp_num = emp_num;
		this.start = start;
		this.end = end;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getEmp_num() {
		return emp_num;
	}

	public void setEmp_num(int emp_num) {
		this.emp_num = emp_num;
	}

	public String getStart() {
		return start;
	}

	public void setStart(String start) {
		this.start = start;
	}

	public String getEnd() {
		return end;
	}

	public void setEnd(String end) {
		this.end = end;
	}

	@Override
	public String toString() {
		return "WorktimebyManagerDTO [date=" + date + ", emp_num=" + emp_num + ", start=" + start + ", end=" + end
				+ "]";
	}
	
	

}
