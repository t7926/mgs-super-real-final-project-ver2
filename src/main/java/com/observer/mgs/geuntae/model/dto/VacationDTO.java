package com.observer.mgs.geuntae.model.dto;

import java.sql.Date;

public class VacationDTO {
	
	private String reason;
	private String manager_main;
	private String manager_sub;
	private String category;
	private String manager_real;
	private String showYN;
	private String paymentState;
	private int no;
	private int emp_num;
	private Date end_date;
	private Date start_date;
	private Date payment_date;
	private Date request_date;
	private String deleteYn;
//	서브쿼리 이름 가져오기
	private String manager_name;
	private String emp_name;
	
	public VacationDTO() {
		// TODO Auto-generated constructor stub
	}

	public VacationDTO(String reason, String manager_main, String manager_sub, String category, String manager_real,
			String showYN, String paymentState, int no, int emp_num, Date end_date, Date start_date, Date payment_date,
			Date request_date, String deleteYn, String manager_name, String emp_name) {
		super();
		this.reason = reason;
		this.manager_main = manager_main;
		this.manager_sub = manager_sub;
		this.category = category;
		this.manager_real = manager_real;
		this.showYN = showYN;
		this.paymentState = paymentState;
		this.no = no;
		this.emp_num = emp_num;
		this.end_date = end_date;
		this.start_date = start_date;
		this.payment_date = payment_date;
		this.request_date = request_date;
		this.deleteYn = deleteYn;
		this.manager_name = manager_name;
		this.emp_name = emp_name;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getManager_main() {
		return manager_main;
	}

	public void setManager_main(String manager_main) {
		this.manager_main = manager_main;
	}

	public String getManager_sub() {
		return manager_sub;
	}

	public void setManager_sub(String manager_sub) {
		this.manager_sub = manager_sub;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getManager_real() {
		return manager_real;
	}

	public void setManager_real(String manager_real) {
		this.manager_real = manager_real;
	}

	public String getShowYN() {
		return showYN;
	}

	public void setShowYN(String showYN) {
		this.showYN = showYN;
	}

	public String getPaymentState() {
		return paymentState;
	}

	public void setPaymentState(String paymentState) {
		this.paymentState = paymentState;
	}

	public int getNo() {
		return no;
	}

	public void setNo(int no) {
		this.no = no;
	}

	public int getEmp_num() {
		return emp_num;
	}

	public void setEmp_num(int emp_num) {
		this.emp_num = emp_num;
	}

	public Date getEnd_date() {
		return end_date;
	}

	public void setEnd_date(Date end_date) {
		this.end_date = end_date;
	}

	public Date getStart_date() {
		return start_date;
	}

	public void setStart_date(Date start_date) {
		this.start_date = start_date;
	}

	public Date getPayment_date() {
		return payment_date;
	}

	public void setPayment_date(Date payment_date) {
		this.payment_date = payment_date;
	}

	public Date getRequest_date() {
		return request_date;
	}

	public void setRequest_date(Date request_date) {
		this.request_date = request_date;
	}

	public String getdeleteYn() {
		return deleteYn;
	}

	public void setdeleteYn(String deleteYn) {
		this.deleteYn = deleteYn;
	}

	public String getManager_name() {
		return manager_name;
	}

	public void setManager_name(String manager_name) {
		this.manager_name = manager_name;
	}

	public String getEmp_name() {
		return emp_name;
	}

	public void setEmp_name(String emp_name) {
		this.emp_name = emp_name;
	}

	@Override
	public String toString() {
		return "VacationDTO [reason=" + reason + ", manager_main=" + manager_main + ", manager_sub=" + manager_sub
				+ ", category=" + category + ", manager_real=" + manager_real + ", showYN=" + showYN + ", paymentState="
				+ paymentState + ", no=" + no + ", emp_num=" + emp_num + ", end_date=" + end_date + ", start_date="
				+ start_date + ", payment_date=" + payment_date + ", request_date=" + request_date + ", deleteYn="
				+ deleteYn + ", manager_name=" + manager_name + ", emp_name=" + emp_name + "]";
	}

	

	
	



}


