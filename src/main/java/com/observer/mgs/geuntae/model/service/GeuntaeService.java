package com.observer.mgs.geuntae.model.service;

import java.util.List;
import java.util.Map;

import com.observer.mgs.geuntae.exception.worktime.SearchPaymentUserException;
import com.observer.mgs.geuntae.model.dto.AnnualVacationDTO;
import com.observer.mgs.geuntae.model.dto.ProfileDTO;
import com.observer.mgs.geuntae.model.dto.VacationDTO;
import com.observer.mgs.geuntae.model.dto.VacationDeleteDTO;
import com.observer.mgs.geuntae.model.dto.WorktimeDTO;
import com.observer.mgs.geuntae.model.dto.WorktimeModDTO;
import com.observer.mgs.geuntae.model.dto.WorktimebyManagerDTO;
import com.observer.mgs.geuntae.model.dto.paymentUserDTO;

public interface GeuntaeService {

	List<WorktimeDTO> selectWorktimeList(Map<String, Object> duration);


	List<paymentUserDTO> findPno(Map<String, Object> search_map);

	void insertWorkTimeModRequest(WorktimeModDTO worktimeModDTO) throws SearchPaymentUserException;

	List<WorktimeDTO> requestedModWorkTimeList(Map mapInfo);

	int updatePaymentWork(Map<String, Object> payment_work_map);


	void insertVacation(VacationDTO vacationDTO);


	List<VacationDTO> selectVacationList(Map<String, Object> mapInfo);


	List<VacationDTO> requestedVacationList(Map<String, Object> mapInfo);


	boolean paymentVacation(Map<String, Object> map_info);


	AnnualVacationDTO selectAnualVacation(int empnum);


	int cancelVacation(int vac_num);


	int insertRequestDeleteVacation(Map<String, Object> map_info);


	List<VacationDeleteDTO> requestedDeleteVacationList(Map<String, Object> mapInfo);


	int updatePaymentVacation(Map<String, Object> mapInfo);


	ProfileDTO selectProfile(Map<String, Object> map_info);


	int updateAnnual(Map<String, Object> map_info);


	WorktimebyManagerDTO selectWorktimeByManager(Map<String, Object> map_info);


	int updateWorktimeByManager(Map<String, Object> map_info);


	VacationDTO selectVacation(Map<String, Object> map_info);


	int deleteVacation(Map<String, Object> map_info);


	WorktimeDTO selectMainWorktime(int empnum);
	

	int registMainWorktime(Map<String, Object> mapInfo);





}
