package com.observer.mgs.geuntae.model.dto;

import java.sql.Date;

public class VacationDeleteDTO {
	
	private int vac_no;
	private String payment_state;
	private String reason;
	private String manager_name;
	private Date start_date;
	private Date end_date;
	private String category;
	private String user_name;
	
	public VacationDeleteDTO() {
		// TODO Auto-generated constructor stub
	}

	public VacationDeleteDTO(int vac_no, String payment_state, String reason, String manager_name, Date start_date,
			Date end_date, String category, String user_name) {
		super();
		this.vac_no = vac_no;
		this.payment_state = payment_state;
		this.reason = reason;
		this.manager_name = manager_name;
		this.start_date = start_date;
		this.end_date = end_date;
		this.category = category;
		this.user_name = user_name;
	}

	public int getVac_no() {
		return vac_no;
	}

	public void setVac_no(int vac_no) {
		this.vac_no = vac_no;
	}

	public String getPayment_state() {
		return payment_state;
	}

	public void setPayment_state(String payment_state) {
		this.payment_state = payment_state;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getManager_name() {
		return manager_name;
	}

	public void setManager_name(String manager_name) {
		this.manager_name = manager_name;
	}

	public Date getStart_date() {
		return start_date;
	}

	public void setStart_date(Date start_date) {
		this.start_date = start_date;
	}

	public Date getEnd_date() {
		return end_date;
	}

	public void setEnd_date(Date end_date) {
		this.end_date = end_date;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	@Override
	public String toString() {
		return "VacationDeleteDTO [vac_no=" + vac_no + ", payment_state=" + payment_state + ", reason=" + reason
				+ ", manager_name=" + manager_name + ", start_date=" + start_date + ", end_date=" + end_date
				+ ", category=" + category + ", user_name=" + user_name + "]";
	}
	
	

}
