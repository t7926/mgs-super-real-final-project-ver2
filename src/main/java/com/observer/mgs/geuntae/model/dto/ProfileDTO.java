package com.observer.mgs.geuntae.model.dto;

import java.sql.Date;

public class ProfileDTO {
	
	private String name;
	private Date joinDate;
	private String phone;
	private String bye;
	private String dept;
	private String team;
	private int empNum;
	private int annual_total;
	private int annual_used;

	public ProfileDTO() {
		// TODO Auto-generated constructor stub
	}

	public ProfileDTO(String name, Date joinDate, String phone, String bye, String dept, String team, int empNum,
			int annual_total, int annual_used) {
		super();
		this.name = name;
		this.joinDate = joinDate;
		this.phone = phone;
		this.bye = bye;
		this.dept = dept;
		this.team = team;
		this.empNum = empNum;
		this.annual_total = annual_total;
		this.annual_used = annual_used;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getJoinDate() {
		return joinDate;
	}

	public void setJoinDate(Date joinDate) {
		this.joinDate = joinDate;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getBye() {
		return bye;
	}

	public void setBye(String bye) {
		this.bye = bye;
	}

	public String getDept() {
		return dept;
	}

	public void setDept(String dept) {
		this.dept = dept;
	}

	public String getTeam() {
		return team;
	}

	public void setTeam(String team) {
		this.team = team;
	}

	public int getEmpNum() {
		return empNum;
	}

	public void setEmpNum(int empNum) {
		this.empNum = empNum;
	}

	public int getAnnual_total() {
		return annual_total;
	}

	public void setAnnual_total(int annual_total) {
		this.annual_total = annual_total;
	}

	public int getAnnual_used() {
		return annual_used;
	}

	public void setAnnual_used(int annual_used) {
		this.annual_used = annual_used;
	}

	@Override
	public String toString() {
		return "ProfileDTO [name=" + name + ", joinDate=" + joinDate + ", phone=" + phone + ", bye=" + bye + ", dept="
				+ dept + ", team=" + team + ", empNum=" + empNum + ", annual_total=" + annual_total + ", annual_used="
				+ annual_used + "]";
	}

	
	
	
}
