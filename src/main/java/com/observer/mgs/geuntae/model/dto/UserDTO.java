package com.observer.mgs.geuntae.model.dto;

import java.sql.Date;

public class UserDTO {
	
	private String ads;
	private String mail;
	private String pwd;
	private String phone;
	private String cphone;
	private String name;
	
	private int num;
	private int serial_code;
	private int dept_code;
	private int level_code;
	
	private Date birthday;
	private String bye;
	private String gender;
	private String id;
	
	public UserDTO() {
		// TODO Auto-generated constructor stub
	}

	public UserDTO(String ads, String mail, String pwd, String phone, String cphone, String name, int num,
			int serial_code, int dept_code, int level_code, Date birthday, String bye, String gender, String id) {
		super();
		this.ads = ads;
		this.mail = mail;
		this.pwd = pwd;
		this.phone = phone;
		this.cphone = cphone;
		this.name = name;
		this.num = num;
		this.serial_code = serial_code;
		this.dept_code = dept_code;
		this.level_code = level_code;
		this.birthday = birthday;
		this.bye = bye;
		this.gender = gender;
		this.id = id;
	}

	public String getAds() {
		return ads;
	}

	public void setAds(String ads) {
		this.ads = ads;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCphone() {
		return cphone;
	}

	public void setCphone(String cphone) {
		this.cphone = cphone;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public int getSerial_code() {
		return serial_code;
	}

	public void setSerial_code(int serial_code) {
		this.serial_code = serial_code;
	}

	public int getDept_code() {
		return dept_code;
	}

	public void setDept_code(int dept_code) {
		this.dept_code = dept_code;
	}

	public int getLevel_code() {
		return level_code;
	}

	public void setLevel_code(int level_code) {
		this.level_code = level_code;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getBye() {
		return bye;
	}

	public void setBye(String bye) {
		this.bye = bye;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "UserDTO [ads=" + ads + ", mail=" + mail + ", pwd=" + pwd + ", phone=" + phone + ", cphone=" + cphone
				+ ", name=" + name + ", num=" + num + ", serial_code=" + serial_code + ", dept_code=" + dept_code
				+ ", level_code=" + level_code + ", birthday=" + birthday + ", bye=" + bye + ", gender=" + gender
				+ ", id=" + id + "]";
	}
	
	
	

}
