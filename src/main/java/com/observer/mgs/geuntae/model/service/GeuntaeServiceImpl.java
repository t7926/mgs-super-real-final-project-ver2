package com.observer.mgs.geuntae.model.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.observer.mgs.geuntae.exception.worktime.SearchPaymentUserException;
import com.observer.mgs.geuntae.model.dao.GeuntaeMapper;
import com.observer.mgs.geuntae.model.dto.AnnualVacationDTO;
import com.observer.mgs.geuntae.model.dto.ProfileDTO;
import com.observer.mgs.geuntae.model.dto.VacationDTO;
import com.observer.mgs.geuntae.model.dto.VacationDeleteDTO;
import com.observer.mgs.geuntae.model.dto.WorktimeDTO;
import com.observer.mgs.geuntae.model.dto.WorktimeModDTO;
import com.observer.mgs.geuntae.model.dto.WorktimebyManagerDTO;
import com.observer.mgs.geuntae.model.dto.paymentUserDTO;


@Service
public class GeuntaeServiceImpl implements GeuntaeService {

	private final GeuntaeMapper mapper;
	
	@Autowired
	public GeuntaeServiceImpl(GeuntaeMapper mapper) {
		
		this.mapper = mapper;
	}
	


	/**
	 * <pre>
	 * 	근무조회용 메소드
	 * </pre>
	 */
	@Override
	public List<WorktimeDTO> selectWorktimeList(Map<String, Object> duration) {
		return mapper.selectWorktimeList(duration);

	}






	/**
	 * <pre>
	 * 	결재자 정보 조회용 메소드
	 * <pre>
	 */
	@Override
	public List<paymentUserDTO> findPno(Map<String, Object> search_map) {
		return mapper.findPaymentUser(search_map);
	}



	/**
	 * <pre>
	 * 	근무시간수정요청 정보 테이블에 인서트하는 메소드
	 * </pre>
	 */
	@Override
	public void insertWorkTimeModRequest(WorktimeModDTO worktimeModDTO) throws SearchPaymentUserException {
		int insert_result = 0;
		int worktime_ModTF_result = 0;
		insert_result = mapper.insertWorkTimeModRequest(worktimeModDTO);
		
		if(insert_result == 1) {
			worktime_ModTF_result = mapper.worktime_ModTF_result(worktimeModDTO.getWork_no());
		}

		
		if(insert_result == 1 && worktime_ModTF_result ==1) {
			System.out.println("성공");
		} else {
			System.out.println("실패");
			throw new SearchPaymentUserException("근무시간 수정 요청에 실패하였습니다.");
		}
	}



	/**
	 * <pre>
	 * 	관리자의 결재대기함에서 수정요청 받은 근무시간 목록을 보여주는 메소드
	 * </pre>
	 */
	@Override
	public List<WorktimeDTO> requestedModWorkTimeList(Map mapInfo) {
		mapInfo.put("empnum", mapInfo.get("empnum").toString());
		return mapper.selectModifyRequestList(mapInfo);
	}



	/**
	 * <pre>
	 * 	관리자가 근무시간 수정 요청을 받은 뒤 승인/반려 를 선택하면 실행되는 메소드!
	 *</pre>
	 * @return 
	 */
	@Override
	public int updatePaymentWork(Map<String, Object> payment_work_map) {
		//'승인'이면 출퇴근시간 수정!
		int result = 0;
				
		if(payment_work_map.get("paymentResult").equals("승인")) {
//			승인시
			result = mapper.updateWorktime(payment_work_map);
			if(result >= 1) {
				result = mapper.updateModWorktime(payment_work_map); // 
			}

		} else {
//			반려시
			result = mapper.updateModWorktime(payment_work_map);
		}
		
		return result;
		
		
	}



	/**
	 * <pre>
	 * 	휴가신청 인서트
	 * <pre>
	 */
	@Override
	public void insertVacation(VacationDTO vacationDTO) {
		mapper.insertVacation(vacationDTO);
		
	}



	/**
	 * <pre>
	 *  휴가조회
	 * </pre>
	 */
	@Override
	public List<VacationDTO> selectVacationList(Map<String, Object> mapInfo) {
		return mapper.selectVacation(mapInfo);
	}



	/**
	 * <pre>
	 * 	관리자의 결재대기함에서 휴가요청을 받은 휴가 목록을 보여주는 메소드
	 * </pre>
	 */
	@Override
	public List<VacationDTO> requestedVacationList(Map<String, Object> mapInfo) {
		return mapper.selectRequestedVacation(mapInfo);
	}



	/**
	 * <pre>
	 * 	휴가결재 후 실행되는 메소드 updateAnnualvacation
	 * </pre>
	 */
	@Override
	public boolean paymentVacation(Map<String, Object> map_info) {
		int result = 0;
		int result2 = 0;
		int result3 = 0;
		//휴가상태표에 승인/반려 업데이트
		result = mapper.updateVacationTable(map_info);
		
		if(result == 1) {
			
			//승인된 휴가가 연차일 경우 연차관리 테이블에 반영
			if(map_info.get("paymentResult").equals("승인") && map_info.get("vacation_category").equals("연차")) {
				
				result2 = mapper.updateAnnualvacation(map_info); //연차 테이블
				
				if(result2 == 1) {
					result3 = mapper.insertAnnualHistory(map_info); //연차 이력테이블
					System.out.println("실행완료!!");

				}
			

			}
			
		}
		
		if(result == 1 && result2 == 1 && result3 ==1) {
			return true;
		} else {
			return false;
		}
	
		

		
		
		
	}



	/**
	 * <pre>
	 * 	연차를 조회하는 메소드
	 * </pre>
	 */
	@Override
	public AnnualVacationDTO selectAnualVacation(int empnum) {
		return mapper.selectAnualVacation(empnum);
		
	}



	/**
	 * <pre>
	 * 	휴가신청을 취소하는 메소드
	 * </pre>
	 */
	@Override
	public int cancelVacation(int vac_num) {
		return mapper.updateVacationTableforCancel(vac_num);
	}



	/**
	 * <pre>
	 * 	휴가 삭제요청을 하면 실행되는 메소드
	 * </pre>
	 */
	@Override
	public int insertRequestDeleteVacation(Map<String, Object> map_info) {
		
		int result = 0;
		int result2 = 0;
		
		 result = mapper.insertDeleteVacationRequested(map_info);
		if(result >= 1) {
			result2 = mapper.updateVacationTableforDelete(map_info);

		}

		if(result >=1 && result2 >=1) {
			return 1;
		} else return 0;
		

	}



	/**
	 * <pre>
	 * 	결재대기함에서 휴가삭제요청을 볼 수 있는 메소드
	 * </pre>
	 */
	@Override
	public List<VacationDeleteDTO> requestedDeleteVacationList(Map<String, Object> mapInfo) {
		return mapper.selectDeleteVacation(mapInfo);
	}



	/**
	 * <pre>
	 * 	관리자가 결재함에서 휴가이력 삭제 요청에 대하여 승인/반려시 실행되는 메소드.
	 * </pre>
	 */
	@Override
	public int updatePaymentVacation(Map<String, Object> mapInfo) {
//		승인or반려 시 이력테이블에 남기기		
		int resultforVacDeleteTable = mapper.updateDeleteVacation(mapInfo);

//		삭제요청 승인시 
		if(mapInfo.get("paymentResult").equals("승인")){
//			휴가테이블에서 안보이게 하기
			int resultforVacationTable = mapper.updatePaymentVacation(mapInfo);
			
//			승인한 휴가가 연차일 때
			if(mapInfo.get("vacationCategory").equals("연차")) {
//				연차관리 테이블에 반영하기
				int resultforAnnualVacation  = mapper.updateAnnualvacationTable(mapInfo);
//				연차관리 이력테이블에 반영하기
				int resultforHistoryAnnual = mapper.insertAnnualVacationHistoryTable(mapInfo);
				
			}
			
//			연차일 때에
			return resultforVacationTable;
		} else {
			return 0;
		}
		
	}



	/**
	 * <pre>
	 * 회원 프로필을 조회하는 메소드
	 * </pre>
	 */
	@Override
	public ProfileDTO selectProfile(Map<String, Object> map_info) {
		return mapper.selectProfile(map_info);
	}



	/**
	 * <pre>
	 * 	관리자가 직접 연차를 관리하는 메소드
	 * </pre>
	 */
	@Override
	public int updateAnnual(Map<String, Object> map_info) {
		int result1 =  mapper.updateAnnualBymanager(map_info);
		int result2 = mapper.insertAnnualHistoryBymanager(map_info);
				
		if(result1>=1 && result2>=1) {
			return 1;
		} else {
			return 0;
		}
	}



	/**
	 * <pre>
	 * 	인사관리에서 출퇴근 시간을 조회하는 메소드
	 * </pre>
	 */
	@Override
	public WorktimebyManagerDTO selectWorktimeByManager(Map<String, Object> map_info) {
		return mapper.selectWorktimeByManager(map_info);
	}



	/**
	 * <pre>
	 * 출퇴근 시간을 관리자 직권으로 수정하는 메소드
	 * </pre>
	 */
	@Override
	public int updateWorktimeByManager(Map<String, Object> map_info) {
		return mapper.updateWorktimeByManager(map_info);
	}



	/**
	 * <pre>
	 * 관리자 직권으로 휴가이력을 조회하는 메소드
	 * </pre>
	 */
	@Override
	public VacationDTO selectVacation(Map<String, Object> map_info) {
		
		return mapper.selectVacationByManager(map_info);
		

	}



	/**
	 * <pre>
	 *  관리자가 직권으로 휴가이력을 삭제할 수 있는 메소드
	 * </pre>
	 */
	@Override
	public int deleteVacation(Map<String, Object> map_info) {
		
		int result = 0;
		int result2 = 0;
		
//		휴가테이블에 삭제YN을 Y로 바꾸기
		 result = mapper.updateVacationManager(map_info);
		 
		 if(result>=1) {
//				삭제한 휴가의 종류가 연차일 때
				if(map_info.get("vac_category").equals("연차")) {
					result2  = mapper.updateAnnualvacationTable(map_info);
				}
		 }

		 if(result>=1 && result2>=1) {
			 return 1;
		 } else {
			 return 0;
		 }
	}



	/**
	 * <pre>
	 *  메인페이지 접속 시 오늘날짜의 출퇴근 기록을 가져오는 메소드
	 * </pre>
	 */
	@Override
	public WorktimeDTO selectMainWorktime(int empnum) {
		return mapper.selectMainWorktime(empnum);
	}


	/**
	 * <pre>
	 * 	메인페이지에서 출근 또는 퇴근 버튼을 누르면 실행되는 메소드
	 * </pre>
	 */
	@Override
	public int registMainWorktime(Map<String, Object> mapInfo) {
		int result = 0;
		int result2 = 0;
		
//		출근버튼시 인서트
		if(mapInfo.get("category").equals("start")) {
			result = mapper.insertMainWorktime(mapInfo);
		}
		
//		퇴근버튼시 업데이트
		 if(mapInfo.get("category").equals("end")) {
			 result2 = mapper.updateMainWorktime(mapInfo);
		  }
		 
//		 둘중에 하나만 실행되기 때문에
		 if(result >= 1 || result2 >= 1) {

			 return 1;

		 } else {
//			 둘다 실패시
			 return 0;
		 
		 }
		
		
	}


	











	

}
