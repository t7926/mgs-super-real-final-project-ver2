package com.observer.mgs.geuntae.model.dto;

import java.sql.Date;

public class WorktimeModDTO {
	
	private String time_start;
	private String time_end;
	private String reason;
	private String manager_main;
	private String manager_sub;
	private String manager_real;
	private Date payment_date;
	private Date request_date;
	private int work_no;
	
	public WorktimeModDTO() {
		// TODO Auto-generated constructor stub
	}

	public WorktimeModDTO(String time_start, String time_end, String reason, String manager_main, String manager_sub,
			String manager_real, Date payment_date, Date request_date, int work_no) {
		super();
		this.time_start = time_start;
		this.time_end = time_end;
		this.reason = reason;
		this.manager_main = manager_main;
		this.manager_sub = manager_sub;
		this.manager_real = manager_real;
		this.payment_date = payment_date;
		this.request_date = request_date;
		this.work_no = work_no;
	}

	public String getTime_start() {
		return time_start;
	}

	public void setTime_start(String time_start) {
		this.time_start = time_start;
	}

	public String getTime_end() {
		return time_end;
	}

	public void setTime_end(String time_end) {
		this.time_end = time_end;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getManager_main() {
		return manager_main;
	}

	public void setManager_main(String manager_main) {
		this.manager_main = manager_main;
	}

	public String getManager_sub() {
		return manager_sub;
	}

	public void setManager_sub(String manager_sub) {
		this.manager_sub = manager_sub;
	}

	public String getManager_real() {
		return manager_real;
	}

	public void setManager_real(String manager_real) {
		this.manager_real = manager_real;
	}

	public Date getPayment_date() {
		return payment_date;
	}

	public void setPayment_date(Date payment_date) {
		this.payment_date = payment_date;
	}

	public Date getRequest_date() {
		return request_date;
	}

	public void setRequest_date(Date request_date) {
		this.request_date = request_date;
	}

	public int getWork_no() {
		return work_no;
	}

	public void setWork_no(int work_no) {
		this.work_no = work_no;
	}

	@Override
	public String toString() {
		return "WorktimeModDTO [time_start=" + time_start + ", time_end=" + time_end + ", reason=" + reason
				+ ", manager_main=" + manager_main + ", manager_sub=" + manager_sub + ", manager_real=" + manager_real
				+ ", payment_date=" + payment_date + ", request_date=" + request_date + ", work_no=" + work_no + "]";
	}
	
	

}
