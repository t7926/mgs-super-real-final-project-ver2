package com.observer.mgs.geuntae.model.dto;

public class paymentUserDTO {
	
	private String name;
	private String dept;
	private String team;
	private String lev;
	private int no;
	
	public paymentUserDTO() {
	}

	public paymentUserDTO(String name, String dept, String team, String lev, int no) {
		super();
		this.name = name;
		this.dept = dept;
		this.team = team;
		this.lev = lev;
		this.no = no;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDept() {
		return dept;
	}

	public void setDept(String dept) {
		this.dept = dept;
	}

	public String getTeam() {
		return team;
	}

	public void setTeam(String team) {
		this.team = team;
	}

	public String getLev() {
		return lev;
	}

	public void setLev(String lev) {
		this.lev = lev;
	}

	public int getNo() {
		return no;
	}

	public void setNo(int no) {
		this.no = no;
	}

	@Override
	public String toString() {
		return "PaymentUserDTO [name=" + name + ", dept=" + dept + ", team=" + team + ", lev=" + lev + ", no=" + no
				+ "]";
	}
	
	

	

}
