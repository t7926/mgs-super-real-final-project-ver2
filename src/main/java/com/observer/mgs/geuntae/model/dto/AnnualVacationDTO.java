package com.observer.mgs.geuntae.model.dto;

import java.sql.Date;

public class AnnualVacationDTO {
	
	private int annual_sum;
	private int annual_used;
	private Date last_vacation;
	
	public AnnualVacationDTO() {
		// TODO Auto-generated constructor stub
	}

	public AnnualVacationDTO(int annual_sum, int annual_used, Date last_vacation) {
		super();
		this.annual_sum = annual_sum;
		this.annual_used = annual_used;
		this.last_vacation = last_vacation;
	}

	public int getAnnual_sum() {
		return annual_sum;
	}

	public void setAnnual_sum(int annual_sum) {
		this.annual_sum = annual_sum;
	}

	public int getAnnual_used() {
		return annual_used;
	}

	public void setAnnual_used(int annual_used) {
		this.annual_used = annual_used;
	}

	public Date getLast_vacation() {
		return last_vacation;
	}

	public void setLast_vacation(Date last_vacation) {
		this.last_vacation = last_vacation;
	}

	@Override
	public String toString() {
		return "AnnualVacationDTO [annual_sum=" + annual_sum + ", annual_used=" + annual_used + ", last_vacation="
				+ last_vacation + "]";
	}
	
	
	

}
