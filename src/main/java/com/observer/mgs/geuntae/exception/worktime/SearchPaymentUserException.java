package com.observer.mgs.geuntae.exception.worktime;

public class SearchPaymentUserException extends Exception{
	
	public SearchPaymentUserException(String msg) {
		super(msg);
	}

}
