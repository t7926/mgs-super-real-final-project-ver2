package com.observer.mgs.payment.model.service;

import com.observer.mgs.payment.model.dao.PaymentMapper;
import com.observer.mgs.payment.model.dto.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PaymentServiceImpl implements PaymentService{

    private final PaymentMapper mapper;

    @Autowired
    public PaymentServiceImpl(PaymentMapper mapper) {
        this.mapper = mapper;
    }

    @Override
    public int approval(ApprovalDTO approvalDTO) { return mapper.approval(approvalDTO);}

    @Override
    public int sendApproval() { return mapper.sendApproval(); }

    @Override
    public List<ManagerDTO> sendManager() {
        return mapper.sendManager();
    }

    @Override
    public int cash(CashDTO cashDTO, List<CashUnitDTO> list) {

        int result1 = mapper.cash(cashDTO);
        int result2 = 0;

        if(result1 > 0) {

            for(int i = 0; i < list.size(); i++) {
                result2 += mapper.insertCash(list.get(i));
            }
        }

        return result1;
    }

    @Override
    public int draft(DraftDTO draftDTO) {
        return mapper.draft(draftDTO);
    }

    @Override
    public int report(ReportDTO reportDTO) {
        return mapper.report(reportDTO);
    }

    @Override
    public int orderProposal(ProposalDTO proposalDTO, List<ProductDTO> list) {

        int result1 = mapper.orderProposal(proposalDTO);
        int result2 = 0;

        if(result1 > 0) {

            for(int i = 0; i < list.size(); i++) {
                result2 += mapper.insertProduct(list.get(i));
            }
        }
        return result1;
    }

    @Override
    public int history(HistoryDTO history) {
        return mapper.history(history);
    }

    @Override
    public int handWriter(HandWriterDTO handWriter) {
        return mapper.handWriter(handWriter);
    }
}
