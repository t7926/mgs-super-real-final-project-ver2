package com.observer.mgs.payment.model.dto;

public class CashUnitDTO {

    private int refEmpNum;
    private String pmtBriefs;
    private String pmtAmount;
    private String pmtNote;
    
    public CashUnitDTO() {}
    
    public CashUnitDTO(int refEmpNum, String pmtBriefs, String pmtAmount, String pmtNote) {
		super();
		this.refEmpNum = refEmpNum;
		this.pmtBriefs = pmtBriefs;
		this.pmtAmount = pmtAmount;
		this.pmtNote = pmtNote;
	}

	public int getRefEmpNum() {
		return refEmpNum;
	}

	public void setRefEmpNum(int refEmpNum) {
		this.refEmpNum = refEmpNum;
	}

	public String getPmtBriefs() {
		return pmtBriefs;
	}

	public void setPmtBriefs(String pmtBriefs) {
		this.pmtBriefs = pmtBriefs;
	}

	public String getPmtAmount() {
		return pmtAmount;
	}

	public void setPmtAmount(String pmtAmount) {
		this.pmtAmount = pmtAmount;
	}

	public String getPmtNote() {
		return pmtNote;
	}

	public void setPmtNote(String pmtNote) {
		this.pmtNote = pmtNote;
	}

	@Override
    public String toString() {
        return "CashUnitDTO{" +
                "refEmpNum='" + refEmpNum + '\'' +
                ", pmtBriefs='" + pmtBriefs + '\'' +
                ", pmtAmount='" + pmtAmount + '\'' +
                ", pmtNote='" + pmtNote +
                '}';
    }
}
