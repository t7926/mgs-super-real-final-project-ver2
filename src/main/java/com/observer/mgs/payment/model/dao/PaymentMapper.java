package com.observer.mgs.payment.model.dao;

import com.observer.mgs.payment.model.dto.*;

import java.util.List;

public interface PaymentMapper {

    int approval(ApprovalDTO approvalDTO);

    List<ManagerDTO> sendManager();

    int sendApproval();

    int cash(CashDTO cashDTO);

    int insertCash(CashUnitDTO cashUnitDTO);

    int draft(DraftDTO draftDTO);

    int report(ReportDTO reportDTO);

    int orderProposal(ProposalDTO proposalDTO);

    int insertProduct(ProductDTO productDTO);

    int history(HistoryDTO history);

    int handWriter(HandWriterDTO handWriter);
}
