package com.observer.mgs.payment.model.dto;

import java.sql.Date;

public class ReportDTO {

    private int empNum;
    private Date pmtDate;
    private String pmtManager;
    private int pmtNo;
    private Date pmtReportDate;
    private String pmtPosition;
    private String pmtName;
    private String pmtTitle;
    private String pmt1Week;
    private String pmt2Week;
    private String pmt3Week;
    private String pmt4Week;
    private String pmtNote;
    private String pmtOpinion;

    public ReportDTO() {}

    public ReportDTO(int empNum, Date pmtDate, String pmtManager, int pmtNo, Date pmtReportDate, String pmtPosition, String pmtName, String pmtTitle, String pmt1Week, String pmt2Week, String pmt3Week, String pmt4Week, String pmtNote, String pmtOpinion) {
        this.empNum = empNum;
        this.pmtDate = pmtDate;
        this.pmtManager = pmtManager;
        this.pmtNo = pmtNo;
        this.pmtReportDate = pmtReportDate;
        this.pmtPosition = pmtPosition;
        this.pmtName = pmtName;
        this.pmtTitle = pmtTitle;
        this.pmt1Week = pmt1Week;
        this.pmt2Week = pmt2Week;
        this.pmt3Week = pmt3Week;
        this.pmt4Week = pmt4Week;
        this.pmtNote = pmtNote;
        this.pmtOpinion = pmtOpinion;
    }

    public int getEmpNum() {
        return empNum;
    }

    public void setEmpNum(int empNum) {
        this.empNum = empNum;
    }

    public Date getPmtDate() {
        return pmtDate;
    }

    public void setPmtDate(Date pmtDate) {
        this.pmtDate = pmtDate;
    }

    public String getPmtManager() {
        return pmtManager;
    }

    public void setPmtManager(String pmtManager) {
        this.pmtManager = pmtManager;
    }

    public int getPmtNo() {
        return pmtNo;
    }

    public void setPmtNo(int pmtNo) {
        this.pmtNo = pmtNo;
    }

    public Date getPmtReportDate() {
        return pmtReportDate;
    }

    public void setPmtReportDate(Date pmtReportDate) {
        this.pmtReportDate = pmtReportDate;
    }

    public String getPmtPosition() {
        return pmtPosition;
    }

    public void setPmtPosition(String pmtPosition) {
        this.pmtPosition = pmtPosition;
    }

    public String getPmtName() {
        return pmtName;
    }

    public void setPmtName(String pmtName) {
        this.pmtName = pmtName;
    }

    public String getPmtTitle() {
        return pmtTitle;
    }

    public void setPmtTitle(String pmtTitle) {
        this.pmtTitle = pmtTitle;
    }

    public String getPmt1Week() {
        return pmt1Week;
    }

    public void setPmt1Week(String pmt1Week) {
        this.pmt1Week = pmt1Week;
    }

    public String getPmt2Week() {
        return pmt2Week;
    }

    public void setPmt2Week(String pmt2Week) {
        this.pmt2Week = pmt2Week;
    }

    public String getPmt3Week() {
        return pmt3Week;
    }

    public void setPmt3Week(String pmt3Week) {
        this.pmt3Week = pmt3Week;
    }

    public String getPmt4Week() {
        return pmt4Week;
    }

    public void setPmt4Week(String pmt4Week) {
        this.pmt4Week = pmt4Week;
    }

    public String getPmtNote() {
        return pmtNote;
    }

    public void setPmtNote(String pmtNote) {
        this.pmtNote = pmtNote;
    }

    public String getPmtOpinion() {
        return pmtOpinion;
    }

    public void setPmtOpinion(String pmtOpinion) {
        this.pmtOpinion = pmtOpinion;
    }

    @Override
    public String toString() {
        return "ReportDTO{" +
                "empNum=" + empNum +
                ", pmtDate=" + pmtDate +
                ", pmtManager='" + pmtManager + '\'' +
                ", pmtNo=" + pmtNo +
                ", pmtReportDate=" + pmtReportDate +
                ", pmtPosition='" + pmtPosition + '\'' +
                ", pmtName='" + pmtName + '\'' +
                ", pmtTitle='" + pmtTitle + '\'' +
                ", pmt1Week='" + pmt1Week + '\'' +
                ", pmt2Week='" + pmt2Week + '\'' +
                ", pmt3Week='" + pmt3Week + '\'' +
                ", pmt4Week='" + pmt4Week + '\'' +
                ", pmtNote='" + pmtNote + '\'' +
                ", pmtOpinion='" + pmtOpinion + '\'' +
                '}';
    }
}
