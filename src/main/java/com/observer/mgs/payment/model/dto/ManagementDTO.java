package com.observer.mgs.payment.model.dto;

import java.sql.Date;

public class ManagementDTO {

    private int empNum;
    private int pmtNo;
    private String pmtTitle;
    private Date pmtDate;
    private String pmtType;
    private String pmtManager;

    public ManagementDTO() {}

    public ManagementDTO(int empNum, int pmtNo, String pmtTitle, Date pmtDate, String pmtType, String pmtManager) {
        this.empNum = empNum;
        this.pmtNo = pmtNo;
        this.pmtTitle = pmtTitle;
        this.pmtDate = pmtDate;
        this.pmtType = pmtType;
        this.pmtManager = pmtManager;
    }

    public int getEmpNum() {
        return empNum;
    }

    public void setEmpNum(int empNum) {
        this.empNum = empNum;
    }

    public int getPmtNo() {
        return pmtNo;
    }

    public void setPmtNo(int pmtNo) {
        this.pmtNo = pmtNo;
    }

    public String getPmtTitle() {
        return pmtTitle;
    }

    public void setPmtTitle(String pmtTitle) {
        this.pmtTitle = pmtTitle;
    }

    public Date getPmtDate() {
        return pmtDate;
    }

    public void setPmtDate(Date pmtDate) {
        this.pmtDate = pmtDate;
    }

    public String getPmtType() {
        return pmtType;
    }

    public void setPmtType(String pmtType) {
        this.pmtType = pmtType;
    }

    public String getPmtManager() {
        return pmtManager;
    }

    public void setPmtManager(String pmtManager) {
        this.pmtManager = pmtManager;
    }

    @Override
    public String toString() {
        return "ManagementDTO{" +
                "empNum=" + empNum +
                ", pmtNo=" + pmtNo +
                ", pmtTitle='" + pmtTitle + '\'' +
                ", pmtDate=" + pmtDate +
                ", pmtType='" + pmtType + '\'' +
                ", pmtManager='" + pmtManager + '\'' +
                '}';
    }
}
