package com.observer.mgs.payment.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.sql.Date;

public class CashDTO {

    private int empNum;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "Asia/Seoul")
    private java.sql.Date pmtDate;
    private String pmtManager;
    private String pmtName;
    private String pmtDepartment;
    private String pmtPosition;
    private String pmtExpenditure;
    private String pmtTitle;
    private int pmtNo;

    public CashDTO() {}

    public CashDTO(int empNum, Date pmtDate, String pmtManager, String pmtName, String pmtDepartment, String pmtPosition, String pmtExpenditure, String pmtTitle, int pmtNo) {
        this.empNum = empNum;
        this.pmtDate = pmtDate;
        this.pmtManager = pmtManager;
        this.pmtName = pmtName;
        this.pmtDepartment = pmtDepartment;
        this.pmtPosition = pmtPosition;
        this.pmtExpenditure = pmtExpenditure;
        this.pmtTitle = pmtTitle;
        this.pmtNo = pmtNo;
    }

    public int getEmpNum() {
        return empNum;
    }

    public void setEmpNum(int empNum) {
        this.empNum = empNum;
    }

    public Date getPmtDate() {
        return pmtDate;
    }

    public void setPmtDate(Date pmtDate) {
        this.pmtDate = pmtDate;
    }

    public String getPmtManager() {
        return pmtManager;
    }

    public void setPmtManager(String pmtManager) {
        this.pmtManager = pmtManager;
    }

    public String getPmtName() {
        return pmtName;
    }

    public void setPmtName(String pmtName) {
        this.pmtName = pmtName;
    }

    public String getPmtDepartment() {
        return pmtDepartment;
    }

    public void setPmtDepartment(String pmtDepartment) {
        this.pmtDepartment = pmtDepartment;
    }

    public String getPmtPosition() {
        return pmtPosition;
    }

    public void setPmtPosition(String pmtPosition) {
        this.pmtPosition = pmtPosition;
    }

    public String getPmtExpenditure() {
        return pmtExpenditure;
    }

    public void setPmtExpenditure(String pmtExpenditure) {
        this.pmtExpenditure = pmtExpenditure;
    }

    public String getPmtTitle() {
        return pmtTitle;
    }

    public void setPmtTitle(String pmtTitle) {
        this.pmtTitle = pmtTitle;
    }

    public int getPmtNo() {
        return pmtNo;
    }

    public void setPmtNo(int pmtNo) {
        this.pmtNo = pmtNo;
    }

    @Override
    public String toString() {
        return "CashDTO{" +
                "empNum=" + empNum +
                ", pmtDate=" + pmtDate +
                ", pmtManager='" + pmtManager + '\'' +
                ", pmtName='" + pmtName + '\'' +
                ", pmtDepartment='" + pmtDepartment + '\'' +
                ", pmtPosition='" + pmtPosition + '\'' +
                ", pmtExpenditure='" + pmtExpenditure + '\'' +
                ", pmtTitle='" + pmtTitle + '\'' +
                ", pmtNo=" + pmtNo +
                '}';
    }
}
