package com.observer.mgs.payment.model.dto;

public class ManagerDTO {

    private String empName;
    private String levName;
    private String depName;
    private String teamName;

    public ManagerDTO() {}

    public ManagerDTO(String empName, String levName, String depName, String teamName) {
        this.empName = empName;
        this.levName = levName;
        this.depName = depName;
        this.teamName = teamName;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public String getLevName() {
        return levName;
    }

    public void setLevName(String levName) {
        this.levName = levName;
    }

    public String getDepName() {
        return depName;
    }

    public void setDepName(String depName) {
        this.depName = depName;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    @Override
    public String toString() {
        return "ManagerDTO{" +
                "empName='" + empName + '\'' +
                ", levName='" + levName + '\'' +
                ", depName='" + depName + '\'' +
                ", teamName='" + teamName + '\'' +
                '}';
    }
}
