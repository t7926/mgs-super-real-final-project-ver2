package com.observer.mgs.payment.model.dto;

import java.sql.Date;

public class HistoryDTO {

    private int pmtNo;
    private String pmtTitle;
    private String pmtName;
    private Date pmtDate;
    private String pmtType;
    private int empNum;

    public HistoryDTO() {}

    public HistoryDTO(int pmtNo, String pmtTitle, String pmtName, Date pmtDate, String pmtType, int empNum) {
        this.pmtNo = pmtNo;
        this.pmtTitle = pmtTitle;
        this.pmtName = pmtName;
        this.pmtDate = pmtDate;
        this.pmtType = pmtType;
        this.empNum = empNum;
    }

    public int getPmtNo() {
        return pmtNo;
    }

    public void setPmtNo(int pmtNo) {
        this.pmtNo = pmtNo;
    }

    public String getPmtTitle() {
        return pmtTitle;
    }

    public void setPmtTitle(String pmtTitle) {
        this.pmtTitle = pmtTitle;
    }

    public String getPmtName() {
        return pmtName;
    }

    public void setPmtName(String pmtName) {
        this.pmtName = pmtName;
    }

    public Date getPmtDate() {
        return pmtDate;
    }

    public void setPmtDate(Date pmtDate) {
        this.pmtDate = pmtDate;
    }

    public String getPmtType() {
        return pmtType;
    }

    public void setPmtType(String pmtType) {
        this.pmtType = pmtType;
    }

    public int getEmpNum() {
        return empNum;
    }

    public void setEmpNum(int empNum) {
        this.empNum = empNum;
    }

    @Override
    public String toString() {
        return "HistoryDTO{" +
                "pmtNo=" + pmtNo +
                ", pmtTitle='" + pmtTitle + '\'' +
                ", pmtName='" + pmtName + '\'' +
                ", pmtDate=" + pmtDate +
                ", pmtType='" + pmtType + '\'' +
                ", empNum=" + empNum +
                '}';
    }
}
