package com.observer.mgs.payment.model.dto;

import java.sql.Date;

public class ApprovalDTO {

    private int empNum;
    private Date pmtDate;
    private String pmtManager;
    private Date pmtDrafting;
    private String pmtPosition;
    private String pmtName;
    private String pmtTitle;
    private String pmtContents;
    private String pmtAddOn1;
    private String pmtAddOn2;
    private String pmtAddOn3;
    private String pmtInstruction;
    private int pmtNo;

    public ApprovalDTO() {}

    public ApprovalDTO(int empNum, Date pmtDate, String pmtManager, Date pmtDrafting, String pmtPosition, String pmtName, String pmtTitle, String pmtContents, String pmtAddOn1, String pmtAddOn2, String pmtAddOn3, String pmtInstruction, int pmtNo) {
        this.empNum = empNum;
        this.pmtDate = pmtDate;
        this.pmtManager = pmtManager;
        this.pmtDrafting = pmtDrafting;
        this.pmtPosition = pmtPosition;
        this.pmtName = pmtName;
        this.pmtTitle = pmtTitle;
        this.pmtContents = pmtContents;
        this.pmtAddOn1 = pmtAddOn1;
        this.pmtAddOn2 = pmtAddOn2;
        this.pmtAddOn3 = pmtAddOn3;
        this.pmtInstruction = pmtInstruction;
        this.pmtNo = pmtNo;
    }

    public int getEmpNum() {
        return empNum;
    }

    public void setEmpNum(int empNum) {
        this.empNum = empNum;
    }

    public Date getPmtDate() {
        return pmtDate;
    }

    public void setPmtDate(Date pmtDate) {
        this.pmtDate = pmtDate;
    }

    public String getPmtManager() {
        return pmtManager;
    }

    public void setPmtManager(String pmtManager) {
        this.pmtManager = pmtManager;
    }

    public Date getPmtDrafting() {
        return pmtDrafting;
    }

    public void setPmtDrafting(Date pmtDrafting) {
        this.pmtDrafting = pmtDrafting;
    }

    public String getPmtPosition() {
        return pmtPosition;
    }

    public void setPmtPosition(String pmtPosition) {
        this.pmtPosition = pmtPosition;
    }

    public String getPmtName() {
        return pmtName;
    }

    public void setPmtName(String pmtName) {
        this.pmtName = pmtName;
    }

    public String getPmtTitle() {
        return pmtTitle;
    }

    public void setPmtTitle(String pmtTitle) {
        this.pmtTitle = pmtTitle;
    }

    public String getPmtContents() {
        return pmtContents;
    }

    public void setPmtContents(String pmtContents) {
        this.pmtContents = pmtContents;
    }

    public String getPmtAddOn1() {
        return pmtAddOn1;
    }

    public void setPmtAddOn1(String pmtAddOn1) {
        this.pmtAddOn1 = pmtAddOn1;
    }

    public String getPmtAddOn2() {
        return pmtAddOn2;
    }

    public void setPmtAddOn2(String pmtAddOn2) {
        this.pmtAddOn2 = pmtAddOn2;
    }

    public String getPmtAddOn3() {
        return pmtAddOn3;
    }

    public void setPmtAddOn3(String pmtAddOn3) {
        this.pmtAddOn3 = pmtAddOn3;
    }

    public String getPmtInstruction() {
        return pmtInstruction;
    }

    public void setPmtInstruction(String pmtInstruction) {
        this.pmtInstruction = pmtInstruction;
    }

    public int getPmtNo() {
        return pmtNo;
    }

    public void setPmtNo(int pmtNo) {
        this.pmtNo = pmtNo;
    }

    @Override
    public String toString() {
        return "ApprovalDTO{" +
                "empNum=" + empNum +
                ", pmtDate=" + pmtDate +
                ", pmtManager='" + pmtManager + '\'' +
                ", pmtDrafting=" + pmtDrafting +
                ", pmtPosition='" + pmtPosition + '\'' +
                ", pmtName='" + pmtName + '\'' +
                ", pmtTitle='" + pmtTitle + '\'' +
                ", pmtContents='" + pmtContents + '\'' +
                ", pmtAddOn1='" + pmtAddOn1 + '\'' +
                ", pmtAddOn2='" + pmtAddOn2 + '\'' +
                ", pmtAddOn3='" + pmtAddOn3 + '\'' +
                ", pmtInstruction='" + pmtInstruction + '\'' +
                ", pmtNo=" + pmtNo +
                '}';
    }
}
