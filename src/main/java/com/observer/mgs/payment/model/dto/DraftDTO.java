package com.observer.mgs.payment.model.dto;

import java.sql.Date;

public class DraftDTO {

    private int empNum;
    private Date pmtDate;
    private String pmtManager;
    private Date pmtDrafting;
    private Date pmtEffective;
    private Date pmtRetention;
    private Date pmtRetention1;
    private Date pmtProcessing;
    private Date pmtProcessing1;
    private String pmtTitle;
    private String pmtCompany;
    private Date pmtPeriodOfUse;
    private Date pmtPeriodOfUse1;
    private String pmtRegistrationFee;
    private String pmtRangeOfUse;
    private String pmtBenefit;
    private int pmtNo;
    private String pmtName;
    private String pmtDepartment;
    private String pmtContents1;
    private String pmtContents2;
    private String pmtContents3;

    public DraftDTO() {}

    public DraftDTO(int empNum, Date pmtDate, String pmtManager, Date pmtDrafting, Date pmtEffective, Date pmtRetention, Date pmtRetention1, Date pmtProcessing, Date pmtProcessing1, String pmtTitle, String pmtCompany, Date pmtPeriodOfUse, Date pmtPeriodOfUse1, String pmtRegistrationFee, String pmtRangeOfUse, String pmtBenefit, int pmtNo, String pmtName, String pmtDepartment, String pmtContents1, String pmtContents2, String pmtContents3) {
        this.empNum = empNum;
        this.pmtDate = pmtDate;
        this.pmtManager = pmtManager;
        this.pmtDrafting = pmtDrafting;
        this.pmtEffective = pmtEffective;
        this.pmtRetention = pmtRetention;
        this.pmtRetention1 = pmtRetention1;
        this.pmtProcessing = pmtProcessing;
        this.pmtProcessing1 = pmtProcessing1;
        this.pmtTitle = pmtTitle;
        this.pmtCompany = pmtCompany;
        this.pmtPeriodOfUse = pmtPeriodOfUse;
        this.pmtPeriodOfUse1 = pmtPeriodOfUse1;
        this.pmtRegistrationFee = pmtRegistrationFee;
        this.pmtRangeOfUse = pmtRangeOfUse;
        this.pmtBenefit = pmtBenefit;
        this.pmtNo = pmtNo;
        this.pmtName = pmtName;
        this.pmtDepartment = pmtDepartment;
        this.pmtContents1 = pmtContents1;
        this.pmtContents2 = pmtContents2;
        this.pmtContents3 = pmtContents3;
    }

    public int getEmpNum() {
        return empNum;
    }

    public void setEmpNum(int empNum) {
        this.empNum = empNum;
    }

    public Date getPmtDate() {
        return pmtDate;
    }

    public void setPmtDate(Date pmtDate) {
        this.pmtDate = pmtDate;
    }

    public String getPmtManager() {
        return pmtManager;
    }

    public void setPmtManager(String pmtManager) {
        this.pmtManager = pmtManager;
    }

    public Date getPmtDrafting() {
        return pmtDrafting;
    }

    public void setPmtDrafting(Date pmtDrafting) {
        this.pmtDrafting = pmtDrafting;
    }

    public Date getPmtEffective() {
        return pmtEffective;
    }

    public void setPmtEffective(Date pmtEffective) {
        this.pmtEffective = pmtEffective;
    }

    public Date getPmtRetention() {
        return pmtRetention;
    }

    public void setPmtRetention(Date pmtRetention) {
        this.pmtRetention = pmtRetention;
    }

    public Date getPmtRetention1() {
        return pmtRetention1;
    }

    public void setPmtRetention1(Date pmtRetention1) {
        this.pmtRetention1 = pmtRetention1;
    }

    public Date getPmtProcessing() {
        return pmtProcessing;
    }

    public void setPmtProcessing(Date pmtProcessing) {
        this.pmtProcessing = pmtProcessing;
    }

    public Date getPmtProcessing1() {
        return pmtProcessing1;
    }

    public void setPmtProcessing1(Date pmtProcessing1) {
        this.pmtProcessing1 = pmtProcessing1;
    }

    public String getPmtTitle() {
        return pmtTitle;
    }

    public void setPmtTitle(String pmtTitle) {
        this.pmtTitle = pmtTitle;
    }

    public String getPmtCompany() {
        return pmtCompany;
    }

    public void setPmtCompany(String pmtCompany) {
        this.pmtCompany = pmtCompany;
    }

    public Date getPmtPeriodOfUse() {
        return pmtPeriodOfUse;
    }

    public void setPmtPeriodOfUse(Date pmtPeriodOfUse) {
        this.pmtPeriodOfUse = pmtPeriodOfUse;
    }

    public Date getPmtPeriodOfUse1() {
        return pmtPeriodOfUse1;
    }

    public void setPmtPeriodOfUse1(Date pmtPeriodOfUse1) {
        this.pmtPeriodOfUse1 = pmtPeriodOfUse1;
    }

    public String getPmtRegistrationFee() {
        return pmtRegistrationFee;
    }

    public void setPmtRegistrationFee(String pmtRegistrationFee) {
        this.pmtRegistrationFee = pmtRegistrationFee;
    }

    public String getPmtRangeOfUse() {
        return pmtRangeOfUse;
    }

    public void setPmtRangeOfUse(String pmtRangeOfUse) {
        this.pmtRangeOfUse = pmtRangeOfUse;
    }

    public String getPmtBenefit() {
        return pmtBenefit;
    }

    public void setPmtBenefit(String pmtBenefit) {
        this.pmtBenefit = pmtBenefit;
    }

    public int getPmtNo() {
        return pmtNo;
    }

    public void setPmtNo(int pmtNo) {
        this.pmtNo = pmtNo;
    }

    public String getPmtName() {
        return pmtName;
    }

    public void setPmtName(String pmtName) {
        this.pmtName = pmtName;
    }

    public String getPmtDepartment() {
        return pmtDepartment;
    }

    public void setPmtDepartment(String pmtDepartment) {
        this.pmtDepartment = pmtDepartment;
    }

    public String getPmtContents1() {
        return pmtContents1;
    }

    public void setPmtContents1(String pmtContents1) {
        this.pmtContents1 = pmtContents1;
    }

    public String getPmtContents2() {
        return pmtContents2;
    }

    public void setPmtContents2(String pmtContents2) {
        this.pmtContents2 = pmtContents2;
    }

    public String getPmtContents3() {
        return pmtContents3;
    }

    public void setPmtContents3(String pmtContents3) {
        this.pmtContents3 = pmtContents3;
    }

    @Override
    public String toString() {
        return "DraftDTO{" +
                "empNum=" + empNum +
                ", pmtDate=" + pmtDate +
                ", pmtManager='" + pmtManager + '\'' +
                ", pmtDrafting=" + pmtDrafting +
                ", pmtEffective=" + pmtEffective +
                ", pmtRetention=" + pmtRetention +
                ", pmtRetention1=" + pmtRetention1 +
                ", pmtProcessing=" + pmtProcessing +
                ", pmtProcessing1=" + pmtProcessing1 +
                ", pmtTitle='" + pmtTitle + '\'' +
                ", pmtCompany='" + pmtCompany + '\'' +
                ", pmtPeriodOfUse=" + pmtPeriodOfUse +
                ", pmtPeriodOfUse1=" + pmtPeriodOfUse1 +
                ", pmtRegistrationFee='" + pmtRegistrationFee + '\'' +
                ", pmtRangeOfUse='" + pmtRangeOfUse + '\'' +
                ", pmtBenefit='" + pmtBenefit + '\'' +
                ", pmtNo=" + pmtNo +
                ", pmtName='" + pmtName + '\'' +
                ", pmtDepartment='" + pmtDepartment + '\'' +
                ", pmtContents1='" + pmtContents1 + '\'' +
                ", pmtContents2='" + pmtContents2 + '\'' +
                ", pmtContents3='" + pmtContents3 + '\'' +
                '}';
    }
}
