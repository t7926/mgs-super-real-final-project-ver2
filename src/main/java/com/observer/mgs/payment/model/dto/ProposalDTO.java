package com.observer.mgs.payment.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.sql.Date;

public class ProposalDTO {

    private int empNum;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd", timezone = "Asia/Seoul")
    private Date pmtDate;
    private String pmtManager;
    private String pmtName;
    private String pmtDepartment;
    private String pmtPosition;
    private String pmtPhoNo;
    private String pmtContent;
    private String pmtNote;
    private int pmtNo;

    public ProposalDTO() {}

    public ProposalDTO(int empNum, Date pmtDate, String pmtManager, String pmtName, String pmtDepartment, String pmtPosition, String pmtPhoNo, String pmtContent, String pmtNote, int pmtNo) {
        this.empNum = empNum;
        this.pmtDate = pmtDate;
        this.pmtManager = pmtManager;
        this.pmtName = pmtName;
        this.pmtDepartment = pmtDepartment;
        this.pmtPosition = pmtPosition;
        this.pmtPhoNo = pmtPhoNo;
        this.pmtContent = pmtContent;
        this.pmtNote = pmtNote;
        this.pmtNo = pmtNo;
    }

    public int getEmpNum() {
        return empNum;
    }

    public void setEmpNum(int empNum) {
        this.empNum = empNum;
    }

    public Date getPmtDate() {
        return pmtDate;
    }

    public void setPmtDate(Date pmtDate) {
        this.pmtDate = pmtDate;
    }

    public String getPmtManager() {
        return pmtManager;
    }

    public void setPmtManager(String pmtManager) {
        this.pmtManager = pmtManager;
    }

    public String getPmtName() {
        return pmtName;
    }

    public void setPmtName(String pmtName) {
        this.pmtName = pmtName;
    }

    public String getPmtDepartment() {
        return pmtDepartment;
    }

    public void setPmtDepartment(String pmtDepartment) {
        this.pmtDepartment = pmtDepartment;
    }

    public String getPmtPosition() {
        return pmtPosition;
    }

    public void setPmtPosition(String pmtPosition) {
        this.pmtPosition = pmtPosition;
    }

    public String getPmtPhoNo() {
        return pmtPhoNo;
    }

    public void setPmtPhoNo(String pmtPhoNo) {
        this.pmtPhoNo = pmtPhoNo;
    }

    public String getPmtContent() {
        return pmtContent;
    }

    public void setPmtContent(String pmtContent) {
        this.pmtContent = pmtContent;
    }

    public String getPmtNote() {
        return pmtNote;
    }

    public void setPmtNote(String pmtNote) {
        this.pmtNote = pmtNote;
    }

    public int getPmtNo() {
        return pmtNo;
    }

    public void setPmtNo(int pmtNo) {
        this.pmtNo = pmtNo;
    }

    @Override
    public String toString() {
        return "ProposalDTO{" +
                "empNum=" + empNum +
                ", pmtDate=" + pmtDate +
                ", pmtManager='" + pmtManager + '\'' +
                ", pmtName='" + pmtName + '\'' +
                ", pmtDepartment='" + pmtDepartment + '\'' +
                ", pmtPosition='" + pmtPosition + '\'' +
                ", pmtPhoNo='" + pmtPhoNo + '\'' +
                ", pmtContent='" + pmtContent + '\'' +
                ", pmtNote='" + pmtNote + '\'' +
                ", pmtNo=" + pmtNo +
                '}';
    }
}
