package com.observer.mgs.payment.model.dto;

public class ProductDTO {

    private int prdNo;
    private String prdItem;
    private String prdUnit;
    private String prdCount;
    private String prdPrice;
    private String prdCompany;
    private String prdNote;
    private int empNum;

    public ProductDTO() {}

    public ProductDTO(int prdNo, String prdItem, String prdUnit, String prdCount, String prdPrice, String prdCompany, String prdNote, int empNum) {
        this.prdNo = prdNo;
        this.prdItem = prdItem;
        this.prdUnit = prdUnit;
        this.prdCount = prdCount;
        this.prdPrice = prdPrice;
        this.prdCompany = prdCompany;
        this.prdNote = prdNote;
        this.empNum = empNum;
    }

    public int getPrdNo() {
        return prdNo;
    }

    public void setPrdNo(int prdNo) {
        this.prdNo = prdNo;
    }

    public String getPrdItem() {
        return prdItem;
    }

    public void setPrdItem(String prdItem) {
        this.prdItem = prdItem;
    }

    public String getPrdUnit() {
        return prdUnit;
    }

    public void setPrdUnit(String prdUnit) {
        this.prdUnit = prdUnit;
    }

    public String getPrdCount() {
        return prdCount;
    }

    public void setPrdCount(String prdCount) {
        this.prdCount = prdCount;
    }

    public String getPrdPrice() {
        return prdPrice;
    }

    public void setPrdPrice(String prdPrice) {
        this.prdPrice = prdPrice;
    }

    public String getPrdCompany() {
        return prdCompany;
    }

    public void setPrdCompany(String prdCompany) {
        this.prdCompany = prdCompany;
    }

    public String getPrdNote() {
        return prdNote;
    }

    public void setPrdNote(String prdNote) {
        this.prdNote = prdNote;
    }

    public int getEmpNum() {
        return empNum;
    }

    public void setEmpNum(int empNum) {
        this.empNum = empNum;
    }

    @Override
    public String toString() {
        return "ProductDTO{" +
                "prdNo=" + prdNo +
                ", prdItem='" + prdItem + '\'' +
                ", prdUnit='" + prdUnit + '\'' +
                ", prdCount='" + prdCount + '\'' +
                ", prdPrice='" + prdPrice + '\'' +
                ", prdCompany='" + prdCompany + '\'' +
                ", prdNote='" + prdNote + '\'' +
                ", empNum=" + empNum +
                '}';
    }
}
