package com.observer.mgs.payment.model.service;

import com.observer.mgs.payment.model.dto.*;

import java.util.List;
import java.util.Map;

public interface PaymentService {

    int approval(ApprovalDTO approvalDTO);

    int sendApproval();

    List<ManagerDTO> sendManager();

    int cash(CashDTO cashDTO, List<CashUnitDTO> list);

    int draft(DraftDTO draftDTO);

    int report(ReportDTO reportDTO);

    int orderProposal(ProposalDTO proposalDTO, List<ProductDTO> list);

    int history(HistoryDTO history);

    int handWriter(HandWriterDTO handWriter);
}
