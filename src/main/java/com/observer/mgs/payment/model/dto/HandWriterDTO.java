package com.observer.mgs.payment.model.dto;

import java.sql.Date;

public class HandWriterDTO {

    private int empNum;
    private Date pmtDate;
    private int pmtNo;
    private String pmtName;
    private Date pmtDateOfIssue;
    private String pmtTitle;
    private String pmtManager;
    private String pmtNote;
    private String originalName;
    private String savedName;
    private String savePath;
    private String filePath;

    public HandWriterDTO() {}

    public HandWriterDTO(int empNum, Date pmtDate, int pmtNo, String pmtName, Date pmtDateOfIssue, String pmtTitle, String pmtManager, String pmtNote, String originalName, String savedName, String savePath, String filePath) {
        this.empNum = empNum;
        this.pmtDate = pmtDate;
        this.pmtNo = pmtNo;
        this.pmtName = pmtName;
        this.pmtDateOfIssue = pmtDateOfIssue;
        this.pmtTitle = pmtTitle;
        this.pmtManager = pmtManager;
        this.pmtNote = pmtNote;
        this.originalName = originalName;
        this.savedName = savedName;
        this.savePath = savePath;
        this.filePath = filePath;
    }

    public int getEmpNum() {
        return empNum;
    }

    public void setEmpNum(int empNum) {
        this.empNum = empNum;
    }

    public Date getPmtDate() {
        return pmtDate;
    }

    public void setPmtDate(Date pmtDate) {
        this.pmtDate = pmtDate;
    }

    public int getPmtNo() {
        return pmtNo;
    }

    public void setPmtNo(int pmtNo) {
        this.pmtNo = pmtNo;
    }

    public String getPmtName() {
        return pmtName;
    }

    public void setPmtName(String pmtName) {
        this.pmtName = pmtName;
    }

    public Date getPmtDateOfIssue() {
        return pmtDateOfIssue;
    }

    public void setPmtDateOfIssue(Date pmtDateOfIssue) {
        this.pmtDateOfIssue = pmtDateOfIssue;
    }

    public String getPmtTitle() {
        return pmtTitle;
    }

    public void setPmtTitle(String pmtTitle) {
        this.pmtTitle = pmtTitle;
    }

    public String getPmtManager() {
        return pmtManager;
    }

    public void setPmtManager(String pmtManager) {
        this.pmtManager = pmtManager;
    }

    public String getPmtNote() {
        return pmtNote;
    }

    public void setPmtNote(String pmtNote) {
        this.pmtNote = pmtNote;
    }

    public String getOriginalName() {
        return originalName;
    }

    public void setOriginalName(String originalName) {
        this.originalName = originalName;
    }

    public String getSavedName() {
        return savedName;
    }

    public void setSavedName(String savedName) {
        this.savedName = savedName;
    }

    public String getSavePath() {
        return savePath;
    }

    public void setSavePath(String savePath) {
        this.savePath = savePath;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    @Override
    public String toString() {
        return "HandWriterDTO{" +
                "empNum=" + empNum +
                ", pmtDate=" + pmtDate +
                ", pmtNo=" + pmtNo +
                ", pmtName='" + pmtName + '\'' +
                ", pmtDateOfIssue=" + pmtDateOfIssue +
                ", pmtTitle='" + pmtTitle + '\'' +
                ", pmtManager='" + pmtManager + '\'' +
                ", pmtNote='" + pmtNote + '\'' +
                ", originalName='" + originalName + '\'' +
                ", savedName='" + savedName + '\'' +
                ", savePath='" + savePath + '\'' +
                ", filePath='" + filePath + '\'' +
                '}';
    }
}
