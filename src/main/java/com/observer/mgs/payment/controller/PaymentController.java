package com.observer.mgs.payment.controller;

import com.observer.mgs.common.exception.document.DocRegistException;
import com.observer.mgs.document.model.dto.DocumentDTO;
import com.observer.mgs.document.model.service.DocumentService;
import com.observer.mgs.login.model.dto.LoginDTO;
import com.observer.mgs.payment.model.dto.*;

import com.observer.mgs.payment.model.service.PaymentService;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.*;


@Controller
@RequestMapping("/payment")
public class PaymentController {

    private final PaymentService paymentService;
    private final DocumentService docService;

    @Autowired
    public PaymentController(PaymentService paymentService, DocumentService docService) {
        this.paymentService = paymentService;
        this.docService = docService;
    }

    @GetMapping("/payment")
    public String selectPayment() {
    	System.out.println(1);
        return "payment/payment";
    }
    

    @GetMapping("/approval")
    public void sendApproval(Model model) {
        int getPmtNo = paymentService.sendApproval();
        List<ManagerDTO> getPmtManager = paymentService.sendManager();
        model.addAttribute("pmtNo", getPmtNo);
        model.addAttribute("pmtManager", getPmtManager);
        
    }

    @PostMapping("/approval")
    public String approval(@ModelAttribute ApprovalDTO approvalDTO, HttpServletRequest request) throws DocRegistException {

        HistoryDTO history = new HistoryDTO();
        DocumentDTO doc = new DocumentDTO();
        
        int empNum = ((LoginDTO) request.getSession().getAttribute("loginMember")).getEmpnum();
        String empName = ((LoginDTO) request.getSession().getAttribute("loginMember")).getName();
        approvalDTO.setEmpNum(empNum);
        
        history.setPmtTitle(approvalDTO.getPmtTitle());
        history.setPmtName(empName);
        history.setPmtDate(approvalDTO.getPmtDate());
        history.setPmtType("품의서");
        history.setEmpNum(approvalDTO.getEmpNum());
        
        doc.setDocTitle(approvalDTO.getPmtTitle());
        doc.setDocContents(approvalDTO.getPmtContents());
        doc.setDocCategory("품의서");
        doc.setDeptName(approvalDTO.getPmtPosition());
        doc.setEmpNum(approvalDTO.getEmpNum());
        doc.setDocGrade("C");

        int result1 = paymentService.approval(approvalDTO);
        int result2 = paymentService.history(history);
        docService.insertDoc(doc);
        
        return "redirect:/payment/payment";
    }

    @GetMapping("/cash")
    public String sendCash(Model model) {
        List<ManagerDTO> getPmtManager = paymentService.sendManager();
        model.addAttribute("pmtManager", getPmtManager);
        return "payment/cash";
    }

    @PostMapping(value="/listCash", produces = "application/json; charset=utf-8")
    @ResponseBody
    public String cashUnit(@ModelAttribute CashDTO cashDTO, @RequestParam("samp") String samp, HttpServletRequest request) throws ParseException, DocRegistException {

        int empNum = ((LoginDTO) request.getSession().getAttribute("loginMember")).getEmpnum();
        String empName = ((LoginDTO) request.getSession().getAttribute("loginMember")).getName();
        cashDTO.setEmpNum(empNum);
        
        HistoryDTO history = new HistoryDTO();
        DocumentDTO doc = new DocumentDTO();

        history.setPmtTitle(cashDTO.getPmtTitle());
        history.setPmtName(empName);
        history.setPmtDate(cashDTO.getPmtDate());
        history.setPmtType("지출결의서");
        history.setEmpNum(cashDTO.getEmpNum());
        
        doc.setDocTitle(cashDTO.getPmtTitle());
        doc.setDocContents("지출결의서");
        doc.setDocCategory("지출결의서");
        doc.setDeptName(cashDTO.getPmtDepartment());
        doc.setEmpNum(cashDTO.getEmpNum());
        doc.setDocGrade("C");
        
        JSONParser parser = new JSONParser();
        JSONArray arr = (JSONArray) parser.parse(samp);

        List<CashUnitDTO> list = new ArrayList<>();

        for (int i = 0; i < arr.size(); i++) {

            JSONObject obj = (JSONObject) arr.get(i);
            CashUnitDTO cashUnit = new CashUnitDTO();
            cashUnit.setRefEmpNum(empNum);

            if(!((obj.get("pmtBriefs")).toString().isEmpty())){

                cashUnit.setPmtBriefs(String.valueOf(obj.get("pmtBriefs")));
                cashUnit.setPmtAmount(String.valueOf(obj.get("pmtAmount")));
                cashUnit.setPmtNote(String.valueOf(obj.get("pmtNote")));

                list.add(cashUnit);
                list.removeAll(Collections.singletonList(null));

            } else {
                break;
            }
        }
        int result1 = paymentService.cash(cashDTO, list);
        int result2 = paymentService.history(history);
        docService.insertDoc(doc);

        return "redirect:/payment/payment";
    }

    @GetMapping("/draft")
    public void sendDraft(Model model) {
        List<ManagerDTO> getPmtManager = paymentService.sendManager();
        model.addAttribute("pmtManager", getPmtManager);
    }

    @PostMapping("/draft")
    public String draft(@ModelAttribute DraftDTO draftDTO, HttpServletRequest request) throws DocRegistException {
        int empNum = ((LoginDTO) request.getSession().getAttribute("loginMember")).getEmpnum();
        String empName = ((LoginDTO) request.getSession().getAttribute("loginMember")).getName();
        draftDTO.setEmpNum(empNum);

        HistoryDTO history = new HistoryDTO();
        DocumentDTO doc = new DocumentDTO();

        history.setPmtTitle(draftDTO.getPmtTitle());
        history.setPmtName(empName);
        history.setPmtDate(draftDTO.getPmtDate());
        history.setPmtType("기안서");
        history.setEmpNum(draftDTO.getEmpNum());

        doc.setDocTitle(draftDTO.getPmtTitle());
        doc.setDocContents(draftDTO.getPmtContents1());
        doc.setDocCategory("기안서");
        doc.setDeptName(draftDTO.getPmtDepartment());
        doc.setEmpNum(draftDTO.getEmpNum());
        doc.setDocGrade("C");
        
        int result1 = paymentService.draft(draftDTO);
        int result2 = paymentService.history(history);
        docService.insertDoc(doc);

        return "redirect:/payment/payment";
    }

    @GetMapping("/report")
    public void sendReport(Model model) {
        List<ManagerDTO> getPmtManager = paymentService.sendManager();
        int getPmtNo = paymentService.sendApproval();

        model.addAttribute("pmtNo", getPmtNo);
        model.addAttribute("pmtManager", getPmtManager);
    }

    @PostMapping("/report")
    public String report(@ModelAttribute ReportDTO reportDTO, HttpServletRequest request) throws DocRegistException {

        int empNum = ((LoginDTO) request.getSession().getAttribute("loginMember")).getEmpnum();
        String empName = ((LoginDTO) request.getSession().getAttribute("loginMember")).getName();
        HistoryDTO history = new HistoryDTO();
        DocumentDTO doc = new DocumentDTO();

        history.setPmtTitle(reportDTO.getPmtTitle());
        history.setPmtName(empName);
        history.setPmtDate(reportDTO.getPmtDate());
        history.setPmtType("보고서");
        history.setEmpNum(reportDTO.getEmpNum());

        doc.setDocTitle(reportDTO.getPmtTitle());
        doc.setDocContents(reportDTO.getPmt1Week());
        doc.setDocCategory("보고서");
        doc.setDeptName(reportDTO.getPmtPosition());
        doc.setEmpNum(reportDTO.getEmpNum());
        doc.setDocGrade("C");
        
        reportDTO.setEmpNum(empNum);
        int result1 = paymentService.report(reportDTO);
        int result2 = paymentService.history(history);
        docService.insertDoc(doc);
        
        return "redirect:/payment/payment";
    }

    @GetMapping("/orderproposal")
    public void sendProposal(Model model) {

        List<ManagerDTO> getPmtManager = paymentService.sendManager();
        model.addAttribute("pmtManager", getPmtManager);
    }

    @PostMapping(value="/orderproposal", produces = "application/json; charset=utf-8")
    @ResponseBody
    public String orderProposal(@ModelAttribute ProposalDTO proposalDTO, @RequestParam("samp") String samp, HttpServletRequest request) throws ParseException, DocRegistException {

        int empNum = ((LoginDTO) request.getSession().getAttribute("loginMember")).getEmpnum();
        String empName = ((LoginDTO) request.getSession().getAttribute("loginMember")).getName();
        proposalDTO.setEmpNum(empNum);

        HistoryDTO history = new HistoryDTO();
        DocumentDTO doc = new DocumentDTO();

        history.setPmtTitle("구매 제안서 입니다.");
        history.setPmtName(empName);
        history.setPmtDate(proposalDTO.getPmtDate());
        history.setPmtType("구매제안서");
        history.setEmpNum(proposalDTO.getEmpNum());

        doc.setDocTitle("구매 제안서 입니다.");
        doc.setDocContents(proposalDTO.getPmtContent());
        doc.setDocCategory("제안서");
        doc.setDeptName(proposalDTO.getPmtDepartment());
        doc.setEmpNum(proposalDTO.getEmpNum());
        doc.setDocGrade("C");
        
        JSONParser parser = new JSONParser();
        JSONArray arr = (JSONArray) parser.parse(samp);

        List<ProductDTO> list = new ArrayList<>();

        for (int i = 0; i < arr.size(); i++) {

            JSONObject obj = (JSONObject) arr.get(i);
            ProductDTO productDTO = new ProductDTO();

            productDTO.setEmpNum(empNum);
            if(!((obj.get("prdItem")).toString().isEmpty())){

                productDTO.setPrdNo(Integer.parseInt(obj.get("prdNo").toString()));
                productDTO.setPrdItem(String.valueOf((obj.get("prdItem"))));
                productDTO.setPrdUnit(String.valueOf(obj.get("prdUnit")));
                productDTO.setPrdCount(String.valueOf(obj.get("prdCount")));
                productDTO.setPrdPrice(String.valueOf((obj.get("prdPrice"))));
                productDTO.setPrdCompany(String.valueOf(obj.get("prdCompany")));
                productDTO.setPrdNote(String.valueOf(obj.get("prdNote")));

                list.add(productDTO);
                System.out.println(list);
                list.removeAll(Collections.singletonList(null));

            } else {
                break;
            }
        }
        int result1 = paymentService.orderProposal(proposalDTO, list);
        int result2 = paymentService.history(history);
        docService.insertDoc(doc);

        return "redirect:/payment/payment";
    }

    @GetMapping("/handwriter")
    public void sendHandWriter(Model model) {
        List<ManagerDTO> getPmtManager = paymentService.sendManager();
        int getPmtNo = paymentService.sendApproval();

        model.addAttribute("pmtNo", getPmtNo);
        model.addAttribute("pmtManager", getPmtManager);
    }

    @PostMapping("/handWriter")
    public String handWriter(@RequestParam MultipartFile singleFile, HttpServletRequest request, @ModelAttribute HandWriterDTO handWriter) {

        int empNum = ((LoginDTO) request.getSession().getAttribute("loginMember")).getEmpnum();
        String empName = ((LoginDTO) request.getSession().getAttribute("loginMember")).getName();

        handWriter.setEmpNum(empNum);
        handWriter.setPmtName(empName);

        String root = request.getSession().getServletContext().getRealPath("resources");
        System.out.println(root);
        String filePath = root + "/payment/uploadFiles";
        System.out.println(filePath);
        File mkdir = new File(filePath);

        if(!mkdir.exists()) {
            mkdir.mkdirs();
        }

        String originFileName = singleFile.getOriginalFilename();
        String ext = originFileName.substring(originFileName.lastIndexOf("."));
        String savedName = UUID.randomUUID().toString().replace("-","") + ext;
        String savedPath = "/resources/payment/uploadFiles" + savedName;

        try {
            singleFile.transferTo(new File(filePath + "\\" + savedName));
            System.out.println("파일업로드 성공");
        } catch (IOException e) {
            e.printStackTrace();
            new File(filePath + "\\" + savedName).delete();
            System.out.println("파일업로드 실패");
        }

        handWriter.setOriginalName(originFileName);
        handWriter.setSavedName(savedName);
        handWriter.setSavePath(filePath);
        handWriter.setFilePath(savedPath);

        HistoryDTO history = new HistoryDTO();

        history.setPmtTitle(handWriter.getPmtTitle());
        history.setPmtName(empName);
        history.setPmtDate(handWriter.getPmtDate());
        history.setPmtType("수기작성");
        history.setEmpNum(handWriter.getEmpNum());


        int result = paymentService.handWriter(handWriter);
        int result1 = paymentService.history(history);


        //filePath가 savePath다다
       return "redirect:/payment/payment";
    }

  
     /*단순 페이지 연결용도*/
       
    @GetMapping("/payment-history")
	public String test1() {
		return "payment/payment-history";
	}
    
    @GetMapping("/download-form")
   	public String test2() {
   		return "payment/download-form";
   	}

    @GetMapping("/payment-manager")
   	public String test3() {
   		return "payment/payment-manager";
   	}
    @GetMapping("/documentbox")
   	public String test4() {
   		return "payment/documentbox";
   	}
    
    @GetMapping("/payment-temp")
   	public String test5() {
   		return "payment/payment-temp";
   	}
    
    @GetMapping("/payment-waiting")
   	public String test6() {
   		return "payment/payment-waiting";
   	}
    
    @GetMapping("/payment-reject")
   	public String test7() {
   		return "payment/payment-reject";
   	}
    
    @GetMapping("/payment-complete")
   	public String test8() {
   		return "payment/payment-complete";
   	}
    
    
    
}
