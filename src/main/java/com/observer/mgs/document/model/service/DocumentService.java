package com.observer.mgs.document.model.service;

import java.util.List;
import java.util.Map;

import com.observer.mgs.common.exception.document.AuthModifyException;
import com.observer.mgs.common.exception.document.AuthRegistException;
import com.observer.mgs.common.exception.document.DocRegistException;
import com.observer.mgs.common.exception.document.DocRemoveException;
import com.observer.mgs.common.paging.SelectCriteria;
import com.observer.mgs.document.model.dto.DocAuthorityDTO;
import com.observer.mgs.document.model.dto.DocumentDTO;
import com.observer.mgs.payment.model.dto.ApprovalDTO;
import com.observer.mgs.payment.model.dto.CashDTO;
import com.observer.mgs.payment.model.dto.CashUnitDTO;
import com.observer.mgs.payment.model.dto.DraftDTO;
import com.observer.mgs.payment.model.dto.ProductDTO;
import com.observer.mgs.payment.model.dto.ProposalDTO;
import com.observer.mgs.payment.model.dto.ReportDTO;

public interface DocumentService {

	List<DocumentDTO> selectDocList(SelectCriteria selectCriteria);

	List<DocumentDTO> selectApprovalList(SelectCriteria selectCriteria);
	
	List<DocumentDTO> selectExpendList(SelectCriteria selectCriteria);
	
	List<DocumentDTO> selectDraftList(SelectCriteria selectCriteria);
	
	List<DocumentDTO> SelectSuggestList(SelectCriteria selectCriteria);
	
	List<DocumentDTO> selectAttendanceList();
	
	List<DocumentDTO> SelectReportList(SelectCriteria selectCriteria);
	
	void insertDoc(DocumentDTO documentDTO) throws DocRegistException;

	void deleteDoc(int docNo) throws DocRemoveException;

	void modifyAuth(DocAuthorityDTO docAuthDTO) throws AuthModifyException;

	int selectTotalCount(Map<String, String> searchMap);

	int selectAuthTotalCount(Map<String, String> searchMap);

	List<DocAuthorityDTO> selectAuthList(SelectCriteria selectCriteria);

	DocAuthorityDTO selectAuthDetail(int no);

	ApprovalDTO selectApprovalDetail(int pmtNo);

	DraftDTO selectDraftDetail(int pmtNo);

	ReportDTO selectReportDetail(int pmtNo);

	CashDTO selectExpendDetail(int pmtNo);

	List<CashUnitDTO> selectUnitList(int pmtNo);

	ProposalDTO selectSuggestDetail(int pmtNo);

	List<ProductDTO> selectProductList(int pmtNo);

	void insertAuth(DocAuthorityDTO auth) throws AuthRegistException;







}
