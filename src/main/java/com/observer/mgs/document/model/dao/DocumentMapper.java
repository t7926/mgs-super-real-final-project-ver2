package com.observer.mgs.document.model.dao;

import java.util.List;
import java.util.Map;

import com.observer.mgs.common.paging.SelectCriteria;
import com.observer.mgs.document.model.dto.DocAuthorityDTO;
import com.observer.mgs.document.model.dto.DocumentDTO;
import com.observer.mgs.payment.model.dto.ApprovalDTO;
import com.observer.mgs.payment.model.dto.CashDTO;
import com.observer.mgs.payment.model.dto.CashUnitDTO;
import com.observer.mgs.payment.model.dto.DraftDTO;
import com.observer.mgs.payment.model.dto.ProductDTO;
import com.observer.mgs.payment.model.dto.ProposalDTO;
import com.observer.mgs.payment.model.dto.ReportDTO;

public interface DocumentMapper {

	List<DocumentDTO> selectDocList(SelectCriteria selectCriteria);

	List<DocumentDTO> selectApprovalList(SelectCriteria selectCriteria);
	
	List<DocumentDTO> selectExpendList(SelectCriteria selectCriteria);

	List<DocumentDTO> selectDraftList(SelectCriteria selectCriteria);
	
	List<DocumentDTO> selectSuggestList(SelectCriteria selectCriteria);
	
	List<DocumentDTO> selectAttendanceList();
	
	List<DocumentDTO> selectReportList(SelectCriteria selectCriteria);
	
	int insertDoc(DocumentDTO documentDTO);

	int deleteDoc(int docNo);

	int modifyAuth(DocAuthorityDTO docAuthDTO);

	int selectTotalCount(Map<String, String> searchMap);

	int selectAuthTotalCount(Map<String, String> searchMap);

	List<DocAuthorityDTO> selectAuthList(SelectCriteria selectCriteria);

	DocAuthorityDTO selectAuthDetail(int no);

	ApprovalDTO selectApprovalDetail(int pmtNo);

	DraftDTO selectDraftDetail(int pmtNo);

	ReportDTO selectReportDetail(int pmtNo);

	CashDTO selectExpendDetail(int pmtNo);

	List<CashUnitDTO> selectUnitList(int pmtNo);

	ProposalDTO selectSuggestDetail(int pmtNo);

	List<ProductDTO> selectProductList(int pmtNo);

	int insertAuth(DocAuthorityDTO auth);
}
