package com.observer.mgs.document.model.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.observer.mgs.common.exception.document.AuthModifyException;
import com.observer.mgs.common.exception.document.AuthRegistException;
import com.observer.mgs.common.exception.document.DocRegistException;
import com.observer.mgs.common.exception.document.DocRemoveException;
import com.observer.mgs.common.paging.SelectCriteria;
import com.observer.mgs.document.model.dao.DocumentMapper;
import com.observer.mgs.document.model.dto.DocAuthorityDTO;
import com.observer.mgs.document.model.dto.DocumentDTO;
import com.observer.mgs.payment.model.dto.ApprovalDTO;
import com.observer.mgs.payment.model.dto.CashDTO;
import com.observer.mgs.payment.model.dto.CashUnitDTO;
import com.observer.mgs.payment.model.dto.DraftDTO;
import com.observer.mgs.payment.model.dto.ProductDTO;
import com.observer.mgs.payment.model.dto.ProposalDTO;
import com.observer.mgs.payment.model.dto.ReportDTO;

@Service
public class DocumentServiceImpl implements DocumentService{
	
	private final DocumentMapper mapper;
	
	@Autowired
	public DocumentServiceImpl(DocumentMapper mapper) {
		this.mapper = mapper;
	}

	/**
	 * 	<pre>
	 * 		문서 전체 조회용 메소드
	 * 	</pre>
	 */
	@Override
	public List<DocumentDTO> selectDocList(SelectCriteria selectCriteria) {
		
		return mapper.selectDocList(selectCriteria);
	}

	/**
	 *	<pre>
	 *		품의서 조회용 메소드
	 *	</pre>
	 */
	@Override
	public List<DocumentDTO> selectApprovalList(SelectCriteria selectCriteria) {
		
		return mapper.selectApprovalList(selectCriteria);
	}
	
	/**
	 *	<pre>
	 *		지출결의서 조회용 메소드
	 *	</pre>
	 */
	@Override
	public List<DocumentDTO> selectExpendList(SelectCriteria selectCriteria) {
		
		return mapper.selectExpendList(selectCriteria);
	}
	
	/**
	 * 	<pre>
	 * 		기안서 조회용 메소드
	 * 	</pre>
	 */
	@Override
	public List<DocumentDTO> selectDraftList(SelectCriteria selectCriteria) {
		return mapper.selectDraftList(selectCriteria);
	}
	
	/**
	 * 	<pre>
	 * 		제안서 조회용 메소드
	 *	</pre>
	 */
	@Override
	public List<DocumentDTO> SelectSuggestList(SelectCriteria selectCriteria) {
		return mapper.selectSuggestList(selectCriteria);
	}
	
	/**
	 *	<pre>
	 *		보고서 조회용 메소드
	 *	</pre>
	 */
	@Override
	public List<DocumentDTO> SelectReportList(SelectCriteria selectCriteria) {
		return mapper.selectReportList(selectCriteria);
	}
	
	/**
	 *	<pre>
	 *		근태신청서 조회용 메소드
	 *	</pre>
	 */
	@Override
	public List<DocumentDTO> selectAttendanceList() {
		return mapper.selectAttendanceList();
	}
	/**
	 * <pre>
	 * 		인서트 테스트용 메소드
	 * </pre>	
	 * @throws DocRegistException 
	 */
	@Override
	public void insertDoc(DocumentDTO documentDTO) throws DocRegistException {
		
		int result = mapper.insertDoc(documentDTO);
		
		if(!(result > 0)) {
			
			throw new DocRegistException("문서 등록을 실패하였습니다.");
		}
		
	}

	/**
	 * <pre>
	 * 		삭제 테스트용 메소드
	 * </pre>	
	 * @throws DocRemoveException 
	 */
	@Override
	public void deleteDoc(int docNo) throws DocRemoveException {
		
		int result = mapper.deleteDoc(docNo);
		
		if(!(result > 0)) {
			
			throw new DocRemoveException("문서 삭제를 실패하였습니다.");
		}
	}

	/**
	 *	<pre>
	 *		권한 수정 테스트용 메소드
	 *	</pre>
	 * @throws AuthModifyException 
	 */
	@Override
	public void modifyAuth(DocAuthorityDTO docAuthDTO) throws AuthModifyException {
		
		int result = mapper.modifyAuth(docAuthDTO);
		
		if(!(result > 0)) {
			
			throw new AuthModifyException("권한 수정을 실패하였습니다.");
		}
	}

	/**
	 *	<pre>
	 *		문서 목록 카운트용 메소드
	 *	</pre>
	 */
	@Override
	public int selectTotalCount(Map<String, String> searchMap) {
		
		return mapper.selectTotalCount(searchMap);
	}

	/**
	 * 	<pre>
	 * 		권한 목록 카운트용 메소드
	 *	</pre>
	 */
	@Override
	public int selectAuthTotalCount(Map<String, String> searchMap) {
		
		return mapper.selectAuthTotalCount(searchMap);
	}

	/**
	 *	<pre>
	 *		권한 목록 조회용 메소드
	 *	</pre>
	 */
	@Override
	public List<DocAuthorityDTO> selectAuthList(SelectCriteria selectCriteria) {
		return mapper.selectAuthList(selectCriteria);
	}

	/**
	 *	<pre>
	 *		권한 상세 조회용 메소드
	 *	</pre>
	 */
	@Override
	public DocAuthorityDTO selectAuthDetail(int no) {
		
		return mapper.selectAuthDetail(no);
	}

	/**
	 *	<pre>
	 *		품의서 상세 조회용 메소드
	 *	</pre>
	 */
	@Override
	public ApprovalDTO selectApprovalDetail(int pmtNo) {
		
		return mapper.selectApprovalDetail(pmtNo);
	}

	/**
	 *	<pre>
	 *		기안서 상세 조회용 메소드
	 *	</pre>
	 */
	@Override
	public DraftDTO selectDraftDetail(int pmtNo) {
		
		return mapper.selectDraftDetail(pmtNo);
	}

	/**
	 *	<pre>
	 *		보고서 상세 조회용 메소드
	 *	</pre>
	 */
	@Override
	public ReportDTO selectReportDetail(int pmtNo) {
		
		return mapper.selectReportDetail(pmtNo);
	}

	/**
	 *	<pre>
	 *		지출결의서 상세 조회용 메소드
	 *	</pre>
	 */
	@Override
	public CashDTO selectExpendDetail(int pmtNo) {
		
		return mapper.selectExpendDetail(pmtNo);
	}
	
	/**
	 *	<pre>
	 *		지출결의서 물품 조회용 메소드
	 *	</pre>
	 */
	@Override
	public List<CashUnitDTO> selectUnitList(int pmtNo) {
		
		return mapper.selectUnitList(pmtNo);
	}

	/**
	 *	<pre>
	 *		제안서 상세 조회용 메소드
	 *	</pre>
	 */
	@Override
	public ProposalDTO selectSuggestDetail(int pmtNo) {
		
		return mapper.selectSuggestDetail(pmtNo);
	}

	/**
	 *	<pre>
	 *		제안서 물품 조회용 메소드
	 *	</pre>
	 */
	@Override
	public List<ProductDTO> selectProductList(int pmtNo) {
		
		return mapper.selectProductList(pmtNo);
	}

	@Override
	public void insertAuth(DocAuthorityDTO auth) throws AuthRegistException {
		
		int result = mapper.insertAuth(auth);
		
		if(!(result > 0)) {
			
			throw new AuthRegistException("권한 부여를 실패하였습니다.");
		}
	}







	
}
