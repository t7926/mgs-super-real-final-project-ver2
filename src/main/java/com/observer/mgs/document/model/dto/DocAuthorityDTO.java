package com.observer.mgs.document.model.dto;

public class DocAuthorityDTO {

	private int empNum;
	private String empName;
	private int deptCode;
	private String deptName;
	private int teamCode;
	private String teamName;
	private int levCode;
	private String levName;
	private String authGrade;
	private String authorityYN;

	
	public DocAuthorityDTO() {
		super();
	}

	public DocAuthorityDTO(int empNum, String empName, int deptCode, String deptName, int teamCode, String teamName,
			int levCode, String levName, String authGrade, String authorityYN) {
		super();
		this.empNum = empNum;
		this.empName = empName;
		this.deptCode = deptCode;
		this.deptName = deptName;
		this.teamCode = teamCode;
		this.teamName = teamName;
		this.levCode = levCode;
		this.levName = levName;
		this.authGrade = authGrade;
		this.authorityYN = authorityYN;
	}

	public int getEmpNum() {
		return empNum;
	}

	public void setEmpNum(int empNum) {
		this.empNum = empNum;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public int getDeptCode() {
		return deptCode;
	}

	public void setDeptCode(int deptCode) {
		this.deptCode = deptCode;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public int getTeamCode() {
		return teamCode;
	}

	public void setTeamCode(int teamCode) {
		this.teamCode = teamCode;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public int getLevCode() {
		return levCode;
	}

	public void setLevCode(int levCode) {
		this.levCode = levCode;
	}

	public String getLevName() {
		return levName;
	}

	public void setLevName(String levName) {
		this.levName = levName;
	}

	public String getAuthGrade() {
		return authGrade;
	}

	public void setAuthGrade(String authGrade) {
		this.authGrade = authGrade;
	}

	public String getAuthorityYN() {
		return authorityYN;
	}

	public void setAuthorityYN(String authorityYN) {
		this.authorityYN = authorityYN;
	}

	@Override
	public String toString() {
		return "DocAuthorityDTO [empNum=" + empNum + ", empName=" + empName + ", deptCode=" + deptCode + ", deptName="
				+ deptName + ", teamCode=" + teamCode + ", teamName=" + teamName + ", levCode=" + levCode + ", levName="
				+ levName + ", authGrade=" + authGrade + ", authorityYN=" + authorityYN + "]";
	}
}