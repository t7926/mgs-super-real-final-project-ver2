package com.observer.mgs.document.model.dto;

import java.sql.Date;

public class DocumentDTO {

	private int docNo;
	private String docTitle;
	private String docContents;
	private String docCategory;
	private String deptName;
	private Date registDate;
	private String docGrade;
	private int empNum;
	
	public DocumentDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public DocumentDTO(int docNo, String docTitle, String docContents, String docCategory, String deptName,
			Date registDate, String docGrade, int empNum) {
		super();
		this.docNo = docNo;
		this.docTitle = docTitle;
		this.docContents = docContents;
		this.docCategory = docCategory;
		this.deptName = deptName;
		this.registDate = registDate;
		this.docGrade = docGrade;
		this.empNum = empNum;
	}

	public int getDocNo() {
		return docNo;
	}

	public void setDocNo(int docNo) {
		this.docNo = docNo;
	}

	public String getDocTitle() {
		return docTitle;
	}

	public void setDocTitle(String docTitle) {
		this.docTitle = docTitle;
	}

	public String getDocContents() {
		return docContents;
	}

	public void setDocContents(String docContents) {
		this.docContents = docContents;
	}

	public String getDocCategory() {
		return docCategory;
	}

	public void setDocCategory(String docCategory) {
		this.docCategory = docCategory;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public Date getRegistDate() {
		return registDate;
	}

	public void setRegistDate(Date registDate) {
		this.registDate = registDate;
	}

	public String getDocGrade() {
		return docGrade;
	}

	public void setDocGrade(String docGrade) {
		this.docGrade = docGrade;
	}

	public int getEmpNum() {
		return empNum;
	}

	public void setEmpNum(int empNum) {
		this.empNum = empNum;
	}

	@Override
	public String toString() {
		return "DocumentDTO [docNo=" + docNo + ", docTitle=" + docTitle + ", docContents=" + docContents
				+ ", docCategory=" + docCategory + ", deptName=" + deptName + ", registDate=" + registDate
				+ ", docGrade=" + docGrade + ", empNum=" + empNum + "]";
	}
}
