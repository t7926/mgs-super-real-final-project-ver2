package com.observer.mgs.document.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.observer.mgs.common.exception.document.AuthModifyException;
import com.observer.mgs.common.exception.document.DocRemoveException;
import com.observer.mgs.common.paging.Pagenation;
import com.observer.mgs.common.paging.SelectCriteria;
import com.observer.mgs.document.model.dto.DocAuthorityDTO;
import com.observer.mgs.document.model.dto.DocumentDTO;
import com.observer.mgs.document.model.service.DocumentService;
import com.observer.mgs.login.model.dto.LoginDTO;
import com.observer.mgs.payment.model.dto.ApprovalDTO;
import com.observer.mgs.payment.model.dto.CashDTO;
import com.observer.mgs.payment.model.dto.CashUnitDTO;
import com.observer.mgs.payment.model.dto.DraftDTO;
import com.observer.mgs.payment.model.dto.ProductDTO;
import com.observer.mgs.payment.model.dto.ProposalDTO;
import com.observer.mgs.payment.model.dto.ReportDTO;



@Controller
@RequestMapping("/document")
public class DocumentController {
		
	private final DocumentService docService;
	
	@Autowired
	public DocumentController(DocumentService docService) {
		this.docService = docService;
	}
	
	@GetMapping("/list")
	public ModelAndView selectDocList(@RequestParam(defaultValue = "1") int currentPage, @ModelAttribute SelectCriteria searchCriteria, ModelAndView mv,
			HttpServletRequest request) {
		
		String searchCondition = searchCriteria.getSearchCondition();
		String searchValue = searchCriteria.getSearchValue();
	
		int empNum = ((LoginDTO) request.getSession().getAttribute("loginMember")).getEmpnum();
		
		DocAuthorityDTO auth = docService.selectAuthDetail(empNum);
		
		Map<String, String> searchMap = new HashMap<>();
		searchMap.put("searchCondition", searchCondition);
		searchMap.put("searchValue", searchValue);
		
		System.out.println("컨트롤러에서 검색 조건 확인하기 : " + searchMap);
		
		
		int totalCount = docService.selectTotalCount(searchMap);
		
		System.out.println("totalBoardCount : " + totalCount);
		
		/* 한 페이지에 보여줄 게시물 수 */
		int limit = 10;
		
		/* 한 번에 보여질 페이징 버튼의 갯수*/
		int buttonAmount = 5; 
		
		/* 페이징 처리를 위한 로직 호출 후 페이징 처리에 관한 정보를 담고 있는 인스턴스를 반환*/
		SelectCriteria selectCriteria = null;
		
		if(searchCondition != null && !"".equals(searchCondition)) {
		
			selectCriteria = Pagenation.getSelectCriteria(currentPage, totalCount, limit, buttonAmount, searchCondition, searchValue);
		} else {
			selectCriteria = Pagenation.getSelectCriteria(currentPage, totalCount, limit, buttonAmount);
		}
		
		System.out.println("selectCriteria : " + selectCriteria);
		
		List<DocumentDTO> docList = docService.selectDocList(selectCriteria);
		
		System.out.println("docList : " + docList);
		
		mv.addObject("docList", docList);
		mv.addObject("selectCriteria", selectCriteria);
		mv.addObject("auth", auth);
		mv.addObject("type","list");
		mv.setViewName("/document/alldocument");
		
		return mv;
	}	
	@GetMapping("/authList")
	public ModelAndView AuthSelectList(@RequestParam(defaultValue = "1") int currentPage, @ModelAttribute SelectCriteria searchCriteria, ModelAndView mv) {
		
		String searchCondition = searchCriteria.getSearchCondition();
		String searchValue = searchCriteria.getSearchValue();
		
		Map<String, String> searchMap = new HashMap<>();
		searchMap.put("searchCondition", searchCondition);
		searchMap.put("searchValue", searchValue);
		
		System.out.println("컨트롤러에서 검색 조건 확인하기 : " + searchMap);
		
		
		int totalCount = docService.selectAuthTotalCount(searchMap);
		
		System.out.println("totalAuthCount : " + totalCount);
		
		/* 한 페이지에 보여줄 게시물 수 */
		int limit = 10;
		
		/* 한 번에 보여질 페이징 버튼의 갯수*/
		int buttonAmount = 5; 
		
		/* 페이징 처리를 위한 로직 호출 후 페이징 처리에 관한 정보를 담고 있는 인스턴스를 반환*/
		SelectCriteria selectCriteria = null;
		
		if(searchCondition != null && !"".equals(searchCondition)) {
			
			selectCriteria = Pagenation.getSelectCriteria(currentPage, totalCount, limit, buttonAmount, searchCondition, searchValue);
		} else {
			selectCriteria = Pagenation.getSelectCriteria(currentPage, totalCount, limit, buttonAmount);
		}
		
		System.out.println("selectCriteria : " + selectCriteria);
		
		List<DocAuthorityDTO> authList = docService.selectAuthList(selectCriteria);
		
		System.out.println("authList : " + authList);
		
		mv.addObject("authList", authList);
		mv.addObject("selectCriteria", selectCriteria);
		mv.addObject("type","authList");
		mv.setViewName("/document/access-auth");
		
		return mv;
	}	
	
	@GetMapping("authModify")
	public String modifyAuth(@RequestParam int empNum, Model model) {
		
		DocAuthorityDTO auth = docService.selectAuthDetail(empNum);
		
		model.addAttribute("auth", auth);
		
		return "/document/contents/auth-detail";
	}
	
	@PostMapping("authModify")
	public String modifyAuth(@ModelAttribute DocAuthorityDTO docAuthDTO, RedirectAttributes rttr) throws AuthModifyException {
		
		docService.modifyAuth(docAuthDTO);
		
		rttr.addFlashAttribute("message", "권한을 변경하였습니다.");
				
		return "redirect:/document/authList";
	}
	
	@GetMapping("/approval")
	public ModelAndView SelectApprovalList(@RequestParam(defaultValue = "1") int currentPage, @ModelAttribute SelectCriteria searchCriteria, ModelAndView mv, 
			HttpServletRequest request) {
		
		String searchCondition = searchCriteria.getSearchCondition();
		String searchValue = searchCriteria.getSearchValue();
		
		int empNum = ((LoginDTO) request.getSession().getAttribute("loginMember")).getEmpnum();
		
		DocAuthorityDTO auth = docService.selectAuthDetail(empNum);
		
		Map<String, String> searchMap = new HashMap<>();
		searchMap.put("searchCondition", searchCondition);
		searchMap.put("searchValue", searchValue);
		
		System.out.println("컨트롤러에서 검색 조건 확인하기 : " + searchMap);
		
		
		int totalCount = docService.selectTotalCount(searchMap);
		
		System.out.println("totalAuthCount : " + totalCount);
		
		/* 한 페이지에 보여줄 게시물 수 */
		int limit = 10;
		
		/* 한 번에 보여질 페이징 버튼의 갯수*/
		int buttonAmount = 5; 
		
		/* 페이징 처리를 위한 로직 호출 후 페이징 처리에 관한 정보를 담고 있는 인스턴스를 반환*/
		SelectCriteria selectCriteria = null;
		
		if(searchCondition != null && !"".equals(searchCondition)) {
			
			selectCriteria = Pagenation.getSelectCriteria(currentPage, totalCount, limit, buttonAmount, searchCondition, searchValue);
		} else {
			selectCriteria = Pagenation.getSelectCriteria(currentPage, totalCount, limit, buttonAmount);
		}
		
		System.out.println("selectCriteria : " + selectCriteria);
		
		List<DocumentDTO> approvalList = docService.selectApprovalList(selectCriteria);
		
		System.out.println("approvalList : " + approvalList);
		
		mv.addObject("approvalList", approvalList);
		mv.addObject("selectCriteria", selectCriteria);
		mv.addObject("auth", auth);
		mv.addObject("type","approval");
		mv.setViewName("/document/approval-docu");
		
		return mv;
	}	
	
	@GetMapping("/expend")
	public ModelAndView SelectExpendList(@RequestParam(defaultValue = "1") int currentPage, @ModelAttribute SelectCriteria searchCriteria, ModelAndView mv,
			HttpServletRequest request) {
		
		String searchCondition = searchCriteria.getSearchCondition();
		String searchValue = searchCriteria.getSearchValue();
		
		int empNum = ((LoginDTO) request.getSession().getAttribute("loginMember")).getEmpnum();
		
		DocAuthorityDTO auth = docService.selectAuthDetail(empNum);
		
		Map<String, String> searchMap = new HashMap<>();
		searchMap.put("searchCondition", searchCondition);
		searchMap.put("searchValue", searchValue);
		
		System.out.println("컨트롤러에서 검색 조건 확인하기 : " + searchMap);
		
		
		int totalCount = docService.selectTotalCount(searchMap);
		
		System.out.println("totalAuthCount : " + totalCount);
		
		/* 한 페이지에 보여줄 게시물 수 */
		int limit = 10;
		
		/* 한 번에 보여질 페이징 버튼의 갯수*/
		int buttonAmount = 5; 
		
		/* 페이징 처리를 위한 로직 호출 후 페이징 처리에 관한 정보를 담고 있는 인스턴스를 반환*/
		SelectCriteria selectCriteria = null;
		
		if(searchCondition != null && !"".equals(searchCondition)) {
			
			selectCriteria = Pagenation.getSelectCriteria(currentPage, totalCount, limit, buttonAmount, searchCondition, searchValue);
		} else {
			selectCriteria = Pagenation.getSelectCriteria(currentPage, totalCount, limit, buttonAmount);
		}
		
		System.out.println("selectCriteria : " + selectCriteria);
		
		List<DocumentDTO> expendList = docService.selectExpendList(selectCriteria);
		
		System.out.println("expendList : " + expendList);
		
		mv.addObject("expendList", expendList);
		mv.addObject("selectCriteria", selectCriteria);
		mv.addObject("auth", auth);
		mv.addObject("type","expand");
		mv.setViewName("/document/expend-docu");
		
		return mv;
	}
	
	@GetMapping("/draft")
	public ModelAndView SelectDraftList(@RequestParam(defaultValue = "1") int currentPage, @ModelAttribute SelectCriteria searchCriteria, ModelAndView mv,
			HttpServletRequest request) {
		
		String searchCondition = searchCriteria.getSearchCondition();
		String searchValue = searchCriteria.getSearchValue();
		
		int empNum = ((LoginDTO) request.getSession().getAttribute("loginMember")).getEmpnum();
		
		DocAuthorityDTO auth = docService.selectAuthDetail(empNum);
		
		Map<String, String> searchMap = new HashMap<>();
		searchMap.put("searchCondition", searchCondition);
		searchMap.put("searchValue", searchValue);
		
		System.out.println("컨트롤러에서 검색 조건 확인하기 : " + searchMap);
		
		
		int totalCount = docService.selectTotalCount(searchMap);
		
		System.out.println("totalAuthCount : " + totalCount);
		
		/* 한 페이지에 보여줄 게시물 수 */
		int limit = 10;
		
		/* 한 번에 보여질 페이징 버튼의 갯수*/
		int buttonAmount = 5; 
		
		/* 페이징 처리를 위한 로직 호출 후 페이징 처리에 관한 정보를 담고 있는 인스턴스를 반환*/
		SelectCriteria selectCriteria = null;
		
		if(searchCondition != null && !"".equals(searchCondition)) {
			
			selectCriteria = Pagenation.getSelectCriteria(currentPage, totalCount, limit, buttonAmount, searchCondition, searchValue);
		} else {
			selectCriteria = Pagenation.getSelectCriteria(currentPage, totalCount, limit, buttonAmount);
		}
		
		System.out.println("selectCriteria : " + selectCriteria);
		
		List<DocumentDTO> draftList = docService.selectDraftList(selectCriteria);
		
		System.out.println("draftList : " + draftList);
		
		mv.addObject("draftList", draftList);
		mv.addObject("selectCriteria", selectCriteria);
		mv.addObject("auth", auth);
		mv.addObject("type","draft");
		mv.setViewName("/document/draft-docu");
		
		return mv;
	}	
	
	@GetMapping("/suggest")
	public ModelAndView SelectSuggestList(@RequestParam(defaultValue = "1") int currentPage, @ModelAttribute SelectCriteria searchCriteria, ModelAndView mv,
			HttpServletRequest request) {
		
		String searchCondition = searchCriteria.getSearchCondition();
		String searchValue = searchCriteria.getSearchValue();
		
		int empNum = ((LoginDTO) request.getSession().getAttribute("loginMember")).getEmpnum();
		
		DocAuthorityDTO auth = docService.selectAuthDetail(empNum);
		
		Map<String, String> searchMap = new HashMap<>();
		searchMap.put("searchCondition", searchCondition);
		searchMap.put("searchValue", searchValue);
		
		System.out.println("컨트롤러에서 검색 조건 확인하기 : " + searchMap);
		
		
		int totalCount = docService.selectTotalCount(searchMap);
		
		System.out.println("totalAuthCount : " + totalCount);
		
		/* 한 페이지에 보여줄 게시물 수 */
		int limit = 10;
		
		/* 한 번에 보여질 페이징 버튼의 갯수*/
		int buttonAmount = 5; 
		
		/* 페이징 처리를 위한 로직 호출 후 페이징 처리에 관한 정보를 담고 있는 인스턴스를 반환*/
		SelectCriteria selectCriteria = null;
		
		if(searchCondition != null && !"".equals(searchCondition)) {
			
			selectCriteria = Pagenation.getSelectCriteria(currentPage, totalCount, limit, buttonAmount, searchCondition, searchValue);
		} else {
			selectCriteria = Pagenation.getSelectCriteria(currentPage, totalCount, limit, buttonAmount);
		}
		
		System.out.println("selectCriteria : " + selectCriteria);
		
		List<DocumentDTO> suggestList = docService.SelectSuggestList(selectCriteria);
		
		System.out.println("suggestList : " + suggestList);
		
		mv.addObject("suggestList", suggestList);
		mv.addObject("selectCriteria", selectCriteria);
		mv.addObject("auth", auth);
		mv.addObject("type","suggest");
		mv.setViewName("/document/suggest-docu");
		
		return mv;
	}
	
	@GetMapping("/report")
	public ModelAndView SelectReportList(@RequestParam(defaultValue = "1") int currentPage, @ModelAttribute SelectCriteria searchCriteria, ModelAndView mv,
			HttpServletRequest request) {
		
		String searchCondition = searchCriteria.getSearchCondition();
		String searchValue = searchCriteria.getSearchValue();
		
		int empNum = ((LoginDTO) request.getSession().getAttribute("loginMember")).getEmpnum();
		
		DocAuthorityDTO auth = docService.selectAuthDetail(empNum);
		
		Map<String, String> searchMap = new HashMap<>();
		searchMap.put("searchCondition", searchCondition);
		searchMap.put("searchValue", searchValue);
		
		System.out.println("컨트롤러에서 검색 조건 확인하기 : " + searchMap);
		
		
		int totalCount = docService.selectTotalCount(searchMap);
		
		System.out.println("totalAuthCount : " + totalCount);
		
		/* 한 페이지에 보여줄 게시물 수 */
		int limit = 10;
		
		/* 한 번에 보여질 페이징 버튼의 갯수*/
		int buttonAmount = 5; 
		
		/* 페이징 처리를 위한 로직 호출 후 페이징 처리에 관한 정보를 담고 있는 인스턴스를 반환*/
		SelectCriteria selectCriteria = null;
		
		if(searchCondition != null && !"".equals(searchCondition)) {
			
			selectCriteria = Pagenation.getSelectCriteria(currentPage, totalCount, limit, buttonAmount, searchCondition, searchValue);
		} else {
			selectCriteria = Pagenation.getSelectCriteria(currentPage, totalCount, limit, buttonAmount);
		}
		
		System.out.println("selectCriteria : " + selectCriteria);
		
		List<DocumentDTO> reportList = docService.SelectReportList(selectCriteria);
		
		System.out.println("reportList : " + reportList);
		
		mv.addObject("reportList", reportList);
		mv.addObject("selectCriteria", selectCriteria);
		mv.addObject("auth", auth);
		mv.addObject("type","report");
		mv.setViewName("/document/report-docu");
		
		return mv;
	}	
	
	@GetMapping("/attendance")
	public ModelAndView SelectAttendanceList(ModelAndView mv) {
		
		List<DocumentDTO> attendanceList = docService.selectAttendanceList();
		
		System.out.println("attendanceList : " + attendanceList);
		
		mv.addObject("attendanceList", attendanceList);
		mv.addObject("type","attendance");
		mv.setViewName("/document/attendance-docu");
		
		return mv;
	}		
	
	@GetMapping("/approvalDetail")
	public String selectApprovalDetail(@RequestParam int pmtNo, Model model) {
		
		ApprovalDTO approval = docService.selectApprovalDetail(pmtNo);
		
		model.addAttribute("approval", approval);
		
		return "/document/contents/approval-detail";
	}
	
	@GetMapping("/expendDetail")
	public String selectExpendDetail(@RequestParam int pmtNo, Model model) {
		
		CashDTO expend = docService.selectExpendDetail(pmtNo);
		
		List<CashUnitDTO> unitList = docService.selectUnitList(pmtNo);
		
		model.addAttribute("expend", expend);
		
		model.addAttribute("unitList", unitList);
		
		return "/document/contents/expend-detail";
	}
	
	@GetMapping("/draftDetail")
	public String selectDraftDetail(@RequestParam int pmtNo, Model model) {
		
		DraftDTO draft = docService.selectDraftDetail(pmtNo);
		
		model.addAttribute("draft", draft);
		
		return "/document/contents/draft-detail";
	}
	
	@GetMapping("/suggestDetail")
	public String selectSuggestDetail(@RequestParam int pmtNo, Model model) {
		
		ProposalDTO suggest = docService.selectSuggestDetail(pmtNo);
		
		List<ProductDTO> productList = docService.selectProductList(pmtNo);
		
		model.addAttribute("suggest", suggest);
		
		model.addAttribute("productList", productList);
		
		return "/document/contents/suggest-detail";
	}
	
	@GetMapping("/reportDetail")
	public String selectReportDetail(@RequestParam int pmtNo, Model model) {
		
		ReportDTO report = docService.selectReportDetail(pmtNo);
		
		model.addAttribute("report", report);
		
		return "/document/contents/report-detail";
	}
	@GetMapping("/attendanceDetail")
	public String selectAttendanceDetail() {
		
		return "/document/contents/attendance-detail";
	}
	
	@GetMapping("deleteDoc")
	public String deleteDoc(@RequestParam int docNo, RedirectAttributes rttr) throws DocRemoveException {
		
		docService.deleteDoc(docNo);
				
		rttr.addFlashAttribute("message", "문서가 삭제되었습니다.");
		
		return "redirect:/document/list";
	}
}
