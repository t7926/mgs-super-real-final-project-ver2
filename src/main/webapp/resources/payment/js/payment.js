window.onload = function () {

    const modal = document.getElementById("myModal");
    const btn = document.getElementById("select-modal");
    const span = document.getElementsByClassName("close")[0];

    btn.onclick = function () {
        modal.style.display = "block";
    }

    span.onclick = function () {
        modal.style.display = "none";
    }

    window.onclick = function (event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
}

function accessName(event) {
    const manager = document.querySelector("#manager");

    manager.innerText = event.target.value;
}

function modalDown() {
    const modal = document.getElementById("myModal");
    alert('담당자 선택을 완료하였습니다.');
    modal.style.display = "none";
}

function buttonClick(){
    let $tbodyDate = document.querySelector("#tbl1 tbody").children;
    let mergeTd = [];

    for(let i = 0; i < $tbodyDate.length; i++) {
        let trDate = $tbodyDate[i].children;

        let tdData = { pmtBriefs : trDate[0].children[0].value,
                       pmtAmount : trDate[1].children[0].value,
                       pmtNote : trDate[2].children[0].value,
        }

        mergeTd.push(tdData);
    }
    let pmtManager = document.querySelector('input[name="pmtManager"]:checked').value;
    let pmtDate = document.querySelector("#calendar").value;
    let pmtName = document.querySelector("#pmtName").value;
    let pmtDepartment = document.querySelector("#pmtDepartment").value;
    let pmtPosition = document.querySelector("#pmtPosition").value;
    let pmtExpenditure = document.querySelector("#pmtExpenditure").value;
    let pmtTitle = document.querySelector("#pmtTitle").value;

    $.ajax({
        url: '/mgs/payment/listCash',
        type:'post',
        data:{samp:JSON.stringify(mergeTd),
              pmtManager:JSON.stringify(pmtManager),
              pmtDate:pmtDate,
              pmtName:JSON.stringify(pmtName),
              pmtDepartment:JSON.stringify(pmtDepartment),
              pmtPosition:JSON.stringify(pmtPosition),
              pmtExpenditure:JSON.stringify(pmtExpenditure),
              pmtTitle:JSON.stringify(pmtTitle),
        },
        success: function (data) {
            alert("등록에 성공하였습니다.")
            location.href= "/mgs/payment/payment";
        }, error: function (e) {
            alert("등록에 성공하였습니다.")
            location.href= "/mgs/payment/payment";
        }
    });
}

function clickBtn() {
    let $tbodyDate = document.querySelector("#tbl1 tbody").children;
    let mergeTd = [];

    for(let i = 0; i < $tbodyDate.length; i++) {
        let trDate = $tbodyDate[i].children;

        let tdData = { prdNo : trDate[0].children[0].value,
            prdItem : trDate[1].children[0].value,
            prdUnit : trDate[2].children[0].value,
            prdCount : trDate[3].children[0].value,
            prdPrice : trDate[4].children[0].value,
            prdCompany : trDate[5].children[0].value,
            prdNote : trDate[6].children[0].value,
        }

        mergeTd.push(tdData);
    }

    let pmtManager = document.querySelector('input[name="pmtManager"]:checked').value;
    let pmtDate = document.querySelector("#calendar").value;
    let pmtName = document.querySelector("#pmtName").value;
    let pmtDepartment = document.querySelector("#pmtDepartment").value;
    let pmtPosition = document.querySelector("#pmtPosition").value;
    let pmtPhoNo = document.querySelector("#pmtPhoNo").value;
    let pmtContent = document.querySelector("#pmtContent").value;
    let pmtNote = document.querySelector("#pmtNote").value;

    $.ajax({
        url:'/mgs/payment/orderproposal',
        type:'post',
        data:{samp:JSON.stringify(mergeTd),
            pmtManager:JSON.stringify(pmtManager),
            pmtDate:pmtDate,
            pmtName:JSON.stringify(pmtName),
            pmtDepartment:JSON.stringify(pmtDepartment),
            pmtPosition:JSON.stringify(pmtPosition),
            pmtPhoNo:JSON.stringify(pmtPhoNo),
            pmtContent:JSON.stringify(pmtContent),
            pmtNote:JSON.stringify(pmtNote),
        },
        success: function (data) {
            alert("등록에 성공하였습니다.");
            location.href= "/mgs/payment/payment";
        }, error: function (data) {
            alert("등록에 성공하였습니다.");
            location.href= "/mgs/payment/payment";
        }
    });
}
