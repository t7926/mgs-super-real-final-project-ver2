window.onload = function(){


	if(document.getElementById("duplicationCheck")) {
	
		const $duplication = document.getElementById("duplicationCheck");
		$duplication.onclick = function(){
			
			let userId = document.getElementById("memberId").value.trim();
			$.ajax({
				url:"/mgs/member/idDupCheck",
				type:"post",
				data: {userId : userId},
				success: function(data){
					console.log(data);
					alert(data);
				},
				error:function(data) {
					console.log(data);
				}
				
			});
		}
	}
	
	
	
	
}

/*  유효성 검사 */
         function validate(){
			
            let id = document.getElementById("memberId");
            let pass = document.getElementById("pwd");
            let pass2 = document.getElementById("pwd2");
            let name = document.getElementById("name");
            let email = document.getElementById("email");
           

            // 이메일 검사
            // 4글자 이상이 나오고 @가 나오고 1글자 이상. 글자 1~3
            if(!chk(/^[\w]{4,}@[\w]+(\.[\w]+){1,3}$/,email,"이메일형식에 어긋납니다.")){
                return false;
            }
            // 아이디 검사
            // 첫글자는 반드시 영문 소문자, 총 4~12자로 이루어지고
            // 숫자가 반드시 하나 이상 포함되어야 한다.
            // 영문 소문자와 숫자로 이루어져야한다.
            if(!chk(/^[a-z][a-z\d]{3,11}$/,id,"아이디의 첫 글자는 영문 소문자, 4~12자 입력할 것!")){
                return false;
            }
            if(!chk(/[0-9]/,id,"아이디에 숫자 하나 이상 포함!")){
                return false;
            }
            
            // 비밀번호 검사
            
            if(!chk(/^(?=.*[a-zA-Z])(?=.*[!@#$%^*+=-])(?=.*[0-9]).{8,25}$/,pass,"영문자+숫자+특수조합(8~25자리 입력)")){
                  return false;
            }
            
            
            // 비밀번호 확인 검사
            // 비밀번호와 비밀번호 확인 값 일치
            if(pass.value != pass2.value){
                alert("비밀번호가 다릅니다");
                pass2.select();
                return false;
            }

            // 이름검사
            // 2글자 이상, 한글만
            if(!chk(/^[가-힣]{2,}$/, name, "이름은 한글로 2글자 이상을 넣으세요~")){
                return false;
            }    

            
        }

         function chk(re, ele, msg){
             if(!re.test(ele.value)){
                 alert(msg);
                 ele.select();
		
                 return false;
             }
             return true;
        }




	