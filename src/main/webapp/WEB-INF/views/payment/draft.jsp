<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Insert title here</title>
    <link href="${pageContext.request.contextPath}/resources/payment/css/reset.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/resources/payment/css/draft.css" rel="stylesheet" type="text/css">
    <script src="https://kit.fontawesome.com/0e493d8011.js" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/payment/js/payment.js"></script>
</head>
<body>
<header class="header">
    <nav class="gnb">
        <h1 class="logo"><a href="#">
            <img src="${pageContext.request.contextPath}/resources/payment/images/logo.png"  style="color: white"alt=""></a>
        </h1>
         <ul class="main-menu">
                <li>
                    <a href="${pageContext.request.contextPath}/payment/payment" style="color: white">전자결재</a>
                </li>
                <li>
                     <a href="${pageContext.request.contextPath}/board/list" style="color: white">게시판</a>
                </li>
                <li>
                     <a href="${pageContext.request.contextPath}/geuntae/calcul-time" style="color: white">근태관리</a>
                </li>
                <li>
                    <a href="#" style="color: white">조직도</a>
                </li>
                <li>
                     <a href="${ pageContext.servletContext.contextPath }/document/list" style="color: white">문서관리</a>
                </li>
            </ul>
    </nav>
</header>

<form name="draft" action="draft" method="post">
    <div class="container">
        <aside id="aside">
            <div class="title-box">
                <div class="icon-box">
                    <i class="fa-solid fa-computer"></i>
                </div>
                <h1 class="aside-title">전자 결재</h1>
            </div>
            <article class="payment-contents">
                <div class="title-box1">
                    <div class="icon-box1">
                        <img class="select-img" src="${pageContext.request.contextPath}/resources/payment/images/select.jpg">
                    </div>
                    <h2>결재양식</h2>
                </div>
               <ul class="push-box1">
                    <li><a href="${pageContext.request.contextPath}/payment/approval">품의서 등록</a></li>
                    <li><a href="${pageContext.request.contextPath}/payment/cash">지출결의서 등록</a></li>
                    <li><a href="${pageContext.request.contextPath}/payment/draft">기안서 등록</a></li>
                    <li><a href="${pageContext.request.contextPath}/payment/orderproposal.">제안서 등록</a></li>
                    <li><a href="${pageContext.request.contextPath}/payment/report">보고서 등록</a></li>
                    <li><a href="${pageContext.request.contextPath}/payment/attendance">근태 신청서 등록</a></li>
                    <li><a href="${pageContext.request.contextPath}/payment/handwriter">수기 작성 등록</a></li>
                </ul>
            </article>
            <articel class="document-contents">
                <div class="title-box1">
                    <div class="icon-box1">
                        <img class="select-img" src="${pageContext.request.contextPath}/resources/payment/images/select.jpg">
                    </div>
                    <h2>개인 문서함</h2>
                </div>
               <ul class="push-box2">
                    <li><a href="${pageContext.request.contextPath}/payment/payment-waiting">결제대기 문서</a></li>
                    <li><a href="${pageContext.request.contextPath}/payment/payment-complete">결재완료 문서</a></li>
                    <li><a href="${pageContext.request.contextPath}/payment/payment-temp">임시보관 문서</a></li>
                    <li><a href="${pageContext.request.contextPath}/payment/payment-reject">반려 문서</a></li>
                </ul>
            </articel>
        </aside>
        <section id="section1">
            <article id="contents1">
                <p><input class="calendar" type="date" name="pmtDate"></p>
                <h1 class="title">기안서</h1>
                <table>
                    <tr>
                        <th class="manager-id">담당</th>
                        <th class="ceo-id">사장</th>
                    </tr>
                    <tr>
                        <td>
                            <div id="manager"></div>
                                <button class="select-modal" id="select-modal" type="button">
                                <img class="select-img" src="${pageContext.request.contextPath}/resources/payment/images/select.jpg">
                            </button>
                        </td>
                        <td>사인</td>
                    </tr>
                </table>
            </article>
            <div id="contents2">
                <table>
                    <tr>
                        <th class="row1">기안 일자</th>
                        <td class="row2">
                            <p><input class="calendar" type="date" name="pmtDrafting"></p>
                        </td>
                        <th class="row1">시행 일자</th>
                        <td class="row2">
                            <p><input class="calendar" type="date" name="pmtEffective"></p>
                        </td>
                    </tr>
                    <tr>
                        <th class="row1">이 름</th>
                        <td class="row2">
                            <p><input class="calendar" type="text" name="pmtName" placeholder="이름을 입력해주세요" required></p>
                        </td>
                        <th class="row1">부 서</th>
                        <td class="row2">
                            <p><input class="calendar" type="text" name="pmtDepartment" placeholder="부서를 입력해주세요" required></p>
                        </td>
                    </tr>
                    <tr>
                        <th class="row1">보존 기한</th>
                        <td class="row3">
                            <p><input class="calendar" type="date" name="pmtRetention"></p>
                            <p>-</p>
                            <p><input class="calendar" type="date" name="pmtRetention1"></p>
                        </td>
                    </tr>
                    <tr>
                        <th class="row1">처리 기간</th>
                        <td class="row3">
                            <p><input class="calendar" type="date" name="pmtProcessing"></p>
                            <p>-</p>
                            <p><input class="calendar" type="date" name="pmtProcessing1"></p>
                        </td>
                    </tr>
                    <tr>
                        <th class="row1">제 목</th>
                        <td class="row3">
                            <input class="text-input" type="text" placeholder="제목을 입력해주세요" name="pmtTitle" required>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-box">
                            <textarea wrap="hard" cols="50" rows="100" name="pmtContents1"></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-box">
                            <textarea wrap="hard" cols="50" rows="100" name="pmtContents2"></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td class="text-box">
                            <textarea wrap="hard" cols="50" rows="100" name="pmtContents3"></textarea>
                        </td>
                    </tr>
                    <tr>
                        <th class="row5">회 사 명</th>
                        <td class="row6">
                            <input class="text-input" type="text" placeholder="회사명을 입력해주세요" required name="pmtCompany">
                        </td>
                    </tr>
                    <tr>
                        <th class="row5">사용 기간</th>
                        <td class="row6">
                            <input class="text-input" type="Date" placeholder="사용 기간을 입력해주세요" required name="pmtPeriodOfUse">
                            <p>-</p>
                            <input class="text-input" type="Date" placeholder="사용 기간을 입력해주세요" required name="pmtPeriodOfUse1">
                        </td>
                    </tr>
                    <tr>
                        <th class="row5">가입 비용</th>
                        <td class="row6">
                            <input class="text-input" type="text" placeholder="가입 비용을 입력해주세요" required name="pmtRegistrationFee">
                        </td>
                    </tr>
                    <tr>
                        <th class="row5">사용 범위</th>
                        <td class="row6">
                            <input class="text-input" type="text" placeholder="사용 범위를 입력해주세요" required name="pmtRangeOfUse">
                        </td>
                    </tr>
                    <tr>
                        <th class="row5">기대 효과</th>
                        <td class="row6">
                            <input class="text-input" type="text" placeholder="기대 효과를 입력해주세요" required name="pmtBenefit">
                        </td>
                    </tr>
                </table>
            </div>
            </article>
        </section>
        <div id="myModal" class="modal">
            <div class="modal-content">
                <div class="modal-header">
                    <span class="close">&times;</span>
                    <h2>담당자 검색창</h2>
                </div>
                <div class="modal-body">
                    <table class="modal-body-table">
                        <tr>
                            <td>이름</td>
                            <td>직급</td>
                            <td>부서</td>
                            <td>팀명</td>
                        </tr>
                        <c:forEach var="pmtManager" items="${ requestScope.pmtManager }">
                            <tr>
                                <td>
                                    <input type="radio" name="pmtManager" value="${pmtManager.empName}" id="empName" onclick="accessName(event);">
                                    <label for="empName">${pmtManager.empName}</label>
                                </td>
                                <td><c:out value="${pmtManager.levName}" /></td>
                                <td><c:out value="${pmtManager.depName}" /></td>
                                <td><c:out value="${pmtManager.teamName}" /></td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
                <div class="modal-footer">
                    <button class="accessBtn" type="button" onclick="modalDown();">확인</button>
                </div>
            </div>
        </div>
        <section id="section2">
            <button class="storage-btn" type="button">임시저장</button>
            <button class="storage-btn" type="submit">등록</button>
        </section>
    </div>
</form>

<footer>
    <div class="footer_logo"><img src="${pageContext.request.contextPath}/resources/payment/images/logo.png" alt=""></div>
    <div class="footer_desc">
        <p>
            MGS그룹웨어 주식회사<br>
            사업자등록번호 : 123-45-67890<br>
            통신판매업신고번호 : 제1234-서울강동-1234호
            주소 : 서울시 강동구 천호동 123로<br>
        </p>
    </div>
    <div class="footer_law">
        <a href="#">개인정보처리방침</a>
        <a href="#">이용약관</a>
    </div>
</footer>
</body>
</html>