<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link href="${pageContext.request.contextPath}/resources/payment/css/reset.css" rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/resources/common/css/common.css" rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/resources/payment/css/download-form.css" rel="stylesheet" type="text/css">
</head>
<body>
    <div id="layout">
        <header class="header">
            <nav class="gnb">
                <h1 class="logo"><a href="#">
                    <img src="${pageContext.request.contextPath}/resources/payment/images/logo.png" alt="">
                </a></h1>
                  <ul class="main-menu">
                <li>
                    <a href="${pageContext.request.contextPath}/payment/payment" style="color: white">전자결재</a>
                </li>
                <li>
                     <a href="${pageContext.request.contextPath}/board/list" style="color: white">게시판</a>
                </li>
                <li>
                     <a href="${pageContext.request.contextPath}/geuntae/calcul-time" style="color: white">근태관리</a>
                </li>
                <li>
                    <a href="#" style="color: white">조직도</a>
                </li>
                <li>
                     <a href="${ pageContext.servletContext.contextPath }/document/list" style="color: white">문서관리</a>
                </li>
            </ul>
            </ul>
            </nav>
        </header>
        <div id="container">
        <div id="wrap">
            <aside id="aside">
                <div class="title-box">
                    <div class="icon-box">
                        <i class="fa-solid fa-computer"></i>
                    </div>
                    <h1 class="aside-title">전자 결재</h1>
                </div>
                <article class="payment-contents">
                    <div class="title-box1">
                        <div class="icon-box1">
                            <img class="select-img" src="${pageContext.request.contextPath}/resources/payment/images/select.jpg">
                        </div>
                        <h2>결재양식</h2>
                    </div>
                   <ul class="push-box1">
                    <li><a href="${pageContext.request.contextPath}/payment/approval">품의서 등록</a></li>
                    <li><a href="${pageContext.request.contextPath}/payment/cash">지출결의서 등록</a></li>
                    <li><a href="${pageContext.request.contextPath}/payment/draft">기안서 등록</a></li>
                    <li><a href="${pageContext.request.contextPath}/payment/orderproposal.">제안서 등록</a></li>
                    <li><a href="${pageContext.request.contextPath}/payment/report">보고서 등록</a></li>
                    <li><a href="${pageContext.request.contextPath}/payment/attendance">근태 신청서 등록</a></li>
                    <li><a href="${pageContext.request.contextPath}/payment/handwriter">수기 작성 등록</a></li>
                </ul>
                </article>
                <articel class="document-contents">
                    <div class="title-box1">
                        <div class="icon-box1">
                            <img class="select-img" src="${pageContext.request.contextPath}/resources/payment/images/select.jpg">
                        </div>
                        <h2>개인 문서함</h2>
                    </div>
                      <ul class="push-box2">
                    <li><a href="${pageContext.request.contextPath}/payment/payment-waiting">결제대기 문서</a></li>
                    <li><a href="${pageContext.request.contextPath}/payment/payment-complete">결재완료 문서</a></li>
                    <li><a href="${pageContext.request.contextPath}/payment/payment-temp">임시보관 문서</a></li>
                    <li><a href="${pageContext.request.contextPath}/payment/payment-reject">반려 문서</a></li>
                </ul>
                </articel>
            </aside>
            <article>
                <div id="docu-top">
                    <h1>수기 양식 다운로드</h1>
                </div>
                <div id="table">
                    <table>
                        <tr>
                            <th>번호</th>
                            <th>제목</th>
                            <th>글쓴이</th>
                            <th>등록일</th>
                            <th>조회수</th>
                        </tr>
                        <tr>
                            <td>공지</td>
                            <td>기타문의는 경영지원팀에 문의 부탁드립니다.</td>
                            <td>관리자</td>
                            <td>22-03-11</td>
                            <td>200</td>
                        </tr>
                        <tr>
                            <td>1</td>
                            <td>근로 계약서 양식.hwp </td>
                            <td>관리자</td>
                            <td>22-01-01 </td>
                            <td>415</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>사직서 양식.hwp </td>
                            <td>관리자</td>
                            <td>22-01-01 </td>
                            <td>89</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>구매 영수증 양식.xlsm </td>
                            <td>관리자</td>
                            <td>17-06-15 </td>
                            <td>152</td>
                        </tr>
                        <tr>
                            <td>4</td>
                            <td>시말서 양식.hwp </td>
                            <td>관리자</td>
                            <td>20-02-15 </td>
                            <td>12</td>
                        </tr>
                        <tr>
                            <td>5</td>
                            <td>세금 계산서 양식.hwp </td>
                            <td>관리자</td>
                            <td>18-04-22 </td>
                            <td>82</td>
                        </tr>
                        <tr>
                            <td>6</td>
                            <td> </td>
                            <td> </td>
                            <td> </td>
                            <td> </td>
                        </tr>
                        <tr>
                            <td>7</td>
                            <td> </td>
                            <td> </td>
                            <td> </td>
                            <td> </td>
                        </tr>
                        <tr>
                            <td>8</td>
                            <td> </td>
                            <td> </td>
                            <td> </td>
                            <td> </td>
                        </tr>
                        <tr>
                            <td>9</td>
                            <td> </td>
                            <td> </td>
                            <td> </td>
                            <td> </td>
                        </tr>
                        <tr>
                            <td>10</td>
                            <td> </td>
                            <td> </td>
                            <td> </td>
                            <td> </td>
                        </tr>
                        <tr>
                            <td>11</td>
                            <td> </td>
                            <td> </td>
                            <td> </td>
                            <td> </td>
                        </tr>
                        <tr>
                            <td>12</td>
                            <td> </td>
                            <td> </td>
                            <td> </td>
                            <td> </td>
                        </tr>
                        <tr>
                            <td>13</td>
                            <td> </td>
                            <td> </td>
                            <td> </td>
                            <td> </td>
                        </tr>
                        <tr>
                            <td>14</td>
                            <td> </td>
                            <td> </td>
                            <td> </td>
                            <td> </td>
                        </tr>
                    </table>
                </div>
                <div id="paging">
                    <a><</a>
                    <a>이전</a>
                    <a>1</a>
                    <a>다음</a>
                    <a>></a>
                    <!-- <a></a>
                    <a></a>
                    <a></a>
                    <a></a>
                    <a></a>
                    <a></a>
                    <a></a>
                    <a></a>
                    <a></a>
                    <a></a> -->
                </div>
                <div id="search">
                    <tr>
                        <td>
                            <select name="document" size="1">
                                <option value="title">제목</option>
                            </select>
                        </td>
                    </tr>
                    <input type="text" name="searching" placeholder="검색어를 입력해주세요." size="50">
                    <button id="btn">검색</button>
                </div>
            </article>
        </div>
        </div>
        <footer>
            <div class="footer_logo"><img src="${pageContext.request.contextPath}/resources/payment/images/logo.png" alt=""></div>
            <div class="footer_desc">
                <p>
                    MGS그룹웨어 주식회사<br>
                    사업자등록번호 : 123-45-67890<br>
                    통신판매업신고번호 : 제1234-서울강동-1234호
                    주소 : 서울시 강동구 천호동 123로<br>
                </p>
            </div>
            <div class="footer_law">
                <a href="#">개인정보처리방침</a>
                <a href="#">이용약관</a>
    
            </div>
        </footer>
    </div>
</body>
</html>