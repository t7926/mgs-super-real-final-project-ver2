<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
    <link href="${pageContext.request.contextPath}/resources/payment/css/reset.css" rel="stylesheet" type="text/css">
	<link href="${pageContext.request.contextPath}/resources/payment/css/payment.css" rel="stylesheet" type="text/css">
	<script src="https://kit.fontawesome.com/0e493d8011.js" crossorigin="anonymous"></script>
</head>
<body>
	<header class="header">
    <nav class="gnb">
        <h1 class="logo"><a href="#">
            <img src="${pageContext.request.contextPath}/resources/payment/images/logo.png" alt=""></a>
        </h1>
        <ul class="main-menu">
                <li>
                    <a href="${pageContext.request.contextPath}/payment/payment" style="color: white">전자결재</a>
                </li>
                <li>
                     <a href="${pageContext.request.contextPath}/board/list" style="color: white">게시판</a>
                </li>
                <li>
                     <a href="${pageContext.request.contextPath}/geuntae/calcul-time" style="color: white">근태관리</a>
                </li>
                <li>
                    <a href="#" style="color: white">조직도</a>
                </li>
                <li>
                     <a href="${ pageContext.servletContext.contextPath }/document/list" style="color: white">문서관리</a>
                </li>
            </ul>
    </nav>
</header>
    <section id="section">
        <h1>전자 결재</h1>
        <article id="article1">
            <div class="contents">
                <i class="fa-solid fa-pen-nib"></i>
                <h2>품의서</h2>
                <a href="${pageContext.request.contextPath}/payment/approval" style="color: white">품의서 등록</a>
            </div>
            <div class="contents">
                <i class="fa-solid fa-money-bill"></i>
                <h2>지출 결의서</h2>
                <a href="${pageContext.request.contextPath}/payment/cash"  style="color: white">지출 결의서 등록</a>
            </div>
            <div class="contents">
                <i class="fa-solid fa-ballot-check"></i>
                <h2>제안서</h2>
                <a href="${pageContext.servletContext.contextPath}/payment/orderproposal"  style="color: white">제안서 등록</a>
            </div>
        </article>
        <article id="article">
            <div class="contents">
                <i class="fa-solid fa-pen-to-square"></i>
                <h2>기안서</h2>
                <a href="${pageContext.servletContext.contextPath}/payment/draft"  style="color: white">기안서 등록</a>
            </div>
            <div class="contents">
                <i class="fa-solid fa-chart-user"></i>
                <h2>보고서</h2>
                <a href="${pageContext.servletContext.contextPath}/payment/report"  style="color: white">보고서 등록</a>
            </div>
            <div class="contents">
                <i class="fa-solid fa-money-check-dollar-pen"></i>
                <h2>결재내역</h2>
                <a href="${pageContext.servletContext.contextPath}/payment/payment-history"  style="color: white">결재내역 조회</a>
            </div>
            <div class="contents">
                <i class="fa-solid fa-cloud-arrow-up"></i>
                <h2>수기작성</h2>
                <a href="${pageContext.servletContext.contextPath}/payment/handwriter"  style="color: white">수기 작성 등록</a>
                <a href="${pageContext.request.contextPath}/payment/download-form"  style="color: white">양식 다운로드</a>
            </div>
        </article>
        <article id="article3">
            <div class="contents-box">
                <div id="contents9" class="contents">
                    <i class="fa-solid fa-list-dropdown"></i>
                    <h2>결재 관리</h2>
                    <a href="${pageContext.request.contextPath}/payment/payment-manager"  style="color: white">결재 관리</a>
                </div>
                <div id="contents10" class="contents">
                    <i class="fa-solid fa-lock"></i>
                    <h2>개인문서함</h2>
                    <a href="${pageContext.request.contextPath}/payment/documentbox"  style="color: white">개인문서함</a>
                    <a href="${pageContext.request.contextPath}/payment/payment-temp"  style="color: white">임시 보관 문서</a>
                    <a href="${pageContext.request.contextPath}/payment/payment-waiting"  style="color: white">결재 대기 문서</a>
                    <a href="${pageContext.request.contextPath}/payment/payment-reject"  style="color: white">결재 반려 문서</a>
                    <a href="${pageContext.request.contextPath}/payment/payment-complete"  style="color: white">결재 완료 문서</a>
                </div>
            </div>
        </article>
    </section>
    <footer>
    <div class="footer_logo"><img src="${pageContext.request.contextPath}/resources/payment/images/logo.png" alt=""></div>
    <div class="footer_desc">
        <p>
            MGS그룹웨어 주식회사<br>
            사업자등록번호 : 123-45-67890<br>
            통신판매업신고번호 : 제1234-서울강동-1234호
            주소 : 서울시 강동구 천호동 123로<br>
        </p>
    </div>
    <div class="footer_law">
        <a href="#">개인정보처리방침</a>
        <a href="#">이용약관</a>

    </div>
</footer>
</body>
</html>