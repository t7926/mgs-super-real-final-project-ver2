<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
    <link href="${pageContext.request.contextPath}/resources/payment/css/reset.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/resources/payment/css/cash.css" rel="stylesheet" type="text/css">
    <script src="https://kit.fontawesome.com/0e493d8011.js" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/payment/js/payment.js"></script>

</head>
<body>
<header class="header"> 
    <nav class="gnb">
        <h1 class="logo"><a href="#">
            <img src="${pageContext.request.contextPath}/resources/payment/images/logo.png" style="color: white" alt=""></a>
        </h1>
        <ul class="main-menu">
                <li>
                    <a href="${pageContext.request.contextPath}/payment/payment" style="color: white">전자결재</a>
                </li>
                <li>
                     <a href="${pageContext.request.contextPath}/board/list" style="color: white">게시판</a>
                </li>
                <li>
                     <a href="${pageContext.request.contextPath}/geuntae/calcul-time" style="color: white">근태관리</a>
                </li>
                <li>
                    <a href="#" style="color: white">조직도</a>
                </li>
                <li>
                     <a href="${ pageContext.servletContext.contextPath }/document/list" style="color: white">문서관리</a>
                </li>
            </ul>
    </nav>
</header>
    <div class="container">
        <aside id="aside">
            <div class="title-box">
                <div class="icon-box">
                    <i class="fa-solid fa-computer"></i>
                </div>
                <h1 class="aside-title">전자 결재</h1>
            </div>
            <article class="payment-contents">
                <div class="title-box1">
                    <div class="icon-box1">
                        <img class="select-img" src="${pageContext.request.contextPath}/resources/payment/images/select.jpg">
                    </div>
                    <h2>결재양식</h2>
                </div>
                <ul class="push-box1">
                    <li><a href="${pageContext.request.contextPath}/payment/approval">품의서 등록</a></li>
                    <li><a href="${pageContext.request.contextPath}/payment/cash">지출결의서 등록</a></li>
                    <li><a href="${pageContext.request.contextPath}/payment/draft">기안서 등록</a></li>
                    <li><a href="${pageContext.request.contextPath}/payment/orderproposal.">제안서 등록</a></li>
                    <li><a href="${pageContext.request.contextPath}/payment/report">보고서 등록</a></li>
                    <li><a href="${pageContext.request.contextPath}/payment/attendance">근태 신청서 등록</a></li>
                    <li><a href="${pageContext.request.contextPath}/payment/handwriter">수기 작성 등록</a></li>
                </ul>
            </article>
            <article class="document-contents">
                <div class="title-box1">
                    <div class="icon-box1">
                        <img class="select-img" src="${pageContext.request.contextPath}/resources/payment/images/select.jpg">
                    </div>
                    <h2>개인 문서함</h2>
                </div>
                <ul class="push-box2">
                    <li><a href="${pageContext.request.contextPath}/payment/payment-waiting">결제대기 문서</a></li>
                    <li><a href="${pageContext.request.contextPath}/payment/payment-complete">결재완료 문서</a></li>
                    <li><a href="${pageContext.request.contextPath}/payment/payment-temp">임시보관 문서</a></li>
                    <li><a href="${pageContext.request.contextPath}/payment/payment-reject">반려 문서</a></li>
                </ul>
            </article>
        </aside>
        <section id="section1">
            <article id="contents1">
                <p><input id="calendar" name="pmtDate" type="date"></p>
                <h1 class="title">지출결의서</h1>
                <table>
                    <tr>
                        <th class="manager-id">담당</th>
                        <th class="ceo-id">사장</th>
                    </tr>
                    <tr>
                        <td>
                            <div id="manager"></div>
                            <button class="select-modal" id="select-modal" type="button">
                                <img class="select-img" src="${pageContext.request.contextPath}/resources/payment/images/select.jpg">
                            </button>
                        </td>
                        <td>사인</td>
                    </tr>
                </table>
            </article>
            <article id="contents2">
                <table>
                    <tr>
                        <th class="row1">성 명</th>
                        <td class="row2">
                            <input id="pmtName" class="text-input" name="pmtName" type="text" placeholder="이름을 입력해주세요" required>
                        </td>
                        <th class="row1">부 서</th>
                        <td class="row2">
                            <input id="pmtDepartment" class="text-input" name="pmtDepartment" type="text" placeholder="부서를 입력해주세요" required>
                        </td>
                        <th class="row1">직 책</th>
                        <td class="row2">
                            <input id="pmtPosition" class="text-input" name="pmtPosition" type="text" placeholder="직책을 입력해주세요" required>
                        </td>
                    </tr>
                    <tr>
                        <th class="row1">지출금액</th>
                        <td class="row4">
                            <input id="pmtExpenditure" class="text-input" name="pmtExpenditure" type="text" placeholder="금액을 입력해주세요" required>
                        </td>
                    </tr>
                    <tr>
                        <th class="row1">제 목</th>
                        <td class="row4">
                            <input id="pmtTitle" class="text-input" name="pmtTitle" type="text" placeholder="제목을 입력해주세요" required>
                        </td>
                    </tr>
                </table>
                <article id="contents3">
                    <div class="rowspan">
                        내 용
                    </div>
                    <table id="tbl1">
                        <thead>
                            <tr>
                                <td class="row5">적 요</td>
                                <td class="row5">금 액</td>
                                <td class="row5">비 고</td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="row6">
                                    <input class="text-input" name="pmtBriefs" type="text">
                                </td>
                                <td class="row6">
                                    <input class="text-input" name="pmtAmount" type="text">
                                </td>
                                <td class="row6">
                                    <input class="text-input" name="pmtNote" type="text">
                                </td>
                            </tr>
                            <tr>
                                <td class="row6">
                                    <input class="text-input" name="pmtBriefs" type="text">
                                </td>
                                <td class="row6">
                                    <input class="text-input" name="pmtAmount" type="text">
                                </td>
                                <td class="row6">
                                    <input class="text-input" name="pmtNote" type="text">
                                </td>
                            </tr>
                            <tr>
                                <td class="row6">
                                    <input class="text-input" name="pmtBriefs" type="text">
                                </td>
                                <td class="row6">
                                    <input class="text-input" name="pmtAmount" type="text">
                                </td>
                                <td class="row6">
                                    <input class="text-input" name="pmtNote" type="text">
                                </td>
                            </tr>
                            <tr>
                                <td class="row6">
                                    <input class="text-input" name="pmtBriefs" type="text">
                                </td>
                                <td class="row6">
                                    <input class="text-input" name="pmtAmount" type="text">
                                </td>
                                <td class="row6">
                                    <input class="text-input" name="pmtNote" type="text">
                                </td>
                            </tr>
                            <tr>
                                <td class="row6">
                                    <input class="text-input" name="pmtBriefs" type="text">
                                </td>
                                <td class="row6">
                                    <input class="text-input" name="pmtAmount" type="text">
                                </td>
                                <td class="row6">
                                    <input class="text-input" name="pmtNote" type="text">
                                </td>
                            </tr>
                            <tr>
                                <td class="row6">
                                    <input class="text-input" name="pmtBriefs" type="text">
                                </td>
                                <td class="row6">
                                    <input class="text-input" name="pmtAmount" type="text">
                                </td>
                                <td class="row6">
                                    <input class="text-input" name="pmtNote" type="text">
                                </td>
                            </tr>
                            <tr>
                                <td class="row6">
                                    <input class="text-input" name="pmtBriefs" type="text">
                                </td>
                                <td class="row6">
                                    <input class="text-input" name="pmtAmount" type="text">
                                </td>
                                <td class="row6">
                                    <input class="text-input" name="pmtNote" type="text">
                                </td>
                            </tr>
                            <tr>
                                <td class="row6">
                                    <input class="text-input" name="pmtBriefs" type="text">
                                </td>
                                <td class="row6">
                                    <input class="text-input" name="pmtAmount" type="text">
                                </td>
                                <td class="row6">
                                    <input class="text-input" name="pmtNote" type="text">
                                </td>
                            </tr>
                            <tr>
                                <td class="row6">
                                    <input class="text-input" name="pmtBriefs" type="text">
                                </td>
                                <td class="row6">
                                    <input class="text-input" name="pmtAmount" type="text">
                                </td>
                                <td class="row6">
                                    <input class="text-input" name="pmtNote" type="text">
                                </td>
                            </tr>
                            <tr>
                                <td class="row6">
                                    <input class="text-input" name="pmtBriefs" type="text">
                                </td>
                                <td class="row6">
                                    <input class="text-input" name="pmtAmount" type="text">
                                </td>
                                <td class="row6">
                                    <input class="text-input" name="pmtNote" type="text">
                                </td>
                            </tr>
                            <tr>
                                <td class="row6">
                                    <input class="text-input" name="pmtBriefs" type="text">
                                </td>
                                <td class="row6">
                                    <input class="text-input" name="pmtAmount" type="text">
                                </td>
                                <td class="row6">
                                    <input class="text-input" name="pmtNote" type="text">
                                </td>
                            </tr>
                            <tr>
                                <td class="row6">
                                    <input class="text-input" name="pmtBriefs" type="text">
                                </td>
                                <td class="row6">
                                    <input class="text-input" name="pmtAmount" type="text">
                                </td>
                                <td class="row6">
                                    <input class="text-input" name="pmtNote" type="text">
                                </td>
                            </tr>
                            <tr>
                                <td class="row6">
                                    <input class="text-input" name="pmtBriefs" type="text">
                                </td>
                                <td class="row6">
                                    <input class="text-input" name="pmtAmount" type="text">
                                </td>
                                <td class="row6">
                                    <input class="text-input" name="pmtNote" type="text">
                                </td>
                            </tr>
                            <tr>
                                <td class="row6">
                                    <input class="text-input" name="pmtBriefs" type="text">
                                </td>
                                <td class="row6">
                                    <input class="text-input" name="pmtAmount" type="text">
                                </td>
                                <td class="row6">
                                    <input class="text-input" name="pmtNote" type="text">
                                </td>
                            </tr>
                            <tr>
                                <td class="row6">
                                    <input class="text-input" name="pmtBriefs" type="text">
                                </td>
                                <td class="row6">
                                    <input class="text-input" name="pmtAmount" type="text">
                                </td>
                                <td class="row6">
                                    <input class="text-input" name="pmtNote" type="text">
                                </td>
                            </tr>
                            <tr>
                                <td class="row6">
                                    <input class="text-input" name="pmtBriefs" type="text">
                                </td>
                                <td class="row6">
                                    <input class="text-input" name="pmtAmount" type="text">
                                </td>
                                <td class="row6">
                                    <input class="text-input" name="pmtNote" type="text">
                                </td>
                            </tr>
                            <tr>
                                <td class="row6">
                                    <input class="text-input" name="pmtBriefs" type="text">
                                </td>
                                <td class="row6">
                                    <input class="text-input" name="pmtAmount" type="text">
                                </td>
                                <td class="row6">
                                    <input class="text-input" name="pmtNote" type="text">
                                </td>
                            </tr>
                            <tr>
                                <td class="row6">
                                    <input class="text-input" name="pmtBriefs" type="text">
                                </td>
                                <td class="row6">
                                    <input class="text-input" name="pmtAmount" type="text">
                                </td>
                                <td class="row6">
                                    <input class="text-input" name="pmtNote" type="text">
                                </td>
                            </tr>
                            <tr>
                                <td class="row6">
                                    <input class="text-input" name="pmtBriefs" type="text">
                                </td>
                                <td class="row6">
                                    <input class="text-input" name="pmtAmount" type="text">
                                </td>
                                <td class="row6">
                                    <input class="text-input" name="pmtNote" type="text">
                                </td>
                            </tr>
                            <tr>
                                <td class="row6">
                                    <input class="text-input" name="pmtBriefs" type="text">
                                </td>
                                <td class="row6">
                                    <input class="text-input" name="pmtAmount" type="text">
                                </td>
                                <td class="row6">
                                    <input class="text-input" name="pmtNote" type="text">
                                </td>
                            </tr>
                            <tr>
                                <td class="row6">
                                    <input class="text-input" name="pmtBriefs" type="text">
                                </td>
                                <td class="row6">
                                    <input class="text-input" name="pmtAmount" type="text">
                                </td>
                                <td class="row6">
                                    <input class="text-input" name="pmtNote" type="text">
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </article>
            </article>
        </section>
        <div id="myModal" class="modal">
            <div class="modal-content">
                <div class="modal-header">
                    <span class="close">&times;</span>
                    <h2>담당자 검색창</h2>
                </div>
                <div class="modal-body">
                    <table class="modal-body-table">
                        <tr>
                            <td>이름</td>
                            <td>직급</td>
                            <td>부서</td>
                            <td>팀명</td>
                        </tr>
                        <c:forEach var="pmtManager" items="${ requestScope.pmtManager }">
                            <tr>
                                <td>
                                    <input type="radio" name="pmtManager" value="${pmtManager.empName}" id="empName" onclick="accessName(event);">
                                    <label for="empName">${pmtManager.empName}</label>
                                </td>
                                <td><c:out value="${pmtManager.levName}" /></td>
                                <td><c:out value="${pmtManager.depName}" /></td>
                                <td><c:out value="${pmtManager.teamName}" /></td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
                <div class="modal-footer">
                    <button class="accessBtn" type="button" onclick="modalDown();">확인</button>
                </div>
            </div>
        </div>
        <section id="section2">
            <button class="storage-btn" type="button"onclick="location.href='#'">임시저장</button>
            <button class="storage-btn" onclick="buttonClick();">등록</button>
        </section>
    </div>
<footer>
    <div class="footer_logo"><img src="${pageContext.request.contextPath}/resources/payment/images/logo.png" alt=""></div>
    <div class="footer_desc">
        <p>
            MGS그룹웨어 주식회사<br>
            사업자등록번호 : 123-45-67890<br>
            통신판매업신고번호 : 제1234-서울강동-1234호
            주소 : 서울시 강동구 천호동 123로<br>
        </p>
    </div>
    <div class="footer_law">
        <a href="#">개인정보처리방침</a>
        <a href="#">이용약관</a>
    </div>
</footer>
</body>
</html>