<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
    <link href="${pageContext.request.contextPath}/resources/payment/css/reset.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/resources/payment/css/orderproposal.css" rel="stylesheet" type="text/css">
    <script src="https://kit.fontawesome.com/0e493d8011.js" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/payment/js/payment.js"></script>
</head>
<body>
<header class="header">
    <nav class="gnb">
        <h1 class="logo">
        	<a href="#">
            	<img src="${pageContext.request.contextPath}/resources/payment/images/logo.png" style="color: white" alt="">
            </a>
        </h1>
        <ul class="main-menu">
                <li>
                    <a href="${pageContext.request.contextPath}/payment/payment" style="color: white">전자결재</a>
                </li>
                <li>
                     <a href="${pageContext.request.contextPath}/board/list" style="color: white">게시판</a>
                </li>
                <li>
                     <a href="${pageContext.request.contextPath}/geuntae/calcul-time" style="color: white">근태관리</a>
                </li>
                <li>
                    <a href="#" style="color: white">조직도</a>
                </li>
                <li>
                     <a href="${ pageContext.servletContext.contextPath }/document/list" style="color: white">문서관리</a>
                </li>
            </ul>
    </nav>
</header>

<div class="container">
    <aside id="aside">
        <div class="title-box">
            <div class="icon-box">
                <i class="fa-solid fa-computer"></i>
            </div>
            <h1 class="aside-title">전자 결재</h1>
        </div>
        <article class="payment-contents">
            <div class="title-box1">
                <div class="icon-box1">
                    <img class="select-img" src="${pageContext.request.contextPath}/resources/payment/images/select.jpg">
                </div>
                <h2>결재양식</h2>
            </div>
            <ul class="push-box1">
                    <li><a href="${pageContext.request.contextPath}/payment/approval">품의서 등록</a></li>
                    <li><a href="${pageContext.request.contextPath}/payment/cash">지출결의서 등록</a></li>
                    <li><a href="${pageContext.request.contextPath}/payment/draft">기안서 등록</a></li>
                    <li><a href="${pageContext.request.contextPath}/payment/orderproposal.">제안서 등록</a></li>
                    <li><a href="${pageContext.request.contextPath}/payment/report">보고서 등록</a></li>
                    <li><a href="${pageContext.request.contextPath}/payment/attendance">근태 신청서 등록</a></li>
                    <li><a href="${pageContext.request.contextPath}/payment/handwriter">수기 작성 등록</a></li>
                </ul>
        </article>
        <article class="document-contents">
            <div class="title-box1">
                <div class="icon-box1">
                    <img class="select-img" src="${pageContext.request.contextPath}/resources/payment/images/select.jpg">
                </div>
                <h2>개인 문서함</h2>
            </div>
            <ul class="push-box2">
                    <li><a href="${pageContext.request.contextPath}/payment/payment-waiting">결제대기 문서</a></li>
                    <li><a href="${pageContext.request.contextPath}/payment/payment-complete">결재완료 문서</a></li>
                    <li><a href="${pageContext.request.contextPath}/payment/payment-temp">임시보관 문서</a></li>
                    <li><a href="${pageContext.request.contextPath}/payment/payment-reject">반려 문서</a></li>
                </ul>
        </article>
    </aside>
	<section id="section1">
        <article id="contents1">
            <p><input id="calendar" type="date" name="pmtDate"></p>
            <h1 class="title">구매 제안서</h1>
            <table>
                <tr>
                    <th class="manager-id">담당</th>
                    <th class="ceo-id">사장</th>
                </tr>
                <tr>
                    <td>
                        <div id="manager"></div>
                        <button class="select-modal" id="select-modal" type="button">
                            <img class="select-img" src="${pageContext.request.contextPath}/resources/payment/images/select.jpg">
                        </button>
                    </td>
                    <td>사인</td>
                </tr>
            </table>
        </article>
        <article id="contents2">
            <table id="tbl1">
                <thead>
                <tr>
                    <th class="row1">성명</th>
                    <td class="row2">
                        <input id="pmtName" class="text-input" type="text" name="pmtName" placeholder="성함을 입력해주세요" required>
                    </td>
                    <th class="row1">부서</th>
                    <td class="row2">
                        <input id="pmtDepartment" class="text-input" type="text" name="pmtDepartment" placeholder="부서를 입력해주세요" required>
                    </td>
                </tr>
                <tr>
                    <th class="row1">직책</th>
                    <td class="row2">
                        <input id="pmtPosition" class="text-input" type="text" name="pmtPosition" placeholder="성함을 입력해주세요" required>
                    </td>
                    <th class="row1">연락처</th>
                    <td class="row2">
                        <input id="pmtPhoNo" class="text-input" type="text" name="pmtPhoNo" placeholder="연락처를 입력해주세요" required>
                    </td>
                </tr>
                <tr>
                    <th class="row3">제안내용</th>
                </tr>
                <tr>
                    <td class="text-box">
                        <textarea id="pmtContent" name="pmtContent" cols="50" rows="100" placeholder="내용을 입력해주세요" required></textarea>
                    </td>
                </tr>
                <tr>
                    <th class="row3">제안물품</th>
                </tr>

                    <tr>
                        <th class="row4">No</th>
                        <th class="row5">품 명</th>
                        <th class="row6">단 가</th>
                        <th class="row6">수 량</th>
                        <th class="row6">금 액</th>
                        <th class="row6">제조사</th>
                        <th class="row7">비 고</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="row4">
                            <input class="text-input1" type="text" maxlength="2" name="prdNo">
                        </td>
                        <td class="row5">
                            <input class="text-input" type="text" name="prdItem">
                        </td>
                        <td class="row6">
                            <input class="text-input" type="text" name="prdUnit">
                        </td>
                        <td class="row6">
                            <input class="text-input" type="text" name="prdCount">
                        </td>
                        <td class="row6">
                            <input class="text-input" type="text" name="prdPrice">
                        </td>
                        <td class="row6">
                            <input class="text-input" type="text" name="prdCompany">
                        </td>
                        <td class="row7">
                            <input class="text-input" type="text" name="prdNote">
                        </td>
                    </tr>
                    <tr>
                        <td class="row4">
                            <input class="text-input1" type="text" maxlength="2" name="prdNo">
                        </td>
                        <td class="row5">
                            <input class="text-input" type="text" name="prdItem">
                        </td>
                        <td class="row6">
                            <input class="text-input" type="text" name="prdUnit">
                        </td>
                        <td class="row6">
                            <input class="text-input" type="text" name="prdCount">
                        </td>
                        <td class="row6">
                            <input class="text-input" type="text" name="prdPrice">
                        </td>
                        <td class="row6">
                            <input class="text-input" type="text" name="prdCompany">
                        </td>
                        <td class="row7">
                            <input class="text-input" type="text" name="prdNote">
                        </td>
                    </tr>
                    <tr>
                        <td class="row4">
                            <input class="text-input1" type="text" maxlength="2" name="prdNo">
                        </td>
                        <td class="row5">
                            <input class="text-input" type="text" name="prdItem">
                        </td>
                        <td class="row6">
                            <input class="text-input" type="text" name="prdUnit">
                        </td>
                        <td class="row6">
                            <input class="text-input" type="text" name="prdCount">
                        </td>
                        <td class="row6">
                            <input class="text-input" type="text" name="prdPrice">
                        </td>
                        <td class="row6">
                            <input class="text-input" type="text" name="prdCompany">
                        </td>
                        <td class="row7">
                            <input class="text-input" type="text" name="prdNote">
                        </td>
                    </tr>
                    <tr>
                        <td class="row4">
                            <input class="text-input1" type="text" maxlength="2" name="prdNo">
                        </td>
                        <td class="row5">
                            <input class="text-input" type="text" name="prdItem">
                        </td>
                        <td class="row6">
                            <input class="text-input" type="text" name="prdUnit">
                        </td>
                        <td class="row6">
                            <input class="text-input" type="text" name="prdCount">
                        </td>
                        <td class="row6">
                            <input class="text-input" type="text" name="prdPrice">
                        </td>
                        <td class="row6">
                            <input class="text-input" type="text" name="prdCompany">
                        </td>
                        <td class="row7">
                            <input class="text-input" type="text" name="prdNote">
                        </td>
                    </tr>
                    <tr>
                        <td class="row4">
                            <input class="text-input1" type="text" maxlength="2" name="prdNo">
                        </td>
                        <td class="row5">
                            <input class="text-input" type="text" name="prdItem">
                        </td>
                        <td class="row6">
                            <input class="text-input" type="text" name="prdUnit">
                        </td>
                        <td class="row6">
                            <input class="text-input" type="text" name="prdCount">
                        </td>
                        <td class="row6">
                            <input class="text-input" type="text" name="prdPrice">
                        </td>
                        <td class="row6">
                            <input class="text-input" type="text" name="prdCompany">
                        </td>
                        <td class="row7">
                            <input class="text-input" type="text" name="prdNote">
                        </td>
                    </tr>
                    <tr>
                        <td class="row4">
                            <input class="text-input1" type="text" maxlength="2" name="prdNo">
                        </td>
                        <td class="row5">
                            <input class="text-input" type="text" name="prdItem">
                        </td>
                        <td class="row6">
                            <input class="text-input" type="text" name="prdUnit">
                        </td>
                        <td class="row6">
                            <input class="text-input" type="text" name="prdCount">
                        </td>
                        <td class="row6">
                            <input class="text-input" type="text" name="prdPrice">
                        </td>
                        <td class="row6">
                            <input class="text-input" type="text" name="prdCompany">
                        </td>
                        <td class="row7">
                            <input class="text-input" type="text" name="prdNote">
                        </td>
                    </tr>
                    <tr>
                        <td class="row4">
                            <input class="text-input1" type="text" maxlength="2" name="prdNo">
                        </td>
                        <td class="row5">
                            <input class="text-input" type="text" name="prdItem">
                        </td>
                        <td class="row6">
                            <input class="text-input" type="text" name="prdUnit">
                        </td>
                        <td class="row6">
                            <input class="text-input" type="text" name="prdCount">
                        </td>
                        <td class="row6">
                            <input class="text-input" type="text" name="prdPrice">
                        </td>
                        <td class="row6">
                            <input class="text-input" type="text" name="prdCompany">
                        </td>
                        <td class="row7">
                            <input class="text-input" type="text" name="prdNote">
                        </td>
                    </tr>
                    <tr>
                        <td class="row4">
                            <input class="text-input1" type="text" maxlength="2" name="prdNo">
                        </td>
                        <td class="row5">
                            <input class="text-input" type="text" name="prdItem">
                        </td>
                        <td class="row6">
                            <input class="text-input" type="text" name="prdUnit">
                        </td>
                        <td class="row6">
                            <input class="text-input" type="text" name="prdCount">
                        </td>
                        <td class="row6">
                            <input class="text-input" type="text" name="prdPrice">
                        </td>
                        <td class="row6">
                            <input class="text-input" type="text" name="prdCompany">
                        </td>
                        <td class="row7">
                            <input class="text-input" type="text" name="prdNote">
                        </td>
                    </tr>
                    <tr>
                        <td class="row4">
                            <input class="text-input1" type="text" maxlength="2" name="prdNo">
                        </td>
                        <td class="row5">
                            <input class="text-input" type="text" name="prdItem">
                        </td>
                        <td class="row6">
                            <input class="text-input" type="text" name="prdUnit">
                        </td>
                        <td class="row6">
                            <input class="text-input" type="text" name="prdCount">
                        </td>
                        <td class="row6">
                            <input class="text-input" type="text" name="prdPrice">
                        </td>
                        <td class="row6">
                            <input class="text-input" type="text" name="prdCompany">
                        </td>
                        <td class="row7">
                            <input class="text-input" type="text" name="prdNote">
                        </td>
                    </tr>
                    <tr>
                        <td class="row4">
                            <input class="text-input1" type="text" maxlength="2" name="prdNo">
                        </td>
                        <td class="row5">
                            <input class="text-input" type="text" name="prdItem">
                        </td>
                        <td class="row6">
                            <input class="text-input" type="text" name="prdUnit">
                        </td>
                        <td class="row6">
                            <input class="text-input" type="text" name="prdCount">
                        </td>
                        <td class="row6">
                            <input class="text-input" type="text" name="prdPrice">
                        </td>
                        <td class="row6">
                            <input class="text-input" type="text" name="prdCompany">
                        </td>
                        <td class="row7">
                            <input class="text-input" type="text" name="prdNote">
                        </td>
                    </tr>
                    <tr>
                        <td class="row4">
                            <input class="text-input1" type="text" maxlength="2" name="prdNo">
                        </td>
                        <td class="row5">
                            <input class="text-input" type="text" name="prdItem">
                        </td>
                        <td class="row6">
                            <input class="text-input" type="text" name="prdUnit">
                        </td>
                        <td class="row6">
                            <input class="text-input" type="text" name="prdCount">
                        </td>
                        <td class="row6">
                            <input class="text-input" type="text" name="prdPrice">
                        </td>
                        <td class="row6">
                            <input class="text-input" type="text" name="prdCompany">
                        </td>
                        <td class="row7">
                            <input class="text-input" type="text" name="prdNote">
                        </td>
                    </tr>
                </tbody>
                <tr>
                    <th class="row3">참고사항</th>
                </tr>
                <tr>
                    <td class="text-box">
                        <textarea id="pmtNote" name="pmtNote" cols="50" rows="100" placeholder="내용을 입력해주세요" required></textarea>
                    </td>
                </tr>
            </table>
        </article>
    </section>
    <div id="myModal" class="modal">
        <div class="modal-content">
            <div class="modal-header">
                <span class="close">&times;</span>
                <h2>담당자 검색창</h2>
            </div>
            <div class="modal-body">
                <table class="modal-body-table">
                    <tr>
                        <td>이름</td>
                        <td>직급</td>
                        <td>부서</td>
                        <td>팀명</td>
                    </tr>
                    <c:forEach var="pmtManager" items="${ requestScope.pmtManager }">
                        <tr>
                            <td>
                                <input type="radio" name="pmtManager" value="${pmtManager.empName}" id="empName" onclick="accessName(event);">
                                <label for="empName">${pmtManager.empName}</label>
                            </td>
                            <td><c:out value="${pmtManager.levName}" /></td>
                            <td><c:out value="${pmtManager.depName}" /></td>
                            <td><c:out value="${pmtManager.teamName}" /></td>
                        </tr>
                    </c:forEach>
                </table>
            </div>
            <div class="modal-footer">
                <button class="accessBtn" type="button" onclick="modalDown();">확인</button>
            </div>
        </div>
    </div>
    <section id="section2">
        <button class="storage-btn" type="button">임시저장</button>
        <button class="storage-btn" onclick="clickBtn();">등록</button>
    </section>
</div>

<footer>
    <div class="footer_logo"><img src="${pageContext.request.contextPath}/resources/payment/images/logo.png" alt=""></div>
    <div class="footer_desc">
        <p>
            MGS그룹웨어 주식회사<br>
            사업자등록번호 : 123-45-67890<br>
            통신판매업신고번호 : 제1234-서울강동-1234호
            주소 : 서울시 강동구 천호동 123로<br>
        </p>
    </div>
    <div class="footer_law">
        <a href="#">개인정보처리방침</a>
        <a href="#">이용약관</a>
    </div>
</footer>
</body>
</html>