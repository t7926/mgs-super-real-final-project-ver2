<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
    <link href="${pageContext.request.contextPath}/resources/payment/css/reset.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/resources/payment/css/approval.css" rel="stylesheet" type="text/css">
    <script src="https://kit.fontawesome.com/0e493d8011.js" crossorigin="anonymous"></script>
    <script src="${pageContext.request.contextPath}/resources/payment/js/payment.js"></script>

</head>
<body>
<header class="header">
    <nav class="gnb">
        <h1 class="logo"><a href="#">
            <img src="${pageContext.request.contextPath}/resources/payment/images/logo.png" alt="" style="color: white"></a>
        </h1>
        <ul class="main-menu">
            <li>
					<a href="${pageContext.request.contextPath}/payment/payment" style="color: white">전자결재</a>
				</li>
				<li>
					 <a href="${pageContext.request.contextPath}/board/list" style="color: white">게시판</a>
				</li>
				<li>
					<a href="${pageContext.request.contextPath}/geuntae/calcul-time" style="color: white">근태관리</a>
				</li>
				<li>
					<a href="#" style="color: white">조직도</a>
				</li>
				<li>
					<a href="${ pageContext.servletContext.contextPath }/document/list" style="color: white">문서관리</a>
				</li>
        </ul>
    </nav>
</header>
<form name="approval" action="approval" method="post">
    <div class="container">
        <aside id="aside">
            <div class="title-box">
                <div class="icon-box">
                    
                </div>
                <h1 class="aside-title">전자 결재</h1>
            </div>
            <article class="payment-contents">
                <div class="title-box1">
                    <div class="icon-box1">
                        <img class="select-img" src="${pageContext.request.contextPath}/resources/payment/images/select.jpg">
                    </div>
                    <h2>결재양식</h2>
                </div>
                <ul class="push-box1">
                    <li><a href="${pageContext.request.contextPath}/payment/approval">품의서 등록</a></li>
                    <li><a href="${pageContext.request.contextPath}/payment/cash">지출결의서 등록</a></li>
                    <li><a href="${pageContext.request.contextPath}/payment/draft">기안서 등록</a></li>
                    <li><a href="${pageContext.request.contextPath}/payment/orderproposal">제안서 등록</a></li>
                    <li><a href="${pageContext.request.contextPath}/payment/report">보고서 등록</a></li>
                    <li><a href="${pageContext.request.contextPath}/payment/handwriter">수기 작성 등록</a></li>
                </ul>
            </article>
            <article class="document-contents">
                <div class="title-box1">
                    <div class="icon-box1">
                        <img class="select-img" src="${pageContext.request.contextPath}/resources/payment/images/select.jpg">
                    </div>
                    <h2>개인 문서함</h2>
                </div>
                <ul class="push-box2">
                    <li><a href="${pageContext.request.contextPath}/payment/payment-waiting">결제대기 문서</a></li>
                    <li><a href="${pageContext.request.contextPath}/payment/payment-complete">결재완료 문서</a></li>
                    <li><a href="${pageContext.request.contextPath}/payment/payment-temp">임시보관 문서</a></li>
                    <li><a href="${pageContext.request.contextPath}/payment/payment-reject">반려 문서</a></li>
                </ul>
            </article>
        </aside>
        <section id="section1">
            <article id="contents1">
                <p><input id="calendar" type="date" name="pmtDate"></p>
                <h1 class="title">품의서</h1>
                <table>
                    <tr>
                        <th class="manager-id">담당</th>
                        <th class="ceo-id">사장</th>
                    </tr>
                    <tr>
                        <td>
                            <div id="manager"></div>
                            <button class="select-modal" id="select-modal" type="button">
                                <img class="select-img" src="${pageContext.request.contextPath}/resources/payment/images/select.jpg">
                            </button>
                        </td>
                        <td>사인</td>
                    </tr>
                </table>
            </article>
            <article id="contents2">
                <table>
                    <tr>
                        <th class="row1">문서번호</th>
                        <td class="row2">
                            ${pmtNo}
                        </td>
                        <th class="row1">기안일자</th>
                        <td class="row2">
                            <input id="calendar1" type="date" name="pmtDrafting">
                        </td>
                    </tr>
                    <tr>
                        <th class="row1">부 서</th>
                        <td class="row2">
                            <input class="text-input" type="text" placeholder="부서를 입력해주세요" name="pmtPosition"required>
                        </td>
                        <th class="row1">기 안 자</th>
                        <td class="row2">
                            <input class="text-input" type="text" placeholder="기안자를 입력해주세요" name="pmtName" required>
                        </td>
                    </tr>
                    <tr>
                        <th class="row3">
                            <input class="text-input2" type="text" placeholder="제목을 입력해주세요" name="pmtTitle" required>
                        </th>
                    </tr>
                    <tr>
                        <td class="text-box">
                            <textarea cols="50" rows="100" placeholder="내용을 입력해주세요" name="pmtContents" required></textarea>
                        </td>
                    </tr>
                    <tr>
                        <th class="row3">결재자 부가사항</th>
                    </tr>
                    <tr>
                        <th class="row1">부서장</th>
                        <td class="row4">
                            <input class="text-input1" type="text" placeholder="부가사항을 입력해주세요(필수사항 아님)" name="pmtAddOn1">
                        </td>
                    </tr>
                    <tr>
                        <th class="row1">임 원</th>
                        <td class="row4">
                            <input class="text-input1" type="text" placeholder="부가사항을 입력해주세요(필수사항 아님)" name="pmtAddOn2">
                        </td>
                    </tr>
                    <tr>
                        <th class="row1">사 장</th>
                        <td class="row4">
                            <input class="text-input1" type="text" placeholder="부가사항을 입력해주세요(필수사항 아님)" name="pmtAddOn3">
                        </td>
                    </tr>
                    <tr>
                        <th class="row3">지 시 사 항</th>
                    </tr>
                    <tr>
                        <td class="text-box">
                            <textarea cols="50" rows="100" placeholder="내용을 입력해주세요" name="pmtInstruction" required></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td class="row3">* 이 품의서는 기안자가 지정한 품의 최종 결재자에게 관련 부서장을 거쳐 필히 결재받아야 합니다.</td>
                    </tr>
                </table>
            </article>
        </section>
        <div id="myModal" class="modal">
            <div class="modal-content">
                <div class="modal-header">
                    <span class="close">&times;</span>
                    <h2>담당자 검색창</h2>
                </div>
                <div class="modal-body">
                    <table class="modal-body-table">
                        <tr>
                            <td>이름</td>
                            <td>직급</td>
                            <td>부서</td>
                            <td>팀명</td>
                        </tr>
                        <c:forEach var="pmtManager" items="${ requestScope.pmtManager }">
                            <tr>
                                <td>
                                    <input type="radio" name="pmtManager" value="${pmtManager.empName}" id="empName" onclick="accessName(event);">
                                    <label for="empName">${pmtManager.empName}</label>
                                </td>
                                <td><c:out value="${pmtManager.levName}" /></td>
                                <td><c:out value="${pmtManager.depName}" /></td>
                                <td><c:out value="${pmtManager.teamName}" /></td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
                <div class="modal-footer">
                    <button class="accessBtn" type="button" onclick="modalDown();">확인</button>
                </div>
            </div>
        </div>
        <section id="section2">
            <button class="storage-btn" type="submit">임시저장</button>
            <button class="storage-btn" type="submit">등록</button>
        </section>
    </div>
</form>
<footer>
    <div class="footer_logo"><img src="${pageContext.request.contextPath}/resources/payment/images/logo.png" alt=""></div>
    <div class="footer_desc">
        <p>
            MGS그룹웨어 주식회사<br>
            사업자등록번호 : 123-45-67890<br>
            통신판매업신고번호 : 제1234-서울강동-1234호
            주소 : 서울시 강동구 천호동 123로<br>
        </p>
    </div>
    <div class="footer_law">
        <a href="#">개인정보처리방침</a>
        <a href="#">이용약관</a>
    </div>
</footer>
</body>
</html>