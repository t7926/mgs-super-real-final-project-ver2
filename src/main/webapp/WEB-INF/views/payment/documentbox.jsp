<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
    <link href="${pageContext.request.contextPath}/resources/payment/css/reset.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/resources/payment/css/documentbox.css" rel="stylesheet" type="text/css">
    <script src="https://kit.fontawesome.com/0e493d8011.js" crossorigin="anonymous"></script>
</head>
<body>
<header class="header">
    <nav class="gnb">
        <h1 class="logo"><a href="#">
            <img src="${pageContext.request.contextPath}/resources/payment/images/logo.png" alt=""></a>
        </h1>
        <ul class="main-menu">
                <li>
                    <a href="${pageContext.request.contextPath}/payment/payment" style="color: white">전자결재</a>
                </li>
                <li>
                     <a href="${pageContext.request.contextPath}/board/list" style="color: white">게시판</a>
                </li>
                <li>
                     <a href="${pageContext.request.contextPath}/geuntae/calcul-time" style="color: white">근태관리</a>
                </li>
                <li>
                    <a href="#" style="color: white">조직도</a>
                </li>
                <li>
                     <a href="${ pageContext.servletContext.contextPath }/document/list" style="color: white">문서관리</a>
                </li>
            </ul>
    </nav>
</header>

<div class="container">
    <aside id="aside">
        <div class="title-box">
            <div class="icon-box">
                <i class="fa-solid fa-computer"></i>
            </div>
            <h1 class="aside-title">전자 결재</h1>
        </div>
        <article class="payment-contents">
            <div class="title-box1">
                <div class="icon-box1">
                    <img class="select-img" src="${pageContext.request.contextPath}/resources/payment/images/select.jpg">
                </div>
                <h2>결재양식</h2>
            </div>
            <ul class="push-box1">
                    <li><a href="${pageContext.request.contextPath}/payment/approval">품의서 등록</a></li>
                    <li><a href="${pageContext.request.contextPath}/payment/cash">지출결의서 등록</a></li>
                    <li><a href="${pageContext.request.contextPath}/payment/draft">기안서 등록</a></li>
                    <li><a href="${pageContext.request.contextPath}/payment/orderproposal.">제안서 등록</a></li>
                    <li><a href="${pageContext.request.contextPath}/payment/report">보고서 등록</a></li>
                    <li><a href="${pageContext.request.contextPath}/payment/attendance">근태 신청서 등록</a></li>
                    <li><a href="${pageContext.request.contextPath}/payment/handwriter">수기 작성 등록</a></li>
                </ul>
        </article>
        <articel class="document-contents">
            <div class="title-box1">
                <div class="icon-box1">
                    <img class="select-img" src="${pageContext.request.contextPath}/resources/payment/images/select.jpg">
                </div>
                <h2>개인 문서함</h2>
            </div>
            <ul class="push-box2">
                    <li><a href="${pageContext.request.contextPath}/payment/payment-waiting">결제대기 문서</a></li>
                    <li><a href="${pageContext.request.contextPath}/payment/payment-complete">결재완료 문서</a></li>
                    <li><a href="${pageContext.request.contextPath}/payment/payment-temp">임시보관 문서</a></li>
                    <li><a href="${pageContext.request.contextPath}/payment/payment-reject">반려 문서</a></li>
                </ul>
        </articel>
    </aside>
	<section id="section1">
        <article id="contents1">
            <div class="select-box1">
            	<div class="select-title">
            		<div class="round"></div>
            		<h3>결재 진행</h3>
            	</div>
                <div class="content-box1">
                    <table>
                        <tr>
                            <th class="row1">미결재 문서</th>
                            <td class="row2"></td>
                        </tr>
                        <tr>
                            <th class="row1">결재완료 문서</th>
                            <td class="row2"></td>
                        </tr>
                        <tr>
                            <th class="row1">반려 문서</th>
                            <td class="row2"></td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="select-box2">
                <div class="select-title">
                    <div class="round"></div>
                    <h3>개인 문서함</h3>
                </div>
                <div class="content-box1">
                    <table>
                        <tr>
                            <th class="row3">상신 문서</th>
                            <td class="row4"></td>
                            <th class="row3">결재완료 문서</th>
                            <td class="row4"></td>
                        </tr>
                        <tr>
                            <th class="row3">임시 보관</th>
                            <td class="row4"></td>
                            <th class="row3">반려 문서</th>
                            <td class="row4"></td>
                        </tr>
                    </table>
                </div>
            </div>
        </article>
        <article class="contents2">
            <div class="select-title">
                <div class="round"></div>
                <h3>결재 진행 > 미결재 문서</h3>
            </div>
            <div class="content-box1">
                <table>
                    <tr>
                        <th class="row5">번 호</th>
                        <th class="row5">문서 분류</th>
                        <th class="row6">제 목</th>
                        <th class="row5">등록 날짜</th>
                        <th class="row7">상 태</th>
                    </tr>
                    <tr>
                        <td class="row8"></td>
                    </tr>
                </table>
            </div>
        </article>
        <article class="contents2">
            <div class="select-title">
                <div class="round"></div>
                <h3>개인 문서함 > 임시 보관</h3>
            </div>
            <div class="content-box1">
                <table>
                    <tr>
                        <th class="row5">번 호</th>
                        <th class="row9">제 목</th>
                        <th class="row5">등록 날짜</th>
                    </tr>
                    <tr>
                        <td class="row8"></td>
                    </tr>
                </table>
            </div>
        </article>
        <article class="contents2">
            <div class="select-title">
                <div class="round"></div>
                <h3>결재 진행 > 미결재 문서</h3>
            </div>
            <div class="content-box1">
                <table>
                    <tr>
                        <th class="row5">번 호</th>
                        <th class="row5">문서 분류</th>
                        <th class="row6">제 목</th>
                        <th class="row5">등록 날짜</th>
                        <th class="row7">상 태</th>
                    </tr>
                    <tr>
                        <td class="row8"></td>
                    </tr>
                </table>
            </div>
        </article>
        <article class="contents2">
            <div class="select-title">
                <div class="round"></div>
                <h3>결재 진행 > 미결재 문서</h3>
            </div>
            <div class="content-box1">
                <table>
                    <tr>
                        <th class="row5">번 호</th>
                        <th class="row5">문서 분류</th>
                        <th class="row6">제 목</th>
                        <th class="row5">등록 날짜</th>
                        <th class="row7">상 태</th>
                    </tr>
                    <tr>
                        <td class="row8"></td>
                    </tr>
                </table>
            </div>
        </article>
    </section>
    <section id="section2"></section>
</div>

<footer>
    <div class="footer_logo"><img src="${pageContext.request.contextPath}/resources/payment/images/logo.png" alt=""></div>
    <div class="footer_desc">
        <p>
            MGS그룹웨어 주식회사<br>
            사업자등록번호 : 123-45-67890<br>
            통신판매업신고번호 : 제1234-서울강동-1234호
            주소 : 서울시 강동구 천호동 123로<br>
        </p>
    </div>
    <div class="footer_law">
        <a href="#">개인정보처리방침</a>
        <a href="#">이용약관</a>
    </div>
</footer>
</body>
</html>