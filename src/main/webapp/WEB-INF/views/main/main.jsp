<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html> 
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<!-- 합쳐지고 최소화된 최신 CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

<!-- full-calendar -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/fullcalendar@5.10.0/main.min.css">

<link href="${pageContext.request.contextPath}/resources/schedule/css/reset.css" rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/resources/schedule/css/main.css" rel="stylesheet" type="text/css">

<!-- script -->
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<!-- 합쳐지고 최소화된 최신 자바스크립트 -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>

<!-- full-calendar -->
<script src="https://cdn.jsdelivr.net/npm/fullcalendar@5.10.0/main.min.js"></script>

<!-- 회원강제 탈퇴 후 확인 알림 메세지 -->
<script>
        const message = '${ requestScope.message }';
        if(message != null && message !== '') {
	    alert(message);
         }

</script>
</head>
 <body onload="showClock()">   

	<header class="header">
        <nav class="gnb">
          <h1 class="logo"><a href="${pageContext.request.contextPath}/">
              <img src="${pageContext.request.contextPath}/resources/geuntae/images/logo.png" alt="">
            </a></h1>
          <ul class="main-menu">
            <li>
              <a href="${pageContext.request.contextPath}/payment/payment">전자결재</a>
            </li>
            <li>
              <a href="${pageContext.request.contextPath}/board/list">게시판</a>
            </li>
            <li>
              <a href="${pageContext.request.contextPath}/geuntae/calcul-time">근태관리</a>
            </li>
            <li>
              <a href="#">조직도</a>
            </li>
            <li>
              <a href="${pageContext.request.contextPath}/document/list">문서관리</a>
            </li>
          </ul>
        </nav>
      </header>
    
        <section class="main">
			
            <div class="privacy">
                <div class="status">
                    <div class="pic"><img src="${pageContext.request.contextPath}/resources/schedule/images/unknown1.png" alt=""></div>
                    <c:if test="${ sessionScope.loginMember.id eq 'admin' }">
				             <input type="button" class="btn btn-or" id="superauth" value="관리자페이지">
				             <!--<button class="btn btn-or" id="writeNotice">관리자 권한</button>  -->
			          </c:if>
                    <c:if test="${ !empty sessionScope.loginMember }">
				       <!--<input type="button" class="superauth" value="관리자페이지" id="superauth">  -->
				       
				     <h3 id="emp_name"><c:out value="${ sessionScope.loginMember.name }"/>님의 방문을 환영합니다.</h3>
				     <!-- 사원정보 숨겨놓기 -->
				     <input type="hidden" id="emp_num" value="${ sessionScope.loginMember.empnum}">
				     <div class="logoutmypage">
                       <input type="button" class="btn btn-yg" value="마이페이지" id="updateMember">
					   <input type="button" class="btn btn-or" value="로그아웃" id="logout">
                    </div>
	                </c:if>
                    
                </div>
                <div class="commute">
                    <div class="currentTime">
                    <span id="msg_top"></span>                    
                    <span id="cTime"></span>
                    </div>
                    
                    <div class="commute_btn">
                        <span id="workStartbtn" onclick="workStart()" class="chul">출근</span>
                        <span id="workEndbtn" onclick="workEnd()" class="towe">퇴근</span>
                    </div>
						<input type="hidden" id="startWork" value="출근시간">
                        <input type="hidden" id="endWork" value="퇴근시간">
                </div>
                <div class="schedule">
                                				
                
                </div>
            </div>

            <div class="schedule" id="schedule_wrap">
            	<div id="selectScope">
							<label class="search_category">
							    <input onclick="allmy()" type="radio" name="allmy" value="all" checked="checked">
							    <span>전사원 일정</span>
							</label>
							 
							<label class="search_category">
							    <input onclick="allmy()" type="radio" name="allmy" value="my">
							    <span>나의 비밀 일정</span>
							</label>
				</div>
                <div id="calendar">
                	
                </div>
            </div>

        </section>

		<div id="modal">
			<div><h5>일정 등록하기</h5></div>
			<div><h5>일정제목</h5><input type="text" id="scheduleTitle"></div>
			<div><h5>공개범위</h5>
    			<label><input type="radio" name="openScope" value="팀원공유">팀원공유</label>
    			<label><input type="radio" name="openScope" value="나만보기" checked="checked">나만보기</label>
			</div>
			<div id="btnBox">
				<span id="close_btn" class="btn">닫기</span>
				<span id="confirm_btn" class="btn">확인</span>
			</div>

		</div>
    
		<footer>
			<div class="footer_logo"><img src="${pageContext.request.contextPath}/resources/geuntae/images/logo.png" alt="">
			</div>
			<div class="footer_desc">
			  <p>
				MGS그룹웨어 주식회사<br>
				사업자등록번호 : 123-45-67890<br>
				통신판매업신고번호 : 제1234-서울강동-1234호
				주소 : 서울시 강동구 천호동 123로<br>
			  </p>
			</div>
			<div class="footer_law">
			  <a href="#">개인정보처리방침</a>
			  <a href="#">이용약관</a>
	
			</div>
		  </footer>
    

</body>
<script>


		/* 일정 삭제용 메소드 */
		function DeleteSchedule(arg, calendar){
				
			
			
			$.ajax({
				
		          url: "/mgs/schedule/delete",
		          type: 'post',
		          data: {
		        	  startStr : arg.event.startStr,
		        	  endStr : arg.event.endStr,
		        	  title : arg.event.title
		          },
		          
 		          async:false,
		          success: function (data) {
		        	if(data == "success"){
		        		alert('삭제완료!');
		        	} else {
		        		alert('삭제에러!');
		        	}
		        	  
		          },
		          error: function () {
		        	alert('error');

		          }
			
			});
		}
		
		/* 전체일정/나의일정 선택시 작동하는 이벤트 */
		let value = "all";
		
		function allmy(){
			value = $('input[name="allmy"]:checked').val();
			check(value);
		}

		
		/* 일정 조회용 메소드 */
		function selectSchedule(value){
			let result = "";

			
			$.ajax({
				
		          url: "/mgs/schedule/select",
		          type: 'get',
		          data : {
		        	  value : value
		          },		          
		          async:false,
		          success: function (data) {
		        	 result = data;
		        	  
		          },
		          error: function () {

		          }
			
			});
	    	return result;    
		}


	    document.addEventListener('DOMContentLoaded', ()=>{
	    	check(value);
	    } )
	    
	    function check(value){
			/* 일정조회 */
	    	
	    	let event = selectSchedule(value);
	    
	    	console.log(event);
	    	/* 캘린더 */
	        var calendarEl = document.getElementById('calendar');
	        var calendar = new FullCalendar.Calendar(calendarEl, {
	          headerToolbar: {
	            left: 'prev,next today',
	            center: 'title',
	            right: 'dayGridMonth,timeGridWeek,timeGridDay'
	          },
	          initialView: 'dayGridMonth',
	          navLinks: true,
	          selectable: true,
		      eventAdd:function(obj){
		        	console.log(obj);
		      },
	    	  /* 날짜 클릭시 */ 	  
		      select:function(arg){
		    	document.getElementById("scheduleTitle").value ='';
		    	showModal(arg,calendar);

		      },
		      eventClick: function(arg) {
		    	  
		    	  /* 삭제할 때 자신의 것인지 확인 */
		    	  let login = document.querySelector('#emp_num').value;
		    	  console.log("로그인 사원번호 : " + login);
		    	  console.log("작성자 사원번호 : " + arg.event._def.extendedProps.emp_num);
		    	 
		    	 if(login != arg.event._def.extendedProps.emp_num){
		    		 alert('본인만 삭제할 수 있습니다!');
		    	 } else {
		    		 
		    		 
		    		  if (confirm('정말삭제하시겠습니까?')) {
				        	DeleteSchedule(arg, calendar);
				            arg.event.remove();
				          }
		    		 
		    		 
		    		 
		    		 
		    	 }
		    	  
		    	  
		    	  
		        
		      },
		      dayMaxEvents: true, // allow "more" link when too many events		      
		      events: event
	        }
        	
        );
        calendar.render();
      };
	    
	 function showModal(arg,calendar){
		 
		 /* 모달창 보이기 */
		 document.querySelector('#modal').classList.add('show');
		 
		 /* 모달창 내에서 일정등록버튼 클릭시 */
		 document.querySelector('#confirm_btn').addEventListener('click', ()=>{
			document.querySelector('#modal').classList.remove('show');
			let titles = document.getElementById("scheduleTitle").value;
			let startStr = arg.startStr;
			let endStr = arg.endStr;
			let openScope = $('input[name="openScope"]:checked').val();
			
			/* ajax호출 함수 */
			/* 호출 전 체크여부확인 */
			if(titles === null || titles === ""){
				alert('제목을 입력해주세요!');
			} else {
				
				/* 제목에 이름붙이기 */
				let emp_name = document.querySelector('#emp_name').innerText;
				emp_name = "[" + emp_name.substr(0,emp_name.indexOf('님의')) + "] ";
				let title = emp_name + titles
				
				/* DB에 INSERT하는 함수호출 */
				insertSchedule(title, startStr, endStr, openScope);
				
				
				
				if (openScope === '팀원공유') {
					/* 화면에 임시로 보이게 하는 객체 */
			          calendar.addEvent({
			            title: title,
			            start: arg.startStr,
			            end: arg.end,
			            allDay: arg.allDay
			            
			          })
			    }
				
			}
			
			/* 중복제거  */
			calendar = null;
			arg = null;
			

});
		 
		 
			/* 일정버튼 모달창에서 닫기 버튼눌렀을 시 */	
			document.querySelector('#close_btn').addEventListener('click', ()=>{
			document.querySelector('#modal').classList.remove('show');
			
			/* 중복제거  */
			calendar = null;
			arg = null;
/* 			calendar.unselect();
 */			});
		
	 } 
	 
	/* 일정 인서트용 ajax 호출 */
	function insertSchedule(title, startStr, endStr, openScope){		
		
		  $.ajax({
				url : "/mgs/schedule/insert",
				type : 'post',
				data : {
					title : title,
					startStr : startStr,
					endStr : endStr,
					openScope : openScope
				},
				success : function(data) {
					if(data === "success"){
						alert('일정등록성공!');
					} else {
						alert('일정등록실패!');
					}
				},
				error : function() {
					alert("일정등록error");
				}
			});		
		  
	}
	 

	
	autoStart();
	/* 로그인하면 자동으로 접속일 기준, 출퇴근 기록이 있는지 확인하는 함수 */
	function autoStart(){
		
		 $.ajax({
	          url: "/mgs/geuntae/main_worktime",
	          type: 'get',
	          success: function (data) {
	        	  /* 빈값이면 input박스에 아무것도 넣지 않는다. */
	        	  if(data !== null){
	        		  
	        		  if(data.work_start_time !== undefined){
	        			  
			        	  document.querySelector('#startWork').value = data.work_start_time;
			        	  //버튼색깔주기
			        	  document.querySelector('#workStartbtn').classList.add('giveColor');
		      				
	        		  
	        		  }
	        		  
	        		  if(data.work_end_time !== undefined){
			        	  document.querySelector('#endWork').value = data.work_end_time;
			        	  //버튼색깔주기
			        	  document.querySelector('#workEndbtn').classList.add('giveColor');

		        	  }
	        		  
	        	  }
	        	  
	          },
	          error: function () {
	        	  alert('실패');

	          }
	        });

	}
	
	
	/* 출근버튼 클릭시 */
	function workStart(){
		
		if(document.querySelector('#startWork').value === '출근시간'){
			
			worktimeAjax("start");
			
			alert('출근시간이 입력되었습니다.');
			
			
		} else {
			alert('출근시간은 하루에 한번만 입력할 수 있습니다.');

		}
		
	}
	
	/* ajax */
	function worktimeAjax(category){
		
		$.ajax({
	          url: "/mgs/geuntae/main_worktimeRegist",
	          type: 'post',
	          data: {
	        	  category: category,
	            },
	          success: function (data) {
	        	  if(data === 'success'){
	        		  
	        		  if(category === 'start'){
	        			  alert('성공적으로 출근시간을 입력하였습니다.');
		        		  document.querySelector('#startWork').value = new Date().getHours() + "시" + new Date().getMinutes() + "분";
			        	  //버튼색깔주기
			        	  document.querySelector('#workStartbtn').classList.add('giveColor');
	        		  }
	        		  
	        		  if(category === 'end'){
	        			  alert('성공적으로 퇴근시간을 입력하였습니다.');
		        		  document.querySelector('#endWork').value = new Date().getHours() + "시" + new Date().getMinutes() + "분";
		        		//버튼색깔주기
			        	  document.querySelector('#workEndbtn').classList.add('giveColor');
	        		  }
	        		  
	        		  
	        	  } else {
	        		  alert('성공적으로 수행하지 못했습니다.');
	        	  }
	          },
	          error: function () {
	        	  alert('error');
	          }
	        });
		
	}
	
	
	/* 퇴근버튼 클릭시 */
	function workEnd(){
		
	if(document.querySelector('#startWork').value === '출근시간'){
		alert('출근시간을 입력하지 않으면 퇴근시간을 입력할 수 없습니다.');
	}
		
	else if(document.querySelector('#endWork').value === '퇴근시간'){
			
			worktimeAjax("end");

			alert('퇴근시간 입력되었습니다.');
			
			
		} else {
			alert('퇴근시간 하루에 한번만 입력할 수 있습니다.');

		}

	}
	
	/* 좌측 시간 */
	function showClock(){
		const now = new Date();	// 현재 날짜 및 시간
		const year = now.getFullYear();
		const month = now.getMonth() + 1;
		const date = now.getDate()
		
        let currentDate = new Date();
		let msg_top = year + "년" + month + "월" + date + "일";
        let msg = "";
        if(currentDate.getHours()>12){      //시간이 12보다 크다면 오후 아니면 오전
          msg += "오후 ";
          msg += currentDate.getHours()-12+"시 ";
       }
       else {
         msg += "오전 ";
         msg += currentDate.getHours()+"시 ";
       }
 
        msg += currentDate.getMinutes()+"분 ";
        msg += currentDate.getSeconds()+"초";
 
        cTime.innerText = msg;
        document.querySelector('#msg_top').innerText = msg_top;
 
        setTimeout(showClock,1000);  //1초마다 갱신
      }



    
    
    /* 로그아웃 스크립트*/
    
     if(document.getElementById("logout")) {
		
		const $logout = document.getElementById("logout");
		
		$logout.onclick = function() {
		    
		    location.href = "/mgs/member/logout";
		}
	}
    
     /* 마이페이지 이동 스크립트*/
     
     if(document.getElementById("updateMember")) {
		
		const $logout = document.getElementById("updateMember");
		
		$logout.onclick = function() {
		    
		    location.href = "/mgs/member/my-page";
		}
	}
    
    
     /* 관리자권한 스크립트*/
     
     if(document.getElementById("superauth")) {
		
		const $superauth = document.getElementById("superauth");
		
		$superauth.onclick = function() {
		    
		    location.href = "/mgs/superauth/superauth";
		}
	}
    
    
    
    </script>
    
    
    
    
</html>