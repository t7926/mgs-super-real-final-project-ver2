<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>    
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link href="${pageContext.request.contextPath}/resources/document/css/reset.css" rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/resources/common/css/common.css" rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/resources/superauth/css/auth-detail.css" rel="stylesheet" type="text/css">

<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
</head>

<body>
	<div id="layout">
		<header class="header">
	<nav class="gnb">
		<h1 class="logo"><a href="${ pageContext.servletContext.contextPath }">
			<img src="${pageContext.request.contextPath}/resources/common/images/logo.png" alt="">
		</a></h1>
		  <ul class="main-menu">
                <li>
                    <a href="${pageContext.request.contextPath}/payment/payment" style="color: white">전자결재</a>
                </li>
                <li>
                     <a href="${pageContext.request.contextPath}/board/list" style="color: white">게시판</a>
                </li>
                <li>
                     <a href="${pageContext.request.contextPath}/geuntae/calcul-time" style="color: white">근태관리</a>
                </li>
                <li>
                    <a href="#" style="color: white">조직도</a>
                </li>
                <li>
                     <a href="${ pageContext.servletContext.contextPath }/document/list"style="color: white">문서관리</a>
                </li>
            </ul>
	</nav>
</header>
<div id="container">
<div id="wrap"> 
	<nav>
		<div id="nav-head">
			<i class="fa fa-file-text" aria-hidden="true"></i>
			<h2>문서관리</h2>
		</div>

		<ul>
			<div id="cate">
				<i class="fa fa-file-text" aria-hidden="true"></i>
				<h3>관리자 권한</h3>
			</div>
			
          <li>
               <a href="${pageContext.request.contextPath}/superauth/superauth">관리자 권한</a><br>
               <a href="${pageContext.request.contextPath}/superauth/superauth">회원강제 탈퇴</a>
          </li>
          </ul>
	</nav>
	<article>
		<div id="docu-top">
			<h1>통합 문서 관리</h1>
		</div>
			<div id="table">
				<form action="${pageContext.servletContext.contextPath}/superauth/update" method="post">
				<table>
						<tr>
							<td>이름</td>
							<td><c:out value="${ superauthDetail.empname }"/></td>
						</tr>
						<tr>
							<td>부서</td>
							<td><c:out value="${ superauthDetail.deptname }"/></td>
						</tr>
						<tr>
							<td>직급</td>
							<td><c:out value="${ superauthDetail.levname }"/></td>
						</tr>
						<tr>
							<td>관리자 권한</td>
							<td>
								<select name="authright_yn" id="authright_yn">
									<option value="Y">Y</option>
									<option value="N">N</option>
								</select>
							</td>
						</tr>
						<tr>
							<td>게시판 권한</td>
							<td>
								<select name="boardright_yn" id="boardright_yn">
									<option value="Y">Y</option>
									<option value="N">N</option>
									
								</select>
							</td>
						</tr>
						<input type="hidden" name="empnum" value="${ superauthDetail.empnum }"/>
				</table>
				<div id="button">
				<button type="button" onclick="location.href='${ pageContext.servletContext.contextPath }/superauth/superauth'">돌아가기</button>
				<button type="submit">수정하기</button>
				
				</div>
				
				</form>
			</div>	
	</article>
</div>
</div>
<footer>
	<div class="footer_logo"><img src="${pageContext.request.contextPath}/resources/common/images/logo.png" alt=""></div>
	<div class="footer_desc">
		<p>
			MGS그룹웨어 주식회사<br>
			사업자등록번호 : 123-45-67890<br>
			통신판매업신고번호 : 제1234-서울강동-1234호
			주소 : 서울시 강동구 천호동 123로<br>
		</p>
	</div>
	<div class="footer_law">
		<a href="#">개인정보처리방침</a>
		<a href="#">이용약관</a>

	</div>
</footer>
</div>
		<script>
		
		// controller에서 넘어온 authright_yn와 boardright_yn 의 값을 변수에 담는다
		 
		
        const authright_yn = '${requestScope.superauthDetail.authright_yn}';
        
        console.log(authright_yn);
        
        if(authright_yn == 'Y') {
        	
        	$('#authright_yn').val('Y').prop("selected",true);
        } else {
        	$('#authright_yn').val('N').prop("selected",true);
        }
        
        const boardright_yn = '${requestScope.superauthDetail.boardright_yn}';
        
        console.log(boardright_yn);
		
        if(boardright_yn== 'Y') {
        	$('#boardright_yn').val('Y').prop("selected",true);
        } else {
        	$('#boardright_yn').val('N').prop("selected",true);
        }
         
		// authright_yn 의 값이 y이면 y에 selected 속성을 추가하고
		//                    n이면 n에 selected 속성을 추가하고
	
		
		
		
		//$('#authright_yn').val('Y').prop("selected",true);
		//$('#authright_yn').val('N').prop("selected",true);
		
		
		
		
		//$('#boardright_yn').val('Y').prop("selected",true);
		//$('#boardright_yn').val('N').prop("selected",true);
		
		</script>
		
		
		
		
		
		
		
		
		
</body>

</html>