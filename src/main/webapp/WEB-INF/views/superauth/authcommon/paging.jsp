<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<div class="pagingArea" align="center">
	

	</div>
	
	<script>
		let type = "${ type }"
		let link = '';
		if(type == 'list'){
			link = "${ pageContext.servletContext.contextPath }/document/list";
		} else if(type == 'approval'){
			link = "${ pageContext.servletContext.contextPath }/document/approval";	
		} else if(type == 'approval'){
			link = "${ pageContext.servletContext.contextPath }/document/expend";	
		} else if(type == 'approval'){
			link = "${ pageContext.servletContext.contextPath }/document/draft";	
		} else if(type == 'approval'){
			link = "${ pageContext.servletContext.contextPath }/document/suggest";	
		} else if(type == 'approval'){
			link = "${ pageContext.servletContext.contextPath }/document/report";	
		} else if(type == 'approval'){
			link = "${ pageContext.servletContext.contextPath }/document/attendance";	
		} else if(type == 'authList'){
			link = "${ pageContext.servletContext.contextPath }/document/authList";	
		}
		let searchText = "";
		
		/* 검색 조건 유무에 따른 경로 처리 */
		if(${ !empty requestScope.selectCriteria.searchCondition? true: false }) {
			searchText += "&searchCondition=${ requestScope.selectCriteria.searchCondition }";
		}
		
		/* 검색 내용 유무에 따른 경로 처리 */
		if(${ !empty requestScope.selectCriteria.searchValue? true: false }) {
			searchText += "&searchValue=${ requestScope.selectCriteria.searchValue }";
		}
			
		/* 첫 페이지 버튼 click 이벤트 처리 */
		if(document.getElementById("startPage")) {
			const $startPage = document.getElementById("startPage");
			$startPage.onclick = function() {
				location.href = link + "?currentPage=1" + searchText;
			}
		}
		
		/* 이전 페이지 버튼 click 이벤트 처리 */
		if(document.getElementById("prevPage")) {
			const $prevPage = document.getElementById("prevPage");
			$prevPage.onclick = function() {
				location.href = link + "?currentPage=${ requestScope.selectCriteria.pageNo - 1 }" + searchText;
			}
		}
		
		/* 다음 페이지 버튼 click 이벤트 처리 */
		if(document.getElementById("nextPage")) {
			const $nextPage = document.getElementById("nextPage");
			$nextPage.onclick = function() {
				location.href = link + "?currentPage=${ requestScope.selectCriteria.pageNo + 1 }" + searchText;
			}
		}
		
		/* 마지막 페이지 버튼 click 이벤트 처리 */
		if(document.getElementById("maxPage")) {
			const $maxPage = document.getElementById("maxPage");
			$maxPage.onclick = function() {
				location.href = link + "?currentPage=${ requestScope.selectCriteria.maxPage }" + searchText;
			}
		}
		
		/* 페이지 번호 버튼 click 이벤트 처리 */
		function pageButtonAction(text) {
			location.href = link + "?currentPage=" + text + searchText;
		}
	</script>
</body>
</html>