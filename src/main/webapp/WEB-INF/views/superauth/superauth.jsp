<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>    
<!DOCTYPE html>
<html>
<head>

<script>
        const message = '${ requestScope.message }';
        if(message != null && message !== '') {
	    alert(message);
         }

</script>

<meta charset="UTF-8">
<title>Insert title here</title>


<link href="${pageContext.request.contextPath}/resources/log-in/css/reset.css" rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/resources/log-in/css/common.css" rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/resources/superauth/css/super-auth.css" rel="stylesheet" type="text/css">

</head>
<body>
<body>
<div id="layout">
    <header class="header">
        <nav class="gnb">
            <h1 class="logo"><a href="#">
                <img src="${pageContext.request.contextPath}/resources/log-in/images/logo.png" alt="">
            </a></h1>
            <ul class="main-menu">
                <li>
                    <a href="${pageContext.request.contextPath}/payment/payment" style="color: white">전자결재</a>
                </li>
                <li>
                     <a href="${pageContext.request.contextPath}/board/list" style="color: white">게시판</a>
                </li>
                <li>
                     <a href="${pageContext.request.contextPath}/geuntae/calcul-time" style="color: white">근태관리</a>
                </li>
                <li>
                    <a href="#" style="color: white">조직도</a>
                </li>
                <li>
                     <a href="${ pageContext.servletContext.contextPath }/document/list"style="color: white">문서관리</a>
                </li>
            </ul>
        </nav>
    </header>
    <div id="container">
        <div id="wrap">
            <nav>
                <div id="nav-head">
                    <i class="fa fa-file-text" aria-hidden="true"></i>
                    <h2>관리자 권한</h2>
                </div>
                <ul>
                    <div id="cate">
                        <i class="fa fa-file-text" aria-hidden="true"></i>
                        <h3>관리자 권한</h3>
                    </div>
                   
<!--                    <li><a href="${pageContext.request.contextPath}/views/document/alldocument.jsp">관리자 권한 설정</a></li>-->
                    <li><a href="${pageContext.request.contextPath}/superauth/superauth">관리자 권한설정</a></li>
                    <li><a href="${pageContext.request.contextPath}/superauth/superauth-fire">회원 강제탈퇴</a></li>
                   
                </ul>
                
            </nav>
            <article>
                <div id="docu-top">
                    <h1>관리자 권한 설정</h1>
                </div>
                <div id="table">
                    <table>
                        <tr>
                            <th>사원번호</th>
                            <th>직원명</th>
                            <th>부서</th>
                            <th>직급</th>
                            
                        </tr>
                        <c:forEach var="auth" items="${ superauthList }">
                        <tr>
                            <td><c:out value="${ auth.empnum }"/></td>
                            <td><c:out value="${ auth.empname }"/></td>
                            <td><c:out value="${ auth.deptname }"/></td>
                            <td><c:out value="${ auth.levname }"/></td>
                        
                        </tr>
                       	</c:forEach>
                    </table>
                </div>
                
                <jsp:include page="authcommon/paging.jsp" />
                
                 <div id="search">
                	<form action="${ pageContext.servletContext.contextPath }/superauth/superauth"
                	method="get">
                            <select name="searchCondition" size="1">
                                <option value="name" ${ requestScope.selectCriteria.searchCondition eq "name"? "selected": "" }>직원명</option>
                                <option value="dept" ${ requestScope.selectCriteria.searchCondition eq "dept"? "selected": "" }>부서</option>
                                <option value="lev" ${ requestScope.selectCriteria.searchCondition eq "lev"? "selected": "" }>직급</option>
                                <!-- <option value="title+contents">문서명+내용</option> -->
                            </select>
                    <input type="search" id="searchValue" name="searchValue" placeholder="검색어를 입력해주세요" size="40"
					value="${ requestScope.selectCriteria.searchValue }">
                    <button id="btn" type="submit">검색</button>
                    </form>
                </div>
            </article>
        </div>
        </div>
    <footer>
        <div class="footer_logo"><img src="${pageContext.request.contextPath}/resources/log-in/images/logo.png" alt=""></div>
        <div class="footer_desc">
            <p>
                MGS그룹웨어 주식회사<br>
                사업자등록번호 : 123-45-67890<br>
                통신판매업신고번호 : 제1234-서울강동-1234호
                주소 : 서울시 강동구 천호동 123로<br>
            </p>
        </div>
        <div class="footer_law">
            <a href="#">개인정보처리방침</a>
            <a href="#">이용약관</a>

        </div>
    </footer>
</div>
</body>

 <script>
	
	/* 게시글 리스트에서 해당 게시글에 마우스를 올릴 떄에 대한 스타일 처리 */
	const $modal = document.querySelector("#modal");
	if (document.querySelectorAll("#table td")) {
		const $tds = document.querySelectorAll("#table td");
		for (let i = 0; i < $tds.length; i++) {

			$tds[i].onmouseenter = function() {
				this.parentNode.style.backgroundColor = "#CADAF2";
				this.parentNode.style.cursor = "pointer";
			}

			$tds[i].onmouseout = function() {
				this.parentNode.style.backgroundColor = "white";
			}

			$tds[i].onclick = function() {
				const empNum = this.parentNode.children[0].innerText;
				location.href = "${ pageContext.servletContext.contextPath }/superauth/detail?empNum=" + empNum;		
			}
			
		}
	}
	
	
</script>

</body>
</html>