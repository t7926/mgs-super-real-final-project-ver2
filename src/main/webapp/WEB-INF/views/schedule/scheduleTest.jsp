<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/schedule/css/reset.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/schedule/css/common.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/schedule/css/scheduleTest.css">
	<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<title>Insert title here</title>
</head>
<body>
	
	<header class="header">
		<nav class="gnb">
			<h1 class="logo"><a href="#">
				<img src="${pageContext.request.contextPath}/resources/schedule/images/logo.png" alt="">
			</a></h1>
			<ul class="main-menu">
				<li>
					<a href="#">전자결재</a>
				</li>
				<li>
					<a href="#">게시판</a>
				</li>
				<li>
					<a href="#">근태관리</a>
				</li>
				<li>
					<a href="#">조직도</a>
				</li>
				<li>
					<a href="#">문서관리</a>
				</li>
			</ul>
		</nav>
	</header>

	<section class="main">
		<div class="privacy">
			<div class="status">
				<div class="pic"><img src="${pageContext.request.contextPath}/resources/schedule/images/unknown1.png" alt=""></div>
				<h4>홍길동 님 환영합니다!</h4>
				<div class="logoutmypage">
					<div class="logout"><a href="#">로그아웃</a></div>
					<div class="mypage"><a href="#">마이페이지</a></div>
				</div>
			</div>
			<div class="commute">
				<div class="currentTime"><h3 id="cTime"></h3></div>
				<div class="commute_btn">
					<div class="chul"></div>
					<div class="towe"></div>
				</div>
			</div>
			<div class="schedule"></div>
		</div>
		<div class="calendar">
			<div class="cal_top">
				<div class="cal_left">
					<a href="">전직원 일정</a>
					<a href="">프로젝트 일정</a>
				</div>
				<div class="cal_center">
					<a href="#" id="movePrevMonth"><span id="prevMonth" class="cal_tit">&lt;</span></a>
					<span id="cal_top_year"></span>
					<span id="cal_top_month"></span>
					<a href="#" id="moveNextMonth"><span id="nextMonth" class="cal_tit">&gt;</span></a>
				</div>
				<div class="cal_right">
					<a href="${pageContext.request.contextPath}views/schedule/registSch.jsp">개인일정 등록</a>
					<a href="${pageContext.request.contextPath}views/schedule/registProjectSch.jsp">프로젝트 일정 등록</a>
					<a href="${pageContext.request.contextPath}views/schedule/schModifyDelete.jsp">일정 수정 및 삭제</a>
				</div>
			</div>
			<div id="cal_tab" class="cal"></div>
		</div>
	</section>

	<script type="text/javascript">
    
		var today = null;
		var year = null;
		var month = null;
		var firstDay = null;
		var lastDay = null;
		var $tdDay = null;
		var $tdSche = null;
		var jsonData = null;
	
		$(document).ready(function() {
			drawCalendar();
			initDate();
			drawDays();
			drawSche();
			$("#movePrevMonth").on("click", function(){movePrevMonth();});
			$("#moveNextMonth").on("click", function(){moveNextMonth();});
		});
		
		//calendar 그리기
		function drawCalendar(){
			var setTableHTML = "";
			setTableHTML+='<table class="calendar">';
			setTableHTML+='<tr><th>SUN</th><th>MON</th><th>TUE</th><th>WED</th><th>THU</th><th>FRI</th><th>SAT</th></tr>';
			for(var i=0;i<6;i++){
				setTableHTML+='<tr height="105">';
				for(var j=0;j<7;j++){
					setTableHTML+='<td style="text-overflow:ellipsis;overflow:hidden;white-space:nowrap">';
					setTableHTML+='    <div class="cal-day"></div>';
					setTableHTML+='    <div class="cal-schedule"></div>';
					setTableHTML+='</td>';
				}
				setTableHTML+='</tr>';
			}
			setTableHTML+='</table>';
			$("#cal_tab").html(setTableHTML);
		}
	
		//날짜 초기화
		function initDate(){
			$tdDay = $("td div.cal-day")
			$tdSche = $("td div.cal-schedule")
			dayCount = 0;
			today = new Date();
			year = today.getFullYear();
			month = today.getMonth()+1;
			if(month < 10){month = "0"+month;}
			firstDay = new Date(year,month-1,1);
			lastDay = new Date(year,month,0);
		}

		// // 현재시간 표시
		// function currentTime() {
		// 	let today = new Date();
		// 	$("#cTime").document.write(today.toLocaleTimeString() + '<br>');
		// }
		
		//calendar 날짜표시
		function drawDays(){
			$("#cal_top_year").text(year);
			$("#cal_top_month").text(month);
			// $("#cTime").text(time);
			for(var i=firstDay.getDay();i<firstDay.getDay()+lastDay.getDate();i++){
				$tdDay.eq(i).text(++dayCount);
			}
			for(var i=0;i<42;i+=7){
				$tdDay.eq(i).css("color","red");
			}
			for(var i=6;i<42;i+=7){
				$tdDay.eq(i).css("color","blue");
			}
		}
	
		//calendar 월 이동
		function movePrevMonth(){
			month--;
			if(month<=0){
				month=12;
				year--;
			}
			if(month<10){
				month=String("0"+month);
			}
			getNewInfo();
			}
		
		function moveNextMonth(){
			month++;
			if(month>12){
				month=1;
				year++;
			}
			if(month<10){
				month=String("0"+month);
			}
			getNewInfo();
		}
	
		// 정보갱신
		function getNewInfo(){
			for(var i=0;i<42;i++){
				$tdDay.eq(i).text("");
				$tdSche.eq(i).text("");
			}
			dayCount=0;
			firstDay = new Date(year,month-1,1);
			lastDay = new Date(year,month,0);
			drawDays();
			drawSche();
		}

		//데이터 등록
		function setData(){
			jsonData = 
			{
				"2022":{
					"03":{
						"01":"3.1절"
					}
					// ,"08":{
					// 	"7":"칠석"
					// 	,"15":"광복절"
					// 	,"23":"처서"
					// }
					// ,"09":{
					// 	"13":"추석"
					// 	,"23":"추분"
					// }
				}
			}
		}
		
		//스케줄 그리기
		function drawSche(){
			setData();
			var dateMatch = null;
			for(var i=firstDay.getDay();i<firstDay.getDay()+lastDay.getDate();i++){
				var txt = "";
				txt =jsonData[year];
				if(txt){
					txt = jsonData[year][month];
					if(txt){
						txt = jsonData[year][month][i];
						dateMatch = firstDay.getDay() + i -1; 
						$tdSche.eq(dateMatch).text(txt);
					}
				}
			}
		}
	</script>
	
	<footer>
		<div class="footer_logo"><img src="${pageContext.request.contextPath}/resources/schedule/images/logo.png" alt=""></div>
		<div class="footer_desc">
			<p>
				MGS그룹웨어 주식회사<br>
				사업자등록번호 : 123-45-67890<br>
				통신판매업신고번호 : 제1234-서울강동-1234호
				주소 : 서울시 강동구 천호동 123로<br>
			</p>
		</div>
		<div class="footer_law">
			<a href="#">개인정보처리방침</a>
			<a href="#">이용약관</a>
	
		</div>
	</footer>
</body>
</html>