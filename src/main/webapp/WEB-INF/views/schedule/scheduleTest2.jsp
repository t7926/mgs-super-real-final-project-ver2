<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/schedule/css/reset.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/schedule/css/common.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/schedule/css/scheduleTest2.css">
	<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
	<!-- fullcalendar CDN -->
	<link href='https://cdn.jsdelivr.net/npm/fullcalendar@5.8.0/main.min.css' rel='stylesheet' />
	<script src='https://cdn.jsdelivr.net/npm/fullcalendar@5.8.0/main.min.js'></script>

<title>Insert title here</title>
</head>
<body>
	
	<header class="header">
		<nav class="gnb">
			<h1 class="logo"><a href="#">
				<img src="${pageContext.request.contextPath}/resources/schedule/images/logo.png" alt="">
			</a></h1>
			<ul class="main-menu">
				<li>
					<a href="#">전자결재</a>
				</li>
				<li>
					<a href="#">게시판</a>
				</li>
				<li>
					<a href="#">근태관리</a>
				</li>
				<li>
					<a href="#">조직도</a>
				</li>
				<li>
					<a href="#">문서관리</a>
				</li>
			</ul>
		</nav>
	</header>

	<section class="main">
		<div class="privacy">
			<div class="status">
				<div class="pic"><img src="${pageContext.request.contextPath}/resources/schedule/images/unknown1.png" alt=""></div>
				<h4>홍길동 님 환영합니다!</h4>
				<div class="logoutmypage">
					<div class="logout"><a href="#">로그아웃</a></div>
					<div class="mypage"><a href="#">마이페이지</a></div>
				</div>
			</div>
			<div class="commute">
				<div class="currentTime"><h3 id="cTime"></h3></div>
				<div class="commute_btn">
					<div class="chul"></div>
					<div class="towe"></div>
				</div>
			</div>
			<div class="schedule"></div>
		</div>
		<div class="calendar2">
			<div id='calendar'></div>
		</div>
	</section>

	<script>
		document.addEventListener('DOMContentLoaded', function() {
  var calendarEl = document.getElementById('calendar');

  var calendar = new FullCalendar.Calendar(calendarEl, {
    timeZone: 'UTC',
    headerToolbar: {
      left: 'prev,next today',
      center: 'title',
      right: 'dayGridMonth,timeGridWeek,timeGridDay'
    },
    editable: true,
    dayMaxEvents: true, // when too many events in a day, show the popover
    events: 'https://fullcalendar.io/api/demo-feeds/events.json?overload-day'
  });

  calendar.render();
});
	</script>
	
	<footer>
		<div class="footer_logo"><img src="${pageContext.request.contextPath}/resources/schedule/images/logo.png" alt=""></div>
		<div class="footer_desc">
			<p>
				MGS그룹웨어 주식회사<br>
				사업자등록번호 : 123-45-67890<br>
				통신판매업신고번호 : 제1234-서울강동-1234호
				주소 : 서울시 강동구 천호동 123로<br>
			</p>
		</div>
		<div class="footer_law">
			<a href="#">개인정보처리방침</a>
			<a href="#">이용약관</a>
	
		</div>
	</footer>
</body>
</html>