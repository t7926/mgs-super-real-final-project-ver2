<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/schedule/css/reset.css">
	<!-- <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/schedule/css/common.css"> -->
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/schedule/css/registSch.css">
	<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<title>개인 일정 등록</title>
</head>
<body>

<section class="scheduleRegist">
	<div class="title">개인 일정 등록</div>
	<div class="contents">
		<div class="sche">
			<h4>일정</h4>
			<input type="date">
		</div>
		<div class="time">
			<h4>시간</h4>
			<input type="time">
		</div>
		<div class="content">
			<h4>내용</h4>
			<input type="text" style="width: 200px; height: 150px;">
		</div>
		<div class="schyn">
			<input type="checkbox">나만보기
			<input type="checkbox">공유하기
		</div>
		<div class="btn1">
			<a href="#"><p>등록하기</p></a>
			<a href="#"><p>닫기</p></a>
		</div>
	</div>
	
</section>

</body>
</html>