<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/schedule/css/reset.css">
	<!-- <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/schedule/css/common.css"> -->
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/schedule/css/registProjectSch.css">
	<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/fullcalendar@5.10.0/main.min.js"></script>
	
<title>프로젝트 일정 등록</title>
</head>
<body>

<section class="scheduleRegist">
	<div class="title">프로젝트 일정 등록</div>
	<div class="contents">
		<div class="sche">
			<h4>프로젝트명</h4>
			<input type="text" placeholder="프로젝트명을 입력해주세요.">
		</div>
		<div class="start">
			<h4>시작일</h4>
			<input type="date">
		</div>
		<div class="over">
			<h4>종료일</h4>
			<input type="date">
		</div>
		<div class="attendantMember">
			<h4>참여인원</h4>
			<input type="text" placeholder="이름을 입력해주세요.">
		</div>
		<div class="importance">
			<input type="checkbox">상
			<input type="checkbox">중
			<input type="checkbox">하
		</div>
		<div class="status">
			<input type="checkbox">작업전
			<input type="checkbox">작업중
			<input type="checkbox">완료
		</div>
		<div class="btn1">
			<a href="#"><p>등록하기</p></a>
			<a href="#"><p>닫기</p></a>
		</div>
	</div>
	
</section>

</body>
</html>