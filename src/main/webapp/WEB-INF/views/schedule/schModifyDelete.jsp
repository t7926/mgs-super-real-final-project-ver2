<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/schedule/css/reset.css">
	<!-- <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/schedule/css/common.css"> -->
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/schedule/css/schModifyDelete.css">
	<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<title>일정 수정 및 삭제</title>
</head>
<body>

<section class="scheduleRegist">
	<div class="title">일정 수정 및 삭제</div>
	<div class="contents">
		<h3>홍길동님! 수정을 원하시는 날짜를 입력해주세요.</h3>
		<div class="selectDate">
			<h4>일정</h4>
			<input type="date">
			<a href="#">조회</a>
		</div>
		<div class="privacySch">
			<h3>홍길동 님이 등록한 개인 일정이 3개 조회되었습니다.</h3>
			<table>
				<th style="font-weight: bold;">번호</th>
				<th style="font-weight: bold;">시간</th>
				<th style="font-weight: bold;">내용</th>
				<th style="font-weight: bold;">공개범위</th>
				<th style="font-weight: bold;">활동</th>
				<tr>
					<td>1</td>
					<td>09:30</td>
					<td>A회의실 홍보1팀 회의</td>
					<td><a href="#">전체보기</a></td>
					<td>55</td>
				</tr>
				<tr>
					<td>2</td>
					<td>11:30</td>
					<td>거래처 운영현황 연락</td>
					<td><a href="#">전체보기</a></td>
					<td>55</td>
				</tr>
				<tr>
					<td>3</td>
					<td>14:00</td>
					<td>인사팀으로부터 신병 인수</td>
					<td><a href="#">전체보기</a></td>
					<td>55</td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
			</table>
		</div>
		<div class="projectSch">
			<h3>홍길동 님이 관리하고 있는 프로젝트가 2개 조회되었습니다.</h3>
			<table>
				<th style="font-weight: bold;">번호</th>
				<th style="font-weight: bold;">프로젝트명</th>
				<th style="font-weight: bold;">상세내용</th>
				<th style="font-weight: bold;">시작일</th>
				<th style="font-weight: bold;">종료일</th>
				<th style="font-weight: bold;">중요도</th>
				<th style="font-weight: bold;">상태</th>
				<th style="font-weight: bold;">참여인원</th>
				<th style="font-weight: bold;">활동</th>
				<tr>
					<td>1</td>
					<td>신제품 사업</td>
					<td>신제품 개발을...</td>
					<td>21.07.15</td>
					<td>21.07.15</td>
					<td>상</td>
					<td>작업중</td>
					<td>홍길동, 김개똥</td>
					<td>99</td>
				</tr>
				<tr>
					<td>11</td>
					<td>22</td>
					<td>33</td>
					<td>44</td>
					<td>55</td>
					<td>66</td>
					<td>77</td>
					<td>88</td>
					<td>99</td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
					<td></td>
				</tr>
			</table>
		</div>
		<div class="btn1">
			<a href="#"><p>등록하기</p></a>
			<a href="#"><p>닫기</p></a>
		</div>
	</div>
	
</section>

</body>
</html>