<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
  <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

    <!DOCTYPE html>
    <html lang="en">

    <head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Document</title>
      <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/geuntae/css/reset.css">
      <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/geuntae/css/common.css">
      <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/geuntae/css/vacation-register.css">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
      <script src="https://kit.fontawesome.com/0295449c8a.js" crossorigin="anonymous"></script>
    </head>

    <body>


      <header class="header">
        <nav class="gnb">
          <h1 class="logo"><a href="#">
              <img src="${pageContext.request.contextPath}/resources/geuntae/images/logo.png" alt="">
            </a></h1>
            <ul class="main-menu">
              <li>
                <a href="${pageContext.request.contextPath}/payment/payment">전자결재</a>
              </li>
              <li>
                <a href="${pageContext.request.contextPath}/board/list">게시판</a>
              </li>
              <li>
                <a href="${pageContext.request.contextPath}/geuntae/calcul-time">근태관리</a>
              </li>
              <li>
                <a href="#">조직도</a>
              </li>
              <li>
                <a href="${pageContext.request.contextPath}/document/list">문서관리</a>
              </li>
            </ul>
        </nav>
      </header>

      <section class="main">
        <asdie id="aside">
          <div>근태관리</div>
          <div>
            <i class="fas fa-clock"></i>
            <a href="${pageContext.request.contextPath}/geuntae/calcul-time">근무시간조회</a>
          </div>
          <div>
            <i class="fas fa-table"></i>
            <a href="${pageContext.request.contextPath}/geuntae/vacation-table">휴가상태표</a>
            <ul>
              <li><a href="#">전체보기</a></li>
              <li><a href="#">승인된 휴가보기</a></li>
              <li><a href="#">승인 대기 중 휴가보기</a></li>
              <li><a href="#">반려된 휴가보기</a></li>
            </ul>
          </div>
          <div>
            <i class="fas fa-plane"></i>
            <a href="${pageContext.request.contextPath}/geuntae/vacation-register">휴가 신청</a>
            <ul>
              <li><a href="#">휴가신청서</a></li>
            </ul>
          </div>
          <div>
            <i class="fas fa-user-cog"></i>
            <a href="${ pageContext.servletContext.contextPath }/geuntae/wait-sign">관리자 설정</a>
            <ul>
              <li>
                <a href="${ pageContext.servletContext.contextPath }/geuntae/wait-sign">결재대기함</a>

              </li>
              <li>
                <a href="${ pageContext.servletContext.contextPath }/geuntae/human-resource">인사관리</a>
              </li>
            </ul>
          </div>
        </asdie>
        <div class="container">
          <div class="top">휴가신청서</div>
          <p>
            <c:out value="${ sessionScope.loginMember.name }" />님 휴가를 가시려는군요! 즐거운 시간 되세요:)
          </p>

          <div class="vacation">
            <form action="${ pageContext.servletContext.contextPath }/geuntae/vacation-register" method="post"
              onsubmit="return check();">
              <div class="start">
                <span>시작일</span>
                <input class="inputDate" name="start_date" type="date">
              </div>
              <div class="end">
                <span>종료일</span>
                <input class="inputDate" name="end_date" type="date">
              </div>
              <div class="vacation_category">
                <span>휴가종류</span>
                <select class="inputDate"  name="category">
                  <option value="연차">연차</option>
                  <option value="출산휴가">출산휴가</option>
                  <option value="배우자 출산 휴가">배우자 출산 휴가</option>
                  <option value="생리휴가">생리휴가</option>
                  <option value="가족 돌봄 휴가">가족 돌봄 휴가</option>
                </select>
              </div>
              <div class="senior">
               
                  <span>결재자</span>
                  <input type="radio" name="search_method" value="pno_name" id="pno_name"><label
                    for="pno_name">사원명</label>
                  <input type="radio" name="search_method" value="pno_number" id="pno_number"><label
                    for="pno_number">사원번호</label>
                  <input id="search_window" type="text" name="manager_main" style="width:100px">
                  <span id="search_btn" onclick="search()">검색</span>
               
                
              </div>
              <div id="reserach_result">

                <table id="search_table" border="1">
                  <thead>
                    <h6>검색결과</h6>
                    <tr>
                      <th width="80">부서</th>
                      <th width="80">팀</th>
                      <th width="80">이름</th>
                      <th width="80">직급</th>
                      <th width="100">사원번호</th>
                      <th width="50">선택</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>

                </table>

              </div>
              <div class="excuse">
                <span>휴가사유</span>
                <input placeholder="사유를 입력해주세요~ 필수는 아닙니다:)" name="reason" id="reasonInput"></input>
              </div>
              <button class="btn_submit">제출하기</button>
            </form>
          </div>
          <div class="vacation_box">
            <div>
              <span>총 연차</span>
              <span>${ annualVacationdto.annual_sum }일</span>
            </div>
            <div>
              <span>사용한 연차</span>
              <span>${ annualVacationdto.annual_used }일 </span>
            </div>
            <div>
              <span>남은 연차</span>
              <span>${annualVacationdto.annual_sum - annualVacationdto.annual_used}일</span>
            </div>
            <div>
              <span>마지막휴가</span>
              <span>${ annualVacationdto.last_vacation } </span>
            </div>
          </div>
        </div>
      </section>

      <footer>
        <div class="footer_logo">
          <img src="${pageContext.request.contextPath}/resources/geuntae/images/logo.png" alt="">

        </div>
        <div class="footer_desc">
          <p>
            MGS그룹웨어 주식회사<br>
            사업자등록번호 : 123-45-67890<br>
            통신판매업신고번호 : 제1234-서울강동-1234호
            주소 : 서울시 강동구 천호동 123로<br>
          </p>
        </div>
        <div class="footer_law">
          <a href="#">개인정보처리방침</a>
          <a href="#">이용약관</a>

        </div>
      </footer>
    </body>
    <script>

      /* 결재자 검색 */
      function search() {
        let search_method = $('input[name=search_method]:checked').val();
        let search_contents = $('input[name=manager_main]').val();

        console.log(search_method);

        if (!(search_contents === "" || typeof search_method === "undefined")) {

          $.ajax({
            url: "search",
            type: 'get',
            data: {
              search_contents: search_contents,
              search_method: search_method
            },
            success: function (data) {
              console.table(data);

              const $table = $("#search_table tbody");
              $table.html("");

              for (let index in data) {
                $tr = $("<tr>");
                $deptTd = $("<td>").text(data[index].dept);
                $teamTd = $("<td>").text(data[index].team);
                $nameTd = $("<td>").text(data[index].name);
                $levTd = $("<td>").text(data[index].lev);
                $noTd = $("<td>").text(data[index].no);
                $selectedTd = $("<td>").append("<button onclick='selectedUser(" + data[index].no + ")'>선택</button>");

                $tr.append($deptTd);
                $tr.append($teamTd);
                $tr.append($nameTd);
                $tr.append($levTd);
                $tr.append($noTd);
                $tr.append($selectedTd);

                $table.append($tr);


              }


            },
            error: function () {
              alert("error");
            }
          });


        } else {
          alert('검색조건을 선택하고, 검색할 내용을 입력해주세요!');


        }

      }

      function selectedUser(no) {
        console.log("선택된 사원번호는 " + no);
        document.querySelector('#pno_number').checked = true;
        document.querySelector('#search_window').value = no;
        document.querySelector('#search_window').readOnly = true;
        alert('선택되었습니다.');
      }


      function check() {

        if ($('input[name=start_date]').val() == "" || $('input[name=end_date]').val() == "") {
          alert('시작일 또는 종료일을 입력해주세요');
          return false;
        }

        else if ($('input[name=start_date]').val() > $('input[name=end_date]').val()) {
          alert('종료일은 시작일보다 빠를 수 없습니다.');
          return false;
        }

        else if (document.querySelector('#search_window').value == "" || document.querySelector('#search_window').value == null) {
          alert('결재자를 선택해주세요!');
          document.querySelector('#search_window').focus();
          return false;
        } else {
          alert('정상적으로 처리되었습니다.');
          return true;
        }
      }



    </script>

    </html>