<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
  <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

    <!DOCTYPE html>
    <html lang="en">

    <head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Document</title>
      <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/geuntae/css/reset.css">
      <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/geuntae/css/common.css">

      <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/geuntae/css/wait-sign.css">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
      <script src="https://kit.fontawesome.com/0295449c8a.js" crossorigin="anonymous"></script>
    </head>

    <body>


      <header class="header">
        <nav class="gnb">
          <h1 class="logo"><a href="#">
              <img src="${pageContext.request.contextPath}/resources/geuntae/images/logo.png" alt="">
            </a></h1>
            <ul class="main-menu">
              <li>
                <a href="${pageContext.request.contextPath}/payment/payment">전자결재</a>
              </li>
              <li>
                <a href="${pageContext.request.contextPath}/board/list">게시판</a>
              </li>
              <li>
                <a href="${pageContext.request.contextPath}/geuntae/calcul-time">근태관리</a>
              </li>
              <li>
                <a href="#">조직도</a>
              </li>
              <li>
                <a href="${pageContext.request.contextPath}/document/list">문서관리</a>
              </li>
            </ul>
        </nav>
      </header>

      <section class="main">
        <aside id="aside">
          <div>근태관리</div>
          <div>
            <i class="fas fa-clock"></i>
            <a href="${pageContext.request.contextPath}/geuntae/calcul-time">근무시간조회</a>
          </div>
          <div>
            <i class="fas fa-table"></i>
            <a href="${pageContext.request.contextPath}/geuntae/vacation-table">휴가상태표</a>
            <ul>
              <li><a href="#">전체보기</a></li>
              <li><a href="#">승인된 휴가보기</a></li>
              <li><a href="#">승인 대기 중 휴가보기</a></li>
              <li><a href="#">반려된 휴가보기</a></li>
            </ul>
          </div>
          <div>
            <i class="fas fa-plane"></i>
            <a href="${pageContext.request.contextPath}/geuntae/vacation-register">휴가 신청</a>
            <ul>
              <li><a href="#">휴가신청서</a></li>
            </ul>
          </div>
          <div>
            <i class="fas fa-user-cog"></i>
            <a href="${ pageContext.servletContext.contextPath }/geuntae/wait-sign">관리자 설정</a>
            <ul>
              <li>
                <a href="${ pageContext.servletContext.contextPath }/geuntae/wait-sign">결재대기함</a>

              </li>
              <li>
                <a href="${ pageContext.servletContext.contextPath }/geuntae/human-resource">인사관리</a>
              </li>
            </ul>
          </div>
        </aside>
        <div class="container">
          <div class="top">결재대기함</div>
          <div class="wrap">

            <div class="table_box">
              <h3>근무시간 수정 요청</h3>

              <table class="tg">
                <thead>
                  <tr>
                    <th class="tg-0pky">번호</th>
                    <th class="tg-0pky">성명</th>
                    <th class="tg-0pky">날짜</th>
                    <th class="tg-0pky">출근시간</th>
                    <th class="tg-0pky">퇴근시간</th>
                    <th class="tg-0pky">수정사유</th>
                    <th class="tg-0pky">결재자</th>
                    <th class="tg-0pky">승인/반려</th>
                  </tr>
                </thead>
                <tbody>
                  <c:forEach var="requestWorktime" items="${requestedModWorkTimeList}" varStatus="status">

                    <tr>
                      <td class="tg-0pky">
                        <c:out value="${ status.count }" />
                      </td>
                      <td class="tg-0pky">
                        <c:out value="${ requestWorktime.user.name }" />
                      </td>
                      <td class="tg-0pky">
                        <c:out value="${ requestWorktime.work_date }" />
                      </td>
                      <td class="tg-0pky">
                        <c:out value="${ requestWorktime.worktimemodify.time_start }" />
                      </td>
                      <td class="tg-0pky">
                        <c:out value="${ requestWorktime.worktimemodify.time_end}" />
                      </td>
                      <td class="tg-0pky">
                        <c:out value="${ requestWorktime.worktimemodify.reason }" />
                      </td>
                      <td class="tg-0pky">
                        <c:out value="${ requestWorktime.worktimemodify.manager_main }" />
                      </td>
                      <td class="tg-0pky">
                        <select name="payment_result" class="payment_result">
                          <option value="선택">선택</option>
                          <option value="승인">승인</option>
                          <option value="반려">반려</option>
                        </select>
                      </td>
                      <input type="hidden" name="work_no" id="work_no" value="${ requestWorktime.work_no }">
                    </tr>
                  </c:forEach>
                </tbody>
              </table>
            </div>

            <div class="table_box">
              <h3>휴가요청</h3>

              <form action="">
                <table class="tg">
                  <thead>
                    <tr>
                      <th class="tg-0pky">번호</th>
                      <th class="tg-0pky">성명</th>
                      <th class="tg-0pky">종류</th>
                      <th class="tg-0pky">시작일자</th>
                      <th class="tg-0pky">종료일자</th>
                      <th class="tg-0pky">사유</th>
                      <th class="tg-0pky">결재</th>
                      <th class="tg-0pky">승인/반려</th>
                    </tr>
                  </thead>
                  <tbody>

                    <c:forEach var="requestVacation" items="${requestedVacationList}" varStatus="status">
                      <tr>
                        <td class="tg-0pky">
                          <c:out value="${ status.count }" />
                        </td>
                        <td class="tg-0pky">
                          <c:out value="${ requestVacation.emp_name }" />
                        </td>
                        <td class="tg-0pky">
                          <c:out value="${ requestVacation.category }" />
                        </td>
                        <td class="tg-0pky">
                          <c:out value="${ requestVacation.start_date}" />
                        </td>
                        <td class="tg-0pky">
                          <c:out value="${ requestVacation.end_date}" />
                        </td>
                        <td class="tg-0pky">
                          <c:out value="${ requestVacation.reason }" />
                        </td>
                        <td class="tg-0pky">
                          <c:out value="${ requestVacation.manager_name }" />
                        </td>
                        <td class="tg-0pky">
                          <select name="vacation_result" class="vacation_result">
                            <option value="선택">선택</option>
                            <option value="승인">승인</option>
                            <option value="반려">반려</option>
                          </select>
                        </td>
                        <input type="hidden" name="vacation_no" id="vacation_no" value="${ requestVacation.no }">
                        <input type="hidden" name="request_no" id="request_no" value="${ requestVacation.emp_num }">
                      </tr>
                    </c:forEach>

                  </tbody>
                </table>
              </form>

            </div>

            <div class="table_box">
              <h3>휴가이력 삭제요청</h3>

              <form action="">
                <table class="tg">
                  <thead>
                    <tr>
                      <th class="tg-0pky">번호</th>
                      <th class="tg-0pky">성명</th>
                      <th class="tg-0pky">종류</th>
                      <th class="tg-0pky">시작일자</th>
                      <th class="tg-0pky">종료일자</th>
                      <th class="tg-0pky">사유</th>
                      <th class="tg-0pky">결재</th>
                      <th class="tg-0pky">승인/반려</th>
                    </tr>
                  </thead>
                  <tbody>
                    <c:forEach var="DeleteVacation" items="${requestedDeleteVacationList}" varStatus="status">

                      <tr>
                        <td class="tg-0pky">
                          <c:out value="${ status.count }" />
                        </td>
                        <td class="tg-0pky">
                          <c:out value="${ DeleteVacation.user_name }" />
                        </td>
                        <td class="tg-0pky">
                          <c:out value="${ DeleteVacation.category }" />
                        </td>
                        <td class="tg-0pky">
                          <c:out value="${ DeleteVacation.start_date}" />
                        </td>
                        <td class="tg-0pky">
                          <c:out value="${ DeleteVacation.end_date}" />
                        </td>
                        <td class="tg-0pky">
                          <c:out value="${ DeleteVacation.reason }" />
                        </td>
                        <td class="tg-0pky">
                          <c:out value="${ DeleteVacation.manager_name }" />
                        </td>
                        <td>
                          <select name="result" class="vacation_delete_result">
                            <option value="선택">선택</option>
                            <option value="승인">승인</option>
                            <option value="반려">반려</option>
                            <input type="hidden" value="${ DeleteVacation.vac_no }">

                          </select>
                        </td>
                      </tr>
                    </c:forEach>
                  </tbody>
                </table>
              </form>
            </div>
          </div>
        </div>
      </section>

      <footer>
        <div class="footer_logo"><img src="${pageContext.request.contextPath}/resources/geuntae/images/logo.png" alt="">
        </div>
        <div class="footer_desc">
          <p>
            MGS그룹웨어 주식회사<br>
            사업자등록번호 : 123-45-67890<br>
            통신판매업신고번호 : 제1234-서울강동-1234호
            주소 : 서울시 강동구 천호동 123로<br>
          </p>
        </div>
        <div class="footer_law">
          <a href="#">개인정보처리방침</a>
          <a href="#">이용약관</a>
        </div>
      </footer>
    </body>
    <script>
      /* 근무시간 수정에 대하여 관리자가 승인/반려 선택 시 실행 */
      $(".payment_result").on('change', function () {
        let rownum = this.parentElement.parentElement.rowIndex - 1;
        let workNum;
        let paymentResult;
        let start;
        let end;
        let request;
        console.dir(work_no);

        /* 결재요청이 하나만 있을 때에는 리스트 형식이 아니기 때문에  */
        if (work_no.length >= 2) {
          workNum = work_no[rownum].value;
          console.log('2이상');
        } else {
          workNum = work_no.value;
          console.log('2미만');
        }

        paymentResult = this.value;
        start = this.parentElement.parentElement.children[3].innerText;
        end = this.parentElement.parentElement.children[4].innerText;

        $.ajax({
          url: "wait-sign/worktime",
          type: 'post',
          data: {
            workNum: workNum,
            paymentResult: paymentResult,
            start: start,
            end: end

          },
          success: function (data) {

            console.log(data);

            if (data == '성공') {
              location.href = "/mgs/geuntae/wait-sign"
            }

          },
          error: function () {
            alert("error");
          }
        });
      })

      let vacationNumber;
      let paymentResult;
      let vacationCategory;

      /* 휴가이력 삭제요청에 대하여 관리자가 승인/반려 시 실행 */
      $(".vacation_delete_result").on('change', function () {
        vacationNumber = this.nextElementSibling.value
        paymentResult = this.value;
        vacationCategory = this.parentElement.parentElement.children[2].innerText;

        $.ajax({
          url: "wait-sign/vacationDelete",
          type: 'post',
          data: {
            vacationNumber: vacationNumber,
            paymentResult: paymentResult,
            vacationCategory: vacationCategory
          },
          success: function (data) {

            console.log(data);

            if (data == '성공') {
              location.href = "/mgs/geuntae/wait-sign"
            }

          },
          error: function () {
            alert("error");
          }
        });


      });


      /* 휴가 결재 */
      $(".vacation_result").on('change', function () {
        let rownum = this.parentElement.parentElement.rowIndex - 1;
        let vacationNum;
        let request_Empnum;
        let paymentResult;

        /* 휴가일수 계산 */
        let start = new Date(this.parentElement.parentElement.children[3].innerText);
        let end = new Date(this.parentElement.parentElement.children[4].innerText);
        let margin = ((end - start) / (1000 * 60 * 60 * 24)) + 1;


        /* 결재요청이 하나만 있을 때에는 리스트 형식이 아니기 때문에  */
        if (vacation_no.length >= 2) {
        	/* 2개 이상일 경우 */
          vacationNum = vacation_no[rownum].value;
          request_Empnum = request_no[rownum].value;
          
        } else {
        	/* 2개 미만일 경우 */
          vacationNum = vacation_no.value;
          request_Empnum = request_no.value;
        }

        paymentResult = this.value;
        vacation_category = this.parentElement.parentElement.children[2].innerText;

        $.ajax({
          url: "wait-sign/vacation",
          type: 'post',
          data: {
            vacationNum: vacationNum,
            paymentResult: paymentResult,
            vacation_category: vacation_category,
            margin: margin,
            request_Empnum: request_Empnum

          },
          success: function (data) {

            console.log(data);

            if (data == '성공') {
              location.href = "/mgs/geuntae/wait-sign"
            }

          },
          error: function () {
            alert("error");
          }
        });
      })




    </script>

    </html>