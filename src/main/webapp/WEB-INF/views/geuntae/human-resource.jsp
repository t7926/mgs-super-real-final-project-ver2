<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

		<!DOCTYPE html>
		<html lang="en">

		<head>
			<meta charset="UTF-8">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<meta name="viewport" content="width=device-width, initial-scale=1.0">
			<title>Document</title>
			<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/geuntae/css/reset.css">
			<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/geuntae/css/common.css">
			<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/geuntae/css/human-resource.css">
			<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
			<script src="https://kit.fontawesome.com/0295449c8a.js" crossorigin="anonymous"></script>

		</head>

		<body>


			<header class="header">
				<nav class="gnb">
					<h1 class="logo"><a href="#">
							<img src="${pageContext.request.contextPath}/resources/geuntae/images/logo.png" alt="">
						</a></h1>
						<ul class="main-menu">
							<li>
							  <a href="${pageContext.request.contextPath}/payment/payment">전자결재</a>
							</li>
							<li>
							  <a href="${pageContext.request.contextPath}/board/list">게시판</a>
							</li>
							<li>
							  <a href="${pageContext.request.contextPath}/geuntae/calcul-time">근태관리</a>
							</li>
							<li>
							  <a href="#">조직도</a>
							</li>
							<li>
							  <a href="${pageContext.request.contextPath}/document/list">문서관리</a>
							</li>
						  </ul>
				</nav>
			</header>

			<section class="main">
				<aside id="aside">
					<div>근태관리</div>
					<div>
						<i class="fas fa-clock"></i>
						<a href="${pageContext.request.contextPath}/geuntae/calcul-time">근무시간조회</a>
					</div>
					<div>
						<i class="fas fa-table"></i>
						<a href="${pageContext.request.contextPath}/geuntae/vacation-table">휴가상태표</a>
						<ul>
							<li><a href="#">전체보기</a></li>
							<li><a href="#">승인된 휴가보기</a></li>
							<li><a href="#">승인 대기 중 휴가보기</a></li>
							<li><a href="#">반려된 휴가보기</a></li>
						</ul>
					</div>
					<div>
						<i class="fas fa-plane"></i>
						<a href="${pageContext.request.contextPath}/geuntae/vacation-register">휴가 신청</a>
						<ul>
							<li><a href="#">휴가신청서</a></li>
						</ul>
					</div>
					<div>
						<i class="fas fa-user-cog"></i>
						<a href="${ pageContext.servletContext.contextPath }/geuntae/wait-sign">관리자 설정</a>
						<ul>
							<li>
								<a href="${ pageContext.servletContext.contextPath }/geuntae/wait-sign">결재대기함</a>

							</li>
							<li>
								<a href="${ pageContext.servletContext.contextPath }/geuntae/human-resource">인사관리</a>
							</li>
						</ul>
					</div>
				</aside>
				<div class="container">
					<div class="top">인사관리</div>
					<div class="wrap">
						<div class="wrap_top">
							<div class="rowBox">
								<h6>사원정보 검색하기</h6>
								<div class="search_wrap">
									<div>
										<label class="search_category">
											<input type="radio" name="category" value="pno_number">
											<span>사원번호</span>
										</label>

										<label class="search_category">
											<input type="radio" name="category" value="pno_name">
											<span>사원명</span>
										</label>
									</div>
									<div>
										<input id="search_content" type="text" placeholder="검색내용을 입력해주세요"
											style="width:200px;height:40px;font-size:13px;"></input>
										<input type="hidden" id="search_result_no" name="search_result_no"></input>
									</div>
									<span class="searchBtn" onclick="search()">검색</span>
								</div>
								<div class="search_result">
									<table id="search_table" border="1">
										<thead>
											<tr class="thTitle">
												<h6>검색결과</h6>
												<th width="80">부서</th>
												<th width="80">팀</th>
												<th width="80">이름</th>
												<th width="80">직급</th>
												<th width="100">사원번호</th>
												<th width="50">선택</th>
											</tr>
										</thead>
										<tbody>
										</tbody>

									</table>
								</div>


							</div>

						</div>
						<h4>조회된 사원</h4>
						<div class="wrap_bottom">
							<div class="left">
								<div class="profile">
									<span class="subtitle">사용자 프로필</span>
									<div class="profile-contents">
										<div class="profile-contents-desc">
											<span>이름 : <span id="name" class="profileData"></span></span>
											<span>입사일 :<span id="joinDate" class="profileData"></span> </span>
											<span>재직상태 : <span id="bye" class="profileData"></span> </span>
											<span>핸드폰 : <span id="phone" class="profileData"></span> </span>
											<span>부서 : <span id="dept" class="profileData"></span></span>
											<span>팀 : <span id="team" class="profileData"></span></span>
											<span>사원번호 : <span id="empnum" class="profileData"></span></span>
											<span>총 연차 : <span id="annaulTotal" class="profileData"></span>일</span>
											<span>사용한 연차 : <span id="annualUsed" class="profileData"></span>일</span>
											<span>남은 연차 : <span id="annualSpare" class="profileData"></span></span>
											<div>
												================연차 추가 부여 또는 회수============
												<input style="width: 50px" id="annualValue" name="annualValue"
													type="number">일 <span class="btns"
													onclick="plusAnnual()">적용하기</span>
											</div>
										</div>
									</div>
								</div>
								<div class="modify">
									<span class="subtitle">출퇴근 시간 수정</span>
									<div><span class="modtitle">날짜: </span><input name="workDate" type="date"><span
											class="btns" onclick="selectWorkTime()">조회</span></div>
									<div><span class="modtitle">출근시간 : </span><span id="selectedWorkStart"></span> ---->
										<input name="workStart" type="time"></div>
									<div><span class="modtitle">퇴근시간 : </span><span id="selectedWorkEnd"></span> ---->
										<input name="workEnd" type="time">
										<span id="worktimeBtn" class="btns" onclick="modifyWorktime()">수정</span>

									</div>
								</div>
							</div>
							<div class="right">
								<span class="subtitle">휴가이력삭제</span>
								<p>조회하고자 하는 날짜를 조회해보세요</p>
								<div class="rightmenu">
									<span class="righttitle">날짜 : </span>
									<input name="vacationDate" type="date">
									<span onclick="selectVacation()" class="btns">조회</span>
								</div>
								<div class="rightmenu">
									<span class="righttitle">시작일 : </span>
									<span id="vacationStart"></span>
								</div>
								<div class="rightmenu">
									<span class="righttitle">종료일 : </span>
									<span id="vacationEnd"></span>
								</div>
								<div class="rightmenu">
									<span class="righttitle">휴가종류 : </span>
									<span id="vacationCategory"></span>
									<input type="hidden" id="vac_no">

								</div>

								<span id="deleteBtn" class="btns" onclick="deleteVacation()">삭제</span>

							</div>
						</div>


					</div>

				</div>
			</section>

			<footer>
				<div class="footer_logo"><img src="${pageContext.request.contextPath}/resources/geuntae/images/logo.png"
						alt="">
				</div>
				<div class="footer_desc">
					<p>
						MGS그룹웨어 주식회사<br>
						사업자등록번호 : 123-45-67890<br>
						통신판매업신고번호 : 제1234-서울강동-1234호
						주소 : 서울시 강동구 천호동 123로<br>
					</p>
				</div>
				<div class="footer_law">
					<a href="#">개인정보처리방침</a>
					<a href="#">이용약관</a>

				</div>
			</footer>
		</body>

		<script>
			/* 검색기능 */
			let search_method = "";
			let search_contents = "";

			function search() {
				search_method = $('input[name=category]:checked').val();
				search_contents = $('input[id=search_content]').val()
				if (search_method == undefined) {
					alert('검색방법을 선택해주세요');
				} else if (search_contents == "") {
					alert('검색내용을 입력해주세요')
				} else {
					/* 검색결과 */
					$.ajax({
						url: "search_emp",
						type: 'get',
						data: {
							search_contents: search_contents,
							search_method: search_method
						},
						success: function (data) {
							console.table("data" + data);
							if (data == null || data == "") {
								alert('존재하지 않는 사원입니다.');
							}

							const $table = $("#search_table tbody");
							$table.html("");

							for (let index in data) {
								$tr = $("<tr>");
								$deptTd = $("<td>").text(data[index].dept);
								$teamTd = $("<td>").text(data[index].team);
								$nameTd = $("<td>").text(data[index].name);
								$levTd = $("<td>").text(data[index].lev);
								$noTd = $("<td>").text(data[index].no);
								$selectedTd = $("<td>").append("<button type='button' onclick='selectedUser(" + data[index].no + ")'>선택</button>");

								$tr.append($deptTd);
								$tr.append($teamTd);
								$tr.append($nameTd);
								$tr.append($levTd);
								$tr.append($noTd);
								$tr.append($selectedTd);

								$table.append($tr);
							}
						},
						error: function () {
							alert("error");
						}
					});
				}
			}

			/* 전역변수로 조회한 사원번호 저장 */
			let emp_no = 0;

			/* 선택 클릭시 작동하는 함수 */
			function selectedUser(no, type) {
				/* 선택한 사원의 사번을 hidden된 input값에 넣는다. */
				emp_no = no;
				/* 선택이 완료되었다는 것을 가시적으로 보여주기 위해 alert를 이용한다. */
				if (type != "check") {

					alert('사원번호 : ' + no + '번이 선택되었습니다.');
				}


				/* 선택한 사원정보로 프로필정보를 가지고 온다. */
				$.ajax({
					url: "search_profile",
					type: 'get',
					data: {
						emp_no: emp_no,
					},
					success: function (data) {
						//alert("success");
						document.querySelector('#name').innerText = data.name;
						document.querySelector('#joinDate').innerText = data.joinDate;
						document.querySelector('#phone').innerText = data.phone;
						document.querySelector('#dept').innerText = data.dept;
						document.querySelector('#team').innerText = data.team;
						document.querySelector('#empnum').innerText = data.empNum;
						document.querySelector('#annaulTotal').innerText = data.annual_total;
						document.querySelector('#annualUsed').innerText = data.annual_used;
						document.querySelector('#annualSpare').innerText = data.annual_total - data.annual_used;

						if (data.bye === 'Y') {
							document.querySelector('#bye').innerText = '퇴직'
						} else {
							document.querySelector('#bye').innerText = '재직중'
						}
						/* 					let i = 0;
											for(let d in data){
												document.getElementsByClassName("profileData")[i].innerText = data[d];
												i++;
												if(i == 9){
													document.getElementsByClassName("profileData")[i].innerText = data["annual_total"] - data["annual_used"];
												}
												
											} */
					},
					error: function () {
						alert("error");
					}
				});
			}

			let annualValue = "";

			/* 연차 부여하기 버튼 누르면 실행되는 함수 */
			function plusAnnual() {

				annualValue = $('input[name=annualValue]').val();

				if (document.querySelector('#name').innerText === null || document.querySelector('#name').innerText === "") {
					alert('사원을 먼저 조회해주세요:)');
				}

				else if (annualValue === "" || annualValue === null) {
					alert('부여할 연차 일수를 입력해주세요');
				}
				else {
					alert(annualValue + '일 적용하겠습니다.');

					/* 연차부여하는 ajax */
					$.ajax({
						url: "annualValuePlus",
						type: 'post',
						data: {
							annualValue: annualValue,
							emp_no: emp_no
						},
						success: function (data) {
							if (data == "success") {
								alert("연차 적용에 성공하였습니다.");
								selectedUser(emp_no, "check");
								console.dir(document.querySelector('#annualValue').value = null)
							} else {

								alert("연차 적용에 실패하였습니다.");
							}
						},
						error: function () {
							alert("error");
						}
					});





				}
			}

			let Workdate = "";

			/* 출퇴근 시간 수정에서 날짜 조회하면 나오는 메소드 */
			function selectWorkTime() {
				if (emp_no !== 0) {

					Workdate = $('input[name=workDate]').val();
					/* 날짜조회버튼 누르면 실행되는 ajax */
					$.ajax({
						url: "WorkbyManager",
						type: 'get',
						data: {
							emp_no: emp_no,
							Workdate: Workdate
						},
						success: function (data) {
							if (data !== null) {

								document.querySelector('#selectedWorkStart').innerText = data.start
								document.querySelector('#selectedWorkEnd').innerText = data.end



								/*  							selectedUser(emp_no, "check");
																console.dir(document.querySelector('#annualValue').value=null) */
							} else {

								alert("해당일에 근무한 이력이 없습니다.");
							}
						},
						error: function () {
							alert("error");
						}
					});


				} else {

					alert('조회할 사원 번호를 입력해주세요!');

				}
			}

			let workStart = "";
			let workEnd = "";
			/* 출퇴근시간 수정 버튼을 누르면 작동하는 함수 */
			function modifyWorktime() {
				workStart = $('input[name=workStart]').val();
				workEnd = $('input[name=workEnd]').val();
				//유효성검사
				if (emp_no === 0) {
					alert('사원정보를 먼저 입력해주세요');
				}

				else if (Workdate === "") {
					alert('날짜를 먼저 조회해주세요');
				}

				else if (workStart === "") {
					alert('변경할 출근 시간을 설정해주세요');
				}

				else if (workEnd === "") {
					alert('변경할 퇴근 시간을 설정해주세요');
				} else {
					$.ajax({
						url: "WorkModbyManager",
						type: 'post',
						data: {
							workStart: workStart,
							workEnd: workEnd,
							emp_no: emp_no,
							Workdate: Workdate
						},
						success: function (data) {

							if (data === "success") {
								alert('성공적으로 수정을 마쳤습니다.');
								reset();

							} else {
								alert('수정에 실패했습니다.');
							}


						},
						error: function () {
							alert("error");
						}
					});
				}




			}

			function reset() {
				document.querySelector("[name=workDate]").value = null;
				document.querySelector("[name=workStart]").value = null;
				document.querySelector("[name=workEnd]").value = null;
			}



			function selectVacation() {

				if (emp_no == 0) {
					alert('사원정보를 먼저 검색해주세요!');
				}

				else if (document.querySelector("[name=vacationDate]").value !== "") {
					let date = document.querySelector("[name=vacationDate]").value;

					$.ajax({
						url: "selectVacation",
						type: 'get',
						data: {
							emp_no: emp_no,
							date: date

						},
						success: function (data) {

							if (data !== null) {

								document.querySelector('#vacationStart').innerText = data.start_date;
								document.querySelector('#vacationEnd').innerText = data.end_date;
								document.querySelector('#vacationCategory').innerText = data.category;
								document.querySelector('#vac_no').value = data.no;




							} else {
								alert('해당일에 휴가를 다녀온 이력이 없습니다.');

							}


						},
						error: function () {
							alert("error");
						}
					});
				} else {
					alert('날짜를 입력해주세요~');

				}
			}

			function deleteVacation() {
				let vacationDate = document.querySelector("[name=vacationDate]").value
				let vac_no = document.querySelector('#vac_no').value
				let vac_category = document.querySelector('#vacationCategory').innerText
				if (emp_no === 0) {
					alert('사원정보를 먼저 검색해주세요!');
				} else if (vacationDate === "") {
					alert('날짜를 먼저 조회해주세요!')
				} else {


					$.ajax({
						url: "deleteVacationManager",
						type: 'post',
						data: {
							emp_no: emp_no,
							vacationDate: vacationDate,
							vac_no: vac_no,
							vac_category: vac_category

						},
						success: function (data) {
							alert("success");

						},
						error: function () {
							alert("error");
						}
					});


				}
			}




		</script>

		</html>