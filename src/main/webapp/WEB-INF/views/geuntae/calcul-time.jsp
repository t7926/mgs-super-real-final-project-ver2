<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
  <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

    <!DOCTYPE html>
    <html lang="en">

    <head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Document</title>
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
      <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/geuntae/css/reset.css">
      <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/geuntae/css/common.css">
      <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/geuntae/css/calcul-time.css">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
      <script src="https://kit.fontawesome.com/0295449c8a.js" crossorigin="anonymous"></script>
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
        crossorigin="anonymous"></script>

    </head>

    <body>
      <header class="header">
        <nav class="gnb">
          <h1 class="logo"><a href="${pageContext.request.contextPath}/">
              <img src="${pageContext.request.contextPath}/resources/geuntae/images/logo.png" alt="">
            </a></h1>
            <ul class="main-menu">
              <li>
                <a href="${pageContext.request.contextPath}/payment/payment">전자결재</a>
              </li>
              <li>
                <a href="${pageContext.request.contextPath}/board/list">게시판</a>
              </li>
              <li>
                <a href="${pageContext.request.contextPath}/geuntae/calcul-time">근태관리</a>
              </li>
              <li>
                <a href="#">조직도</a>
              </li>
              <li>
                <a href="${pageContext.request.contextPath}/document/list">문서관리</a>
              </li>
            </ul>
        </nav>
      </header>

      <section class="main">
        <asdie id="aside">
          <div>근태관리</div>
          <div>
            <i class="fas fa-clock"></i>
            <a href="${pageContext.request.contextPath}/geuntae/calcul-time">근무시간조회</a>
          </div>
          <div>
            <i class="fas fa-table"></i>
            <a href="${pageContext.request.contextPath}/geuntae/vacation-table">휴가상태표</a>
            <ul>
              <li><a href="#">전체보기</a></li>
              <li><a href="#">승인된 휴가보기</a></li>
              <li><a href="#">승인 대기 중 휴가보기</a></li>
              <li><a href="#">반려된 휴가보기</a></li>
            </ul>
          </div>
          <div>
            <i class="fas fa-plane"></i>
            <a href="${pageContext.request.contextPath}/geuntae/vacation-register">휴가 신청</a>
            <ul>
              <li><a href="#">휴가신청서</a></li>
            </ul>
          </div>
          <div>
            <i class="fas fa-user-cog"></i>
            <a href="${ pageContext.servletContext.contextPath }/geuntae/wait-sign">관리자 설정</a>
            <ul>
              <li>
                <a href="${ pageContext.servletContext.contextPath }/geuntae/wait-sign">결재대기함</a>

              </li>
              <li>
                <a href="${ pageContext.servletContext.contextPath }/geuntae/human-resource">인사관리</a>
              </li>
            </ul>
          </div>
        </asdie>
        <div class="container">

          <div class="top">근무시간조회</div>
          <div class="month">
            <span class="moveBtn" onclick="prev()"><i class="far fa-arrow-alt-circle-left"></i></span>
            <span id="duration">
              <c:out value="${year}년 ${month}월" />
            </span>

            <span class="moveBtn" onclick="post()"><i class="far fa-arrow-alt-circle-right"></i></span>
          </div>
          <div class="month_table">
            <table class="tg">
              <thead>
                <tr>
                  <th class="tg-0lax">일시</th>
                  <th class="tg-0lax">출근시간</th>
                  <th class="tg-0lax">퇴근시간</th>
                  <th class="tg-0lax">근무시간</th>
                  <th class="tg-0lax">수정일</th>
                  <th class="tg-0lax">수정요청</th>
                </tr>
              </thead>
              <tbody>
                <c:forEach var="WorktimeDTO" items="${worktimeList}">
                  <tr>
                    <td class="tg-0lax"><span id="date_">
                        <c:out value="${ WorktimeDTO.work_date }" />
                      </span></td>
                    <td class="tg-hmp3">
                      <c:out value="${ WorktimeDTO.work_start_time }" />
                    </td>
                    <td class="tg-0lax">
                      <c:out value="${ WorktimeDTO.work_end_time }" />
                    </td>
                    <td class="tg-hmp3">
                      <c:out value="${ WorktimeDTO.work_time } 시간" />
                    </td>
                    <td class="tg-0lax">${ WorktimeDTO.worktimemodify.payment_date }</td>
                    <input type="hidden" id="work_no_" value="${ WorktimeDTO.work_no }">

                    <td class="tg-hmp3">
                      <!-- Button trigger modal -->
                      <c:choose>
                        <c:when test="${ WorktimeDTO.modTF == 'T'}">
                          <button disabled="disabled" id="request_btn" type="button" class="btn btn-primary"
                            data-bs-toggle="modal" data-bs-target="#exampleModal">
                            요청됨
                          </button>
                        </c:when>
                        <c:otherwise>
                          <button id="request_btn" type="button" class="btn btn-primary" onclick="test()"
                            data-bs-toggle="modal" data-bs-target="#exampleModal">
                            요청
                          </button>
                        </c:otherwise>
                      </c:choose>

                      <!--    <button id="request_btn" type="button" class="btn btn-primary" onclick="test()" data-bs-toggle="modal" data-bs-target="#exampleModal">
                        요청
                      </button> -->
                      <!-- Modal -->
                      <form action="${ pageContext.servletContext.contextPath }/geuntae/calcul-time" method="post"
                        onsubmit="return check();">
                        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel"
                          aria-hidden="true">
                          <div class="modal-dialog">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">근무시간 수정</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                  aria-label="Close"></button>
                              </div>
                              <div class="modal-body">
                                <div class="category">
                                  <div>일시</div>
                                  <div>출근시간</div>
                                  <div>퇴근시간</div>
                                  <div>결재자</div>
                                  <div>수정사유</div>
                                </div>
                                <div class="content">
                                  <div id="date"></div>
                                  <div><input type="time" name="time_start">으로 바꾸고 싶어요!</div>
                                  <div><input type="time" name="time_end">으로 바꾸고 싶어요</div>
                                  <input type="radio" name="search_method" value="pno_name" id="pno_name"><label
                                    for="pno_name">사원명</label>
                                  <input type="radio" name="search_method" value="pno_number" id="pno_number"><label
                                    for="pno_number">사원번호</label>
                                  <input id="search_window" type="text" name="manager_main" style="width:100px">
                                  <span id="search_btn" onclick="search()">검색</span>
                                  <div class="searchBox">

                                    <p>검색결과</p>
                                    <div class="search_result">
                                      <table id="search_table" border="1">
                                        <thead>
                                          <tr>
                                            <th width="80">부서</th>
                                            <th width="80">팀</th>
                                            <th width="80">이름</th>
                                            <th width="80">직급</th>
                                            <th width="100">사원번호</th>
                                            <th width="50">선택</th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>

                                      </table>
                                    </div>
                                  </div>


                                  <input type="hidden" id="work_no" name="work_no">
                                  <div><textarea name="reason" id="" cols="40" rows="3"></textarea></div>
                                </div>
                              </div>
                              <div class="modal-footer">
                                <button type="submit" class="btn btn-secondary" data-bs-dismiss="modal">제출</button>
                                <!-- <button type="submit" class="btn btn-primary">제출</button> -->
                              </div>
                            </div>
                          </div>
                        </div>
                      </form>
                    </td>
                  </tr>
                </c:forEach>


              </tbody>

            </table>

          </div>
        </div>
      </section>

      <footer>
        <div class="footer_logo"><img src="${pageContext.request.contextPath}/resources/geuntae/images/logo.png" alt="">
        </div>
        <div class="footer_desc">
          <p>
            MGS그룹웨어 주식회사<br>
            사업자등록번호 : 123-45-67890<br>
            통신판매업신고번호 : 제1234-서울강동-1234호
            주소 : 서울시 강동구 천호동 123로<br>
          </p>
        </div>
        <div class="footer_law">
          <a href="#">개인정보처리방침</a>
          <a href="#">이용약관</a>

        </div>
      </footer>



    </body>
    <script>

      /* 모달창 제출 전 입력값 체크 */
      function check() {

        if ($('input[name=time_start]').val() == "") {
          alert('정상적으로 등록되지 않았습니다!!!! 출근시간을 입력 후 다시 신청하세요.');
          return false;
        }

        else if ($('input[name=time_end]').val() == "") {
          alert('정상적으로 등록되지 않았습니다!!!! 퇴근시간을 입력 후 다시 신청하세요.');
          return false;
        }

        else if ($('input[name=time_start]').val() > $('input[name=time_end]').val()) {
          alert('정상적으로 등록되지 않았습니다!!!! 출근시간이 퇴근시간보다 늦을 수 없습니다. 확인 후 다시 신청하세요');
          return false;
        }

        else if ($('input[name=manager_main]').val() == null) {
          alert('정상적으로 등록되지 않았습니다!!!! 결재자를 입력 후 다시 신청하세요.');
          return false;

        } else {
          return true;

        }
      }






      /* 연월 화살표 클릭시 이동 */
      let yearNum = Number(document.querySelector('#duration').innerText.split(" ")[0].substr(0, 4));
      let monthNum = Number(document.querySelector('#duration').innerText.split(" ")[1].substr(0, 2));
      let duration;

      function prev() {
        console.log("prev 작동완료");
        monthNum -= 1;
        if (monthNum == 0) {
          yearNum -= 1;
          monthNum = 12;
        }
        years = yearNum.toString();
        months = monthNum.toString().padStart(2, '0');
        location.href = "/mgs/geuntae/calcul-time?year=" + years + "&month=" + months;
      }

      function post() {
        console.log("post 작동완료");
        monthNum += 1;

        if (monthNum == 13) {
          yearNum += 1;
          monthNum = 1;
        }

        years = yearNum.toString();
        months = monthNum.toString().padStart(2, '0');
        location.href = "/mgs/geuntae/calcul-time?year=" + years + "&month=" + months;

      }


      /* 수정요청 버튼 클릭시 일시, 근무시간 고유번호 가져오기 */
      let request_btn = document.querySelectorAll('#request_btn');
      let work_no_ = document.querySelectorAll('#work_no_');
      let work_no = document.querySelector('#work_no');
      for (let i = 0; i < request_btn.length; i++) {
        request_btn[i].onclick = function (e) {
          work_no.value = work_no_[i].value;
          let test1 = e.target.parentNode.parentNode.children[0].innerText;
          document.getElementById('date').innerText = test1;
        }
      }

      /* 결재자 검색 */
      function search() {
        let search_method = $('input[name=search_method]:checked').val();
        let search_contents = $('input[name=manager_main]').val();

        console.log(search_method);

        if (!(search_contents === "" || typeof search_method === "undefined")) {

          $.ajax({
            url: "search",
            type: 'get',
            data: {
              search_contents: search_contents,
              search_method: search_method
            },
            success: function (data) {
              console.table("data" + data);

              const $table = $("#search_table tbody");
              $table.html("");

              for (let index in data) {
                $tr = $("<tr>");
                $deptTd = $("<td>").text(data[index].dept);
                $teamTd = $("<td>").text(data[index].team);
                $nameTd = $("<td>").text(data[index].name);
                $levTd = $("<td>").text(data[index].lev);
                $noTd = $("<td>").text(data[index].no);
                $selectedTd = $("<td>").append("<button type='button' onclick='selectedUser(" + data[index].no + ")'>선택</button>");

                $tr.append($deptTd);
                $tr.append($teamTd);
                $tr.append($nameTd);
                $tr.append($levTd);
                $tr.append($noTd);
                $tr.append($selectedTd);

                $table.append($tr);


              }


            },
            error: function () {
              alert("error");
            }
          });


        } else {
          alert('검색조건을 선택하고, 검색할 내용을 입력해주세요!');


        }

      }

      function selectedUser(no) {
        console.log("선택된 사원번호는 " + no);
        document.querySelector('#pno_number').checked = true;
        document.querySelector('#search_window').value = no;
        document.querySelector('#search_window').readOnly = true;
        alert('선택되었습니다.');
      }

    </script>

    </html>