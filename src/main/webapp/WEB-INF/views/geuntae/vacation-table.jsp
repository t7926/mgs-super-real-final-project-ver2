<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
  <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


    <!DOCTYPE html>
    <html lang="en">

    <head>
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Document</title>
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
      <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/geuntae/css/reset.css">
      <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/geuntae/css/common.css">
      <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/geuntae/css/vacation-table.css">
      
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
      <script src="https://kit.fontawesome.com/0295449c8a.js" crossorigin="anonymous"></script>
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM"
        crossorigin="anonymous"></script>

    </head>

    <body>




      <header class="header">
        <nav class="gnb">
          <h1 class="logo"><a href="#">
              <img src="${pageContext.request.contextPath}/resources/geuntae/images/logo.png" alt="">
            </a></h1>
            <ul class="main-menu">
              <li>
                <a href="${pageContext.request.contextPath}/payment/payment">전자결재</a>
              </li>
              <li>
                <a href="${pageContext.request.contextPath}/board/list">게시판</a>
              </li>
              <li>
                <a href="${pageContext.request.contextPath}/geuntae/calcul-time">근태관리</a>
              </li>
              <li>
                <a href="#">조직도</a>
              </li>
              <li>
                <a href="${pageContext.request.contextPath}/document/list">문서관리</a>
              </li>
            </ul>
        </nav>

      </header>

      <section class="main">

        <aside id="aside">
          <div>근태관리</div>
          <div>
            <i class="fas fa-clock"></i>
            <a href="${pageContext.request.contextPath}/geuntae/calcul-time">근무시간조회</a>
          </div>
          <div>
            <i class="fas fa-table"></i>
            <a href="${pageContext.request.contextPath}/geuntae/vacation-table">휴가상태표</a>
            <ul>
              <li><a href="#">전체보기</a></li>
              <li><a href="#">승인된 휴가보기</a></li>
              <li><a href="#">승인 대기 중 휴가보기</a></li>
              <li><a href="#">반려된 휴가보기</a></li>
            </ul>
          </div>
          <div>
            <i class="fas fa-plane"></i>
            <a href="${pageContext.request.contextPath}/geuntae/vacation-register">휴가 신청</a>
            <ul>
              <li><a href="#">휴가신청서</a></li>
            </ul>
          </div>
          <div>
            <i class="fas fa-user-cog"></i>
            <a href="${ pageContext.servletContext.contextPath }/geuntae/wait-sign">관리자 설정</a>
            <ul>
              <li>
                <a href="${ pageContext.servletContext.contextPath }/geuntae/wait-sign">결재대기함</a>

              </li>
              <li>
                <a href="${ pageContext.servletContext.contextPath }/geuntae/human-resource">인사관리</a>
              </li>
            </ul>
          </div>

        </aside>

        <div class="container">
          <div class="top">휴가상태표</div>
          <div class="month">
            <span class="moveBtn" onclick="prev()"><i class="far fa-arrow-alt-circle-left"></i></span>
            <span id="duration">
              <c:out value="${year}년" />
            </span>
            <span class="moveBtn" onclick="post()"><i class="far fa-arrow-alt-circle-right"></i></span>
          </div>
          <div class="month_table">
            <table class="tg">
              <thead>
                <tr>
                  <th class="tg-0lax">번호</th>
                  <th class="tg-0lax">휴가시작일</th>
                  <th class="tg-0lax">휴가종료일</th>
                  <th class="tg-0lax">결재자</th>
                  <th class="tg-0lax">휴가종류</th>
                  <th class="tg-0lax">상태</th>
                  <th class="tg-0lax">수정 및 취소</th>

                </tr>
              </thead>
              <tbody>
                <c:forEach var="VacationDTO" items="${vacationList}" varStatus="status">
                  <tr>
                    <td class="tg-0lax"><span id="date_">
                        <c:out value="${ status.count }" />
                      </span></td>
                    <td class="tg-hmp3">
                      <c:out value="${ VacationDTO.start_date }" />
                    </td>
                    <td class="tg-0lax">
                      <c:out value="${ VacationDTO.end_date}" />
                    </td>
                    <td class="tg-hmp3">
                      <c:out value="${ VacationDTO.manager_name}" />
                    </td>
                    <td class="tg-0lax">
                      <c:out value="${ VacationDTO.category}" />
                    </td>
                    <td class="tg-hmp3">
                      <c:out value="${ VacationDTO.paymentState}" />

                    </td>
                    <td class="tg-0lax">
                      <c:choose>
                        <c:when test="${ VacationDTO.paymentState != '승인대기중'}">

                          <c:if test="${ VacationDTO.deleteYn == 'Y' }">
                            <button disabled="disabled" id="request_btn" type="button"
                              class="btn btn-primary request_btn" data-bs-toggle="modal" data-bs-target="#exampleModal">
                              삭제요청됨
                              <input type="hidden" id="vacation_no" value="${ VacationDTO.no }">
                            </button>
                          </c:if>
                          <c:if test="${VacationDTO.deleteYn != 'Y' }">
                            <button id="request_btn" type="button" class="btn btn-primary request_btn"
                              data-bs-toggle="modal" data-bs-target="#exampleModal">
                              삭제요청
                              <input type="hidden" id="vacation_no" value="${ VacationDTO.no }">
                            </button>
                          </c:if>

                        </c:when>
                        <c:otherwise>
                          <button onclick="btn_click(${ VacationDTO.no })" id="calcelVacationBtn" type="button"
                            class="btn btn-primary calcel_btn">
                            신청취소
                            <input type="hidden" id="vacation_no" value="${ VacationDTO.no }">
                          </button>
                        </c:otherwise>
                      </c:choose>

                      <!--휴가 삭제 요청에 대한 Modal -->
                      <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel"
                        aria-hidden="true">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <h5 class="modal-title" id="exampleModalLabel">휴가 삭제 요청</h5>
                              <button type="button" class="btn-close" data-bs-dismiss="modal"
                                aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                              <div class="rowBox">
                                <h6>휴가시작일</h6>
                                <div id="start"></div>
                              </div>
                              <div class="rowBox">
                                <h6>휴가종료일</h6>
                                <div id="end">2022년 4월 11일</div>
                              </div>
                              <div>
                                <div class="rowBox">
                                  <h6>결재자</h6>
                                  <div class="search_wrap">
                                    <div>
                                      <label class="search_category">
                                        <input type="radio" name="category" value="pno_number">
                                        <span>사원번호</span>
                                      </label>

                                      <label class="search_category">
                                        <input type="radio" name="category" value="pno_name">
                                        <span>사원명</span>
                                      </label>
                                    </div>
                                    <div>
                                      <input id="search_content" type="text" placeholder="검색내용을 입력해주세요"
                                        style="width:200px;height:40px;font-size:13px;"></input>
                                      <input type="hidden" id="search_result_no" name="search_result_no"></input>
                                    </div>
                                    <span class="searchBtn" onclick="search()">검색</span>
                                  </div>
                                  <div class="search_result">
                                    <table id="search_table" border="1">
                                      <thead>
                                        <tr>
                                          <th width="80">부서</th>
                                          <th width="80">팀</th>
                                          <th width="80">이름</th>
                                          <th width="80">직급</th>
                                          <th width="100">사원번호</th>
                                          <th width="50">선택</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                      </tbody>

                                    </table>
                                  </div>


                                </div>
                                <div class="reason rowBox">
                                  <h6>사유</h6>
                                  <input name="reason" type="text" placeholder="사유를 입력해주세요"
                                    style="border:none;width:100%"></input>
                                </div>
                              </div>
                              <div class="modal-footer">
                                <button onclick="submit()" type="submit" class="btn btn-secondary"
                                  data-bs-dismiss="modal">제출</button>
                              </div>
                            </div>
                          </div>
                        </div>
                </c:forEach>

              </tbody>
            </table>

            <!-- 휴가 취소전 체크하기 위한 모달창 -->
            <div id="calcel_check_modal">



              <div class="calcel_check_content">
                <h2>정말 취소하시겠습니까?</h2>

                <div class="btn_box">
                  <div id="calcel_check_close_btn">아니오</div>
                  <div id="cancel_yes" onclick="yes()">네</div>
                </div>





              </div>

              <div class="calcel_check_layer"></div>
            </div>

          </div>

        </div>

      </section>

      <footer>
        <div class="footer_logo">
          <img src="${pageContext.request.contextPath}/resources/geuntae/images/logo.png" alt="">
        </div>
        <div class="footer_desc">
          <p>
            MGS그룹웨어 주식회사<br>
            사업자등록번호 : 123-45-67890<br>
            통신판매업신고번호 : 제1234-서울강동-1234호
            주소 : 서울시 강동구 천호동 123로<br>
          </p>
        </div>
        <div class="footer_law">
          <a href="#">개인정보처리방침</a>
          <a href="#">이용약관</a>

      </footer>
    </body>
    <script>
      /* 휴가 삭제 요청 클릭시 모달창에 시작일과 종료일 고정값으로 놓기 위한 전역변수 설정 */
      let startDate;
      let endDate;
      let vacNum;
      let request_btn_list = document.querySelectorAll('.request_btn');

      /* 삭제요청 클릭시 해당 row의 휴가시작일, 종료일, 고유번호를 모달창(hidden)에 나타내기. */
      for (let i = 0; i < request_btn_list.length; i++) {
        request_btn_list[i].addEventListener('click', function () {
          startDate = this.parentElement.parentElement.children[1].innerText;
          endDate = this.parentElement.parentElement.children[2].innerText;
          vacNum = this.firstElementChild.value;

          document.querySelector('#start').innerText = startDate;
          document.querySelector('#end').innerText = endDate;

        })
      }

      /* 검색기능 */
      let search_method;
      let search_contents;

      function search() {
        search_method = $('input[name=category]:checked').val();
        search_contents = $('input[id=search_content]').val()
        if (search_method == undefined) {
          alert('검색방법을 선택해주세요');
        } else if (search_contents == "") {
          alert('검색내용을 입력해주세요')
        } else {
          /* 검색결과 */
          $.ajax({
            url: "search",
            type: 'get',
            data: {
              search_contents: search_contents,
              search_method: search_method
            },
            success: function (data) {
              console.table("data" + data);
              if (data == null || data == "") {
                alert('존재하지 않는 사원입니다.');
              }

              const $table = $("#search_table tbody");
              $table.html("");

              for (let index in data) {
                $tr = $("<tr>");
                $deptTd = $("<td>").text(data[index].dept);
                $teamTd = $("<td>").text(data[index].team);
                $nameTd = $("<td>").text(data[index].name);
                $levTd = $("<td>").text(data[index].lev);
                $noTd = $("<td>").text(data[index].no);
                $selectedTd = $("<td>").append("<button type='button' onclick='selectedUser(" + data[index].no + ")'>선택</button>");

                $tr.append($deptTd);
                $tr.append($teamTd);
                $tr.append($nameTd);
                $tr.append($levTd);
                $tr.append($noTd);
                $tr.append($selectedTd);

                $table.append($tr);


              }


            },
            error: function () {
              alert("error");
            }
          });


        }
      }

      let search_result_no = document.querySelector('#search_result_no').innerText;
      let search_content = document.querySelector('#search_content');

      /* 선택 클릭시 작동하는 함수 */
      function selectedUser(no) {
        /* 선택한 사원의 사번을 hidden된 input값에 넣는다. */
        search_result_no = no;

        /* 선택이 완료되었다는 것을 가시적으로 보여주기 위해 alert를 이용한다. */
        alert('사원번호 : ' + no + '번이 선택되었습니다.');

      }

      /* 제출 누르면 작동되는 함수 */
      function submit() {
        alert("사원번호 : " + search_result_no);
        alert("휴가번호 : " + vacNum)
        reason = $('input[name=reason]').val();
        alert("사유는 : " + reason);

        $.ajax({
          url: "vacation-table/deleteRequest",
          type: 'post',
          data: {
            search_result_no: search_result_no,
            vacNum: vacNum,
            reason: reason

          },
          success: function (data) {
            alert(data);
            location.href = "/mgs/geuntae/vacation-table";

          },
          error: function () {
            alert("error");
            location.href = "/mgs/geuntae/vacation-table";

          }
        });

      }




      /* 화살표 클릭시 연도 이동 */
      let year = Number(document.querySelector('#duration').innerText.substr(0, 4));
      let duration;

      function prev() {
        console.log("prev 작동완료");
        console.log(year);
        year -= 1;
        location.href = "/mgs/geuntae/vacation-table?year=" + year;
      }

      function post() {
        console.log("post 작동완료");
        console.log("prev 작동완료");
        console.log(year);
        year += 1;
        location.href = "/mgs/geuntae/vacation-table?year=" + year;

      }


      let yesBtn = document.querySelector('#cancel_yes');
      let vac_num;


      /*휴가 취소전 체크하기 위한 모달창 */
      function btn_click(vac_no) {
        document.getElementById("calcel_check_modal").style.display = "block";

        vac_num = vac_no;
      }

      function yes() {
        /* location.href = "/mgs/geuntae/calcul-time?year=" + years+"&month="+ months; */
        location.href = "/mgs/geuntae/vacation-cancel?vac_num=" + vac_num

      }

      document.getElementById("calcel_check_close_btn").onclick = function () {
        document.getElementById("calcel_check_modal").style.display = "none";
      }   
    </script>


    </html>