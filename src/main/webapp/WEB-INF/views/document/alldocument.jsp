<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<script>

	/* 비지니스 로직 성공 alert 메시지 처리 */
	const message = '${ requestScope.message }';
	if(message != null && message !== '') {
		alert(message);
	}
</script>
<link href="${pageContext.request.contextPath}/resources/document/css/reset.css" rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/resources/document/css/common.css" rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/resources/document/css/alldocument.css" rel="stylesheet" type="text/css">
</head>
<body>
    <div id="layout">
                <header class="header">
            <nav class="gnb">
                <h1 class="logo"><a href="${ pageContext.servletContext.contextPath }/">
                    <img src="${pageContext.request.contextPath}/resources/common/images/logo.png" alt="">
                </a></h1>
                <ul class="main-menu">
                <li>
                        <a href="${pageContext.request.contextPath}/payment/payment">전자결재</a>
                    </li>
                    <li>
                        <a href="${pageContext.request.contextPath}/board/list">게시판</a>
                    </li>
                    <li>
                        <a href="${pageContext.request.contextPath}/geuntae/calcul-time">근태관리</>
                    </li>
                    <li>
                        <a href="#">조직도</a>
                    </li>
                    <li>
                        <a href="${ pageContext.servletContext.contextPath }/document/list">문서관리</a>
                    </li>
                </ul>
            </nav>
        </header>
        <div id="container">
        <div id="wrap"> 
            <nav>
                <div id="nav-head">
                    <i class="fa fa-file-text" aria-hidden="true"></i>
                    <h2>문서관리</h2>
                </div>

                <ul>
                    <div id="cate">
                        <i class="fa fa-file-text" aria-hidden="true"></i>
                        <h3>문서관리</h3>
                    </div>
                    <hr>
                   <li><a href="${pageContext.request.contextPath}/document/list">통합 문서 관리</a></li>
                    <li><a href="${pageContext.request.contextPath}/document/approval">품의서 문서 관리</a></li>
                    <li><a href="${pageContext.request.contextPath}/document/expend">지출결의서 문서 관리</a></li>
                    <li><a href="${pageContext.request.contextPath}/document/draft">기안서 문서 관리</a></li>
                    <li><a href="${pageContext.request.contextPath}/document/suggest">제안서 문서 관리</a></li>
                    <li><a href="${pageContext.request.contextPath}/document/report">보고서 문서 관리</a></li>
                </ul>
                   	<c:if test="${ sessionScope.loginMember.depname eq 5 }">
                    <hr>
                   	<li>
                        <a href="${pageContext.request.contextPath}/document/authList">접근 권한 설정</a>
                    </li>    
                	</c:if>
            </nav>
            <article>
                <div id="docu-top">
                    <h1>통합 문서 관리</h1>
                </div>
                <div id="table">

                    <table>
                        <tr>
                            <th>번호</th>
                            <th>문서명</th>
                            <th>종류</th>
                            <th>부서</th>
                            <th>등록일</th>
                            <th>등급</th>
                        </tr>
				<c:forEach var="document" items="${ requestScope.docList }">
                        <tr>
                            <td><c:out value="${ document.docNo }" /></td>
                            <td><c:out value="${ document.docTitle }" /></td>
                            <td><c:out value="${ document.docCategory }" /></td>
                            <td><c:out value="${ document.deptName }" /></td>
                            <td><c:out value="${ document.registDate }" /></td>
                            <td><c:out value="${ document.docGrade }" /></td>
                        </tr>
                </c:forEach>
                    </table>
                </div>
               
              <jsp:include page="common/paging.jsp" />
               
                <div id="search">
                	<form action="${ pageContext.servletContext.contextPath }/document/list"
                	method="get"> 	
                            <select name="searchCondition" size="1">
                                <option value="title" ${ requestScope.selectCriteria.searchCondition eq "title"? "selected": "" }>문서명</option>
                                <option value="contents" ${ requestScope.selectCriteria.searchCondition eq "contents"? "selected": "" }>내용</option>
                                <option value="category" ${ requestScope.selectCriteria.searchCondition eq "category"? "selected": "" }>종류</option>
                                <option value="dept" ${ requestScope.selectCriteria.searchCondition eq "dept"? "selected": "" }>부서</option>
                                <option value="grade" ${ requestScope.selectCriteria.searchCondition eq "grade"? "selected": "" }>등급</option>
                                <!-- <option value="title+contents">문서명+내용</option> -->
                            </select>
                    <input type="search" id="searchValue" name="searchValue" placeholder="검색어를 입력해주세요." size="50" value="<c:out value="${ requestScope.selectCriteria.searchValue }"/>">
                    <button id="btn" type="submit">검색</button>
                    </form>
                </div>
            </article>
        </div>
        </div>
       <footer>
            <div class="footer_logo"><img src="${pageContext.request.contextPath}/resources/common/images/logo.png" alt=""></div>
            <div class="footer_desc">
                <p>
                    MGS그룹웨어 주식회사<br>
                    사업자등록번호 : 123-45-67890<br>
                    통신판매업신고번호 : 제1234-서울강동-1234호
                    주소 : 서울시 강동구 천호동 123로<br>
                </p>
            </div>
            <div class="footer_law">
                <a href="#">개인정보처리방침</a>
                <a href="#">이용약관</a>
    
            </div>
        </footer>
    </div>
    
    <script>
	
		/* 게시글 리스트에서 해당 게시글에 마우스를 올릴 떄에 대한 스타일 처리 */
		if (document.querySelectorAll("#table td")) {
			const $tds = document.querySelectorAll("#table td");
			for (let i = 0; i < $tds.length; i++) {

				$tds[i].onmouseenter = function() {
					this.parentNode.style.backgroundColor = "#5584AC";
					this.parentNode.style.cursor = "pointer";
                    this.parentNode.style.color = "white";
				}

				$tds[i].onmouseout = function() {
					this.parentNode.style.backgroundColor = "white";
                    this.parentNode.style.color = "black";
				}

				$tds[i].onclick = function() {
					const pmtNo = this.parentNode.children[0].innerText;
				    const type = this.parentNode.children[2].innerText;
				    const grade = this.parentNode.children[5].innerText;
				    const link = "${ pageContext.servletContext.contextPath }/document/";
				    
				    if(${ requestScope.auth.authGrade eq "S"}){
				    	if(grade == 'S' || grade == 'A' || grade == 'B' || grade == 'C'){				    		
							if(type == '품의서'){
							location.href = link + "approvalDetail?pmtNo=" + pmtNo;
							} else if(type == '지출결의서'){
							location.href = link + "expendDetail?pmtNo=" + pmtNo;
							} else if(type == '기안서'){
							location.href = link + "draftDetail?pmtNo=" + pmtNo;
							} else if(type == '제안서'){
							location.href = link + "suggestDetail?pmtNo=" + pmtNo;
							} else if(type == '보고서'){
							location.href = link + "reportDetail?pmtNo=" + pmtNo;
							} else if(type == '근태신청서'){
							location.href = link + "attendanceDetail?pmtNo=" + pmtNo;
							}
				    	}
				    } else if(${ requestScope.auth.authGrade eq "A"}){
				    	if(grade == 'A' || grade == 'B' || grade == 'C'){				    		
				    		if(type == '품의서'){
								location.href = link + "approvalDetail?pmtNo=" + pmtNo;
								} else if(type == '지출결의서'){
								location.href = link + "expendDetail?pmtNo=" + pmtNo;
								} else if(type == '기안서'){
								location.href = link + "draftDetail?pmtNo=" + pmtNo;
								} else if(type == '제안서'){
								location.href = link + "suggestDetail?pmtNo=" + pmtNo;
								} else if(type == '보고서'){
								location.href = link + "reportDetail?pmtNo=" + pmtNo;
								} else if(type == '근태신청서'){
								location.href = link + "attendanceDetail?pmtNo=" + pmtNo;
								}
				    	}
				    } else if(${ requestScope.auth.authGrade eq "B"}){
				    	if(grade == 'B' || grade == 'C'){				    		
				    		if(type == '품의서'){
								location.href = link + "approvalDetail?pmtNo=" + pmtNo;
								} else if(type == '지출결의서'){
								location.href = link + "expendDetail?pmtNo=" + pmtNo;
								} else if(type == '기안서'){
								location.href = link + "draftDetail?pmtNo=" + pmtNo;
								} else if(type == '제안서'){
								location.href = link + "suggestDetail?pmtNo=" + pmtNo;
								} else if(type == '보고서'){
								location.href = link + "reportDetail?pmtNo=" + pmtNo;
								} else if(type == '근태신청서'){
								location.href = link + "attendanceDetail?pmtNo=" + pmtNo;
								}
				    	}
				    } else if(${ requestScope.auth.authGrade eq "C"}){
				    	if(grade == 'C'){				    		
				    		if(type == '품의서'){
								location.href = link + "approvalDetail?pmtNo=" + pmtNo;
								} else if(type == '지출결의서'){
								location.href = link + "expendDetail?pmtNo=" + pmtNo;
								} else if(type == '기안서'){
								location.href = link + "draftDetail?pmtNo=" + pmtNo;
								} else if(type == '제안서'){
								location.href = link + "suggestDetail?pmtNo=" + pmtNo;
								} else if(type == '보고서'){
								location.href = link + "reportDetail?pmtNo=" + pmtNo;
								} else if(type == '근태신청서'){
								location.href = link + "attendanceDetail?pmtNo=" + pmtNo;
								}
				    	}
				    } else {
				    	alert("접근이 제한된 문서입니다.");
				    }
				}
			}
		}
	</script>
</body>
</html>