<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>     
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link href="${pageContext.request.contextPath}/resources/document/css/reset.css" rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/resources/document/css/common.css" rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/resources/document/contents/css/expend-detail.css" rel="stylesheet" type="text/css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
</head>
<body>
    <div id="layout">
        <header class="header">
            <nav class="gnb">
                <h1 class="logo"><a href="${ pageContext.servletContext.contextPath }/">
                    <img src="${pageContext.request.contextPath}/resources/common/images/logo.png" alt="">
                </a></h1>
                <ul class="main-menu">
                	         <li>
                        <a href="${pageContext.request.contextPath}/payment/payment">전자결재</a>
                    </li>
                    <li>
                        <a href="${pageContext.request.contextPath}/board/list">게시판</a>
                    </li>
                    <li>
                        <a href="${pageContext.request.contextPath}/geuntae/calcul-time">근태관리</>
                    </li>
                    <li>
                        <a href="#">조직도</a>
                    </li>
                    <li>
                        <a href="${ pageContext.servletContext.contextPath }/document/list">문서관리</a>
                    </li>
                </ul>
            </nav>
        </header>
        <div id="container">
        <div id="wrap"> 
            <nav>
                <div id="nav-head">
                    <i class="fa fa-file-text" aria-hidden="true"></i>
                    <h2>문서관리</h2>
                </div>

                <ul>
                    <div id="cate">
                        <i class="fa fa-file-text" aria-hidden="true"></i>
                        <h3>문서관리</h3>
                    </div>
                    <hr>
                  <li><a href="${pageContext.request.contextPath}/document/list">통합 문서 관리</a></li>
                    <li><a href="${pageContext.request.contextPath}/document/approval">품의서 문서 관리</a></li>
                    <li><a href="${pageContext.request.contextPath}/document/expend">지출결의서 문서 관리</a></li>
                    <li><a href="${pageContext.request.contextPath}/document/draft">기안서 문서 관리</a></li>
                    <li><a href="${pageContext.request.contextPath}/document/suggest">제안서 문서 관리</a></li>
                    <li><a href="${pageContext.request.contextPath}/document/report">보고서 문서 관리</a></li>
                </ul>
                   	<c:if test="${ sessionScope.loginMember.depname eq 5 }">
                    <hr>
                   	<li>
                        <a href="${pageContext.request.contextPath}/document/authList">접근 권한 설정</a>
                    </li>    
                	</c:if>
            </nav>
            <section id="section1">
                <article id="contents1">
                    <p><c:out value="${ requestScope.expend.pmtDate }"/></p>
                    <h1 class="title">지출결의서</h1>
                    <table>
                        <tr>
                            <th class="manager-id">담당</th>
                            <th class="ceo-id">사장</th>
                        </tr>
                        <tr>
                            <td>
                                <c:out value="${ requestScope.expend.pmtManager }"/>
                            </td>
                            <td>사인</td>
                        </tr>
                    </table>
                </article>
                <article id="contents2">
                    <table>
                        <tr>
                            <th class="row1">성 명</th>
                            <td class="row2">
                                <c:out value="${ requestScope.expend.pmtName }"/>
                            </td>
                            <th class="row1">부 서</th>
                            <td class="row2">
                               <c:out value="${ requestScope.expend.pmtDepartment }"/>
                            </td>
                            <th class="row1">직 책</th>
                            <td class="row2">
                                <c:out value="${ requestScope.expend.pmtPosition }"/>
                            </td>
                        </tr>
                        <tr>
                            <th class="row1">지출금액</th>
                            <td class="row4">
                                <c:out value="${ requestScope.expend.pmtExpenditure }"/>
                            </td>
                        </tr>
                        <tr>
                            <th class="row1">제 목</th>
                            <td class="row4">
                                <c:out value="${ requestScope.expend.pmtTitle }"/>
                            </td>
                        </tr>
                    </table>
                    <article id="contents3">
                        <div class="rowspan">
                            내 용
                        </div>
                        <table>
                            <tr>
                                <td class="row5">적 요</td>
                                <td class="row5">금 액</td>
                                <td class="row5">비 고</td>
                            </tr>
                            <c:forEach var="unit" items="${ requestScope.unitList }">
                            <tr>
                                <td class="row6">
                                	<c:out value="${ unit.pmtBriefs }"/>
                                </td>
                                <td class="row6">
                                	<c:out value="${ unit.pmtAmount }"/>
                                </td>
                                <td class="row6">
                                	<c:out value="${ unit.pmtNote }"/>
                                </td>
                            </tr>
                            </c:forEach>
                        </table>
                    </article>
                    <div id="button">
                    <button type="button" onclick="location.href='javascript:history.back();'">돌아가기</button>
                    <c:if test="${ sessionScope.loginMember.depname eq 5 }">
                 		<button type="button" id="delete">삭제하기</button>
                 	</c:if>	
                    </div>
                </article>
            </section>
        </div>
        <div id="layer">
			<div id="modal">
				<h3>문서를 삭제 하시겠습니까?</h3>
				<div id=buttonWrap>
					<button type="button" onclick="location.href='${ pageContext.servletContext.contextPath }/document/deleteDoc?docNo=${ requestScope.expend.pmtNo }'">네</button>
					<button type="button" id="cancle">아니요</button>
				</div>
			</div>
		</div>
        </div>
        <footer>
            <div class="footer_logo"><img src="${pageContext.request.contextPath}/resources/common/images/logo.png" alt=""></div>
            <div class="footer_desc">
                <p>
                    MGS그룹웨어 주식회사<br>
                    사업자등록번호 : 123-45-67890<br>
                    통신판매업신고번호 : 제1234-서울강동-1234호
                    주소 : 서울시 강동구 천호동 123로<br>
                </p>
            </div>
            <div class="footer_law">
                <a href="#">개인정보처리방침</a>
                <a href="#">이용약관</a>
    
            </div>
        </footer>
    </div>
    <script>
    $(function(){
		$('#delete').on('click', function(){
			$('#layer').css("display","block");
			$('body').css("overflow","hidden");
		});
		$('#cancle').on('click', function(){
			$('#layer').css("display", "none");
			$('body').css("overflow","visible");
		});
    });
    </script>
</body>
</html>