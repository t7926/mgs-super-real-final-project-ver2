<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>    
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link href="${pageContext.request.contextPath}/resources/document/css/reset.css" rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/resources/document/css/common.css" rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/resources/document/contents/css/approval-detail.css" rel="stylesheet" type="text/css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
</head>
<body>
    <div id="layout">
        <header class="header">
            <nav class="gnb">
                <h1 class="logo"><a href="${ pageContext.servletContext.contextPath }/">
                    <img src="${pageContext.request.contextPath}/resources/common/images/logo.png" alt="">
                </a></h1>
                <ul class="main-menu">
               	         <li>
                        <a href="${pageContext.request.contextPath}/payment/payment">전자결재</a>
                    </li>
                    <li>
                        <a href="${pageContext.request.contextPath}/board/list">게시판</a>
                    </li>
                    <li>
                        <a href="${pageContext.request.contextPath}/geuntae/calcul-time">근태관리</>
                    </li>
                    <li>
                        <a href="#">조직도</a>
                    </li>
                    <li>
                        <a href="${ pageContext.servletContext.contextPath }/document/list">문서관리</a>
                    </li>
                </ul>
            </nav>
        </header>
        <div id="container">
        <div id="wrap"> 
            <nav>
                <div id="nav-head">
                    <i class="fa fa-file-text" aria-hidden="true"></i>
                    <h2>문서관리</h2>
                </div>

                <ul>
                    <div id="cate">
                        <i class="fa fa-file-text" aria-hidden="true"></i>
                        <h3>문서관리</h3>
                    </div>
                    <hr>
                   <li><a href="${pageContext.request.contextPath}/document/list">통합 문서 관리</a></li>
                    <li><a href="${pageContext.request.contextPath}/document/approval">품의서 문서 관리</a></li>
                    <li><a href="${pageContext.request.contextPath}/document/expend">지출결의서 문서 관리</a></li>
                    <li><a href="${pageContext.request.contextPath}/document/draft">기안서 문서 관리</a></li>
                    <li><a href="${pageContext.request.contextPath}/document/suggest">제안서 문서 관리</a></li>
                    <li><a href="${pageContext.request.contextPath}/document/report">보고서 문서 관리</a></li>
                </ul>
                   	<c:if test="${ sessionScope.loginMember.depname eq 5 }">
                    <hr>
                   	<li>
                        <a href="${pageContext.request.contextPath}/document/authList">접근 권한 설정</a>
                    </li>    
                	</c:if>
            </nav>
            <section id="section1">
                <article id="contents1">
                    <p><c:out value="${ requestScope.approval.pmtDate }"/></p>
                    <h1 class="title">품의서</h1>
                    <table>
                        <tr>
                            <th class="manager-id">담당</th>
                            <th class="ceo-id">사장</th>
                        </tr>
                        <tr>
                            <td>
                                <c:out value="${ requestScope.approval.pmtManager }"/>
                            </td>
                            <td>사인</td>
                        </tr>
                    </table>
                </article>
                <article id="contents2">
                    <table>
                        <tr>
                            <th class="row1"></th>
                            <td class="row2">
                                <c:out value="${ requestScope.approval.pmtNo }"/>
                            </td>
                            <th class="row1">기안일자</th>
                            <td class="row2">
                               <c:out value="${ requestScope.approval.pmtDrafting }"/>
                            </td>
                        </tr>
                        <tr>
                            <th class="row1">소 속</th>
                            <td class="row2">
                                <c:out value="${ requestScope.approval.pmtPosition }"/>
                            </td>
                            <th class="row1">기 안 자</th>
                            <td class="row2">
                                <c:out value="${ requestScope.approval.pmtName }"/>
                            </td>
                        </tr>
                        <tr>
                            <th class="row3">
                                <c:out value="${ requestScope.approval.pmtTitle }"/>
                            </th>
                        </tr>
                        <tr>
                            <td class="text-box">
                                <c:out value="${ requestScope.approval.pmtContents }"/>
                            </td>
                        </tr>
                        <tr>
                            <th class="row3">결재자 부가사항</th>
                        </tr>
                        <tr>
                            <th class="row1">부서장</th>
                            <td class="row4">
                                <c:out value="${ requestScope.approval.pmtAddOn1 }"/>
                            </td>
                        </tr>
                        <tr>
                            <th class="row1">임 원</th>
                            <td class="row4">
                                <c:out value="${ requestScope.approval.pmtAddOn2 }"/>
                            </td>
                        </tr>
                        <tr>
                            <th class="row1">사 장</th>
                            <td class="row4">
                                <c:out value="${ requestScope.approval.pmtAddOn3 }"/>
                            </td>
                        </tr>
                        <tr>
                            <th class="row3">지 시 사 항</th>
                        </tr>
                        <tr>
                            <td class="text-box">
                                <c:out value="${ requestScope.approval.pmtInstruction }"/>
                            </td>
                        </tr>
                        <tr>
                            <td class="row3">* 이 품의서는 기안자가 지정한 품의 최종 결재자에게 관련 부서장을 거쳐 필히 결재받아야 합니다.</td>
                        </tr>
                    </table>
                    <div id="button">
                    <button type="button" onclick="location.href='javascript:history.back();'">돌아가기</button>
                    <c:if test="${ sessionScope.loginMember.depname eq 5 }">
	                    <button type="button" id="delete">삭제하기</button>               	    
                	</c:if>
                    </div>
                </article>
            </section>
        </div>
        <div id="layer">
			<div id="modal">
				<h3>문서를 삭제 하시겠습니까?</h3>
				<div id=buttonWrap>
					<button type="button" onclick="location.href='${ pageContext.servletContext.contextPath }/document/deleteDoc?docNo=${ requestScope.approval.pmtNo }'">네</button>
					<button type="button" id="cancle">아니요</button>
				</div>
			</div>
		</div>
        </div>
        <footer>
            <div class="footer_logo"><img src="${pageContext.request.contextPath}/resources/common/images/logo.png" alt=""></div>
            <div class="footer_desc">
                <p>
                    MGS그룹웨어 주식회사<br>
                    사업자등록번호 : 123-45-67890<br>
                    통신판매업신고번호 : 제1234-서울강동-1234호
                    주소 : 서울시 강동구 천호동 123로<br>
                </p>
            </div>
            <div class="footer_law">
                <a href="#">개인정보처리방침</a>
                <a href="#">이용약관</a>
    
            </div>
        </footer>
    </div>
    
    <script>
    $(function(){
		$('#delete').on('click', function(){
			$('#layer').css("display","block");
			$('body').css("overflow","hidden");
		});
		$('#cancle').on('click', function(){
			$('#layer').css("display", "none");
			$('body').css("overflow","visible");
		});
    });
    </script>
</body>
</html>