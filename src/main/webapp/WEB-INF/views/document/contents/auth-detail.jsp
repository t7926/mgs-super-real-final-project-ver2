<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>    
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link href="${pageContext.request.contextPath}/resources/document/css/reset.css" rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/resources/document/css/common.css" rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/resources/document/contents/css/auth-detail.css" rel="stylesheet" type="text/css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
</head>
<body>
	<div id="layout">
		<header class="header">
	<nav class="gnb">
		<h1 class="logo"><a href="${ pageContext.servletContext.contextPath }/">
			<img src="${pageContext.request.contextPath}/resources/common/images/logo.png" alt="">
		</a></h1>
		<ul class="main-menu">
	         <li>
                        <a href="${pageContext.request.contextPath}/payment/payment">전자결재</a>
                    </li>
                    <li>
                        <a href="${pageContext.request.contextPath}/board/list">게시판</a>
                    </li>
                    <li>
                        <a href="${pageContext.request.contextPath}/geuntae/calcul-time">근태관리</>
                    </li>
                    <li>
                        <a href="#">조직도</a>
                    </li>
                    <li>
                        <a href="${ pageContext.servletContext.contextPath }/document/list">문서관리</a>
                    </li>
		</ul>
	</nav>
</header>
<div id="container">
<div id="wrap"> 
	<nav>
		<div id="nav-head">
			<i class="fa fa-file-text" aria-hidden="true"></i>
			<h2>문서관리</h2>
		</div>

		<ul>
			<div id="cate">
				<i class="fa fa-file-text" aria-hidden="true"></i>
				<h3>문서관리</h3>
			</div>
			<hr>
		  <li><a href="${pageContext.request.contextPath}/document/list">통합 문서 관리</a></li>
          <li><a href="${pageContext.request.contextPath}/document/approval">품의서 문서 관리</a></li>
          <li><a href="${pageContext.request.contextPath}/document/expend">지출결의서 문서 관리</a></li>
          <li><a href="${pageContext.request.contextPath}/document/draft">기안서 문서 관리</a></li>
          <li><a href="${pageContext.request.contextPath}/document/suggest">제안서 문서 관리</a></li>
          <li><a href="${pageContext.request.contextPath}/document/report">보고서 문서 관리</a></li>
        </ul>
          	<c:if test="${ sessionScope.loginMember.depname eq 5 }">
                    <hr>
                   	<li>
                        <a href="${pageContext.request.contextPath}/document/authList">접근 권한 설정</a>
                    </li>    
            </c:if>
	</nav>
	<article>
		<div id="docu-top">
			<h1>통합 문서 관리</h1>
		</div>
			<div id="table">
				<form action="${pageContext.servletContext.contextPath}/document/authModify" method="post">
				<table>
						<tr>
							<td>이름</td>
							<td><c:out value="${ requestScope.auth.empName }"/></td>
						</tr>
						<tr>
							<td>부서</td>
							<td><c:out value="${ requestScope.auth.deptName }"/></td>
						</tr>
						<tr>
							<td>팀</td>
							<td><c:out value="${ requestScope.auth.teamName }"/></td>
						</tr>
						<tr>
							<td>직급</td>
							<td><c:out value="${ requestScope.auth.levName }"/></td>
						</tr>
						<tr>
							<td>권한 여부</td>
							<td>
								<select name="authorityYN" id="authority">
									<option value="Y" ${ requestScope.auth.authorityYN eq 'Y'? "selected": "" }>Y</option>
									<option value="N" ${ requestScope.auth.authorityYN eq 'N'? "selected": "" }>N</option>
								</select>
							</td>
						</tr>
						<tr>
							<td>접근 등급</td>
							<td>
								<select name="authGrade" id="grade">
									<option value="S" ${ requestScope.auth.authGrade eq 'S'? "selected": "" }>S</option>
									<option value="A" ${ requestScope.auth.authGrade eq 'A'? "selected": "" }>A</option>
									<option value="B" ${ requestScope.auth.authGrade eq 'B'? "selected": "" }>B</option>
									<option value="C" ${ requestScope.auth.authGrade eq 'C'? "selected": "" }>C</option>
									<option value="N" ${ requestScope.auth.authGrade eq 'N'? "selected": "" }>N</option>
									
								</select>
							</td>
						</tr>
						
				</table>
				<div id="button">
				<button type="button" onclick="location.href='javascript:history.back();'">돌아가기</button>
				<button type="button" id="modify">수정하기</button>
				</div>
					<div id="layer">
						<div id="modal">
							<h3>권한을 변경 하시겠습니까?</h3>
							<div id=buttonWrap>
								<button type="submit">네</button>
								<input type="hidden" name="empNum" value="${ requestScope.auth.empNum }">
								<button type="button" id="cancle">아니요</button>
							</div>
						</div>
					</div>
				</form>
			</div>	
	</article>
</div>
</div>
<footer>
	<div class="footer_logo"><img src="${pageContext.request.contextPath}/resources/common/images/logo.png" alt=""></div>
	<div class="footer_desc">
		<p>
			MGS그룹웨어 주식회사<br>
			사업자등록번호 : 123-45-67890<br>
			통신판매업신고번호 : 제1234-서울강동-1234호
			주소 : 서울시 강동구 천호동 123로<br>
		</p>
	</div>
	<div class="footer_law">
		<a href="#">개인정보처리방침</a>
		<a href="#">이용약관</a>

	</div>
</footer>
</div>
<script>
	$(function(){
		$('#modify').on('click', function(){
			$('#layer').css("display","block");
			$('body').css("overflow","hidden");
		});
		$('#cancle').on('click', function(){
			$('#layer').css("display", "none");
			$('body').css("overflow","visible");
		});
		
		$('#authority').on('change', function(){			
		if($('#authority option:selected').val() == 'Y'){
			$("#grade option[value='S']").removeAttr('disabled');
			$("#grade option[value='A']").removeAttr('disabled');
			$("#grade option[value='B']").removeAttr('disabled');
			$("#grade option[value='C']").removeAttr('disabled');
			$("#grade option[value='N']").attr('disabled', true);
		} else {
			$("#grade option[value='N']").removeAttr('disabled');
			$("#grade option[value='S']").attr('disabled', true);
			$("#grade option[value='A']").attr('disabled', true);
			$("#grade option[value='B']").attr('disabled', true);
			$("#grade option[value='C']").attr('disabled', true);
		}
		})
	});	
</script>		
</body>
</html>