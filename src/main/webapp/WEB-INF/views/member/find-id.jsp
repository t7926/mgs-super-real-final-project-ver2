<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>    
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>

<link href="${pageContext.request.contextPath}/resources/log-in/css/reset.css" rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/resources/log-in/css/find-id.css" rel="stylesheet" type="text/css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
</head>
<body>
<div class="layout">
    <header class="header"></header>
    <h2>아이디 찾기</h2>
    <div class="main">
        <p class="main-text"><strong>※ 본인확인 이메일로 인증  </strong></p>
        <p class="main-text">본인확인 이메일 주소와 입력한 이메일 주소가 같아야. 인증번호를 받을 수 있습니다.</p><br><br>
        <form action="${pageContext.request.contextPath}/member/find-id" method="post">
        <ul class="container-1">	
            <li class="item center">
                <p>이름:</p>
            </li>
            <li class="item">
                <input id="memName" class= "id" name="name" type="text"  autofocus required>
            </li>

        </ul>

        <ul class="container-2">
            <li class="item center">
                <p>이메일 주소:</p>
            </li>
            <li class="item">
                <input id="memEmail" class= "mail" name="email" type="text"  autofocus required>
                <button type="button" id="sendCode">인증번호 받기</button><br><br>
                <input class= "mail" name="scode" type="text" placeholder="인증번호 입력">
            </li>
        </ul>
    <c:if test="${ not empty requestScope.member.name }">
    	<p id="nameId"><c:out value="${ requestScope.member.name }" />님의 아이디는
    	<c:out value="${ requestScope.member.id } "/>입니다.</p>
    </c:if>
        <div class="login-button">
            <button type="submit">확 인</button>
            <button type="button" onclick="location.href='${ pageContext.servletContext.contextPath }/'">취 소</button>
    	</div>
		</form>

</div>
<script>
	if(document.getElementById("sendCode")){
		const $sendCode = document.getElementById("sendCode");	
		$sendCode.onclick = function() {
			let name = document.getElementById("memName").value;
			let mail = document.getElementById("memEmail").value;
			if(name.trim() == ""){
				
				alert('이름을 입력해 주세요.');
			} else if(mail.trim() == ""){
				
				alert('이메일을 입력해 주세요.');
			} else {
				
				$.ajax({
					url:"/mgs/member/sendCode",
					type:"post",
					data:{
						name : name, 
						email : mail
					},
					success:function(data){
						console.log(data);
						alert("인증번호를 전송하였습니다.");
					},
					error:function(date){
						console.log(data);
						alert("인증번호 전송을 실패하였습니다.");
					}
				});
			}
		};
	}
</script>
</body>
</html>