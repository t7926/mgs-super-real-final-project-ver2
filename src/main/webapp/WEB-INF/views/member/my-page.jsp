<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>

<link href="${pageContext.request.contextPath}/resources/log-in/css/reset.css" rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/resources/log-in/css/my-page.css" rel="stylesheet" type="text/css">
<script src="${ pageContext.servletContext.contextPath }/resources/log-in/js/event.js"></script>
</head>
<body>
 <div class="layout">
     <header class="header"></header>
     <h2>마이페이지</h2>

         <div class="main">
             <form id="my-page" action="${ pageContext.servletContext.contextPath }/member/my-page" method="post">
                 <p>이름</p>
                 <input type="text" name="name" id="name" value="">
                 <br>
                 <br><p>비밀번호</p>
                 <input type="password" name="pwd" value="" placeholder="영문자+숫자+특수조합(8~25자리 입력)"><br>
                 <br><p>비밀번호 재확인</p>
                 <input type="password" name="pwd2" value="" placeholder="영문자+숫자+특수조합(8~25자리 입력)">
                 
                 <br>
                 <br><p>생년월일</p>
                 <select class="year" name="year">
                     <option value="">-- 선택 --</option>
                     <option value="1980">1980</option>
                     <option value="1981">1981</option>
                     <option value="1982">1982</option>
                     <option value="1983">1983</option>
                     <option value="1984">1984</option>
                     <option value="1985">1985</option>
                     <option value="1986">1986</option>
                     <option value="1987">1987</option>
                     <option value="1988">1988</option>
                     <option value="1989">1989</option>
                     <option value="1990">1990</option>
                     <option value="1991">1991</option>
                     <option value="1992">1992</option>
                     <option value="1993">1993</option>
                     <option value="1994">1994</option>
                     <option value="1995">1995</option>
                     <option value="1996">1996</option>
                     <option value="1997">1997</option>
                     <option value="1998">1998</option>
                     <option value="1999">1999</option>
                     <option value="2000">2000</option>
                 </select>
                 <select class="year" name="month">
                     <option value="">-- 선택 --</option>
                     <option value="1">1</option>
                     <option value="2">2</option>
                     <option value="3">3</option>
                     <option value="4">4</option>
                     <option value="5">5</option>
                     <option value="6">6</option>
                     <option value="7">7</option>
                     <option value="8">8</option>
                     <option value="9">9</option>
                     <option value="10">10</option>
                     <option value="11">11</option>
                     <option value="12">12</option>
                 </select>
                 <select class="year" name="day">
                     <option value="">-- 선택 --</option>
                     <option value="1">1</option>
                     <option value="2">2</option>
                     <option value="3">3</option>
                     <option value="4">4</option>
                     <option value="5">5</option>
                     <option value="6">6</option>
                     <option value="7">7</option>
                     <option value="8">8</option>
                     <option value="9">9</option>
                     <option value="10">10</option>
                     <option value="11">11</option>
                     <option value="12">12</option>
                     <option value="13">13</option>
                     <option value="14">14</option>
                     <option value="15">15</option>
                     <option value="16">16</option>
                     <option value="17">17</option>
                     <option value="18">18</option>
                     <option value="19">19</option>
                     <option value="20">20</option>
                     <option value="21">21</option>
                     <option value="22">22</option>
                     <option value="23">23</option>
                     <option value="24">24</option>
                     <option value="25">25</option>
                     <option value="26">26</option>
                     <option value="27">27</option>
                     <option value="28">28</option>
                     <option value="29">29</option>
                     <option value="30">30</option>
                     <option value="31">31</option>
                 </select>
                 <br><br>
                 <p>부서</p>
                 <select class="depname" name="depname">
                 <option value="">-- 부서선택 --</option>
                     <option value="1">사업부</option>
                     <option value="2">마케팅부</option>
                     <option value="3">경영지원부</option>
                     <option value="4">개발부</option>
                 </select>
                 <select class="teamname" name="teamname">
                 <option value="">-- 팀 선택 --</option>
                     <option value="11">영업팀</option>
                     <option value="12">무역팀</option>
                     <option value="13">홍보팀</option>
                     <option value="14">마케팅팀</option>
                     <option value="15">회계팀</option>
                     <option value="16">인사관리팀</option>
                     <option value="17">개발1팀</option>
                     <option value="18">개발2팀</option>
                 </select><br>
                 <p>성별</p>
                 <label for="man">남</label>
                 <input id= "input-2"type="radio" name="gender" value="m" id="man">
                 <label for="woman">여</label>
                 <input type= "radio" name="gender" value="m" id="woman"> <br><br>
                 <p>이메일</p>
                 <input id= "email" name="email" type="email" placeholder="email@gmail.com"><br><br>
                 <p>휴대전화</p>
                 <input type="text" name="cphone" placeholder="ex)010-1234-5678"><br>
                 <p>유선전화</p>
                 <input type="text" name="phone" placeholder="ex)02-765-4321"><br>
                 <p>주소</p>
                 <input id= "zipCode" type="text" name="address" placeholder="우편번호">
                 <input id= "searchZipCode" type="button" name="certification" value="우편번호 찾기">
                 <br><br>
                 <input id= "address2" type="text" name="address2" placeholder="주소 입력"><br>
                 <input id= "address3" type="text" name="address3" placeholder="상세 주소 입력">
                 <br>
                 <br>
                 <div class="btn">
                     <input type="submit" value="수정하기" class="btn btn-or">
                     <!--<input type="submit" value="탈퇴하기" class="btn btn-or">  -->
                     <input type="button" value="탈퇴하기" class="btn btn-bs" onclick="location.href='${ pageContext.servletContext.contextPath }/superauth/delete?empnum=${ sessionScope.loginMember.empnum }'">
                     
                 </div>
             </form>
         </div>








 </div>

</body>


<script src="https://t1.daumcdn.net/mapjsapi/bundle/postcode/prod/postcode.v2.js"></script>
	<script>
		const $searchZipCode = document.getElementById("searchZipCode");
		const $goMain = document.getElementById("goMain");
		
		$searchZipCode.onclick = function() {
		
			/* 다음 우편번호 검색 창을 오픈하면서 동작할 콜백 메소드를 포함한 객체를 매개변수로 전달한다. */
			new daum.Postcode({
				oncomplete: function(data){
					
					/* 팝업에서 검색결과 항목을 클릭했을 시 실행할 코드를 작성하는 부분 */
					document.getElementById("zipCode").value = data.zonecode;
					document.getElementById("address2").value = data.address;
					document.getElementById("address3").focus();
				}
			}).open();
		}
		
		/* $goMain.onclick = function() {
			location.href = "${ pageContext.servletContext.contextPath }";
		} */
		
		
		/* 업데이트 버튼*/
		if(document.getElementById("updateMember")) {
			
			const $update = document.getElementById("updateMember");
			
			$update.onclick = function() {
			    
			    location.href = "/mgs/member/my-page";
			}
		}
		
		
		
		
	</script>




</html>