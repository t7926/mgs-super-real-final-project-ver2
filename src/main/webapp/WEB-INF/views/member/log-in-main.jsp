<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>login</title>

<link href="${pageContext.request.contextPath}/resources/log-in/css/reset.css" rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/resources/log-in/css/log-in-main.css" rel="stylesheet" type="text/css">


</head>
<body>
  <div class="layout">
        <header class="header"></header>
        <div class="main">
            <div class="left-box"></div>
            <div class="right-box">
              <form class="login-form" action="${pageContext.request.contextPath}/member/log-in" method="post">
                <h1>Welcome to Observer</h1>
                <div class="login">
                  <input class="txtb" type="text" name="id" placeholder="User ID">
                  <a href="${pageContext.request.contextPath}/member/find-id">아이디 찾기</a>
                  <input class="txtb" type="password" name="pwd" placeholder="Password">
                  <a href="${pageContext.request.contextPath}/member/find-pwd">비밀번호 찾기</a><br>

                  <div class="login-btn">
                    
                  <input type="submit" class="txtb" id="login" value="로그인">
                  <a href="${pageContext.request.contextPath}/member/log-in-check">회원가입</a>                 
                  
                </div>
                </div>
              </form>

            </div>
        </div>

  </div>




<script>
        
      
        
        const message = '${ requestScope.message }';
    	if(message != null && message !== '') {
    		alert(message);
    	}


</script>

</body>
</html>