<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>직원 등록</title>
<link href="${pageContext.request.contextPath}/resources/board/css/reset.css" rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/resources/organization/css/or-user-create.css" rel="stylesheet" type="text/css">
<script src="https://kit.fontawesome.com/9be7870010.js" crossorigin="anonymous"></script>
</head>
<body>
<div id="layout">
	<header id="header">
		<nav class="gnb">
			<h1 class="logo"><a href="#">
				<img src="${pageContext.request.contextPath}/resources/board/images/logo.png" alt="">
			</a></h1>
			<ul class="main-menu">
				<li>
					<a href="#">전자결재</a>
				</li>
				<li>
					<a href="${pageContext.request.contextPath}/views/board/board-main.jsp">게시판</a>
				</li>
				<li>
					<a href="#">근태관리</a>
				</li>
				<li>
					<a href="#">조직도</a>
				</li>
				<li>
					<a href="#">문서관리</a>
				</li>
			</ul>
		</nav>
	
	</header>
	<div id="container">
		<div id="wrap">
			<nav>
				<div id="nav-head">
					<i class="fa fa-thin fa-sitemap" aria-hidden="true"></i>
					<h2>조직도</h2>
				</div>
				
				<ul>
					<div id="board">
						<i class="fa fal fa-bezier-curve" aria-hidden="true"></i>
						<h3>조직 관리</h3>
					</div>
					<hr>
					<li><a href="#">조직도 등록</a></li>
					<li><a href="#">직원 등록</a></li>
				</ul>
				<hr>
				<div id="club">
					<i class="fa fas fa-search" aria-hidden="true"></i>
					<h3>조직 조회</h3>
				</div>
				<hr>
				<li>마케팅<i class="fa fas fa-plus-square"></i></li>
				<li>경영지원<i class="fa fas fa-plus-square"></i></li>
				<li>개발<i class="fa fas fa-plus-square"></i></li>
				<li>사업<i class="fa fas fa-plus-square"></i></li>
			
			</nav>
			<article>
				<div id="board-top">
					<h1>직원 등록</h1>
				</div>
				<div id="user-register">
					<div id="register-box">
						<div id="face">
							<div class="face-picture">
								<img src="${pageContext.request.contextPath}/resources/organization/images/user.jpg" alt="">
							</div>
							<button>이미지등록</button>
						</div>
						<div id="information">
							<ul>
								<li>이름<input type="text"></li>
								<li>생년월일<input type="text"></li>
								<li>직급<input type="text"></li>
								<li>부서<input type="text"></li>
								<li>이메일<input type="text"></li>
								<li>전화번호<input type="text"></li>
								<li>사내번호<input type="text"></li>
								<li>주소<input type="text"></li>
							</ul>
						</div>
					</div>
				</div>
				<button>등록하기</button>
			</article>
		</div>
	</div>
	<footer id="footer">
		<div class="footer_logo"><img src="${pageContext.request.contextPath}/resources/board/images/logo.png" alt=""></div>
		<div class="footer_desc">
			<p>
				MGS그룹웨어 주식회사<br>
				사업자등록번호 : 123-45-67890<br>
				통신판매업신고번호 : 제1234-서울강동-1234호
				주소 : 서울시 강동구 천호동 123로<br>
			</p>
		</div>
		<div class="footer_law">
			<a href="#">개인정보처리방침</a>
			<a href="#">이용약관</a>
		
		</div>
	
	</footer>
</div>


</body>
</html>