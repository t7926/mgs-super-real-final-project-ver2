<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>게시판</title>
<link href="${pageContext.request.contextPath}/resources/board/css/reset.css" rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/resources/board/css/club-request-list.css" rel="stylesheet" type="text/css">
<script src="https://kit.fontawesome.com/9be7870010.js" crossorigin="anonymous"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/board/js/jquery-3.5.1.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/board/js/onebutton.js"></script>

<script>
const message = '${ requestScope.message }';
if(message != null && message !== '') {
	alert(message);
}
</script>

</head>


<body>

<div id="layout">
	<header id="header">
		<nav class="gnb">
			<h1 class="logo"><a href="#">
				<img src="${pageContext.request.contextPath}/resources/board/images/logo.png" alt="">
			</a></h1>
			<ul class="main-menu">
               	         <li>
                        <a href="${pageContext.request.contextPath}/payment/payment">전자결재</a>
                    </li>
                    <li>
                        <a href="${pageContext.request.contextPath}/board/list">게시판</a>
                    </li>
                    <li>
                        <a href="${pageContext.request.contextPath}/geuntae/calcul-time">근태관리</>
                    </li>
                    <li>
                        <a href="#">조직도</a>
                    </li>
                    <li>
                        <a href="${ pageContext.servletContext.contextPath }/document/list">문서관리</a>
                    </li>
                </ul>
		</nav>
	
	</header>
	<div id="container">
		<div id="wrap">
			<nav>
				<div id="nav-head">
					<i class="fa fa-solid fa-list" aria-hidden="true"></i>
					<h2>게시판</h2>
				</div>
				
				<ul>
					<div id="board">
						<i class="fa fa-solid fa-rectangle-list" aria-hidden="true"></i>
						<h3>게시판</h3>
					</div>
					<hr>
					<li><a href="${pageContext.request.contextPath}/board/list">공지 게시판</a></li>
					<li><a href="${pageContext.request.contextPath}/board/anonymous-list">익명 게시판</a></li>
				</ul>
				
				<div id="club">
					<i class="fa fa-solid fa-hand-holding-heart" aria-hidden="true"></i>
					<h3>동호회 게시판</h3>
				</div>
				<hr>
				<ul>
					<li><a href="${pageContext.request.contextPath}/views/board/manager/board-club-football.jsp">족구 게시판</a></li>
					<li><a href="${pageContext.request.contextPath}/views/board/manager/board-club-mountain.jsp">산악회 게시판</a></li>
				</ul>
				<div id="request">
					
						<i class="fa fa-solid fa-chalkboard-user" aria-hidden="true"></i>
						<h3>게시판 신청</h3>
					<!-- <li id="modal_open_btn">신규 게시판 생성</li> -->
				</div>
				<hr>
					<ul>
						<li><a href="${pageContext.request.contextPath}/board/club-request">동호회 신청</a></li>
						<li><a href="${pageContext.request.contextPath}/board/club-request-list">동호회 신청 목록</a></li>
					</ul>
				
			
			</nav>
			<article>
				
			<div id="board-top">
					<h1>동호회 신청 목록</h1>
			</div>
				
			<div id="request-table">
				<table id="requestArea">
						<tr>
							<th class="th2">신청번호</th>
							<th class="th3">작성자</th>
							<th class="th3">신청날짜</th>
							<th class="th1">신청이름</th>
							<th class="th2">신청상태</th>
						</tr>
<<<<<<< HEAD
						<c:forEach var="club" items="${ requestList }" >

						<tr>
							<%-- <td><c:out value="${ notice.noticeNo }"/></td> --%>
							<td><c:out value="${ club.clubReqNo }"/></td>
							<td><c:out value="${ club.writer }"/></td>
							<td><c:out value="${ club.clubReqDate }"/></td>
							<td><c:out value="${ club.clubReqName }"/></td>
							<td><c:out value="${ club.statusNo }"/></td>
						</tr>
						
						</c:forEach>
					</table>
					<input type="hidden" name="empNum" value="${ requestScope.club.empNum }">
				
			</div>
			
			</article>
		</div>
	</div>
	<footer id="footer">
		<div class="footer_logo"><img src="${pageContext.request.contextPath}/resources/board/images/logo.png" alt=""></div>
		<div class="footer_desc">
			<p>
				MGS그룹웨어 주식회사<br>
				사업자등록번호 : 123-45-67890<br>
				통신판매업신고번호 : 제1234-서울강동-1234호
				주소 : 서울시 강동구 천호동 123로<br>
			</p>
		</div>
		<div class="footer_law">
			<a href="#">개인정보처리방침</a>
			<a href="#">이용약관</a>
		
		</div>
	
	</footer>
</div>

<script>

if(document.querySelectorAll("#requestArea td")) {
	const $tds = document.querySelectorAll("#requestArea td");
	for(let i = 0; i < $tds.length; i++) {
		
		$tds[i].onmouseenter = function() {
			this.parentNode.style.backgroundColor = "#FEDBD0";
			this.parentNode.style.cursor = "pointer";
		}
		$tds[i].onmouseout = function() {
			this.parentNode.style.backgroundColor = "white";
		}
		
		$tds[i].onclick = function() {
			const clubReqNo = this.parentNode.children[0].innerText;
			location.href = "${ pageContext.servletContext.contextPath }/board/club-request-detail?clubReqNo=" + clubReqNo;
=======
						<c:forEach var="club" items="${ qwe }" >

						<tr>
							<%-- <td><c:out value="${ notice.noticeNo }"/></td> --%>
							<td><c:out value="${ club.clubReqNo }"/></td>
							<td><c:out value="${ club.writer }"/></td>
							<td><c:out value="${ club.clubReqDate }"/></td>
							<td><c:out value="${ club.clubReqName }"/></td>
							<td></td>
						</tr>
						
						</c:forEach>
					</table>
				
			</div>
			
			</article>
		</div>
	</div>
	<footer id="footer">
		<div class="footer_logo"><img src="${pageContext.request.contextPath}/resources/board/images/logo.png" alt=""></div>
		<div class="footer_desc">
			<p>
				MGS그룹웨어 주식회사<br>
				사업자등록번호 : 123-45-67890<br>
				통신판매업신고번호 : 제1234-서울강동-1234호
				주소 : 서울시 강동구 천호동 123로<br>
			</p>
		</div>
		<div class="footer_law">
			<a href="#">개인정보처리방침</a>
			<a href="#">이용약관</a>
		
		</div>
	
	</footer>
</div>

<script>

if(document.querySelectorAll("#requestArea td")) {
	const $tds = document.querySelectorAll("#requestArea td");
	for(let i = 0; i < $tds.length; i++) {
		
		$tds[i].onmouseenter = function() {
			this.parentNode.style.backgroundColor = "#FEDBD0";
			this.parentNode.style.cursor = "pointer";
		}
		$tds[i].onmouseout = function() {
			this.parentNode.style.backgroundColor = "white";
		}
		
		$tds[i].onclick = function() {
			const noticeNo = this.parentNode.children[0].innerText;
			location.href = "${ pageContext.servletContext.contextPath }/board/notice-detail?noticeNo=" + noticeNo;
>>>>>>> refs/remotes/origin/jaehyeon
		}
	}
}
</script>

</body>
</html>