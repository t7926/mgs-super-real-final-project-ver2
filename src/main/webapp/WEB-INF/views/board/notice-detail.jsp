<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>공지 보기</title>
<link href="${pageContext.request.contextPath}/resources/board/css/reset.css" rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/resources/board/css/notice-detail.css" rel="stylesheet" type="text/css">
<script src="https://kit.fontawesome.com/9be7870010.js" crossorigin="anonymous"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/board/js/jquery-3.5.1.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/board/js/twobutton.js"></script>
</head>
<body>

<div id="layout">
	<header id="header">
		<nav class="gnb">
			<h1 class="logo"><a href="#">
				<img src="${pageContext.request.contextPath}/resources/board/images/logo.png" alt="">
			</a></h1>
			<ul class="main-menu">
               	         <li>
                        <a href="${pageContext.request.contextPath}/payment/payment">전자결재</a>
                    </li>
                    <li>
                        <a href="${pageContext.request.contextPath}/board/list">게시판</a>
                    </li>
                    <li>
                        <a href="${pageContext.request.contextPath}/geuntae/calcul-time">근태관리</>
                    </li>
                    <li>
                        <a href="#">조직도</a>
                    </li>
                    <li>
                        <a href="${ pageContext.servletContext.contextPath }/document/list">문서관리</a>
                    </li>
                </ul>
		</nav>
	
	</header>
	<div id="container">
		<div id="wrap">
			<nav class="nav">
				<div id="nav-head">
					<i class="fa fa-solid fa-list" aria-hidden="true"></i>
					<h2>게시판</h2>
				</div>
				
				<ul>
					<div id="board">
						<i class="fa fa-solid fa-rectangle-list" aria-hidden="true"></i>
						<h3>게시판</h3>
					</div>
					<hr>
					<li><a href="${pageContext.request.contextPath}/board/list">공지 게시판</a></li>
					<li><a href="${pageContext.request.contextPath}/board/anonymous-list">익명 게시판</a></li>
				</ul>
				
				<div id="club">
					<i class="fa fa-solid fa-hand-holding-heart" aria-hidden="true"></i>
					<h3>동호회 게시판</h3>
				</div>
				<hr>
				<ul>
					<li><a href="${pageContext.request.contextPath}/views/board/manager/board-club-football.jsp">족구 게시판</a></li>
					<li><a href="${pageContext.request.contextPath}/views/board/manager/board-club-mountain.jsp">산악회 게시판</a></li>
				</ul>
				<div id="request">
					
						<i class="fa fa-solid fa-chalkboard-user" aria-hidden="true"></i>
						<h3 style="color:white">게시판 신청</h3>
					<!-- <li id="modal_open_btn">신규 게시판 생성</li> -->
				</div>
				<hr>
					<ul>
						<li><a href="${pageContext.request.contextPath}/board/club-request">동호회 신청</a></li>
						<li><a href="${pageContext.request.contextPath}/board/club-request-list">동호회 신청 목록</a></li>
					</ul>
				
			
			</nav>
			<article>
				
 				<div id="modal" style="display:none;">
					
					<div class="modal_content">
						<h2>게시판 수정</h2>
						<hr>
						<p>수정하시겠습니까?</p>
						<hr>
						<div class="modal_button-box">
							<button type="button" id="modal-done-btn" onclick="location.href='${ pageContext.servletContext.contextPath }/board/notice-modify?noticeNo=${ noticeNo }'">예</button>
							<button type="button" id="modal_close_btn">아니오</button>
						</div>
					</div>
					
					<div class="modal_layer"></div>
				</div>
				
				<div id="modal2" style="display:none;">
					
					<div class="modal_content">
						<h2>게시판 삭제</h2>
						<hr>
						<p>삭제하시겠습니까?</p>
						<hr>
						<div class="modal_button-box">
							<button type="button" id="modal-done-btn2" onclick="location.href='${ pageContext.servletContext.contextPath }/board/delete?noticeNo=${ noticeNo }'">예</button>
							<button type="button" id="modal_close_btn2">아니오</button>
						</div>
					</div>
					
					<div class="modal_layer"></div>
				</div>
				
				<div class="table">
					<table>
						<tr id="table-head">
							<th colspan="7"><c:out value="${ requestScope.notice.noticeTitle }"/></th>
						</tr>
						<tr id="table-look">
							<td>작성자</td>
							<td><c:out value="${ notice.noticeName }"/></td>
							<td><c:out value="${ requestScope.notice.noticeDate }"/></td>
							<td>조회</td>
							<td><c:out value="${ requestScope.notice.noticeViews }"/></td>
						</tr>
						<tr>
							<td colspan="7"><c:out value="${ requestScope.notice.noticeContents }"/></td>
						</tr>
					
					</table>
					<input type="hidden" name="empnum" value="${ requestScope.loginMember.empnum }">
				
				</div>
				<div class="buttons">
					<button type="button" class="btn btn-pk" onclick="location.href='${ pageContext.servletContext.contextPath }/board/list'">목록</button>
				  <c:if test="${ sessionScope.loginMember.depname eq 5 }">
					<button id="modal_open_btn" >수정하기</button>
					<button id="modal_open_btn2">삭제하기</button>
				  </c:if>
				</div>
			
			</article>
		</div>
	</div>
	<footer id="footer">
		<div class="footer_logo"><img src="${pageContext.request.contextPath}/resources/board/images/logo.png" alt=""></div>
		<div class="footer_desc">
			<p>
				MGS그룹웨어 주식회사<br>
				사업자등록번호 : 123-45-67890<br>
				통신판매업신고번호 : 제1234-서울강동-1234호
				주소 : 서울시 강동구 천호동 123로<br>
			</p>
		</div>
		<div class="footer_law">
			<a href="#">개인정보처리방침</a>
			<a href="#">이용약관</a>
		</div>
	
	</footer>
</div>

</body>
</html>