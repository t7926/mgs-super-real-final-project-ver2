<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>게시판</title>
<link href="${pageContext.request.contextPath}/resources/board/css/reset.css" rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/resources/board/css/club-request-modify.css" rel="stylesheet" type="text/css">
<script src="https://kit.fontawesome.com/9be7870010.js" crossorigin="anonymous"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/board/js/jquery-3.5.1.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/board/js/onebutton.js"></script>

<script>
const message = '${ requestScope.message }';
if(message != null && message !== '') {
	alert(message);
}
</script>

</head>


<body>

<div id="layout">
	<header id="header">
		<nav class="gnb">
			<h1 class="logo"><a href="#">
				<img src="${pageContext.request.contextPath}/resources/board/images/logo.png" alt="">
			</a></h1>
			<ul class="main-menu">
               	         <li>
                        <a href="${pageContext.request.contextPath}/payment/payment">전자결재</a>
                    </li>
                    <li>
                        <a href="${pageContext.request.contextPath}/board/list">게시판</a>
                    </li>
                    <li>
                        <a href="${pageContext.request.contextPath}/geuntae/calcul-time">근태관리</>
                    </li>
                    <li>
                        <a href="#">조직도</a>
                    </li>
                    <li>
                        <a href="${ pageContext.servletContext.contextPath }/document/list">문서관리</a>
                    </li>
                </ul>
		</nav>
	
	</header>
	<div id="container">
		<div id="wrap">
			<nav>
				<div id="nav-head">
					<i class="fa fa-solid fa-list" aria-hidden="true"></i>
					<h2>게시판</h2>
				</div>
				
				<ul>
					<div id="board">
						<i class="fa fa-solid fa-rectangle-list" aria-hidden="true"></i>
						<h3>게시판</h3>
					</div>
					<hr>
					<li><a href="${pageContext.request.contextPath}/board/list">공지 게시판</a></li>
					<li><a href="${pageContext.request.contextPath}/board/anonymous-list">익명 게시판</a></li>
				</ul>
				
				<div id="club">
					<i class="fa fa-solid fa-hand-holding-heart" aria-hidden="true"></i>
					<h3>동호회 게시판</h3>
				</div>
				<hr>
				<ul>
					<li><a href="${pageContext.request.contextPath}/views/board/manager/board-club-football.jsp">족구 게시판</a></li>
					<li><a href="${pageContext.request.contextPath}/views/board/manager/board-club-mountain.jsp">산악회 게시판</a></li>
				</ul>
				<div id="request">
					
						<i class="fa fa-solid fa-chalkboard-user" aria-hidden="true"></i>
						<h3>게시판 신청</h3>
					<!-- <li id="modal_open_btn">신규 게시판 생성</li> -->
				</div>
				<hr>
					<ul>
						<li><a href="${pageContext.request.contextPath}/board/club-request">동호회 신청</a></li>
						<li><a href="${pageContext.request.contextPath}/board/club-request-list">동호회 신청 목록</a></li>
					</ul>
				
			
			</nav>
			<article>
				
			<div id="board-top">
					<h1>동호회 신청</h1>
			</div>
				
			<div id="request-container">
				<form action="${ pageContext.servletContext.contextPath }/board/club-request-modify" method="post">
				<div class="request-box">
					<div class="writer">
					 	<h4>작성자</h4>
					 	<div><p><c:out value="${ requestScope.club.writer }"/></p></div>
					 </div>
					 <div class="date">
					 	<h4>날짜</h4>
					 	<div><p><c:out value="${ requestScope.club.clubReqDate }"/></p></div>
					 </div>
					 <div class="club-name">
					 	<h4>신청이름</h4>
					 	<div><p><c:out value="${ requestScope.club.clubReqDate }"/></p></div>
					 	
					 	<h4 class="status">승인상태</h4>
					 	
					 	<select id="status" name="statusNo">
					 		<option value="1">승인대기</option>
					 		<option value="2">승인</option>
					 		<option value="3">반려</option>
					 	</select>
					 </div>
					 <input type="hidden" name="clubReqNo" value="${ requestScope.club.clubReqNo }">
					 <div class="request-reason">
					 	<h4>신청사유</h4>
					 	<div><p><c:out value="${ requestScope.club.clubReqReason }"/></p></div>
					 </div>
					 <div class="cancle-reason">
					 	<h4>반려사유</h4>
					 	<textarea name="clubCancleReason" placeholder="내용을 입력하세요" value="${ requestScope.club.clubCancleReason }"></textarea>
					 </div>
				</div>	
				<div class="button-box">
					<button type="submit">완료</button>
					<button>뒤로가기</button>
				</div>
				</form>
			</div>
			
			</article>
		</div>
	</div>
	<footer id="footer">
		<div class="footer_logo"><img src="${pageContext.request.contextPath}/resources/board/images/logo.png" alt=""></div>
		<div class="footer_desc">
			<p>
				MGS그룹웨어 주식회사<br>
				사업자등록번호 : 123-45-67890<br>
				통신판매업신고번호 : 제1234-서울강동-1234호
				주소 : 서울시 강동구 천호동 123로<br>
			</p>
		</div>
		<div class="footer_law">
			<a href="#">개인정보처리방침</a>
			<a href="#">이용약관</a>
		
		</div>
	
	</footer>
</div>

<script>

const statusBoxChange = function(value){
	console.log("값변경 : " + value);
	$("#status-select").val("승인대기").prop("selected", true);
	$("#status-select").val("승인").prop("selected", true);
	$("#status-select").val("반려").prop("selected", true);
}

</script>

</body>
</html>