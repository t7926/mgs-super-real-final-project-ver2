<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>익명게시판</title>
<link href="${pageContext.request.contextPath}/resources/board/css/reset.css" rel="stylesheet" type="text/css">
<link href="${pageContext.request.contextPath}/resources/board/css/common-list.css" rel="stylesheet" type="text/css">
<script src="https://kit.fontawesome.com/9be7870010.js" crossorigin="anonymous"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/board/js/jquery-3.5.1.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/board/js/onebutton.js"></script>

<script>
const message = '${ requestScope.message }';
if(message != null && message !== '') {
	alert(message);
}
</script>

</head>


<body>

<div id="layout">
	<header id="header">
		<nav class="gnb">
			<h1 class="logo"><a href="#">
				<img src="${pageContext.request.contextPath}/resources/board/images/logo.png" alt="">
			</a></h1>
			<ul class="main-menu">
               	         <li>
                        <a href="${pageContext.request.contextPath}/payment/payment">전자결재</a>
                    </li>
                    <li>
                        <a href="${pageContext.request.contextPath}/board/list">게시판</a>
                    </li>
                    <li>
                        <a href="${pageContext.request.contextPath}/geuntae/calcul-time">근태관리</>
                    </li>
                    <li>
                        <a href="#">조직도</a>
                    </li>
                    <li>
                        <a href="${ pageContext.servletContext.contextPath }/document/list">문서관리</a>
                    </li>
                </ul>
		</nav>
	
	</header>
	<div id="container">
		<div id="wrap">
			<nav>
				<div id="nav-head">
					<i class="fa fa-solid fa-list" aria-hidden="true"></i>
					<h2>게시판</h2>
				</div>
				
				<ul>
					<div id="board">
						<i class="fa fa-solid fa-rectangle-list" aria-hidden="true"></i>
						<h3>게시판</h3>
					</div>
					<hr>
					<li><a href="${pageContext.request.contextPath}/board/list">공지 게시판</a></li>
					<li><a href="${pageContext.request.contextPath}/board/anonymous-list">익명 게시판</a></li>
				</ul>
				
				<div id="club">
					<i class="fa fa-solid fa-hand-holding-heart" aria-hidden="true"></i>
					<h3>동호회 게시판</h3>
				</div>
				<hr>
				<ul>
					<li><a href="${pageContext.request.contextPath}/views/board/manager/board-club-football.jsp">족구 게시판</a></li>
					<li><a href="${pageContext.request.contextPath}/views/board/manager/board-club-mountain.jsp">산악회 게시판</a></li>
				</ul>
				<div id="request">
					
						<i class="fa fa-solid fa-chalkboard-user" aria-hidden="true"></i>

	

						<h3 style="color:white">게시판 신청</h3>

					<!-- <li id="modal_open_btn">신규 게시판 생성</li> -->
				</div>
				<hr>
					<ul>
						<li><a href="${pageContext.request.contextPath}/board/club-request">동호회 신청</a></li>
						<li><a href="${pageContext.request.contextPath}/board/club-request-list">동호회 신청 목록</a></li>
					</ul>



			
			</nav>
			
			<article>
			
				<div id="modal" style="display:none;">
				
				<div class="modal_content">
					<h2>새로 생성할 게시판의 이름을 입력하세요.</h2>
					<input type="text" name="board-name">
					<div class="modal_button-box">
						<button type="button" id="modal-done-btn">확인</button>
						<button type="button" id="modal_close_btn">취소</button>
					</div>
				</div>
				
				<div class="modal_layer"></div>
			</div>
			
			
				<div id="board-top">
					<h1>익명게시판</h1>
				</div>
				<div id="table">
					
					<table id="anonyArea">
						<tr>
							<th>번호</th>
							<th>제목</th>
							<th>작성자</th>
							<th>등록일</th>
							<th>조회수</th>
						</tr>
						<c:forEach var="anony" items="${ anonyList }" >

						<tr>
							<td><c:out value="${ anony.anoNo }"/></td>
							<td><c:out value="${ anony.anoTitle }"/></td>
							<td><c:out value="${ anony.anoName }"/></td>
							<td><c:out value="${ anony.anoDate }"/></td>
							<td><c:out value="${ anony.anoViews }"/></td>
						</tr>
						
						</c:forEach>
					</table>
					
				</div>
		<!-- 페이지 처리 -->
		<jsp:include page="./common/paging.jsp" />

		<!-- 검색 폼 -->
		<div class="search-area" align="center">
			<form id="loginForm"
				action="${ pageContext.servletContext.contextPath }/board/anonymous-list"
				method="get" style="display: inline-block">
				<input type="hidden" name="currentPage" value="1">
				<select id="searchCondition" name="searchCondition">
					<option value="anoName"
						${ requestScope.selectCriteria.searchCondition eq "anoName"? "selected": "" }>작성자</option>
					<option value="anoTitle"
						${ requestScope.selectCriteria.searchCondition eq "anoTitle"? "selected": "" }>제목</option>
					<option value="anoContents"
						${ requestScope.selectCriteria.searchCondition eq "anoContents"? "selected": "" }>내용</option>
				</select> 
				<input type="search" id="searchValue" name="searchValue" value="<c:out value="${ requestScope.selectCriteria.searchValue }"/>">

				<button class="btn btn-bs" type="submit">검색하기</button>
				<input type="hidden" name="anoPassword" value="${ anony.anoPassword }">

			</form> 
					
					<button id="write" onclick="location.href='${pageContext.request.contextPath}/board/anonymous-write'">글쓰기</button>
					
				</div>
			</article>
		</div>
	</div>
	<footer id="footer">
		<div class="footer_logo"><img src="${pageContext.request.contextPath}/resources/board/images/logo.png" alt=""></div>
		<div class="footer_desc">
			<p>
				MGS그룹웨어 주식회사<br>
				사업자등록번호 : 123-45-67890<br>
				통신판매업신고번호 : 제1234-서울강동-1234호
				주소 : 서울시 강동구 천호동 123로<br>
			</p>
		</div>
		<div class="footer_law">
			<a href="#">개인정보처리방침</a>
			<a href="#">이용약관</a>
		
		</div>
	
	</footer>
</div>

<script>

if(document.querySelectorAll("#anonyArea td")) {
	const $tds = document.querySelectorAll("#anonyArea td");
	for(let i = 0; i < $tds.length; i++) {
		
		$tds[i].onmouseenter = function() {
			this.parentNode.style.backgroundColor = "#5584AC";
			this.parentNode.style.color = "white";
			this.parentNode.style.cursor = "pointer";
		}
		$tds[i].onmouseout = function() {
			this.parentNode.style.backgroundColor = "white";
			this.parentNode.style.color = "black";
		}
		
		$tds[i].onclick = function() {
			const anoNo = this.parentNode.children[0].innerText;
			location.href = "${ pageContext.servletContext.contextPath }/board/anonymous-detail?anoNo=" + anoNo;
		}
	}
}
</script>

</body>
</html>